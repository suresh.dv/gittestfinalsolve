global class WeldingTrackerInactivateUsers implements Database.Batchable<SObject>, Schedulable {

    global WeldingTrackerInactivateUsers() {}
    
    global void execute(SchedulableContext pSC)
    {
        WeldingTrackerInactivateUsers b = new WeldingTrackerInactivateUsers();
        Database.executeBatch(b);
    }
    
    global Database.QueryLocator start(Database.BatchableContext BC) {
        Id profileId = [SELECT Id, Name, UserType FROM Profile WHERE UserType = 'PowerPartner' AND Name = 'Welding Tracker Community User'][0].Id;
        Welding_Tracker_Settings__c setting = Welding_Tracker_Settings__c.getOrgDefaults();
        
        Date d = System.Today().addDays(Integer.valueOf(setting.MaximumUserInactiveDays__c));
        
        return Database.getQueryLocator([SELECT Id, Name, ProfileId, UserType, IsPortalEnabled, IsActive, LastLoginDate 
                                         FROM User
                                         WHERE profileId =: profileId AND LastLoginDate <: d AND IsActive = True AND IsPortalEnabled = True
                                        	   AND UserType = 'PowerPartner']); 
    }
    
    global void execute(Database.BatchableContext BC, List<SOBject> scope) {
        for(SObject s : scope) {
            s.put('IsActive',False);
        }
        update scope;
    }
    
    global void finish(Database.BatchableContext BC) {
        
    }
}