public class RelatedWeldingProcedureTestData {
    public static List<Related_Welding_Procedure__c> createRelatedWeldingProcedure(Welding_Request__c request, List<Welding_Procedure__c> procedures) {
        List<Related_Welding_Procedure__c> relatedProcedures = new List<Related_Welding_Procedure__c>();
        
        for(Welding_Procedure__c procedure : procedures) {
            Related_Welding_Procedure__c relatedprocedure = new Related_Welding_Procedure__c();
            relatedprocedure.Welding_Request__c = request.Id;
            relatedprocedure.Welding_Procedure__c = procedure.Id;
            relatedprocedure.Comments__c = procedure.Name;
            relatedprocedure.Status__c = 'Accepted';
            relatedprocedure.Reviewed_Date__c = System.today();
            
            relatedProcedures.add(relatedprocedure);            
        }
        
        insert relatedProcedures;
        return relatedProcedures;
    }
}