public class ShiftConfigurationValidationHelper {

	public static Map<String, List<Shift_Configuration__c>> addToWeekDayShiftList(Map<String, List<Shift_Configuration__c>> WeekDayToShiftList, Shift_Configuration__c shiftConfig, String DayField) {
		
		String key = DayField + '_' + shiftConfig.RecordTypeId;
		
		List<Shift_Configuration__c> shiftList = WeekDayToShiftList.get(key);
		
		if(shiftList == Null)
			shiftList = new List<Shift_Configuration__c>();
		
		
		shiftList.add(shiftConfig);
		
		WeekDayToShiftList.put(key, shiftList);

		return WeekDayToShiftList;
	}
	
	public static Map<String, List<Shift_Configuration__c>> processShiftConfig(Map<String, List<Shift_Configuration__c>> WeekDayToShiftList, Shift_Configuration__c shiftConfig) {
		IF(shiftConfig.Sunday__c)
			WeekDayToShiftList = ShiftConfigurationValidationHelper.addToWeekDayShiftList(WeekDayToShiftList, shiftConfig, 'Sunday');	
		
		IF(shiftConfig.Monday__c)
			WeekDayToShiftList = ShiftConfigurationValidationHelper.addToWeekDayShiftList(WeekDayToShiftList, shiftConfig, 'Monday');
				
		IF(shiftConfig.Tuesday__c)
			WeekDayToShiftList = ShiftConfigurationValidationHelper.addToWeekDayShiftList(WeekDayToShiftList, shiftConfig, 'Tuesday');		
		
		IF(shiftConfig.Wednesday__c)
			WeekDayToShiftList = ShiftConfigurationValidationHelper.addToWeekDayShiftList(WeekDayToShiftList, shiftConfig, 'Wednesday');		
		
		IF(shiftConfig.Thursday__c)
			WeekDayToShiftList = ShiftConfigurationValidationHelper.addToWeekDayShiftList(WeekDayToShiftList, shiftConfig, 'Thursday');		
		
		IF(shiftConfig.Friday__c)
			WeekDayToShiftList = ShiftConfigurationValidationHelper.addToWeekDayShiftList(WeekDayToShiftList, shiftConfig, 'Friday');		
		
		IF(shiftConfig.Saturday__c)
			WeekDayToShiftList = ShiftConfigurationValidationHelper.addToWeekDayShiftList(WeekDayToShiftList, shiftConfig, 'Saturday');
			
		return WeekDayToShiftList;
	}
	
}