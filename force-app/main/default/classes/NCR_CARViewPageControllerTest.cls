@isTest
private class NCR_CARViewPageControllerTest{
    static testMethod void testCARViewPageControllerAsQaLead(){
         User ncrQaLead = NCRUserTestData.createNCRQALead();
         System.AssertNotEquals(ncrQaLead.Id, Null);
         
         System.runAs(ncrQaLead) {
            PageReference pageRef = Page.NCR_CARViewPage;
            Test.setCurrentPageReference(pageRef);    
            
            Date currentDate = Date.Today();
            NCR_CAR__c car = NCR_CAR_TestData.createNCRCAR('short', 'detailed', currentDate, true);                  
            
            System.AssertNotEquals(car.Id, Null);
            
            ApexPages.StandardController standardController = new ApexPages.standardController(car);    
            NCR_CARViewPageControllerX viewPageController = new NCR_CARViewPageControllerX(standardController);
            
            //Need a set method for the NCR_WorkFlowEngine()
            System.AssertEquals(viewPageController.NCR_Action1(), Null);
            System.AssertEquals(viewPageController.NCR_Action2(), Null);
            System.AssertEquals(viewPageController.NCR_Action3(), Null);
            System.AssertEquals(viewPageController.NCR_Save(), Null);
            
            System.AssertNotEquals(viewPageController.NCR_CAR_Pdf(), Null);
            System.AssertNotEquals(viewPageController.getBusinessUnits(), Null);
            System.AssertNotEquals(viewPageController.getProjects(), Null);
            
            System.AssertEquals(viewPageController.uploadFile() , Null);
            System.AssertEquals(viewPageController.removeFile(), Null);
            
            Document d = new Document();
            d.name = 'Document Name';
            d.body = Blob.valueOf('Unit Test Document Body');
            
            viewPageController.documentFile = d;
            viewPageController.uploadFile();
            
            viewPageController.selectedAttachmentId = d.id;
            System.AssertNotEquals(viewPageController.selectedAttachmentId, Null);
            
            viewPageController.removeFile();
            
            /*
            viewPageController.documentFile = d;
            viewPageController.uploadFile();            
            System.AssertNotEquals(viewPageController.Save(), Null);
            */
            
         }
    }
     
}