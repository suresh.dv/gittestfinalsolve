global class FM_CarryoverTrucktripBatch implements Database.Batchable<sObject> {
	
	String query = 'Select Id, Name, Truck_Trip_Status__c, Run_Sheet_Date__c'
		+ ' From FM_Truck_Trip__c'
		+ ' Where Truck_Trip_Status__c In (\'' + FM_Utilities.TRUCKTRIP_STATUS_CARRYOVER + '\', \'' + FM_Utilities.TRUCKTRIP_STATUS_NEW + '\',\'' + FM_Utilities.TRUCKTRIP_STATUS_REDISPATCH + '\')'
		+ ' And Load_Request__c <> null'
		+ ' And Run_Sheet_Date__c = YESTERDAY';
	
	global FM_CarryoverTrucktripBatch() {
		System.debug('FM_CarryoverTrucktripBatch initiating with query: ' + query);
	}
	
	global Database.QueryLocator start(Database.BatchableContext BC) {
		System.debug('FM_CarryoverTrucktripBatch starting\nJob Id: ' + BC.getJobId() + '\nChunk Job Id: ' + BC.getChildJobId());
		return Database.getQueryLocator(query);
	}

   	global void execute(Database.BatchableContext BC, List<sObject> scope) {
		System.debug('HOG_PVRUpdateBatch executing ... ');

		List<FM_Truck_Trip__c> carryOverTruckTrips = (List<FM_Truck_Trip__c>) scope;
		FM_Utilities.UpdateCarryoverTruckTripsToToday(carryOverTruckTrips);
	}
	
	global void finish(Database.BatchableContext BC) {
		// Get the ID of the AsyncApexJob representing this batch job
	   // from Database.BatchableContext.
	   // Query the AsyncApexJob object to retrieve the current job's information.
	   AsyncApexJob a = [SELECT Id, Status, NumberOfErrors, JobItemsProcessed,
	      TotalJobItems, CreatedBy.Email, CreatedBy.Id
	      FROM AsyncApexJob WHERE Id =:BC.getJobId()];
	   // Send an email to the Apex job's submitter notifying of job completion.
	   Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
	   mail.setTargetObjectId(a.CreatedBy.Id);
	   mail.setSaveAsActivity(false);
	   mail.setSubject('HOG PVR Average Volume Update ' + a.Status);
	   mail.setPlainTextBody
	   ('The batch Apex job processed ' + a.TotalJobItems +
	   ' batches with '+ a.NumberOfErrors + ' failures.');
	   Messaging.sendEmail(new Messaging.SingleEmailMessage[] { mail });
	}
	
}