/*------------------------------------------------------------
Author:        Yen Le
Company:       Thirdwave
Description:   Controller of EquipmentCorrectionFormEdit/EquipmentCorrectionFormView page
Test Class:    EquipmentCorrectionFormTest

History:       03-Jul-17    Miro Zelina - included System & Sub-System into Correction Form
               04.09.17    Marcel Brimus - added FEL and Yard 
------------------------------------------------------------*/
public with sharing class EquipmentCorrectionFormControllerX {
    public Equipment__c equip {get;set;}
    public Equipment_Correction_Form__c form {get;set;}
    private String equipId;
    private ApexPages.StandardController controller;
    public String formId {get;set;}
    public Attachment photo {get;set;}
     
    public EquipmentCorrectionFormControllerX(ApexPages.StandardController std)
    {
        if (!Test.isRunningTest())
        {
            std.addFields(new List<String>{'Name', 'Status__c', 'Equipment_Status__c', 'Description_old__c', 'Description_new__c',
                        'Manufacturer_old__c', 'Manufacturer_new__c', 'Model_Number_old__c', 'Model_Number_new__c',
                        'Manufacturer_Serial_No_old__c', 'Manufacturer_Serial_No_new__c', 'Tag_Number_old__c', 'Tag_Number_new__c',
                        'Equipment__c', 'Equipment__r.Location__c', 'Equipment__r.Well_Event__c','Equipment__r.System__c','Equipment__r.Sub_System__c',
                        'Equipment__r.Functional_Equipment_Level__c','Equipment__r.Yard__c',  
                        'Equipment__r.Location__r.Route__c', 'Equipment__r.Planner_Group__c',
                        'Equipment__r.Well_Event__r.Route__c', 'Equipment__r.System__r.Route__c', 'Equipment__r.Sub_System__r.Route__c', 
                        'Equipment__r.Functional_Equipment_Level__r.Route__c', 'Equipment__r.Yard__r.Route__c',
                        'CreatedById', 'CreatedDate','LastModifiedDate', 'LastModifiedById', 'Comments__c',
                		'Equipment__r.Facility__r.Plant_Section__c', 'Equipment__r.Facility__c'});
        }
        controller = std;     
        form = (Equipment_Correction_Form__c)std.getRecord();   
        photo = new Attachment();
        equipId = ApexPages.currentPage().getParameters().get('equipId');
        //User have to new a record from Equipment detail view
        if (form.Id == null && equipId != null) //creating a new form
            setDefault();
        else
        {
            photo = getAttachmentPhoto();
        }
         
    } 
    private Attachment getAttachmentPhoto()
    {
        List<Attachment> att = [select Id, Name, Body, ContentType from Attachment where ParentId =: form.Id];
        if (att.size() > 0)
           return att[0];
        
        return new Attachment();
    }
    private void setDefault()
    {
        equip = [select Id, Description_of_Equipment__c, Manufacturer__c, Model_Number__c, 
                    Manufacturer_Serial_No__c, Location__c, Location__r.Route__r.Route_Number__c,
                    Well_Event__c, Well_Event__r.Route__r.Route_Number__c,
                    System__c, System__r.Route__r.Route_Number__c,
                    Sub_System__c, Sub_System__r.Route__r.Route_Number__c,
                    Functional_Equipment_Level__c, Functional_Equipment_Level__r.Route__r.Route_Number__c,
                    Yard__c, Yard__r.Route__r.Route_Number__c,
                    Planner_Group__c, Tag_Number__c, Facility__c, Facility__r.Plant_Section__c, Facility__r.Planner_Group__c
                from Equipment__c where Id =: equipId];
        form.Description_old__c = equip.Description_of_Equipment__c;
        form.Manufacturer_old__c = equip.Manufacturer__c;
        form.Model_Number_old__c = equip.Model_Number__c;
        form.Manufacturer_Serial_No_old__c = equip.Manufacturer_Serial_No__c;
        form.Tag_Number_old__c = equip.Tag_Number__c;
        form.Equipment__c = equipId;
        form.Equipment__r = equip;
    }
    private void clearViewState()
    {
        photo.body = null;
        photo = new Attachment();
    }
    public PageReference save()
    {
        if (form.Equipment_Status__c == null)
        {
            clearViewState();
            form.Equipment_Status__c.addError('You must enter a value');
            return null;
        }
      
        if(form.Equipment__c == null)
        {
            clearViewState();
            form.Equipment__c.addError('Please select an Equipment and click the "Equipment Data Update" button on Equipment detail page.');
            return null;
        }
        if (form.Description_new__c == null && form.Manufacturer_new__c == null 
               && form.Model_Number_new__c == null && form.Manufacturer_Serial_No_new__c == null 
               && form.Tag_Number_new__c == null)
        {
            clearViewState();
            ApexPages.addMessage(new ApexPages.message(ApexPages.severity.ERROR, 'Please enter new equipment value for correction.'));
            return null;
        }   
        try
        {
           PageReference nextPage = controller.save();
           System.debug('SAVE the FORM photo =' + photo.Id + photo.Body);
           if (photo.Id == null && photo.Body != null)
           {
               //Create new instance of uploaded file
               Attachment a = photo.clone(false, true, false, false);
               a.ParentId = controller.getId();
               //clear body of uploaded file to remove from view state limit error
               clearViewState();
               insert a;
           }
           if (ApexPages.currentPage().getParameters().get('retURL') != null)
           {
               System.debug('retURL =' + ApexPages.currentPage().getParameters().get('retURL'));
               nextPage = new PageReference(ApexPages.currentPage().getParameters().get('retURL'));
               nextPage.setRedirect(true);
           }
           return nextPage;
        }
        catch(Exception e)
        {
            System.debug('save error');
            clearViewState();
            ApexPages.addMessage(new ApexPages.message(ApexPages.severity.ERROR, e.getMessage()));
            return null;
        }
    }
    public void DeletePhoto()
    {
        try
        {
            delete photo;
        }
        catch (DMLException e) 
        {
            ApexPages.addMessage(new ApexPages.message(ApexPages.severity.ERROR,'Error deleting file'));
            
        } 
        finally 
        {
            clearViewState();
        }
     
        ApexPages.addMessage(new ApexPages.message(ApexPages.severity.INFO,'File deleted successfully'));
        
    }
    public PageReference CloseRequest()
    {
        try
        {
            form.Status__c = 'Closed';
            update form;
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.CONFIRM, 'The form has been successfully CLOSED.'));
            return null;
        
        }
        catch(Exception ex)    
        {
           ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, ex.getMessage()));
           return null;
        }     
        
    }
   
}