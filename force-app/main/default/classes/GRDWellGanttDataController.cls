public without sharing class GRDWellGanttDataController
{
    public WellWrapper well      {get; private set;}
    public String jsonMilestones {get; private set;}
    
    public class WellWrapper
    {
        public String name {get;set;}
        public Date RTD_Initiated {get;set;}
        public Date Go_No_Go_Decision {get;set;}
        public Date Lease_Construction_Start {get;set;}
        public Date Lease_Construction_End {get;set;}
        public Date Drilling_Start {get;set;}
        public Date Drilling_End {get;set;}
        public Date Frac_Start {get;set;}
        public Date Frac_End {get;set;}
        public Date Completions_Start {get;set;}
        public Date Completions_End {get;set;}        
        public Date Test_Start {get;set;}
        public Date Test_End {get;set;}
        public Date Extended_Test_Start {get;set;}
        public Date Extended_Test_End {get;set;}
        public Date Production_Tubing_Start {get;set;}
        public Date Production_Tubing_End {get;set;}
        public Date Early_Production_Facility_Start {get;set;}
        public Date Early_Production_Facility_End {get;set;}
        public Date Tie_In_Start {get;set;}
        public Date Tie_In_End {get;set;}
        public Date Tank_Farms_Start {get;set;}
        public Date Tank_Farms_End {get;set;}
        public Date Production_Start {get;set;}
        public Date Production_End {get;set;}
        
        public WellWrapper(String name,
                           Date RTD_Initiated,
                           Date Go_No_Go_Decision,
                           Date Lease_Construction_Start,
                           Date Lease_Construction_End,
                           Date Drilling_Start,
                           Date Drilling_End,
                           Date Frac_Start,
                           Date Frac_End,
                           Date Completions_Start,
                           Date Completions_End,
                           Date Test_Start,
                           Date Test_End,
                           Date Extended_Test_Start,
                           Date Extended_Test_End,
                           Date Production_Tubing_Start,
                           Date Production_Tubing_End,
                           Date Early_Production_Facility_Start,
                           Date Early_Production_Facility_End,
                           Date Tie_In_Start,
                           Date Tie_In_End,
                           Date Tank_Farms_Start,
                           Date Tank_Farms_End,
                           Date Production_Start,
                           Date Production_End)
        {
            this.name = name;
            this.RTD_Initiated = RTD_Initiated;
            this.Go_No_Go_Decision = Go_No_Go_Decision;
            this.Lease_Construction_Start = Lease_Construction_Start;
            this.Lease_Construction_End = Lease_Construction_End;
            this.Drilling_Start = Drilling_Start;
            this.Drilling_End = Drilling_End;
            this.Frac_Start = Frac_Start;
            this.Completions_Start = Completions_Start;
            this.Completions_End = Completions_End;
            this.Frac_End = Frac_End;
            this.Test_Start = Test_Start;
            this.Test_End = Test_End;
            this.Extended_Test_Start = Extended_Test_Start;
            this.Extended_Test_End = Extended_Test_End;
            this.Production_Tubing_Start = Production_Tubing_Start;
            this.Production_Tubing_End = Production_Tubing_End;
            this.Early_Production_Facility_Start = Early_Production_Facility_Start;
            this.Early_Production_Facility_End = Early_Production_Facility_End;
            this.Tie_In_Start = Tie_In_Start;
            this.Tie_In_End = Tie_In_End;
            this.Tank_Farms_Start = Tank_Farms_Start;
            this.Tank_Farms_End = Tank_Farms_End;
            this.Production_Start = Production_Start;
            this.Production_End = Production_End;
        }
    }
    
    public GRDWellGanttDataController()
    {
        String wellId = ApexPages.currentPage().getParameters().get('id');
        
        GRD_Borehole__c wellSObject = [SELECT Id,
                                          Name,
                                          RTD_Initiated__c,
                                          Go_No_Go_Decision__c,
                                          Lease_Construction_Start__c,
                                          Lease_Construction_End__c,
                                          Drilling_Start__c,
                                          Drilling_End__c,
                                          Frac_Start__c,
                                          Frac_End__c,
                                          Completion_Start__c,
                                          Completion_End__c,
                                          Test_Start__c,
                                          Test_End__c,
                                          Extended_Test_Start__c,
                                          Extended_Test_End__c,
                                          Production_Tubing_Start__c,
                                          Production_Tubing_End__c,
                                          Early_Production_Facility_Start__c,
                                          Early_Production_Facility_End__c,
                                          Tie_In_Start__c,
                                          Tie_In_End__c,
                                          Tank_Farms_Start__c,
                                          Tank_Farms_End__c,
                                          Production_Start__c,
                                          Production_End__c
                                   FROM GRD_Borehole__c
                                   WHERE Id =: wellId];
        
        well = new WellWrapper(wellSObject.Name,
                               wellSObject.RTD_Initiated__c,
                               wellSObject.Go_No_Go_Decision__c,
                               wellSObject.Lease_Construction_Start__c,
                               wellSObject.Lease_Construction_End__c,
                               wellSObject.Drilling_Start__c,
                               wellSObject.Drilling_End__c,
                               wellSObject.Frac_Start__c,
                               wellSObject.Frac_End__c,
                               wellSObject.Completion_Start__c,
                               wellSObject.Completion_End__c,
                               wellSObject.Test_Start__c,
                               wellSObject.Test_End__c,
                               wellSObject.Extended_Test_Start__c,
                               wellSObject.Extended_Test_End__c,
                               wellSObject.Production_Tubing_Start__c,
                               wellSObject.Production_Tubing_End__c,
                               wellSObject.Early_Production_Facility_Start__c,
                               wellSObject.Early_Production_Facility_End__c,
                               wellSObject.Tie_In_Start__c,
                               wellSObject.Tie_In_End__c,
                               wellSObject.Tank_Farms_Start__c,
                               wellSObject.Tank_Farms_End__c,
                               wellSObject.Production_Start__c,
                               wellSObject.Production_End__c);
        
        system.debug('mikep '+well);
        
        jsonMilestones = JSON.serialize(well);
    }
    
    public String getjsonMilestones()
    {
        return JSON.serializePretty(well);
    }
}