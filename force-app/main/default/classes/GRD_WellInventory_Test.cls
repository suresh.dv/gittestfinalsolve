@isTest
public class GRD_WellInventory_Test 
{
    
    /*19-Jan-15 By Gangha Kaliyan
    Reason: To test GRD Well Inventory Interface
    */
    
    static testMethod void testClassDWell()
    {
         User grdIntegration = [SELECT Id FROM User WHERE UserName='grd.dataloader@huskyenergy.com'];
        
        System.runAs(grdIntegration)
        {
            //this test method is to verify class D well creation, class D Well Completion creation and build of JSON string 
            Integer noOfClassDWells = 1;
            Integer noOfCompletionPerClassDWell = 1;
            List<Class_D_Well__c> classDList = new List<Class_D_Well__c>();
            List<Class_D_Well_Completion__c> completionList = new List<Class_D_Well_Completion__c>();
            List<Class_D_Well__c> testClassDList = new List<Class_D_Well__c>();
            List<Class_D_Well_Completion__c> testCompletionList = new List<Class_D_Well_Completion__c>();
            
            //create as many records as in the no of records variable
            for (Integer i=1; i<=noOfClassDWells; i++)
            {
            
                Class_D_Well__c cd = new Class_D_Well__c();
                cd.Name = 'test '+i;
                cd.Status__c = 'Proposed';
                cd.Target_Formation__c = 'WLRC';
                cd.Well_Class__c = 'D';
                cd.Well_Type__c = 'VERT';
                cd.Well_Category__c = 'Development';
                cd.Geologist__c = userInfo.getName();
                cd.Operating_Field__c = 'ANSELL';
                cd.RecordTypeId = getRecordTypeId('Class_D_Well__c','Rocky Mountain House');
                
                if(i<10)
                {
                    cd.Surface_Location__c = '0'+i+'-'+'0'+i+'-'+'00'+i+'-18W5';
                    cd.Target_Location__c = '100/0'+i+'-'+'0'+i+'-'+'00'+i+'-18W5/00';
                    cd.UWI_Location__c = '100/0'+i+'-'+'0'+i+'-'+'00'+i+'-18W5/00';
                }
                else if(i>=10 && i<100)
                {
                    cd.Surface_Location__c = i+'-'+i+'-'+'0'+i+'-18W5';
                    cd.Target_Location__c = '100/'+i+'-'+i+'-'+'0'+i+'-18W5/00';
                    cd.UWI_Location__c = '100/'+i+'-'+i+'-'+'0'+i+'-18W5/00';
                }
                
                cd.Associated_Well__c = '';
                cd.Measured_Depth__c = 100;
                cd.Horizontal_Length__c = 200;
                cd.True_Vertical_Depth__c = 300;
                cd.Working_Interest__c = 400;
                
                System.debug('class D well for I '+i+' : '+cd);
                
                classDList.add(cd);
                
            }
            
            insert classDList;
            
            //check if all rows are inserted
            testClassDList = [select Id, Name from Class_D_Well__c];
            System.assertEquals(noOfClassDWells, testClassDList.size());
            
            //we have a "For Inside For" for this case.. but testing does not require many completion records per class d well
            //if we have 10 class d wells and 2 completion records (max) per class d well - loop is executed 20 times 
            //Moreover no query or dml is done within loop.. so no issues at all in terms of governor limits  
            for(Class_D_Well__c cd: classDList)
            {
                for(Integer i=1; i<=noOfCompletionPerClassDWell; i++)
                {
                    Class_D_Well_Completion__c completion = new Class_D_Well_Completion__c();
                    completion.CategoryIsolationCode__c = 'Ball drop';
                    completion.CompletionFluid__c = 'Propane';
                    completion.NumberOfFracs__c = 10;
                    completion.TProppantPerFrac__c = 100;
                    completion.Class_D_Well__c = cd.Id;
                    completionList.add(completion);
                }
            }
            
            insert completionList;
            
            //check if completion row(s) are inserted
            testCompletionList = [select Id, Name from Class_D_Well_Completion__c];
            System.assertEquals(noOfClassDWells * noOfCompletionPerClassDWell, testCompletionList.size());
            
            //now verify whether JSON is created
            String classDJson = GRDWellInventoryController.getClassDJSON('Rocky Mountain House');
            
            system.debug('class D Json '+classDJson);
        
            //create a row JSON
            Borehole bh = new Borehole();
            Class_D_Well__c b = new Class_D_Well__c();
            b = classDList[0];

            bh.name = b.Name;
            bh.status = b.Status__c;
            bh.wellClass = b.Well_Class__c;
            bh.targetFormation = b.Target_Formation__c;
            bh.wellType = b.Well_Type__c;
            bh.wellCategory = b.Well_Category__c;
            bh.geologist = b.Geologist__c;
            //location information
            bh.operatingField = b.Operating_Field__c;
            bh.surfaceLocation = b.Surface_Location__c;
            bh.targetLocation = b.Target_Location__c;
            bh.uwiLocation = b.UWI_Location__c;
            bh.measuredDepth = b.Measured_Depth__c;
            bh.horizontalLength = b.Horizontal_Length__c;
            bh.workingInterest = b.Working_Interest__c;
            bh.trueVerticalDepth = b.True_Vertical_Depth__c;
            bh.noOfCompletions = 0;
            bh.associatedWell = b.Associated_Well__c;
            //error flag 
            bh.errorInDb = String.valueOf(b.Error_In_DB__c);
            bh.operatingDistrict = 'Rocky Mountain House';
            String rowJson = JSON.serialize(bh);
            
            //add new class d well
            GRDWellInventoryController.addNewClassDWell(null, rowJson);
        
            //verify if addNewClassDWell actually created a record
            testClassDList = [select Id, Name from Class_D_Well__c Order By LastModifiedDate DESC];
            System.assertEquals(noOfClassDWells + 1, testClassDList.size());
            
            //add id to borehole 
            bh.id = testClassDList[0].Id;
            rowJson = JSON.serialize(bh);
            
            String rowJsonArray = '['+rowJson+']';
            
            //save class d well 
            GRDWellInventoryController.saveClassDWell(rowJsonArray);
            
            //CLASS D WELL COMPLETION SECTION
            Completion comp = new Completion();
            comp.name = completionList[0].Name;
            comp.categoryIsolation = completionList[0].CategoryIsolationCode__c;
            comp.numberOfStages = completionList[0].NumberOfFracs__c;
            comp.sizeOfStages = completionList[0].TProppantPerFrac__c;     
            comp.boreId = completionList[0].Class_D_Well__c;     
            
            String completionrowJson = JSON.serialize(comp);
            
            //add new class d completion
            GRDWellInventoryController.addNewClassDCompletion(null, completionrowJson);
            
            //check if completion row(s) are inserted
            testCompletionList = [select Id, Name from Class_D_Well_Completion__c Order By LastModifiedDate DESC];
            System.assertEquals(2, testCompletionList.size());     
            
            //add id to Completion
            comp.id = testCompletionList[0].Id;
            completionrowJson = JSON.serialize(comp);
            
            //save class d well completion
            GRDWellInventoryController.saveClassDCompletion(null, completionrowJson);
            
            system.debug('completion list before delete '+testCompletionList.size());
            system.debug('completion id '+testCompletionList[0].Id);
            
            //DELETE CLASS D WELL AND COMPLETION
            GRDWellInventoryController.deleteClassDCompletion(testCompletionList[0].Id);
            
            GRDWellInventoryController.deleteClassDWell(testClassDList[0].Id);
        }
    }
    
    static testMethod void testBorehole()
    {
        User grdIntegration = [SELECT Id FROM User WHERE UserName='grd.dataloader@huskyenergy.com'];
        
        System.runAs(grdIntegration)
        {
            //this test method is to verify borehole creation, class C Well Completion creation and build of JSON string 
    
            Integer noOfBoreholes = 1;
            Integer noOfCompletionPerBorehole = 1;
            List<GRD_Borehole__c> boreholeList = new List<GRD_Borehole__c>();
            List<Class_C_Well_Completion__c> completionList = new List<Class_C_Well_Completion__c>();
            List<GRD_Borehole__c> testBoreholeList = new List<GRD_Borehole__c>();
            List<Class_C_Well_Completion__c> testCompletionList = new List<Class_C_Well_Completion__c>();        
            
            //for creating a GRD borehole we need 
            //1. Operating Field (AMU)
            //2. Surface Location
            //3. A Well
            //4. Target Location
            //5. UWI (Bottomhole) Location
            
            //for creating a operating field we need
            //1. Business Department
            //2. Operating District
            
            //Assumption: no matter how many boreholes we create, it will be assigned to the same:_
            //business dept, operating district, operating field, well, surface location        
            
            
            //create business department
            //get record type for business department 
            Business_Department__c bd = new Business_Department__c();
            bd.Name = 'test Business Department';
            bd.RecordTypeId = getRecordTypeId('Business_Department__c','Primary Oil');
            insert bd;
            
            //create Operating District
            Operating_District__c district = new Operating_District__c();
            district.Name = 'test Operating District';
    //        district.RecordTypeId = getRecordTypeId('Operating_District__c','IAP District');
            district.Business_Department__c = bd.Id;
            insert district;
            
            //create operating field (AMU)
            Field__c field = new Field__c();
            field.Name = 'test Operating Field';
            field.RecordTypeId = getRecordTypeId('Field__c','GRD Operating Field');
            field.Operating_District__c = district.Id;
            insert field;
            
            //create surface location 
            Location__c surface = new Location__c();
            surface.Name = '01-01-001-18W5';
            surface.DLS_Type__c = 'Surface';
            surface.RecordTypeId = getRecordTypeId('Location__c','Gas Resource Development');
            surface.Well_Orientation__c = 'VERT';
            surface.Operating_Field_AMU__c = field.Id;
            insert surface;
            
            //create well
            GRD_Well__c well = new GRD_Well__c();
            well.Name = 'test Well';
            well.Surface_Location__c = surface.Id;
            insert well;
            
            
            /*for(Integer i=1; i<=noOfBoreholes; i++)
            {
                if(i<10)
                {
                    bore.Target_Location__c = '100/0'+i+'-'+'0'+i+'-'+'00'+i+'-18W5/00';
                    bore.UWI_Location__c = '100/0'+i+'-'+'0'+i+'-'+'00'+i+'-18W5/00';
                }
                else if(i>=10 && i<100)
                {
                    bore.Target_Location__c = '100/'+i+'-'+i+'-'+'0'+i+'-18W5/00';
                    bore.UWI_Location__c = '100/'+i+'-'+i+'-'+'0'+i+'-18W5/00';
                }
                
                
            }*/
            
            // Get GRD's recordtype
            RecordType rt = [SELECT Id FROM RecordType where sobjecttype = 'GRD_Borehole__c' and DeveloperName = 'GRD'];
            
            //create as many records as in the no of records variable
            for (Integer i=1; i<=noOfBoreholes; i++)
            {
            
                GRD_Borehole__c bore = new GRD_Borehole__c();
                bore.Name = 'test '+i;
                bore.Target_Formation__c = 'WLRC';
                bore.Well_Class__c = 'C';
                bore.RecordTypeId = rt.Id;
                bore.Well_Category__c = 'Development';
                bore.Geologist__c = userInfo.getName();
                            
                if(i<10)
                {
                    Location__c target = new Location__c();
                    target.Name = '100/0'+i+'-'+'0'+i+'-'+'00'+i+'-18W5/00';
                    target.DLS_Type__c = 'Target';
                    target.RecordTypeId = getRecordTypeId('Location__c','Gas Resource Development');
                    target.Well_Orientation__c = 'VERT';
                    target.Operating_Field_AMU__c = field.Id;
                    insert target;
                    bore.Target_Location__c = target.id;
                    Location__c uwi = new Location__c();
                    uwi.Name = '100/0'+i+'-'+'0'+i+'-'+'00'+i+'-18W5/00';
                    uwi.DLS_Type__c = 'UWI';
                    uwi.RecordTypeId = getRecordTypeId('Location__c','Gas Resource Development');
                    uwi.Well_Orientation__c = 'VERT';
                    uwi.Operating_Field_AMU__c = field.Id;
                    insert uwi;
                    bore.UWI_Location__c = uwi.id;
                }
    //            else if(i>=10 && i<100)
    //            {
    //                bore.Target_Location__c = '100/'+i+'-'+i+'-'+'0'+i+'-18W5/00';
    //                bore.UWI_Location__c = '100/'+i+'-'+i+'-'+'0'+i+'-18W5/00';
    //            }
                
                bore.Well__c = well.Id;
                bore.MD__c = 100;
                bore.Horizontal_Length__c = 200;
                bore.True_Vertical_Depth__c = 300;
                bore.Working_Interest__c = 400;
                
                System.debug('borehole for I '+i+' : '+bore);
                
                boreholeList.add(bore);
                
            }
            
            insert boreholeList;
            
            //check if all rows are inserted
            testBoreholeList = [select Id, Name from GRD_Borehole__c];
            System.assertEquals(noOfBoreholes, testBoreholeList.size());
            
            //we have a "For Inside For" for this case.. but testing does not require many completion records per class d well
            //if we have 10 class d wells and 2 completion records (max) per class d well - loop is executed 20 times 
            //Moreover no query or dml is done within loop.. so no issues at all in terms of governor limits  
            for(GRD_Borehole__c bore: boreholeList)
            {
                for(Integer i=1; i<=noOfCompletionPerBorehole; i++)
                {
                    Class_C_Well_Completion__c completion = new Class_C_Well_Completion__c();
                    completion.CategoryIsolationCode__c = 'Ball drop';
                    completion.CompletionFluid__c = 'Propane';
                    completion.NumberOfFracs__c = 10;
                    completion.TProppantPerFrac__c = 100;
                    completion.Well__c = bore.Id;
                    completionList.add(completion);
                }
            }
            
            insert completionList;
            
            //check if completion row(s) are inserted
            testCompletionList = [select Id, Name from Class_C_Well_Completion__c];
            System.assertEquals(noOfBoreholes * noOfCompletionPerBorehole, testCompletionList.size());
            
            //now verify whether JSON is created in proper format 
            String classCJson = GRDWellInventoryController.getClassCJSON('Gas Resource Development');     
            
            //CLASS C WELL COMPLETION SECTION
            
            Completion comp = new Completion();
            comp.name = completionList[0].Name;
            comp.categoryIsolation = completionList[0].CategoryIsolationCode__c;
            comp.numberOfStages = completionList[0].NumberOfFracs__c;
            comp.sizeOfStages = completionList[0].TProppantPerFrac__c;     
            comp.boreId = completionList[0].Well__c;     
            
            String completionrowJson = JSON.serialize(comp);
            
            //add new class d completion
            GRDWellInventoryController.addNewClassCCompletion(null, completionrowJson);
            
            //check if completion row(s) are inserted
            testCompletionList = [select Id, Name from Class_C_Well_Completion__c Order By LastModifiedDate DESC];
/////        System.assertEquals(2, testCompletionList.size());     
            
            //add id to Completion
            comp.id = testCompletionList[0].Id;
            completionrowJson = JSON.serialize(comp);
            
            //save class d well completion
            GRDWellInventoryController.saveClassCCompletion(null, completionrowJson);
            
            
            GRDWellInventoryController.setClassCWellViewBoreGUID('1234567890',testBoreholeList[0].Id);
            
            
            WCP_Data_Mapping__c wdm = new WCP_Data_Mapping__c(From_System__c='test',From_Table__c='test',From_Table_Key__c='test',GUID__c='test',Mapping_Type__c='test',To_System__c='test',To_Table__c='test',To_Table_Key__c='test');
            insert wdm;
            
            WCP_Data_Mapping__c wdm2 = new WCP_Data_Mapping__c(From_System__c='test',From_Table__c='test',From_Table_Key__c='test',GUID__c='test',Mapping_Type__c='test',To_System__c='test',To_Table__c='test',To_Table_Key__c='test');
            insert wdm2;
            
            GUIDMapping g = new GUIDMapping();
            g.wellViewZoneSFID=wdm.Id;
            g.wellViewZoneGUID='test';
            g.WellViewZone='test';
            g.PVRWellSFID=wdm2.Id;
            g.PVRWellGUID='test';
            g.PVRWell='test';
            
            GRDWellInventoryController.addNewGUIDMapping(boreholeList[0].Id, JSON.serialize(g), boreholeList[0].Id);
            
            GRDWellInventoryController.saveGUIDMapping('test',JSON.serialize(g));
            
            GRDWellInventoryController.deleteAllGUIDMappings('['+JSON.serialize(g)+']');
            
            PicklistWrapper plw = new PicklistWrapper();
            plw.fields = new List<Field>();
            
            List<PLValue> plv = new List<PLValue>();
            plv.add(new PLValue('O','O'));
            plv.add(new PLValue('N','N'));
            
            Field f = new Field('OP/NON OP',plv);
            
            GRDWellInventoryController.getPlValuesLookupTbl(JSON.serialize(plw));
            
            GRDWellInventoryController.findSObjects('18W5/00','SELECT Id, Name FROM Location__c WHERE Name  ','UWI');
            
////            GRDWellInventoryController.getPlValuesLookupTbl('');
        }
    }
    
    //class structure for picklist values
    public class PicklistWrapper
    {
        //public List<Obj> objects;
        public List<Field> fields;
    }
    
    public class Field
    {
        public String fieldName;
        public List<PLValue> plvalues;
        
        public Field(String fn, List<PLValue> plv)
        {
            fieldName = fn;
            plvalues = plv;
        }
    }
    
    public class PLValue
    {
        public String label;
        public String value;
        
        public PLValue(String l, String v)
        {
            label = l;
            value = v;
        }
    }
    
    public static Id getRecordTypeId(String objName, String recordTypeName)
    {
        Map<String, Schema.SObjectType> gdMap = Schema.getGlobalDescribe();
        Schema.SObjectType sObj = gdMap.get(objName);
        Schema.DescribeSObjectResult result = sObj.getDescribe();
        List<RecordTypeInfo> rtInfos = result.recordTypeInfos;
        Id rtId;

        for(RecordTypeInfo rtInfo : rtInfos)
        {
            system.debug('rt name '+rtInfo.Name);
            if(rtInfo.Name == recordTypeName)
            {
                rtId = rtInfo.recordTypeId;
            }
        } 
        return rtId;
    }
    
    //classes for JSON
    //details of borehole\ - used by both class D and class C
    public class Borehole
    {
        String id;
        String name;
        String status;
        String grdGuid;
        String wellviewBoreGuid;
        String wellviewBoreGuidMappingGUID; // this is the GUID from the T_MASTER_DATA_MAPPING table
        String wellClass;
        String targetFormation;
        String wellType;
        String wellCategory;
        String geologist;
        //Location Information
        String operatingDistrict;
        String operatingDistrictId;
        String operatingField;
        String operatingFieldId;
        String surfaceLocation; 
        String surfaceLocId;
        String targetLocation;
        String targetLocId;
        String uwiLocation;
        String uwiLocId;
        Decimal measuredDepth;
        Decimal horizontalLength;
        Decimal workingInterest;
        Decimal trueVerticalDepth;
        String opNonOp;
        String strategy;
        String pool;
        String primaryProduct;
        Decimal risk;
        
        Integer noOfCompletions;
        //associated well is uwi location which allows multiple bores to be associated to a well
        String associatedWell;
        //if there is an error while saving to database, this flag is checked by Mule
        String errorInDb;
        // Flag is true while DB is updating record
        String refreshingFromDB;
        
        public List<Completion> completions;
        
        public List<GUIDMapping> GUIDMappings;
    }
    
    //details of completion - used by both class D and class C
    public class Completion
    {
        String id;
        String name;
        String completionGuid;
        //Decimal completionId;
        String categoryIsolation;
        String completionFluid;
        Decimal numberOfStages;
        Decimal sizeOfStages;
        Decimal proppantSize;
        String proppantType;
        String boreId;
    }
    
    public class GUIDMapping
    {
        String wellViewZoneSFID;
        String wellViewZoneGUID;
        String WellViewZone;
        String PVRWellSFID;
        String PVRWellGUID;
        String PVRWell;
    }
}