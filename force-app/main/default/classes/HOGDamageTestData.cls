/*-----------------------------------------------------------------------------------------------
Author     : Rey Ocampo
Company    : Husky Energy
Description: A utility class creating test data for the HOG_Damage__c
-------------------------------------------------------------------------------------------------*/
@isTest 
public class HOGDamageTestData
{
    public static HOG_Damage__c createHOGDamage
    (
    	String name,    	
    	String damage_Code, 
    	String damage_Description,
    	String part_Code,
    	Boolean active
    )
    {                
        HOG_Damage__c results = new HOG_Damage__c
            (           
                Name = name,
                Damage_Code__c = damage_Code,
                Damage_Description__c = damage_Description,
                Part_Code__c = part_Code, 
                Active__c = active
            ); 

        return results;
    }
}