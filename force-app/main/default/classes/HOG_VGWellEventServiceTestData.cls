/*-----------------------------------------------------------------------------------------------
Author     : Miro Zelina
Company    : Husky Energy
Description: A utility class creating test data for the HOG_VGWellEventServiceTest class
History    : 29-Mar-2018 - Created
-------------------------------------------------------------------------------------------------*/
@isTest 
public class HOG_VGWellEventServiceTestData {
    
    public static HOG_VGWellEventService.APIVGWellEvent createAPIVGWellEvent
    (
        String RAW_PVR_UWI,
		String PVR_GOR_Factor,
		String GOR_Test_Date,
		String GOR_Effective_Date,
		String Measured_Vent_Rate,
		String PVR_Fuel_Consumption,
		String Single_Well_Battery,
		String Monthly_Trucked_Oil 
    )
    {
       HOG_VGWellEventService.APIVGWellEvent apiVGWellEvent = new  HOG_VGWellEventService.APIVGWellEvent();
       
       apiVGWellEvent.RAW_PVR_UWI = RAW_PVR_UWI;
       apiVGWellEvent.PVR_GOR_Factor = PVR_GOR_Factor;
       apiVGWellEvent.GOR_Test_Date = GOR_Test_Date;
       apiVGWellEvent.GOR_Effective_Date = GOR_Effective_Date;
       apiVGWellEvent.Measured_Vent_Rate = Measured_Vent_Rate;
       apiVGWellEvent.PVR_Fuel_Consumption = PVR_Fuel_Consumption;
       apiVGWellEvent.Single_Well_Battery = Single_Well_Battery;
       apiVGWellEvent.Monthly_Trucked_Oil = Monthly_Trucked_Oil;
       
       return apiVGWellEvent;
    }
}