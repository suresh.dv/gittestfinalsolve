public class ATSTestData{
    /*
    KK 2014-11-24 - this function is not used anymore.
    public static ATS_Parent_Opportunity__c createTender(String pTenderNumber, Date pBidDueDateTime) {

        ATS_Parent_Opportunity__c tender = new ATS_Parent_Opportunity__c();
        tender.Tender__c= pTenderNumber;
        tender.Bid_Due_Date_Time__c= pBidDueDateTime; //Date.today();
        tender.Freight_Due_Date_Time__c = DateTime.now();
         
        
        User tempUser = UserTestData.createTestUser();
        insert tempUser;
        
        tender.Marketer__c = tempUser.Id;

        return tender; 
    } 
    */
     
     public static ATS_Parent_Opportunity__c createTender(String pTenderNumber, DateTime pBidDueDateTime, User tempUser) {

        ATS_Parent_Opportunity__c tender = new ATS_Parent_Opportunity__c();
        tender.Tender__c= pTenderNumber;
        tender.Bid_Due_Date_Time__c= pBidDueDateTime; //Date.today();
        tender.Freight_Due_Date_Time__c = DateTime.now();
         
//        User tempUser = UserTestData.createTestUser();
//        insert tempUser;

         
//        tender.Marketer__c = tempUser.Id;

        tender.Marketer__c = tempUser.Id;

         
        return tender; 
    }
       
    public static User CreateUser(){
        User tempUser = UserTestData.createTestUser();
        insert tempUser;
                
        return tempUser;
    }
    
    public static ATS_Tender_Customers__c createTenderCustomer(ID pAccountID, ID pTenderID, ID pContactID) {
        ATS_Tender_Customers__c tenderCustomer = new ATS_Tender_Customers__c();
          tenderCustomer.Account__c = pAccountID;
          tenderCustomer.ATS_Parent_Opportunity__c = pTenderID;
          tenderCustomer.Contact__c = pContactID ;
        return tenderCustomer ; 
    } 
    
    public static RecordType getAsphaltRecordType()
    {
        RecordType RecType = [Select Id From RecordType  Where SobjectType = 'Account' and DeveloperName = 'Asphalt'];
        return RecType;
    }
    
    public static Opportunity CreateOpportunity(ID pAccountID, ID pTenderID, String stage)
    {
        Opportunity newOpportunity = new Opportunity();
        newOpportunity.Opportunity_ATS_Product_Category__c = pTenderID;
        newOpportunity.AccountId = pAccountID;   
        newOpportunity.Name = 'Test Opportunity (ATS)' + stage;
        newOpportunity.CloseDate = date.newInstance(2014,12,1);
        newOpportunity.StageName = stage;
       
        
        return newOpportunity;
    }
    
    
    public static ATS_Freight__c createATSFreight(ID oppId)
    {
        ATS_Freight__c freight = new ATS_Freight__c();
        freight.Prices_F_O_B__c = 'Destination';
        freight.Local_Freight_Via__c = 'Rail';
        freight.Husky_Supplier_1__c = 'Burnco--9796-9796';
        freight.ATS_Freight__c = oppId;
        
        return freight;
    }

}