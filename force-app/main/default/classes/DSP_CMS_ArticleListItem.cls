global virtual with sharing class DSP_CMS_ArticleListItem extends DSP_CMS_ArticleController
{
    global DSP_CMS_ArticleListItem(cms.GenerateContent cc)
    {
        super(cc);
    }
    
    global DSP_CMS_ArticleListItem()
    {
        super();
    }
    
    global override String getHTML()
    {
        return articleListItemHTML();
    }
}