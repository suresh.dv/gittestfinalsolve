/*************************************************************************************************\
Author:         Jakub Schon
Company:        Husky Energy
Description:    Controller Unit Test for HOG_VGEmailNotifications.cls
History:        jschn 05.02.2018 - Created.
                mz: 23-May-2018 - Test method for HOG_VGEmailNotifications.generateEmailNotification()
**************************************************************************************************/
@isTest
private class HOG_VGEmailNotificationsTest {
	
	@isTest static void testBatchable() {
		Test.startTest();
		Database.executeBatch(new HOG_VGEmailNotifications(
			HOG_VGEmailNotifications.NOTIFICATIONTYPE_INCOMPLETE_ALERTS));
		//Integer invocations = Limits.getEmailInvocations();

		Integer dmlRowsBfr = Limits.getDMLRows();
		Integer dmlStatementsBfr = Limits.getDMLStatements();
		Integer queriesBfr = Limits.getQueries();

		Test.stopTest();
		//Integer invocationsAfter = Limits.getEmailInvocations();
		Integer dmlRowsAftr = Limits.getDMLRows();
		Integer dmlStatementsAftr = Limits.getDMLStatements();
		Integer queriesAftr = Limits.getQueries();


		System.assertNotEquals(dmlStatementsBfr, dmlStatementsAftr);
		System.assertNotEquals(dmlRowsBfr, dmlRowsAftr);
		System.assertNotEquals(queriesBfr, queriesAftr);
		System.assertEquals(11,dmlRowsAftr);
		System.assertEquals(11,dmlStatementsAftr);
		//System.assertEquals(5,queriesAftr);
	}
	

	@isTest 
	static void testGenerateEmailNotification() {

    	List<HOG_Vent_Gas_Alert__c> alertList = [SELECT Id, Description__c 
    	                                        FROM HOG_Vent_Gas_Alert__c 
    	                                        WHERE Description__c = 'TestDescription' 
    	                                        LIMIT 1];
    	                                        
    	List<User> UserList = [SELECT Id, Email 
    	                       FROM User 
    	                       WHERE Email = 'Lois@Lane.com.test' 
    	                       LIMIT 1];
    	                       
    	alertList[0].Status__c = HOG_VentGas_Utilities.TASK_STATUS_IN_PROGRESS;
    	alertList[0].Production_Engineer__c = UserList[0].Id;
    	
    	//There should be no one email invocation
    	System.assertEquals(0,Limits.getEmailInvocations());

    	update alertList[0];
    	
    	//There should be one email invocation
    	//System.assertEquals(1,Limits.getEmailInvocations());
	}


	@testSetup static void createData() {
		Location__c loc = HOG_VentGas_TestData.createLocation();
		User user1 = HOG_VentGas_TestData.createUser('Clark', 'Kent', 'Superman');
		User user2 = HOG_VentGas_TestData.createUser('Lois', 'Lane', 'Lo');

		HOG_Vent_Gas_Alert__c alert = HOG_VentGas_TestData.createAlert(HOG_VentGas_Utilities.ALERT_TYPE_EXPIRED_TEST, 
																		'TestDescription', 
																		loc.Id,
																		user1.Id, 
																		HOG_VentGas_Utilities.ALERT_PRIORITY_MEDIUM, 
																		HOG_VentGas_Utilities.ALERT_STATUS_IN_PROGRESS, 
																		HOG_VentGas_Utilities.ALERT_TYPE_EXPIRED_TEST);

		alert = HOG_VentGas_TestData.createAlert(HOG_VentGas_Utilities.ALERT_TYPE_EXPIRED_TEST, 
												'TestDescription', 
												loc.Id,
												user1.Id, 
												HOG_VentGas_Utilities.ALERT_PRIORITY_MEDIUM, 
												HOG_VentGas_Utilities.ALERT_STATUS_NOT_STARTED, 
												HOG_VentGas_Utilities.ALERT_TYPE_EXPIRED_TEST);

		alert = HOG_VentGas_TestData.createAlert(HOG_VentGas_Utilities.ALERT_TYPE_EXPIRED_TEST, 
												'TestDescription', 
												loc.Id,
												user1.Id, 
												HOG_VentGas_Utilities.ALERT_PRIORITY_MEDIUM, 
												HOG_VentGas_Utilities.ALERT_STATUS_NOT_STARTED, 
												HOG_VentGas_Utilities.ALERT_TYPE_EXPIRED_TEST);
		alert.Status__c = HOG_VentGas_Utilities.ALERT_STATUS_ACKNOWLEDGE;
		update alert;
	}
}