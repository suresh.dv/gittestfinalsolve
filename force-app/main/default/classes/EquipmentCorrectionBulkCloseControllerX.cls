/*------------------------------------------------------------
Author:        Yen Le
Company:       Thirdwave
Description:   Controller of EquipmentCorrectionBulkClose page to close Status of multiple Equipment Data Update records
Test Class:    EquipmentCorrectionFormTest

History:       03-Jul-17 Miro Zelina - included System & Sub-System into bulk Close Form
               27-Jul-17 Miro Zelina - added role check for SAP Support Lead
			   04-Aug-17 Miro Zelina - added profile & PS check for process button
         04.09.17    Marcel Brimus - added FEL and Yard 
------------------------------------------------------------*/

public with sharing class EquipmentCorrectionBulkCloseControllerX {
    
    public List<Equipment_Correction_Form__c> selectedList {get; private set;}
    public integer CurrentIndex {get; private set;}
    public Equipment_Correction_Form__c CurrentForm {get; private set;}
    public EquipmentUtilities.UserPermissions userPermissions;

    public EquipmentCorrectionBulkCloseControllerX(ApexPages.StandardSetController controller)
    {
       userPermissions = new EquipmentUtilities.UserPermissions();
       
       if (!Test.isRunningTest())
       {
           controller.addFields(new List<String>{'Name', 'Status__c', 'Equipment_Status__c', 'Description_old__c', 'Description_new__c',
                'Manufacturer_old__c', 'Manufacturer_new__c', 'Model_Number_old__c', 'Model_Number_new__c',
                'Manufacturer_Serial_No_old__c', 'Manufacturer_Serial_No_new__c', 'Tag_Number_old__c', 'Tag_Number_new__c',
                'Equipment__c', 'Equipment__r.Location__c', 'Equipment__r.Well_Event__c', 'Equipment__r.System__c', 'Equipment__r.Sub_System__c',
                'Equipment__r.Functional_Equipment_Level__c','Equipment__r.Yard__c',
                'Equipment__r.Location__r.Route__c', 'Equipment__r.Planner_Group__c','Equipment__r.Well_Event__r.Route__c',
                'Equipment__r.System__r.Route__c','Equipment__r.Sub_System__r.Route__c',
                'Equipment__r.Functional_Equipment_Level__r.Route__c', 'Equipment__r.Yard__r.Route__c',
                'CreatedById', 'CreatedDate','LastModifiedDate', 'LastModifiedById', 'Comments__c',
                'Equipment__r.Facility__r.Plant_Section__c', 'Equipment__r.Facility__c'});
       }  
       
       if (!userPermissions.isSystemAdmin && !userPermissions.isSapSupportLead && !userPermissions.isHogAdmin){
           
           selectedList = new List<Equipment_Correction_Form__c>();
           ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, 'Only SAP Support Lead, HOG - Administrator or System Administrator can process.'));
           return;
       }
    
       else if(controller.getSelected().Size()==0)
       {
           selectedList = new List<Equipment_Correction_Form__c>();
           ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, 'Please select at least one Equipment Correction Form'));
           return;
       }
       selectedList = controller.getSelected();
       CurrentIndex = 1;       
       
       CurrentForm = selectedList[CurrentIndex-1];
    }
    //get the Attachment of the Current Form
    public Attachment photo
    {
        get{
            if (photo == null)
            {
                List<Attachment> att = [select Id, Name, Body, ContentType from Attachment where ParentId =: CurrentForm.Id];
                if (att.size() > 0)
                   photo = att[0];
                else
                    photo = new Attachment();   
            }
            return photo;
        }
        set;
    }
    public boolean getHasPreviousForm()
    {
        return (CurrentIndex > 1);
    }   
   
    public boolean getHasNextForm()
    {
        return (CurrentIndex < selectedList.size());
    }
    
    public PageReference NextForm()
    {
        if(getHasNextForm())
        { 
            CurrentIndex = CurrentIndex + 1 ;
            CurrentForm = selectedList[CurrentIndex-1];   
            photo = null;
        }
       return null; 
    }
   
    public PageReference PreviousForm()
    { 
       if(getHasPreviousForm())
       { 
           CurrentIndex = CurrentIndex - 1 ;
           CurrentForm = selectedList[CurrentIndex-1];       
           photo = null;
       }
       return null;
    }    
    //Change Form's Status to Closed
    public PageReference CloseForm()
    {
       Equipment_Correction_Form__c obj = CurrentForm;  

       if(obj.Status__c != 'Closed')
       {
           try
           {
            obj.Status__c = 'Closed';
            update obj;
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.CONFIRM, 'The form has been successfully CLOSED.'));
            return null;
            
           }
           catch(Exception ex)    
           {
               ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, ex.getMessage()));
               return null;
           }               
       }   
  
       return null;   
   
    }

}