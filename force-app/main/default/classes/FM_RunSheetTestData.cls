@isTest
public class FM_RunSheetTestData {


  public static FM_Run_Sheet__c createRunSheet(Date dt, ID WellID, String tank,
    Decimal tankSize,  Decimal tankLowLevel, Decimal actTankLevel, decimal actFlowRate, 
    integer tonightOil, integer tonightWater,
    integer tomorrowOil, integer tomorrowWater,
    String axle,
    String standingComments,
    Boolean Sour,
    Boolean save) {

//[select id, name, Date__c, Axle__c, Standing_Comments__c, Tank__c , Tank_Size__c, Tank_Low_Level__c, Act_Tank_Level__c,Act_Flow_Rate__c,
// Tonight_Oil__c,Tonight_Water__c,Tomorrow_Oil__c,Tomorrow_Water__c,Outstanding_Oil__c,Outstanding_Water__c


    FM_Run_Sheet__c testData = new FM_Run_Sheet__c();

    testData.Date__c = dt;
    testData.Well__c = wellID;
    testData.Tank__c = tank;
    testData.Tank_Size__c = tankSize;
    testData.Tank_Low_Level__c = tankLowLevel;
    testData.Act_Tank_Level__c = actTankLevel;
    testData.Act_Flow_Rate__c = actFlowRate;
    testData.Tonight_Oil__c = tonightOil;
    testData.Tonight_Water__c = tonightWater;
    testData.Tomorrow_Oil__c = tomorrowOil;
    testData.Tomorrow_Water__c = tomorrowWater;
    testData.Axle__c = axle;
    testData.Standing_Comments__c = standingComments;
    testData.Sour__c = Sour;
    if(save)
    {
		try
		{
			insert testData;
		}
		catch(Exception ex)
		{
			System.Assert(ex.getMessage().contains('Historical Run Sheet cannot be modified'), 'Historical Run Sheet');
		}
    }

    return testData;


  }


    public Static Business_Unit__c  createBusinessUnit(String pname,  Boolean save)
    {
            Business_Unit__c businessUnit = new Business_Unit__c
            (
                Name = pname
            );
            if(save)
            {
                insert businessUnit;
            }
            return businessUnit;
    }

    public Static Business_Department__c createBusinessDepartment(String pname,  Boolean save)
    {
            Business_Department__c businessDepartment = new Business_Department__c
            (
                Name = pname
            );

            if(save)
            {
                insert businessDepartment;
            }
            return businessDepartment;
    }

    public Static Operating_District__c createOperatingDistrict(String pname, ID businessDepartmentId, ID businessUnitId,  Boolean save)
    {
            Operating_District__c operatingDistrict = new Operating_District__c
                (
                    Name = pname,
                    Business_Department__c = businessDepartmentId,
                    Business_Unit__c = businessUnitId
                );
            if(save)
            {
                insert operatingDistrict ;
            }
            return operatingDistrict ;
    }


    public Static Field__c  createField(String pname, ID operatingDistrictId, Boolean save)
    {
            Field__c field = new Field__c
                (
                    Name = pname,
                    Operating_District__c = operatingDistrictId//,
                    //RecordTypeId = recordTypeId
                );
            if(save)
            {
                insert field;
            }
            return field;
    }

    public Static Route__c  createRoute(String pname, Boolean save)
    {

        User fieldSeniorUser = UserTestData.createTestUser();
        insert fieldSeniorUser;

        Route__c route = new Route__c
              (
                  Name = pname,
                  Fluid_Management__c = true,
                  Field_Senior__c = fieldSeniorUser.ID,
                  Route_Number__c = pname
              );

        if(save)
        {
            insert route;
        }
        System.debug(route);
        return route;
    }

    public Static Location__c  createLocation(String pname, ID fieldid, ID routeid, Boolean Sour, Boolean save)
    {
            Location__c location =
                new Location__c
                    (
                        Name = pname,
                        Route__c = routeid,
                        Operating_Field_AMU__c = fieldid
                    );

            if(Sour)
            {
                location.Hazardous_H2S__c ='Y';
            }
            if(save)
            {
                insert location;
            }
            return location;
    }




/*
    public static User createTestCustomerPortalUser(Id contactId) {
        ID ProfileID = [ Select id from Profile where name = 'US Portal User Profile'].id;

        User user = new User();
        Double random = Math.Random();
        user.email = 'test-user' + random + '@fakeemail.com';
        user.ContactId = contactId;
        user.ProfileId = ProfileID;
        user.UserName = 'test-user' + random + '@fakeemail.com';
        user.alias = 'tu' + String.ValueOf(contactId).right(6);
        user.CommunityNickName = 'tuser1' + random;
        user.TimeZoneSidKey = 'America/New_York';
        user.LocaleSidKey = 'en_US';
        user.EmailEncodingKey = 'ISO-8859-1';
        user.LanguageLocaleKey='en_US';
        user.FirstName = 'Test';
        user.LastName = 'User';
        insert user;

        return user;
    }
*/

}