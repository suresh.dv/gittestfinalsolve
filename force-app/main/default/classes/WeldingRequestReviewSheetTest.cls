@isTest(SeeAllData = true)
public class WeldingRequestReviewSheetTest{
    @isTest static void GenerateReviewSheetAndSendTestMethod() { 
        Test.setMock(WebServiceMock.class, new WeldingRequestReviewSheetWebServMockImpl());
        
        Welding_Request__c request = WeldingRequestTestData.createWeldingRequest();
                
        RecordType recType = [SELECT Id, Name, DeveloperName FROM RecordType WHERE SObjectType = 'Account' AND DeveloperName = 'Welding_Partner_Account'];
        Account acct = AccountTestData.createAccount('accountName', null);
        acct.RecordTypeId = recType.Id;
        /*acct.BillingCity = 'Calgary';
        acct.BillingStreet = 'Calgary Street';
        acct.BillingState = 'Calgary';
        acct.BillingPostalCode = '302010';
        acct.BillingCountry = 'Canada';*/
        insert acct;
        List<Welding_Procedure__c> procedures = WeldingProcedureTestData.createProcedures(2, acct);
        
        List<Related_Welding_Procedure__c> relatedProcedures = RelatedWeldingProcedureTestData.createRelatedWeldingProcedure(request, procedures);       
        
        request.Status__c = 'Approved';
        update request;
        
        PageReference pageRef = Page.WeldingProcedureReviewTemplate;
        Test.setCurrentPage(pageRef);
        ApexPages.currentPage().getParameters().put('relatedWeldingProcedureId', relatedProcedures[0].Id);
        WeldingProcedureReviewTemplate template = new WeldingProcedureReviewTemplate();
    }
    
    @isTest static void GenerateReviewSheetAndSendTestMethod2() { 
        Test.setMock(WebServiceMock.class, new WeldingRequestReviewSheetWebServMockImpl());
        
        Welding_Request__c request = WeldingRequestTestData.createWeldingRequest();
                
        RecordType recType = [SELECT Id, Name, DeveloperName FROM RecordType WHERE SObjectType = 'Account' AND DeveloperName = 'Welding_Partner_Account'];
        Account acct = AccountTestData.createAccount('accountName', null);
        acct.RecordTypeId = recType.Id;
        /*acct.BillingState = 'Calgary';
        acct.BillingPostalCode = '302010';
        acct.BillingCountry = 'Canada';*/
        insert acct;
        List<Welding_Procedure__c> procedures = WeldingProcedureTestData.createProcedures(2, acct);
        
        List<Related_Welding_Procedure__c> relatedProcedures = RelatedWeldingProcedureTestData.createRelatedWeldingProcedure(request, procedures);       
        
        WeldingRequestReviewSheet.GenerateReviewSheetAndSend(new list<Id>{request.Id});
    }
    /*@isTest static void SetAccount()
    {
        RecordType recType = [SELECT Id, Name, DeveloperName FROM RecordType WHERE SObjectType = 'Account' AND DeveloperName = 'Welding_Partner_Account'];
        Account acct = AccountTestData.createAccount('accountName', null);
        acct.RecordTypeId = recType.Id;
        acct.AccountNumber= '302010';        
        insert acct;
        Bill_To__c billTo = new Bill_To__c(Account__c=acct.Id, Name='BillTo',Bill_To_Key__c='900990', Customer__c='302010');
        insert billTo;
        acct.AccountNumber= '302010';
        acct.Customer_Number__c =  '302012';
        update acct;       
        billTo.Bill_To_Key__c='900992';
        billTo.Customer__c='302012';
        update billTo;
    }*/
}