@isTest
private class AA_UtilitiesTest {
	
	@isTest static void testAfterInsertRecord() {
		User runningUser  = AA_TestData.createAgileInitiativeUser();

		System.runAs(runningUser) {	
			AA_Initiative__c testData = AA_TestData.createInitiative(runningUser);
			List<Agile_Initiative_Stage__c> stages = [SELECT id 
												FROM Agile_Initiative_Stage__c 
												WHERE Agile_Initiative__c =: testData.id];

			System.assertEquals(AA_Utilities.getPhasePickListValues().size(), stages.size());
		}

	}
	
}