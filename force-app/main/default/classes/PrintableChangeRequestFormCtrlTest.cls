@isTest
private class PrintableChangeRequestFormCtrlTest{
    private testmethod static void defaultMethods(){
        Id uid = UserInfo.getUserId();
        
        Project_Area__c proj = new Project_Area__c( Name = 'Test', SPA__c = uid, Project_Manager__c = uid,
                                                    Change_Coordinator__c = uid );
        insert proj;
        
        Change_Request__c chq = new Change_Request__c( Document_Number__c = 'Doc 1', Title__c = 'Test',
                                                        Project_Area__c = proj.Id, Contractor__c = uid );
        insert chq;
        
        Impact_Assessor__c impact = new Impact_Assessor__c( Change_Request__c = chq.Id, Name = 'Test',
                                        ChangeRequestImpactedFuntionAssessorKey__c = chq.Id, 
                                        Assessor__c = uid, SPA__c = uid ); 
        insert impact;
        
        Project__c projectVal = new Project__c( Name = 'Test', VP__c = uid, Project_Manager__c = uid );
        insert projectVal;
        
        //CR_Affected_Program_Area__c prog = new CR_Affected_Program_Area__c( Name = 'Test', Change_Request__c = chq.Id );
        //insert prog;
        
        CR_Program_Area__c crProg = new CR_Program_Area__c( Name = 'test' );
        insert crProg;
        
        CR_Impacted_Functions__c crFunc = new CR_Impacted_Functions__c( Name = 'test' );
        insert crFunc;
        
        ApexPages.StandardController sc = new ApexPages.StandardController( chq );
        test.startTest();
            PrintableChangeRequestFormCtrl controller = new PrintableChangeRequestFormCtrl( sc );
            controller.getCostImpactListSize();
            controller.getJustificationOptions();
            controller.getProjectAreas();
            controller.getProgramAreas();
            controller.getImpactedFunctions();
            controller.getApprovers();
            controller.getSPAApprovalPro();
            controller.getPMApprovedDate();
        test.stopTest();
    }
}