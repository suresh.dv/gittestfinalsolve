public class PTCalculatePCSOBillingCacheQuery {
    public static Map<id,Service__c> billingMap = new Map<id,Service__c>();
    public static boolean billingMapQueried = false;
    
    public static Map<id,Service__c> querybillingMap(){
        if(billingMapQueried == false){    
            for(Service__c service : [select id,site_id__c,(select Average_Billing_Demand_last_12_months__c,Average_Metered_Demand_last_12_months__c,Average_kVA_Demand_last_12_months__c,Average_Total_Volume_last_12_months__c  from utility_billings__r order by billing_date__c desc limit 1 ) from service__c limit 50000]){
                if(service.Site_Id__c != null){
                    billingMap.put(service.id,service);
                }
            }                       
        }
        billingMapQueried = true; 
        return billingMap;
    }
}