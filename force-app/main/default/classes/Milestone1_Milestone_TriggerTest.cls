@isTest
private class Milestone1_Milestone_TriggerTest {
    static testMethod void testCourseSchedule() {
        Map<String, Id> courseRecTypeMap = new Map<String, Id>();
        
        for(RecordType recType :[SELECT 
                                    Id, Name, DeveloperName, SobjectType
                                  FROM 
                                    RecordType 
                                  WHERE 
                                    DeveloperName =: 'Course_Schedule' 
                                 AND SobjectType in ('Milestone1_Milestone__c', 'Milestone1_Task__c', 'Milestone1_Project__c')]) {
            courseRecTypeMap.put(recType.sobjectType, recType.Id);
        }
        
        Milestone1_Project__c project = Milestone1_Test_Utility.sampleProject('Test Project');
        project.RecordTypeId = courseRecTypeMap.get('Milestone1_Project__c');
        
        insert project;
        System.assertNotEquals(project.Id, Null);
        
        HROECourseManagement__c customSetting = new HROECourseManagement__c();
        customSetting.Name = 'AssignedToId';
        customSetting.Value__c = UserInfo.getUserId();
        
        insert customSetting;
            
        Milestone1_Milestone__c milestone = new Milestone1_Milestone__c();
        milestone.Name = '<<Do Not Remove>>';
        milestone.Project__c = project.Id;
        milestone.Location__c = 'Location';
        milestone.Venue__c = 'Venue';
        milestone.Kickoff__c = Date.today();
        milestone.Last_Day_Of_Class__c = Date.today();
        milestone.Session_Module__c = 'Session 1';
        milestone.RecordTypeId = courseRecTypeMap.get('Milestone1_Milestone__c');
        
        insert milestone;
        System.assertNotEquals(milestone.Id, Null);
        
        List<Milestone1_Task__c> listOfTasksCreated = [SELECT Id FROM Milestone1_Task__c WHERE Project_Milestone__c =: milestone.Id];
        //System.assertEquals(listOfTasksCreated.size(), 14);
        
        milestone = [SELECT Id, Name, Location__c, Session_Module__c, Kickoff__c FROM Milestone1_Milestone__c WHERE Id =:milestone.Id];
        //System.assertEquals(milestone.Name, milestone.Location__c + ' (' + milestone.Kickoff__c.format() + ') - ' + milestone.Session_Module__c );
        
    }
}