/// controller classs for reports displayed in customer portals
public with sharing class USCP_MSDS_Controller {

/// Gets the corect base URL regardless if the page is called from platform 
/// or from a Force.com site
    public String FolderID {get;set;}
    
    public USCP_MSDS_Controller (){
        try
        {
            Folderid= [Select id from folder where Name = 'USCP MSDS' LIMIT 1][0].ID; 
        }
        catch(Exception ex)
        {}        
    }
    
    public List<LinkData> DocumentList {    
    get {
      if (DocumentList == null) {
            DocumentList = new List<LinkData>();   
            
            for(Document doc :   [Select id, Name, folderid, lastModifiedDate from Document where folderid = :FolderID ORDER BY Name])
            {
                LinkData docLink = new LinkData();
                docLink.name = doc.Name;
                docLink.URL = '/servlet/servlet.FileDownload?file='+doc.Id;
                docLink.lastModifiedDate  = doc.lastModifiedDate.format('MMM dd, yyyy');
                DocumentList.Add(docLink );
            }       
        }
        return DocumentList ;           
     }        
    set;
    }
    

    // Wrapper class
    public class LinkData{

        public String name { get; set; }    
        public String URL{ get; set; }          
        public String lastModifiedDate { get; set; }            
    }    
}