/*-------------------------------------------------------------------
Author      : Gangha Kaliyan * UPDATED BY GUI MANDERS ON 2015-06-08 *
Company     : Husky
Description : Used for print PDF and generate PDF attachment for truck lists
Test Class  : FM_TruckList_Test
Date        : 9-Mar-2015
OBSOLETE CLASS, NOT USED AFTER TRUCK LIST REMOVAL
----------------------------------------------------------------------*/

public class FM_TruckTripEmailComponentController
{
    
    // THIS NUMBER INDICATES THE MAXIMUM NUMBER OF ROWS ON THE FIRST PAGE FOR FORMATTING PURPOSES
    private final Integer FIRST_PAGE_ROW_LIMIT = 10;
    private final Integer OPTIONAL_LINES_DEFAULT = 3;

    public FM_Truck_Trip__c truckTripGeneral {get; set;}
    
    private List<FM_Truck_Trip__c> lFirstPageList;
    private List<FM_Truck_Trip__c> lRemainingList;

    private Integer iTotalCount;
    
    public String dv_date {get;set;}
    public String dv_status {get;set;}
    public String dv_shift {get;set;}
    public String dv_shiftday {get;set;}
    public String dv_unit {get;set;}

    public FM_TruckTripEmailComponentController()
    {

        //String sTruckTripGeneralId = ApexPages.currentPage().getParameters().get('id');
        
        dv_date = ApexPages.currentPage().getParameters().get('dv_date') != null ? ApexPages.currentPage().getParameters().get('dv_date') : null;
        dv_status = ApexPages.currentPage().getParameters().get('dv_status') != null ? ApexPages.currentPage().getParameters().get('dv_status') : null;
        dv_shift = ApexPages.currentPage().getParameters().get('dv_shift') != null ? ApexPages.currentPage().getParameters().get('dv_shift') : null;
        dv_shiftday = ApexPages.currentPage().getParameters().get('dv_shiftday') != null ? ApexPages.currentPage().getParameters().get('dv_shiftday') : null;
        dv_unit = ApexPages.currentPage().getParameters().get('dv_unit') != null ? ApexPages.currentPage().getParameters().get('dv_unit') : null;
		system.debug('dv_date:'+dv_date+'   dv_status:'+dv_status+'   dv_shift:'+dv_shift+'   dv_shiftday:'+dv_shiftday+'   dv_unit:'+dv_unit);
        
        String fileName;

        Date dateInstance = Date.valueOf(dv_date);
        Datetime runSheetDateTime = datetime.newInstance(dateInstance.year(), dateInstance.month(), dateInstance.day());
        
        if(dv_status == FM_Utilities.TRUCKLIST_STATUS_CARRYOVER || dv_status == FM_Utilities.TRUCKLIST_STATUS_CANCELLED)
        {
            fileName = 'TruckList_' + dv_status + ' ';
        }
        else
        {
            // i.e. TruckList_Dispatched TE110 Night 2015-07-08.pdf
            fileName = 'TruckList_Dispatched ';
            if(dv_unit != null){
                fileName += [select Name from Carrier_Unit__c where Id =: dv_unit limit 1].Name + ' ';
            }
            fileName += dv_shift + '-' + dv_shiftday;
        }
        //format('yyyy-MM-dd')
        
        BuildTruckTripData();

        if (lFirstPageList != null && lFirstPageList.size() > 0)
            fileName += lFirstPageList.get(0).Planned_Dispatch_Date__c;
        else fileName += FM_Utilities.getHaulDate(dv_shift, dv_shiftday, Date.valueOf(dv_date));
        fileName += '.pdf'; 
        Apexpages.currentPage().getHeaders().put('content-disposition', 'attachment; filename="' + fileName + '"');
    }

    private void BuildTruckTripData()
    {
        HOG_Settings__c oSettings = HOG_Settings__c.getInstance();
        Integer iExtraLineCount = 0;

        if(oSettings.Fluid_Truck_List_Blank_Lines__c < 1)
        {
            iExtraLineCount = 0;
        }
        else if(oSettings.Fluid_Truck_List_Blank_Lines__c != null)
        {
            iExtraLineCount = Integer.valueOf(oSettings.Fluid_Truck_List_Blank_Lines__c);
        }

        List<FM_Truck_Trip__c> lTruckTripData = new List<FM_Truck_Trip__c>();

        lFirstPageList = new List<FM_Truck_Trip__c>(); 
        lRemainingList = new List<FM_Truck_Trip__c>();

        Integer iCount = 0;
        Integer iExtras = 0;
	
        lTruckTripData = FM_TruckListCustomUtility.getTruckTripList(Date.valueOf(dv_date),dv_shift,dv_status,dv_unit,null);
        
        truckTripGeneral = lTruckTripData.isEmpty() ? new FM_Truck_Trip__c() : lTruckTripData.get(0);
        
        this.iTotalCount = lTruckTripData.size();

        for(FM_Truck_Trip__c oOneTripRecord : lTruckTripData)
        {
            if(iCount++ < FIRST_PAGE_ROW_LIMIT)
            {
                lFirstPageList.add(oOneTripRecord);
                System.debug('TruckTrip: ' + oOneTripRecord);
            }
            else
            {
                lRemainingList.add(oOneTripRecord);
            }
        }

        // ADD BLANK LINES PER USER STORY W-000259

        for(iExtras = 0; iExtras < iExtraLineCount; iExtras++)
        {
            if(iCount++ < FIRST_PAGE_ROW_LIMIT)
            {
                lFirstPageList.add(new FM_Truck_Trip__c());
            }
            else
            {
                lRemainingList.add(new FM_Truck_Trip__c());
            }
        }
    }

    public List<FM_Truck_Trip__c> lFirstPage
    {
        get
        {
            return this.lFirstPageList;
        }

        private set;
    }

    public List<FM_Truck_Trip__c> lNextPages
    {
        get
        {
            return this.lRemainingList;
        }

        private set;
    }

    public Integer getNextPagesSize()
    {
        return this.lRemainingList.size();
    }
    
    
    public FM_Truck_List_Comments__c oComments{
        get{
            if(oComments == null){
                String sGroupingSelection = truckTripGeneral.TL_Phone_Lookup__r.Grouping__c; //(oOneTruckList.Final_Grouping__c == 'DefaultTruckListGrouping' || oOneTruckList.Final_Grouping__c != '' || oOneTruckList.Final_Grouping__c != null) ? oOneTruckList.Phone_Lookup__r.Grouping__c : oOneTruckList.Final_Grouping__c;
                system.debug('comments:'+sGroupingSelection);
                
                List<FM_Truck_List_Comments__c> lComments = !String.isBlank(sGroupingSelection) ? [SELECT Id, Dispatch_Comments__c FROM FM_Truck_List_Comments__c WHERE Run_Sheet_Date__c <= :truckTripGeneral.Run_Sheet_Date__c AND Dispatch_Desk__c = :sGroupingSelection ORDER BY CreatedDate DESC, Dispatch_Desk__c DESC NULLS FIRST LIMIT 1] : [SELECT Id, Dispatch_Comments__c FROM FM_Truck_List_Comments__c WHERE Run_Sheet_Date__c <= :truckTripGeneral.Run_Sheet_Date__c AND Dispatch_Desk__c = null ORDER BY CreatedDate DESC, Dispatch_Desk__c DESC NULLS FIRST LIMIT 1];
                system.debug('lComments:'+lComments);
                
                oComments = new FM_Truck_List_Comments__c();
                if(lComments.size() == 1){
                    oComments = lComments[0];
                }
            }
            
            system.debug('oComments:'+oComments);
            return oComments;
        }
        private set;
    }

    public String getUpperStatus()
    {
        return (this.truckTripGeneral.Truck_Trip_Status__c != FM_Utilities.TRUCKTRIP_STATUS_CARRYOVER && this.truckTripGeneral.Truck_Trip_Status__c != FM_Utilities.TRUCKTRIP_STATUS_CANCELLED) ? Schema.SobjectType.FM_Truck_List__c.fields.Dispatched_By__c.label + ':' : this.truckTripGeneral.Truck_Trip_Status__c.toUpperCase() + ' by ';
    }
    

    public String getFormattedDate() {
        //Doing this because sometimes the planned dispatched date is null
        //System.debug('getFormattedDate->lFirstPageList[0].Run_Sheet_Date__c: ' + lFirstPageList[0].Run_Sheet_Date__c);
        Date dDate = (this.truckTripGeneral.Shift_Day__c == 'Today') ? this.truckTripGeneral.Run_Sheet_Date__c : 
                (this.truckTripGeneral.Shift_Day__c == 'Yesterday') ? 
                (this.truckTripGeneral.Run_Sheet_Date__c < Date.today()) ? this.truckTripGeneral.Run_Sheet_Date__c : this.truckTripGeneral.Run_Sheet_Date__c.addDays(-1) :
                this.truckTripGeneral.Run_Sheet_Date__c.addDays(1);
        DateTime dFormat = DateTime.newInstance(dDate.year(), dDate.month(), dDate.day());
        return dFormat.format('MMMM dd yyyy');
    }

}