/*------------------------------------------------------------
Author:        Yen Le
Company:       Thirdwave
Description:   Apex Scheduler to send SAP Team notification about "Open" Add Equipment Form, EquipmentDataUpdate, Equipment Transfer Fom

Test Class:    EquipmentInventoryFormNotificationTest

------------------------------------------------------------*/
global class EquipmentInventoryFormNotification implements Schedulable
{
    global void execute(SchedulableContext sc)
    {
    	
    	Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
       //'Equip_llyd_hog@huskyenergy.com'
        mail.setToAddresses(new List<String>{'Equip_llyd_hog@huskyenergy.com' });
        mail.setSubject('Open HOG Equipment Inventory Forms');
        String messageBody = '<html><body>Hi SAP Data Team, <br/>' ;
        
        messageBody += 'Please complete following "Opening" Inventory Forms: <br/>'    ;
    	List<Equipment_Correction_Form__c> correctionForms = [select Id, Name, CreatedBy.Name 
    	                   from Equipment_Correction_Form__c where Status__c = 'Open' order by Name];
    	if (correctionForms.size() > 0)
    	{ 
    		messageBody += '<p><strong>Equipment Data Updates: </strong></p> ';
    		messageBody += '<table>';
    		for(Equipment_Correction_Form__c f : correctionForms)
    		{                                                                                                                                                                                                                                                                                                                                                                                                                                                                   
    			messageBody += '<tr>';
    			messageBody += '<td  width="150">&#10021; <a href="'+ URL.getSalesforceBaseUrl().toExternalForm() + '/' + f.Id +'">'+ f.Name +'</a></td>';
    			messageBody += '<td width="150">'+ f.CreatedBy.Name + '</td>';
    		//	messageBody += '<td>'+ f.Reason_for_Discrepancy__c + '</td>';
    			messageBody += '</tr>';
    		} 
    		messageBody += '</table>';
    	} 
    	
    	List<Equipment_Transfer_Form__c> transferForms = [select Id, Name, Reason_for_Transfer_Additional_Details__c, CreatedBy.Name 
                   from Equipment_Transfer_Form__c where Status__c = 'Open' order by Name];
    	if (transferForms.size() > 0)
    	{
    		messageBody += '<p><strong>Equipment Transfer Requests: </strong></p> ';
            messageBody += '<table>';
            for(Equipment_Transfer_Form__c f : transferForms)
            {
                messageBody += '<tr>';
                messageBody += '<td  width="150">&#10021; <a href="'+ URL.getSalesforceBaseUrl().toExternalForm() + '/' + f.Id +'">'+ f.Name +'</a></td>';
                messageBody += '<td width="150">'+ f.CreatedBy.Name + '</td>';
                messageBody += '<td>' + f.Reason_for_Transfer_Additional_Details__c + '</td>';
                messageBody += '</tr>';
            }
            messageBody += '</table>';
    	}
    	
    	List<Equipment_Missing_Form__c> missingForms = [select Id, Name, CreatedBy.Name 
                                                    from Equipment_Missing_Form__c where Status__c = 'Open' order by Name];
        if (missingForms.size() > 0)
        {
            messageBody += '<p><strong>Add Equipment Requests: </strong></p> ';
            messageBody += '<table>';
            for(Equipment_Missing_Form__c f : missingForms)
            {
                messageBody += '<tr>';
                messageBody += '<td  width="150">&#10021; <a href="'+ URL.getSalesforceBaseUrl().toExternalForm() + '/' + f.Id +'">'+ f.Name +'</a></td>';
                messageBody += '<td width="150">'+ f.CreatedBy.Name + '</td>';
           //     messageBody += '<td>' + f.Reason_for_Discrepancy__c + '</td>';
                messageBody += '</tr>';
            }
            messageBody += '</table>';
        }
    	messageBody += '</body></html>';
    	mail.setHtmlBody(messageBody); 
      
    	if (correctionForms.size() > 0 || transferForms.size() > 0)
    	{
    		 Messaging.sendEmail(new Messaging.SingleEmailMessage[] { mail });
    	}
    	
    }
   
}