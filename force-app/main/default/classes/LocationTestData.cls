/*-----------------------------------------------------------------------------------------------
Author     : Rey Ocampo
Company    : Husky Energy
Description: A utility class creating test data for the Location__c and Well_Event__c
-------------------------------------------------------------------------------------------------*/
@isTest        
public class LocationTestData
{
    public static Location__c createLocation(String name, Id routeId, Id fieldId)
    {                
        Location__c results = new Location__c
            (           
                Name = name,
                Route__c = routeId, 
                Operating_Field_AMU__c = fieldId
            );            

        return results;
    }

    public static Location__c createLocation()
    {
        //Business Department
        Business_Department__c businessDept = new Business_Department__c(Name = 'Test Business Department');
        insert businessDept;

        //Operating District
        Operating_District__c district = new Operating_District__c(Name = 'Test Operating District',
                                                                   Business_Department__c = businessDept.Id);
        insert district;

        Field__c amu = new Field__c(Name = 'Test AMU',
                                    Operating_District__c = district.Id);
        insert amu;

        Location__c testLocation = new Location__c();
        testLocation.Name = 'TestLocation';
        testLocation.Operating_Field_AMU__c = amu.Id;
        insert testLocation;
        return testLocation;
    }
    
    public static Well_Event__c createEvent (String name, Id locationId)
    {                
        Well_Event__c results = new Well_Event__c
            (           
                Name = name,
                Well_Id__c = locationId
            );            

        return results;
    }

    public static Well_Event__c createEvent()
    {
        Location__c testLocation = createLocation();

        Well_Event__c testEvent = new Well_Event__c (Name = 'Test Well Event', Well_ID__c = testLocation.Id); 
        insert testEvent;
        return testEvent;
    }
    
    public static System__c createSystem(String name)
    {
        //Business Department
        Business_Department__c businessDept = new Business_Department__c(Name = 'Test Business Department Sys');
        insert businessDept;

        //Operating District
        Operating_District__c district = new Operating_District__c(Name = 'Test Operating District Sys',
                                                                   Business_Department__c = businessDept.Id);
        insert district;

        Field__c amu = new Field__c(Name = 'Test AMU Sys',
                                    Operating_District__c = district.Id);
        insert amu;

        System__c testSystem = new System__c();
        testSystem.Name = name;
        testSystem.Operating_Field_AMU__c = amu.Id;
        insert testSystem;
        return testSystem;
    }

     public static Functional_Equipment_Level__c createFEL(String name)
    {
        //Business Department
        Business_Department__c businessDept = new Business_Department__c(Name = 'Test Business Department FEL');
        insert businessDept;

        //Operating District
        Operating_District__c district = new Operating_District__c(Name = 'Test Operating District FEL',
                                                                   Business_Department__c = businessDept.Id);
        insert district;

        Field__c amu = new Field__c(Name = 'Test AMU FEL',
                                    Operating_District__c = district.Id);
        insert amu;

        Functional_Equipment_Level__c fel = new Functional_Equipment_Level__c();
        fel.Name = name;
        fel.Operating_Field_AMU__c = amu.Id;
        insert fel;
        return fel;
    }

     public static Yard__c createYard(String name)
    {
        //Business Department
        Business_Department__c businessDept = new Business_Department__c(Name = 'Test Business Department Yard');
        insert businessDept;

        //Operating District
        Operating_District__c district = new Operating_District__c(Name = 'Test Operating District Yard',
                                                                   Business_Department__c = businessDept.Id);
        insert district;

        Field__c amu = new Field__c(Name = 'Test AMU Yard',
                                    Operating_District__c = district.Id);
        insert amu;

        Yard__c yard = new Yard__c();
        yard.Name = name;
        yard.Operating_Field_AMU__c = amu.Id;
        insert yard;
        return yard;
    }
    
    public static Sub_System__c createSubSystem(String name)
    {

        Sub_System__c testSubSystem = new Sub_System__c();
        testSubSystem.Name = name;
        insert testSubSystem;
        return testSubSystem;
    }
}