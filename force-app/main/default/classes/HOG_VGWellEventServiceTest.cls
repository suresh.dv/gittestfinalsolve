/*-----------------------------------------------------------------------------------------------
Author     : Miro Zelina
Company    : Husky Energy
Description: A test class to test HOG_VGWellEventService
History    : 29-Mar-2018 - Created
-------------------------------------------------------------------------------------------------*/
@isTest 

private class HOG_VGWellEventServiceTest {
    
    static testMethod void testPushRecords(){
        
        HOG_VGWellEventService.APIVGWellEvent vgWellEvent_succ = new  HOG_VGWellEventService.APIVGWellEvent();
        HOG_VGWellEventService.APIVGWellEvent vgWellEvent_err1 = new  HOG_VGWellEventService.APIVGWellEvent();
        HOG_VGWellEventService.APIVGWellEvent vgWellEvent_err2 = new  HOG_VGWellEventService.APIVGWellEvent();
        HOG_VGWellEventService.APIVGWellEvent vgWellEvent_err3 = new  HOG_VGWellEventService.APIVGWellEvent();
        
        vgWellEvent_succ = HOG_VGWellEventServiceTestData.createAPIVGWellEvent('1000000000000',
                                                                               '0.50',
                                                                               '03/10/2018',
                                                                               '01/10/2018',
                                                                               '450',
                                                                               '300',
                                                                               'true',
                                                                               '2.50');
                                                                          
        vgWellEvent_err1 = HOG_VGWellEventServiceTestData.createAPIVGWellEvent('1000000000001', //upser failed check
                                                                               '0.50',
                                                                               '03/10/2018',
                                                                               '01/10/2018',
                                                                               '450',
                                                                               '300',
                                                                               'true',
                                                                               '2.50');
                                                                              
        vgWellEvent_err2 = HOG_VGWellEventServiceTestData.createAPIVGWellEvent('1000000000002', 
                                                                               '0.50',
                                                                               '03/10/2018',
                                                                               '01/10/2018',
                                                                               null, //error check missing input
                                                                               '300',
                                                                               'true',
                                                                               '2.50');  
                                                                               
        vgWellEvent_err2 = HOG_VGWellEventServiceTestData.createAPIVGWellEvent('1000000000003', 
                                                                               '0.50',
                                                                               '03/10/2018',
                                                                               '01/10/2018',
                                                                               'abc', //error check parsing error
                                                                               '300',
                                                                               'true',
                                                                               '2.50');                                                                         
                                                                              
        List<HOG_VGWellEventService.APIVGWellEvent> vgWellEventList = new List<HOG_VGWellEventService.APIVGWellEvent>{vgWellEvent_succ, 
                                                                                                                      vgWellEvent_err1,
                                                                                                                      vgWellEvent_err2,
                                                                                                                      vgWellEvent_err3};
                                                                                                                      
                                                                                                    
      
      
        Test.startTest();
            
            System.assertEquals(2, [SELECT Id FROM Well_Event__c].size()); //check for events in db before push
            System.assertEquals(null , [SELECT PVR_GOR_Factor__c FROM Well_Event__c WHERE PVR_UWI_RAW__c = '1000000000000'].PVR_GOR_Factor__c); 
            System.assertEquals(null, [SELECT Measured_Vent_Rate__c FROM Well_Event__c WHERE PVR_UWI_RAW__c = '1000000000000'].Measured_Vent_Rate__c); 
            System.assertEquals(null, [SELECT PVR_Fuel_Consumption__c FROM Well_Event__c WHERE PVR_UWI_RAW__c = '1000000000000'].PVR_Fuel_Consumption__c); 
            System.assertEquals(null, [SELECT Monthly_Trucked_Oil__c FROM Well_Event__c WHERE PVR_UWI_RAW__c = '1000000000000'].Monthly_Trucked_Oil__c);
            System.assertEquals(null, [SELECT GOR_Test_Date__c FROM Well_Event__c WHERE PVR_UWI_RAW__c = '1000000000000'].GOR_Test_Date__c);
            System.assertEquals(null, [SELECT GOR_Effective_Date__c FROM Well_Event__c WHERE PVR_UWI_RAW__c = '1000000000000'].GOR_Effective_Date__c); 
            System.assertEquals(false, [SELECT Single_Well_Battery__c FROM Well_Event__c WHERE PVR_UWI_RAW__c = '1000000000000'].Single_Well_Battery__c); 

            HOG_VGApiUtils.Response responses = HOG_VGWellEventService.pushRecords(vgWellEventList);

            System.assertEquals(2, [SELECT Id FROM Well_Event__c].size()); //check for events in db after push
            System.assertEquals(decimal.valueOf(vgWellEvent_succ.PVR_GOR_Factor), [SELECT PVR_GOR_Factor__c FROM Well_Event__c WHERE PVR_UWI_RAW__c = '1000000000000'].PVR_GOR_Factor__c);
            System.assertEquals(decimal.valueOf(vgWellEvent_succ.Measured_Vent_Rate), [SELECT Measured_Vent_Rate__c FROM Well_Event__c WHERE PVR_UWI_RAW__c = '1000000000000'].Measured_Vent_Rate__c); 
            System.assertEquals(decimal.valueOf(vgWellEvent_succ.PVR_Fuel_Consumption), [SELECT PVR_Fuel_Consumption__c FROM Well_Event__c WHERE PVR_UWI_RAW__c = '1000000000000'].PVR_Fuel_Consumption__c); 
            System.assertEquals(decimal.valueOf(vgWellEvent_succ.Monthly_Trucked_Oil), [SELECT Monthly_Trucked_Oil__c FROM Well_Event__c WHERE PVR_UWI_RAW__c = '1000000000000'].Monthly_Trucked_Oil__c); 
            System.assertEquals(date.parse(vgWellEvent_succ.GOR_Test_Date), [SELECT GOR_Test_Date__c FROM Well_Event__c WHERE PVR_UWI_RAW__c = '1000000000000'].GOR_Test_Date__c); 
            System.assertEquals(date.parse(vgWellEvent_succ.GOR_Effective_Date), [SELECT GOR_Effective_Date__c FROM Well_Event__c WHERE PVR_UWI_RAW__c = '1000000000000'].GOR_Effective_Date__c); 
            System.assertEquals(boolean.valueOf(vgWellEvent_succ.Single_Well_Battery), [SELECT Single_Well_Battery__c FROM Well_Event__c WHERE PVR_UWI_RAW__c = '1000000000000'].Single_Well_Battery__c); 
        
            for (HOG_VGApiUtils.Result result : responses.results){
                
                if (result.id == '1000000000000'){
                    System.assertEquals(HOG_VGApiUtils.STATUS_SUCCESS_UPSERT,result.statusCode);    
                }
                if (result.id == '1000000000001'){
                    System.assertEquals(HOG_VGApiUtils.STATUS_ERROR_UPSERT,result.statusCode); 
                }
                if (result.id == '1000000000002'){
                    System.assertEquals(HOG_VGApiUtils.STATUS_ERROR_PARSE,result.statusCode); 
                }
                if (result.id == '1000000000003'){
                    System.assertEquals(HOG_VGApiUtils.STATUS_ERROR_PARSE,result.statusCode); 
                }
            }
            
            
        Test.stopTest();    
    }
    
    
    @testSetup 
    static void setupData() {

        Location__c location = LocationTestData.createLocation();  
        Well_Event__c wellEvent1 = LocationTestData.createEvent('Test Well Event 1', location.Id);
        wellEvent1.PVR_UWI_RAW__c = '1000000000000';
        insert wellEvent1;
        
        //update wellEvent1;
        Well_Event__c wellEvent2 = LocationTestData.createEvent('Test Well Event 2', location.Id);
        wellEvent2.PVR_UWI_RAW__c = '1000000000002';
        insert wellEvent2;
    }
    
}