/*-----------------------------------------------------------------------------------------------
Author     : Rey Ocampo
Company    : Husky Energy
Description: A utility class creating test data for the Well_Tracker__c
-------------------------------------------------------------------------------------------------*/
@isTest                
public class WellTrackerTestData
{
    public static Well_Tracker__c createWellTracker
    (
    	Id locationId, 
    	Id facilityId,
    	Id fieldId, 
    	Decimal oilProdM3Day,
    	DateTime wellDown, 
    	String wellStatus,
    	String wellStatusReason
    )
    {
        Well_Tracker__c results = new Well_Tracker__c
            (
                Location__c = locationId,
                Facility__c = facilityId, 
                Control_Centre__c = fieldId,
                Oil_Prod_m3_day__c = oilProdM3Day,
                Well_down__c = wellDown,
                Well_Status__c = wellStatus,
                Well_Status_Reason__c = wellStatusReason
            );

        return results;
    }
}