@isTest
public class SEP_CommonTaskTriggerTest{

    /* 
      Gangha Kaliyan on 12Sep14
      To test Populate_Fields_On_Task trigger
    */
    
    static testMethod void testTaskFields()
    {
        
        //create user
        Profile PMOUser = [select Id, Name from Profile where Name='Standard Downstream User'];
       User testUser = createTestUser('Gangha2','Kaliyan1','gk@gk2.com','gk@gk2.com',PMOUser.ID);
       insert testUser;
       // User testUser=[select id from user where profileid=:PMOUser.id and isactive=TRUE limit 1];
       // insert testUser;
        //create business department
        Business_Department__c bd = createBusDept('testBusDept');
        insert bd;
        
        //create users (custom object) linking user and business department
        Junction_User_BusinessDepartment__c users = createUsers(bd.Id, testUser.Id);
        insert users;
        
        //create business unit
        Business_Unit__c bu = createBusUnit('testBusUnit');
        insert bu;
        
        //create project
        Milestone1_Project__c mp = createProject('testProj', bu.Id);
        insert mp;
        
        //create task with user and project
        Common_Task__c ct1 = createTask(testUser.Id, mp.Id, date.today(), date.today(), 150.00, 10, 'Is this effective', 'Was not Done');
        //create task without a user and project 
        Common_Task__c ct2 = createTask(null, null, date.today(), date.today(), 250.11, 20, 'Is this effective-t2', 'Was not Done-t2');

        List<Common_Task__c> ctList = new List<Common_Task__c>{ct1, ct2};
        insert ctList;

        //query for the updated task by trigger
        List<Common_Task__c> ctAfterUpdateList = [Select Id, Name, User__c, Project_Name__c, Business_Unit__c, Business_Department__c from Common_Task__c where Id=:ct1.Id or Id=:ct2.Id];
        
        for(Common_Task__c ct:ctAfterUpdateList)
        {
            if(ct.User__c!=null && ct.Project_Name__c!=null)
            {
                //assert statements
                //check if business unit and business department are populated
                System.assertEquals(bu.Id, ct.Business_Unit__c);
                System.assertEquals(bd.Id, ct.Business_Department__c);
            }
            else
            {
                //if there is no user and project given while creating task
                //then bus unit and bus dept should be null
                System.assertEquals(null, ct.Business_Unit__c);
                System.assertEquals(null, ct.Business_Department__c);
            }
        }  
    }
    
    // Some of the fields necessary to create a User account
    static User DummyUser 
    {
        get {
            if (DummyUser==null) {
                DummyUser = [select id, TimeZoneSidKey, LocaleSidKey, 
                    EmailEncodingKey, ProfileId, LanguageLocaleKey
                    from User limit 1];
            }
            return DummyUser ;
        } set ;
    }

    // Creates a Test User with a given Profile
    static User createTestUser(String firstName,String lastName,String email,String userName,Id profileId) 
    {
        return new User(
            FirstName = firstName,
            LastName = lastName,
            Email = email,
            Username = userName,
            ProfileId = profileId,
            Alias = lastName.substring(0,5),
            CommunityNickname = lastName.substring(0,5),
            TimeZoneSidKey=DummyUser.TimeZoneSidKey,
            LocaleSidKey=DummyUser.LocaleSidKey,
            EmailEncodingKey=DummyUser.EmailEncodingKey,
            LanguageLocaleKey=DummyUser.LanguageLocaleKey
        );
    }    
    
    //to create business department record
    static Business_Department__c createBusDept(String name)
    {
        Business_Department__c bd = new Business_Department__c();
        bd.Name = name;
        return bd;
    }
    
    //to create users (custom) record 
    static Junction_User_BusinessDepartment__c createUsers(Id bdId, Id testUserId)
    {
        Junction_User_BusinessDepartment__c users = new Junction_User_BusinessDepartment__c();
        users.Business_Department__c = bdId;
        users.User__c = testUserId;
        return users;
    }
    
    //to create business unit
    static Business_Unit__c createBusUnit(String name)
    {
        Business_Unit__c bu = new Business_Unit__c();
        bu.Name = name;
        return bu;
    }
    
    //to create project
    static Milestone1_Project__c createProject(String name, Id buId)
    {
        Milestone1_Project__c mp = new Milestone1_Project__c();
        mp.Name = name;
        mp.Business_Unit__c = buId;
        
        //start of recordType query
        Schema.DescribeSObjectResult d = Schema.SObjectType.Milestone1_Project__c;
        Map<String,Schema.RecordTypeInfo> rtMapByName = d.getRecordTypeInfosByName();
        Schema.RecordTypeInfo rtByName =  rtMapByName.get('SEP - Project Services');
        Id rtId = rtByName.getRecordTypeId();
        //end of recordType query
        
        mp.RecordTypeId = rtId;
        return mp;
    }
    
    //to create common task
    static Common_Task__c createTask(Id userId, Id projectId, date start, date estEnd, Decimal valAmount, Integer hrsSpent, String isEffective, String wasDone)
    {
        Common_Task__c ct = new Common_Task__c();
        ct.User__c = userId;
        ct.Project_Name__c = projectId;
        ct.Start__c = start;
        ct.Estimated_End__c = estEnd;
        ct.Hours_Spent__c = hrsSpent;
        ct.Value_Amount__c = valAmount;        
        ct.How_was_this_effective__c = isEffective;
        ct.Notes__c = wasDone;
        //record type is assigned to SEP - Project Services
        ct.RecordTypeId = Schema.SObjectType.Common_Task__c.getRecordTypeInfosByName().get('SEP - Project Services').getRecordTypeId();
        return ct;
    }
}