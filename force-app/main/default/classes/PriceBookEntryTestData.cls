public class PriceBookEntryTestData {

    public static List<PriceBookEntry> createPriceBookEntry(List<Product2> productList) {
        List<PriceBookEntry> priceBookEntryList = new List<PriceBookEntry>();
        
        List<PriceBook2> standardPriceBook = [SELECT Id FROM PriceBook2 WHERE IsStandard = True];
        
        for(Product2 product : productList) {
            PriceBookEntry priceBookEntry = new PriceBookEntry();
            priceBookEntry.Pricebook2Id = standardPriceBook[0].Id;
            priceBookEntry.Product2Id = product.Id;
            priceBookEntry.UnitPrice = 0;
            priceBookEntry.IsActive = True;
            
            priceBookEntryList.add(priceBookEntry);
        }
        
        return priceBookEntryList;
    }
}