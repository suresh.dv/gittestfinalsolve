@isTest(SeeAllData=true)
public class PTBillingRateCheckTestClass {
    private static testmethod void initializeReport(){        
        PTTestData.createPTTestRecords();
        PTTestData.testBillingRateCheck();
        PTbillingRateCheck billingRateCheck = new PTbillingRateCheck();
        test.startTest();
        PTBillingRateCheckCacheQuery.infoQueried = false;
        billingRateCheck.initializeReport();
        test.stopTest();
    }
    private static testmethod void allNull(){
        
        PTTestData.createPTTestRecords();
		PTTestData.testBillingRateCheck(); 
        PTbillingRateCheck billingRateCheck = new PTbillingRateCheck();         
    	billingRateCheck.dateFromString = null;
        billingRateCheck.dateToString = null;
        billingRateCheck.checkedFlag = true;
        test.startTest();
        PTBillingRateCheckCacheQuery.infoQueried = false;
        billingRateCheck.checkBillingRate();
        test.stopTest();
    }
    
    private static testmethod void dateFromNull(){
        
        PTTestData.createPTTestRecords();
        PTTestData.testBillingRateCheck(); 
        PTbillingRateCheck billingRateCheck = new PTbillingRateCheck();            
    	billingRateCheck.dateFromString = null;
        billingRateCheck.dateToString = '01/01/2016';
        billingRateCheck.checkedFlag = true;
        test.startTest();
        PTBillingRateCheckCacheQuery.infoQueried = false;
        billingRateCheck.checkBillingRate();
        test.stopTest();
    }
    
    private static testmethod void dateToNull(){
        PTTestData.createPTTestRecords();
        PTTestData.testBillingRateCheck();        
        PTbillingRateCheck billingRateCheck = new PTbillingRateCheck();
    	billingRateCheck.dateFromString = '01/01/2016';
        billingRateCheck.dateToString = null;
		billingRateCheck.checkedFlag = true;
        test.startTest();
        PTBillingRateCheckCacheQuery.infoQueried = false;
        billingRateCheck.checkBillingRate();
        test.stopTest();
    }
    
    private static testmethod void allFilled(){
        
        PTTestData.createPTTestRecords();
		PTTestData.testBillingRateCheck();                     
        PTbillingRateCheck billingRateCheck = new PTbillingRateCheck();
    	billingRateCheck.dateFromString = '01/01/2016';
        billingRateCheck.dateToString = '01/03/2016';
        billingRateCheck.checkedFlag = true;
        test.startTest();
        PTBillingRateCheckCacheQuery.infoQueried = false;
        billingRateCheck.checkBillingRate();
        test.stopTest();
    }
}