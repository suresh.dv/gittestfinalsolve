@isTest
private class DEFT_EngineeringDiagnosisControllerXTest {
	
	@isTest static void testEntireController() {
		User runningUser = DEFT_TestData.createDeftUser();
        //User productionEnegineer = DEFT_TestData.createProductionEnegineer();
		User productionCoordinator = DEFT_TestData.createProductionCoordinator();
		User serviceRigCoordinator = DEFT_TestData.createServiceRigCoordinator();
		User serviceRigPlanner = DEFT_TestData.createServiceRigPlanner();

		HOG_Service_Rig_Program__c serviceRigProgram;
		Approval.ProcessResult result;

		System.runAs(serviceRigPlanner)
		{
			serviceRigProgram = DEFT_TestData.createServiceRigProgram(runningUser, productionCoordinator, serviceRigCoordinator);
			serviceRigProgram.Service_Rig_Planner__c = serviceRigPlanner.Id;
			serviceRigProgram.Status__c = 'Started';
            serviceRigProgram.Budget__c = 49000;
			update serviceRigProgram;

			system.debug ( 'Technical_Approver__c ' + serviceRigProgram.Technical_Approver__c );
            system.debug ( 'First_Financial_Approver__c ' + serviceRigProgram.First_Financial_Approver__c );

			//Submit the approval request
			Approval.ProcessSubmitRequest req = new Approval.ProcessSubmitRequest();
            req.setObjectId(serviceRigProgram.Id);
            req.setProcessDefinitionNameOrId('Service_Rig_Program_Approval_Process_1');
            req.setSkipEntryCriteria(true);
            result = Approval.process(req);

            System.debug('Approval Result 1' + result);
        }
            

		System.runAs(serviceRigPlanner) 
		{
			List<Id> newWorkItemIds = result.getNewWorkitemIds();
            system.debug ('serviceRigCoordinator newWorkItemIds ' +newWorkItemIds );

            Approval.ProcessWorkitemRequest req2 = new Approval.ProcessWorkitemRequest();
            req2.setComments('Approving request.');
            req2.setAction('Approve');
            req2.setWorkitemId(newWorkItemIds.get(0));
			result =  Approval.process(req2);
            System.debug('Approval Result 2' + result);
		}

		System.runAs(serviceRigPlanner) 
		{
			List<Id> newWorkItemIds = result.getNewWorkitemIds();
            system.debug ('productionCoordinator newWorkItemIds ' +newWorkItemIds );

            Approval.ProcessWorkitemRequest req2 = new Approval.ProcessWorkitemRequest();
            req2.setComments('Approving request.');
            req2.setAction('Approve');
            req2.setWorkitemId(newWorkItemIds.get(0));
			result =  Approval.process(req2);
            System.debug('Approval Result 3' + result);

		}

		System.runAs(runningUser) 
		{
            System.debug('User and srp ' + serviceRigProgram.Status__c );
			HOG_EOJ__c eoj = new HOG_EOJ__c();
            eoj.Service_Rig_Program__c = serviceRigProgram.Id;
            eoj.Service_Started__c = Date.today().addDays(-5); //service started 5 days ago
            eoj.Service_Completed__c = Date.today();
            eoj.Final_Cost__c = 5000;
            eoj.Service_General__c  = 'Downhole Suspension - BD13';
            eoj.Service_Detail__c = 'Bridge Plug - CPB4';
			eoj.Status__c = 'Submitted';
            insert eoj;
            
			HOG_Engineering_Diagnosis__c engDiag = new HOG_Engineering_Diagnosis__c();
			engDiag.Service_Rig_Program__c = serviceRigProgram.Id;
			insert engDiag;

			Test.startTest();

			ApexPages.StandardController stdController = new ApexPages.StandardController(engDiag);
			DEFT_EngineeringDiagnosisControllerX con = new DEFT_EngineeringDiagnosisControllerX(stdController);
			con.retrievePickListValues('HOG_Engineering_Diagnosis__c', 'Diagnosis_General__c');
            Boolean displayForm = con.displayForm;
			Boolean generalPicklistValues  = con.generalPicklistsMatch();
            Boolean detailPicklistValues = con.detailPicklistMatch();
            con.initializeEDPicklists();
            

            
            
            //If values for picklist 
            //HOG_EOJ__c.Service_General__c doesnt match with 
            //HOG_Engineering_Diagnosis__c.Diagnosis_General__c it will fail
            //This is because we are expecting theese two field to have same values because
            //we are pre setting the Diagnosis_General__c from Engineering diagnosis 
            //with values from HOG_EOJ__c.Service_General__c
            //and same if for these two picklists Service_Detail__c and Diagnosis_Detail__c
            System.assertEquals(true, generalPicklistValues);
            System.assertEquals(true, detailPicklistValues);
			
            engDiag.Status__c = 'Complete';
            con.saveED();
            
            Test.stopTest();  

		}
        
        
    }
    
	@isTest static void testErrorMessages() {
            User runningUser = DEFT_TestData.createDeftUser();
            //User productionEnegineer = DEFT_TestData.createProductionEnegineer();
            User productionCoordinator = DEFT_TestData.createProductionCoordinator();
            User serviceRigCoordinator = DEFT_TestData.createServiceRigCoordinator();
            User serviceRigPlanner = DEFT_TestData.createServiceRigPlanner();
        	HOG_Service_Rig_Program__c serviceRigProgram;
        	Approval.ProcessResult result;
        	
        System.runAs(serviceRigPlanner) 
		{
			serviceRigProgram = DEFT_TestData.createServiceRigProgram(runningUser, productionCoordinator, serviceRigCoordinator);
			serviceRigProgram.Service_Rig_Planner__c = serviceRigPlanner.Id;
			serviceRigProgram.Status__c = 'Started';
			update serviceRigProgram;

			system.debug ( serviceRigProgram );

			//Submit the approval request
			Approval.ProcessSubmitRequest req = new Approval.ProcessSubmitRequest();
            req.setObjectId(serviceRigProgram.Id);
            req.setProcessDefinitionNameOrId('Service_Rig_Program_Approval_Process_1');
            req.setSkipEntryCriteria(true);
            result = Approval.process(req);
        }
            

		System.runAs(serviceRigPlanner) 
		{
			List<Id> newWorkItemIds = result.getNewWorkitemIds();

            Approval.ProcessWorkitemRequest req2 = new Approval.ProcessWorkitemRequest();
            req2.setComments('Approving request.');
            req2.setAction('Approve');
            req2.setWorkitemId(newWorkItemIds.get(0));
			result =  Approval.process(req2);
		}

		System.runAs(serviceRigPlanner) 
		{
			List<Id> newWorkItemIds = result.getNewWorkitemIds();

            Approval.ProcessWorkitemRequest req2 = new Approval.ProcessWorkitemRequest();
            req2.setComments('Approving request.');
            req2.setAction('Approve');
            req2.setWorkitemId(newWorkItemIds.get(0));
			result =  Approval.process(req2);

		}
        
        System.runAs(runningUser) 
        {
            //This will force fist message because engDiag will not have Service_Rig_Program__c field prepopulated
            HOG_Engineering_Diagnosis__c engDiag = new HOG_Engineering_Diagnosis__c();
            
            HOG_EOJ__c eoj = new HOG_EOJ__c();
            eoj.Service_Rig_Program__c = serviceRigProgram.Id;
            eoj.Service_Started__c = Date.today().addDays(-5); //service started 5 days ago
            eoj.Service_Completed__c = Date.today();
            eoj.Final_Cost__c = 5000;
            eoj.Service_General__c  = 'Downhole Suspension - BD13';
            eoj.Service_Detail__c = 'Bridge Plug - CPB4';
            eoj.Status__c = 'New';
            insert eoj;
           
            Test.startTest();
                ApexPages.StandardController stdController = new ApexPages.StandardController(engDiag);
                DEFT_EngineeringDiagnosisControllerX con = new DEFT_EngineeringDiagnosisControllerX(stdController);
            	
            	//This will force second message, because user is 
            	//trying to create Engineering Diagnosis on SRP that has no EOJ with status Submitted
            	con.initializeEndOfJobReport();
            Test.stopTest();  

        }
       
	}

    @isTest static void testPVRServiceCallout() {
        User runningUser = DEFT_TestData.createDeftUser();
        //User productionEnegineer = DEFT_TestData.createProductionEnegineer();
        User productionCoordinator = DEFT_TestData.createProductionCoordinator();
        User serviceRigCoordinator = DEFT_TestData.createServiceRigCoordinator();
        User serviceRigPlanner = DEFT_TestData.createServiceRigPlanner();

        HOG_Service_Rig_Program__c serviceRigProgram;
        Approval.ProcessResult result;
        List<Id> newWorkItemIds;

        System.runAs(serviceRigPlanner) {
            //Data Setup - Step 1 (Create Service Rig Program)
            serviceRigProgram = DEFT_TestData.createServiceRigProgram(runningUser, productionCoordinator, serviceRigCoordinator);
            serviceRigProgram.Service_Rig_Planner__c = serviceRigPlanner.Id;
            serviceRigProgram.Status__c = 'Started';
            update serviceRigProgram;

            //Submit the approval request
            Approval.ProcessSubmitRequest req = new Approval.ProcessSubmitRequest();
            req.setObjectId(serviceRigProgram.Id);
            req.setProcessDefinitionNameOrId('Service_Rig_Program_Approval_Process_1');
            req.setSkipEntryCriteria(true);
            result = Approval.process(req);
        }

        System.runAs(serviceRigPlanner) {     
            newWorkItemIds = result.getNewWorkitemIds();
            Approval.ProcessWorkitemRequest req = new Approval.ProcessWorkitemRequest();
            req.setComments('Approving request.');
            req.setAction('Approve');
            req.setWorkitemId(newWorkItemIds.get(0));
            result =  Approval.process(req);
        }

        System.runAs(serviceRigPlanner) {
            newWorkItemIds = result.getNewWorkitemIds();
            Approval.ProcessWorkitemRequest req = new Approval.ProcessWorkitemRequest();
            req.setComments('Approving request.');
            req.setAction('Approve');
            req.setWorkitemId(newWorkItemIds.get(0));
            result =  Approval.process(req);
        }
        
        System.runAs(runningUser) {
            //Create Custom Setting Data
            HOG_Settings__c hogCustomSettings = new HOG_Settings__c();
            hogCustomSettings.SAP_DPVR_Endpoint__c = 'https://test.pvrservice.com';
            hogCustomSettings.SAP_DPVR_Timeout__c = 120000;
            hogCustomSettings.Client_Certificate_Name__c = 'HuskyCertificate';
            insert hogCustomSettings;

            HOG_EOJ__c eoj = new HOG_EOJ__c();
            eoj.Service_Rig_Program__c = serviceRigProgram.Id;
            eoj.Previous_Service_Date__c = Date.today().addDays(-10);
            eoj.Service_Started__c = Date.today().addDays(-5);//service started 5 days ago
            eoj.Service_Completed__c = Date.today();
            eoj.Final_Cost__c = 5000;
            eoj.Service_General__c  = 'Downhole Suspension - BD13';
            eoj.Service_Detail__c = 'Bridge Plug - CPB4';
            eoj.Status__c = 'Submitted';
            insert eoj;
            
            HOG_Engineering_Diagnosis__c engDiag = new HOG_Engineering_Diagnosis__c();
            engDiag.Service_Rig_Program__c = serviceRigProgram.Id;
            insert engDiag;

            Test.startTest();
            PageReference viewPage = Page.DEFT_EngineeringDiagnosisView;
            viewPage.getParameters().put('id', engDiag.Id);
            Test.setCurrentPage(viewPage);

            ApexPages.StandardController stdController = new ApexPages.StandardController(engDiag);
            DEFT_EngineeringDiagnosisControllerX con = new DEFT_EngineeringDiagnosisControllerX(stdController);
            System.assertEquals(eoj.Id, con.endOfJobReport.Id);
            System.assertEquals(engDiag.Id, con.eDiagnosis.Id);

            DEFT_Utilities.PVRProductionValuesResponse response = new DEFT_Utilities.PVRProductionValuesResponse();
            response.producedOilBetweenServices = 50.54;
            response.producedWaterBetweenServices = 65.45;
            response.producedGasBetweenServices = 34.45;
            response.producedSandBetweenServices = 45.22;
            response.operatingHoursBetweenServices = 72;

            //Test Webservice Invalid Reponse
            response.pvrUwiRaw = 'invalidPVRRawUWI';
            Test.setMock(HttpCalloutMock.class, new DEFT_PVRProductionServiceCalloutMock(200, 'OK', JSON.serialize(response), 
                new Map<String, String>{'Content-Type' => 'application/json'}));
            System.assertEquals(null, con.updatePVRProductionValues());
            System.assert(wasMessageAdded(
                new ApexPages.Message(ApexPages.severity.info, 
              'Invalid response from PVR Service'),
                ApexPages.getMessages()
            ));

            //TEST Webservice Error Response (StatusCode <> 200)
            Test.setMock(HttpCalloutMock.class, new DEFT_PVRProductionServiceCalloutMock(404, 'Not Found', JSON.serialize(response), 
                new Map<String, String>()));
            System.assertEquals(null, con.updatePVRProductionValues());
            System.assert(wasMessageAdded(
                new ApexPages.Message(ApexPages.severity.error, 
                'Received following error code from PVR service: ' + 404),
                ApexPages.getMessages()
            ));
            
            //Test Webservice Successful Response
            response.pvrUwiRaw = con.endOfJobReport.Well_Event_PVR_UWI_RAW__c;
            Test.setMock(HttpCalloutMock.class, new DEFT_PVRProductionServiceCalloutMock(200, 'OK', JSON.serialize(response), 
                new Map<String, String>{'Content-Type' => 'application/json'}));
            System.assertEquals(null, con.updatePVRProductionValues());
            engDiag = [Select Id, Name, Total_Oil_Production_Between_Services__c, Total_Water_Production_Between_Services__c,
                        Total_Gas_Production_Between_Services__c, Total_Sand_Production_Between_Services__c, PVR_Runtime__c
                       From HOG_Engineering_Diagnosis__c
                       Where Id =: engDiag.Id];
            System.assertEquals(response.producedOilBetweenServices, engDiag.Total_Oil_Production_Between_Services__c);
            System.assertEquals(response.producedWaterBetweenServices, engDiag.Total_Water_Production_Between_Services__c);
            System.assertEquals(response.producedGasBetweenServices, engDiag.Total_Gas_Production_Between_Services__c);
            System.assertEquals(response.producedSandBetweenServices, engDiag.Total_Sand_Production_Between_Services__c);
            Test.stopTest();
        }
    }

    private static Boolean wasMessageAdded(ApexPages.Message message, ApexPages.Message[] pageMessages) {
        Boolean messageFound = false;

        for(ApexPages.Message msg : pageMessages) {
            if(msg.getSummary() == message.getSummary()
                && msg.getDetail() == message.getDetail()
                && msg.getSeverity() == message.getSeverity()) {
                messageFound = true;        
            }
        }

        return messageFound;
    }
    
}