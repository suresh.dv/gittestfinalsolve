public class LOM_TestData {

  public static LOM_Daily_Fuel_Sale__c createDailyFuelSale(Id retailLocationId, Date dt, String grade, decimal totalVol ) {
  
    LOM_Daily_Fuel_Sale__c testData = new LOM_Daily_Fuel_Sale__c();
   
    testData.Retail_Location__c = retailLocationId;
    testData.Date__c = dt;
    testData.Grade__c = grade;
    testData.Total_Vol__c = totalVol;
    return testData;
  }
  public static LOM_Department_Sale__c createDepartmentSale(Id retailLocationId, Date dt, String code, Decimal sales ) {
    LOM_Department_Sale__c testData = new LOM_Department_Sale__c();
   
    testData.Retail_Location__c = retailLocationId;
    testData.Date__c = dt;
    testData.Code__c = code;
    testData.Sales__c = sales;
    
    return testData;
  }
  public static LOM_Invoice__c createInvoice(Id retailLocationId, Date dt, Decimal amount, Date paidOfDate ) {
  
    LOM_Invoice__c testData = new LOM_Invoice__c();
   
    testData.Retail_Location__c = retailLocationId;
    testData.Amount__c = amount;
    testData.Paid_Of_Date__c = paidOfDate;
    testData.Date__c = dt;
    
    return testData;
  }
}