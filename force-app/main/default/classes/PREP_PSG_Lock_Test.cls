@isTest(SeeAllData=true)
private class PREP_PSG_Lock_Test {

    static testMethod void testLockRecord() {
        Profile p = [SELECT Id FROM Profile WHERE Name='Standard SEP User (Platform)']; 
        
        User u = new User(Alias = 'standt', Email='PREP@testorg.com', 
            EmailEncodingKey='UTF-8', LastName='Testing', LanguageLocaleKey='en_US', 
            LocaleSidKey='en_US', ProfileId = p.Id, 
            TimeZoneSidKey='America/Los_Angeles', UserName='PREP@testorg.com');
            
        /*User u2 = new User(Alias = 'standt2', Email='PREP2@testorg.com', 
            EmailEncodingKey='UTF-8', LastName='Testing2', LanguageLocaleKey='en_US', 
            LocaleSidKey='en_US', ProfileId = p.Id, 
            TimeZoneSidKey='America/Los_Angeles', UserName='PREP2@testorg.com');*/
            
            
        insert u;
        //insert u2;
        
        System.runAs(u) {
            PREP_Discipline__c disp = new PREP_Discipline__c();
            disp.Name = 'Disp 1';
            disp.Active__c = true;
            disp.SCM_Manager__c = u.Id;
            
            insert disp;
            
            PREP_Discipline__c disp2 = new PREP_Discipline__c();
            disp2.Name = 'Atlantic MM';
            disp2.Active__c = true;
            disp2.SCM_Manager__c = u.Id;
            
            insert disp2;
            
            
            Id bvsRecordTypeId = Schema.SObjectType.PREP_Business_Value_Submission__c.getRecordTypeInfosByName().get('PS&G/MM Business Value Submission').getRecordTypeId();        
            
            PREP_Business_Value_Submission__c BVS = new PREP_Business_Value_Submission__c();
            BVS.Discipline__c = disp.Id;
            BVS.SCM_Manager__c = u.Id;
            BVS.Specialist_Rep__c = u.Id;
            BVS.RecordTypeId = bvsRecordTypeId;
            BVS.Value_Created__c = 40000;
            BVS.Year__c = '2015';
            BVS.Savings_Month__c = 'April';
            BVS.Comments__c = 'Test';
            BVS.Awarded_Spend__c = 100;
            BVS.Local_Awarded_Spend__c = 100;
            
            insert BVS;    
        
        
            BVS.Awarded_Spend__c = 200;
            BVS.Local_Awarded_Spend__c = 200;            
        }
        
        PermissionSet ps = [SELECT ID From PermissionSet WHERE Name = 'PREP_Initiative_Admin'];
        insert new PermissionSetAssignment(AssigneeId = u.id, PermissionSetId = ps.Id );

        System.runAs(u) {
            /*PREP_Discipline__c disp = new PREP_Discipline__c();
            disp.Name = 'Disp 2';
            disp.Active__c = true;
            disp.SCM_Manager__c = u.Id;
            
            insert disp;
            
            PREP_Discipline__c disp2 = new PREP_Discipline__c();
            disp2.Name = 'Atlantic MM';
            disp2.Active__c = true;
            disp2.SCM_Manager__c = u.Id;
            
            insert disp2;
            
            
            Id bvsRecordTypeId = Schema.SObjectType.PREP_Business_Value_Submission__c.getRecordTypeInfosByName().get('PS&G/MM Business Value Submission').getRecordTypeId();        
            
            PREP_Business_Value_Submission__c BVS = new PREP_Business_Value_Submission__c();
            BVS.Discipline__c = disp.Id;
            BVS.SCM_Manager__c = u.Id;
            BVS.Specialist_Rep__c = u.Id;
            BVS.RecordTypeId = bvsRecordTypeId;
            BVS.Value_Created__c = 40000;
            BVS.Year__c = '2015';
            BVS.Savings_Month__c = 'April';
            BVS.Comments__c = 'Test';
            BVS.Awarded_Spend__c = 100;
            BVS.Local_Awarded_Spend__c = 100;
            
            insert BVS;  
            
            BVS.Status__c = 'Approved';*/

            PREP_Discipline__c disp = new PREP_Discipline__c();
            disp.Name = 'Disp 1';
            disp.Active__c = true;
            disp.SCM_Manager__c = u.Id;
            
            insert disp;
            
            PREP_Category__c cat = new PREP_Category__c();
            cat.Name = 'Category';
            cat.Active__c = true;
            cat.Discipline__c = disp.Id;
            
            insert cat;
            
            PREP_Sub_Category__c sub = new PREP_Sub_Category__c();
            sub.Name = 'SubCat';
            sub.Category__c = cat.Id;
            sub.Category_Manager__c = u.Id;
            sub.Category_Specialist__c = u.Id;
            sub.Active__c = true;
            
            insert sub;
         

            PREP_Initiative__c ini = new PREP_Initiative__c();
            ini.Initiative_Name__c = 'Test ini';
            ini.Discipline__c = disp.Id;
            ini.TIC__c = 10000;
            ini.PDE_Phase__c = '1';
            ini.Phase_4_Start_Date__c = Date.newInstance(2015, 9, 10);
            ini.Phase_4_Completion_Date__c = Date.newInstance(2015, 9, 10);
            ini.Status__c = 'Active';
            ini.Business_Unit__c = 'Atlantic Region';
            ini.Allocation_Level__c = 'Atlantic Region - Operations';
            ini.Project_Complexity__c = 'High';
            ini.X01_Segmentation_Team_Selection__c = Date.newInstance(2015, 5, 23);
            ini.X02_Business_Requirements__c = Date.newInstance(2015, 5, 24);
            ini.X03_Supplier_Market_Analysis__c = Date.newInstance(2015, 5, 25);
            ini.X04_Sourcing_Options__c = Date.newInstance(2015, 5, 26);
            ini.Strategy_Approved__c = Date.newInstance(2015, 5, 27); 
            ini.Project_End_Date__c = Date.newInstance(2015, 5, 27);
            ini.SCM_Manager__c = u.Id;
            ini.SCM_Team__c = 'HOG';
            ini.RecordTypeId = Schema.SObjectType.PREP_Initiative__c.getRecordTypeInfosByName().get('Projects Initiative').getRecordTypeId();
            
            insert ini; 
            
            
            PREP_Baseline__c baseline = new PREP_Baseline__c();
            baseline.P_Baseline_Spend_Dollar_To_Be_Awarded__c = 10000;
            baseline.MSA_Baseline_Spend_Dollar__c = 10000;
            baseline.Local_Baseline_Spend_Percent__c = 10;
            baseline.Local_Baseline_Savings_Percent__c = 10;
            baseline.Global_Baseline_Savings_Percent__c = 10;
            baseline.X05_Going_To_Market__c = Date.newInstance(2015, 5, 27);
            baseline.XCAC2_CAC_Award_Approval__c = Date.newInstance(2015, 5, 27);
            baseline.X07_Contract_Start_Up__c = Date.newInstance(2015, 5, 28);
            baseline.RecordTypeId = Schema.SObjectType.PREP_Baseline__c.getRecordTypeInfosByName().get('Projects Baseline').getRecordTypeId();
            baseline.Initiative_Id__c = ini.Id;
            insert baseline;
            
            PREP_Material_Service_Group__c MS = new PREP_Material_Service_Group__c();
            MS.Name = 'MS Name';
            MS.Material_Service_Group_Name__c = 'MS';
            MS.Sub_Category__c = sub.Id;
            MS.Category_Manager__c = u.Id;
            MS.SAP_Short_Text_Name__c = 'MS';
            MS.Active__c = true;
            MS.Type__c = 'Material';
            MS.Category_Specialist__c = u.Id;
            MS.Includes__c = 'MS';
            MS.Does_Not_Include__c = 'MS';
            insert MS;
        
            PREP_Mat_Serv_Group_Baseline_Allocation__c MSGBA = new PREP_Mat_Serv_Group_Baseline_Allocation__c();
            MSGBA.Material_Service_Group__c = MS.Id;
            MSGBA.Global_Sourcing__c = 'Yes';
            MSGBA.Planned_Spend_Allocation_Dollar__c = 10000;
           
            MSGBA.Baseline_Id__c = baseline.Id;
            MSGBA.Contract_Type__c = 'MSA';
            MSGBA.RecordTypeId = Schema.SObjectType.PREP_Mat_Serv_Group_Baseline_Allocation__c.getRecordTypeInfosByName().get('Projects Mat Group Baseline Allocation').getRecordTypeId();
            
            insert MSGBA;
            
            PREP_Business_Value_Submission__c BVS = new PREP_Business_Value_Submission__c();
            BVS.Mat_Serv_Group_Baseline_Allocation_Id__c = MSGBA.Id;
            BVS.Category__c = cat.Id;
            BVS.Discipline__c = disp.Id;
            BVS.Sub_Category__c = sub.Id;
            BVS.Initiative_Id__c = ini.Id;
            BVS.SCM_Manager__c = u.Id;
            BVS.Specialist_Rep__c = u.Id;
            BVS.Contract_Purchase_Approve_Date__c = Date.newInstance(2015, 4, 10);
            //BVS.Local_Sourcing__c = 10;
            //BVS.Global_Sourcing__c = 90;
            BVS.Short_Term_Interests_Rate__c = 1.50;
            BVS.RecordTypeId = Schema.SObjectType.PREP_Business_Value_Submission__c.getRecordTypeInfosByName().get('Cat-Project Business Value Submission').getRecordTypeId();
            BVS.Contract_Start_Date__c = Date.newInstance(2015, 4, 10);
            BVS.Contract_End_Date__c = Date.newInstance(2015, 12, 10);
            BVS.Awarded_Spend__c = 100000000;
            BVS.Rate_Reduction_Increase__c = 100000000;
           
            BVS.Status__c = 'Approved';
            insert BVS;
        
            try{
                BVS.Awarded_Spend__c = 200;
                BVS.Local_Awarded_Spend__c = 200;  
                
                update BVS; 
            } catch(Exception e){
            
            }
        }               
        
    }
    
}