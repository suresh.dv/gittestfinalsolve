public class ATSFormatGmtDateTimeToLocalTime
{
    public DateTime date_time{ get; set; } //property that reads the datetime value from component attribute tag
    public String convertedDateTime{ get; set;} //property that reads the string value from component attribute tag
    public String getFormattedDatetime()
    {
        if (date_time == null) {return ''; }
        else { 
            if (convertedDateTime  == null) 
            {
                return date_time.format(); //return the full date/time in user's locale and time zone
            }
            else { 
                convertedDateTime = date_time.format();
                return convertedDateTime;
            }
        }
    }
}