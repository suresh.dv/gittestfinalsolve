/*-------------------------------------------------------------------
Author      : King Koo
Company     : Husky
Description : For single edits, redirects to truck list page
Test Class  : FM_TruckList_Test
Date        : 2015-05-22
OBSOLETE CLASS, NOT USED AFTER TRUCK LIST REMOVAL, @isTest to skip unit tests
----------------------------------------------------------------------*/
@isTest
public with sharing class FM_TruckTripSingleEditControllerX 
{

/*
	public String truckTripId {get;set;}
	private String truckListId;


    public FM_TruckTripSingleEditControllerX(ApexPages.StandardController stdController)
    {
            truckTripId = ApexPages.currentPage().getParameters().get('id');
            // given the truck trip is a child to a master, there will be one parent.
            FM_Truck_Trip__c truckTrip = [SELECT Truck_List_Lookup__c FROM FM_Truck_Trip__c WHERE Id = :truckTripID limit 1][0];
            truckListId = truckTrip.Truck_List_Lookup__c;
    }
    
    public PageReference redirectToTruckListEdit()
    {
            PageReference truckListEditPage = Page.FM_TruckListEdit;
        	truckListEditPage.getParameters().put('id', truckListId);
        	truckListEditPage.setRedirect(true);
        	return truckListEditPage;
    }
*/
}