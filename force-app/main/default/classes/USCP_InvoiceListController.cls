public with sharing class USCP_InvoiceListController extends DynamicListController {

    Public List<USCP_Invoice__c> selectedInvoices   {get; private set;}
    Public List<InvoiceWrapper> currentPageInvoices {get; private set;}
    public Boolean AllInvoicesSelected {get;set;}
    public Boolean InvoicesSelected {get;set;}    
    public String selectedInvoiceIDs {get;set;}
    
    public Boolean IsAdmin {get;private set;}
    public String AccountId {get;set;}    
    public String Account {get;set;}     
    public String InvoiceNumber {get;set;}
    public String Description {get;set;}  
    public String BOLNumber {get;set;} 
    public String Location {get;set;}  
    public String selectedTerminalID{get;set;}          
    public string DateRangeType {get;set;}
    public Date dateFrom {get;set;}
    public Date dateTo {get;set;}  
    public String dateFromStr 
    {
        get
        {
            Date d = dateFrom;
            if(d==null)
            {
                return null;
            }
            return d.format() ;
        }
        set
        {
            dateFrom = USCP_Utils.convertDate(value);    //Date.valueOf(value);
        }
    }
    public String dateToStr 
    {
        get
        {
            Date d = dateTo ;
            if(d==null)
            {
                return null;
            }            
            return d.format() ;
        }
        set
        {
            dateTo =  USCP_Utils.convertDate(value); //Date.valueOf(value);
        }
    }


   public USCP_InvoiceListController () {
        // TODO: call the parent class constructor with a base query
        super(''); 
        String sql='';       
        sql+='select Id, Name, Account__c, Account__r.Name,Account__r.Customer_Number__c, Invoice_Date__c, Ship_Date__c, Due_Date__c, Amount__c, CreatedDate, ' ; 
        sql+='(select id, BOL_Number__c, Line_Number__c, Location__c, Location__r.Name, Movement_Date__c, product__r.Name from AccTransactions__r) from USCP_Invoice__c';
        this.BaseQuery = sql; 
        
        sortColumn = 'Invoice_Date__c'; //'Account__c, Name';        
        sortAsc = false;

        pageSize = 20;



        currentPageInvoices = new List<InvoiceWrapper>(); 
        selectedInvoices = new List<USCP_Invoice__c>();
        InvoicesSelected =false;
        
        
        ResetFilters();        

        //query(); do not load data 
        IsAdmin = USCP_Utils.IsAdminUser();   
    }
    
    public override void reloadpage() {
        AllInvoicesSelected  = false; 
        InvoicesSelected =false;
        selectedInvoices.clear();
        currentPageInvoices  = getWInvoices();    
    }
    
    public override void first() {
        super.first();
        reloadpage();
    }

    public override void previous() {
        super.previous();
        reloadpage();
    }

    public override void next() {
        super.next();
        reloadpage();
    }

    public override void last() {
        super.last();
        reloadpage();
    }

    // cast the resultset
    public List<USCP_Invoice__c> getInvoices() {
        return (List<USCP_Invoice__c>) getRecords();
    }
    // cast the resultset
    public List<USCP_Invoice__c> getAllInvoices() {
        return (List<USCP_Invoice__c>) getAllRecords();
    }    
    //work around collection size limitation
    public List<USCP_Invoice__c> getInvoices(integer pageNumber) {
        if(this.getPageCount()<pageNumber)
            return new List<USCP_Invoice__c>(); // return empty list
        else
        {   
            this.setPageNumber(pageNumber);
            return (List<USCP_Invoice__c>) getRecords();
        }
    }    

    public PageReference getSelected()
    {
        InvoicesSelected =false;
        selectedInvoiceIDs = '';
        System.debug('======getSelected======');
        System.debug('total ' + currentPageInvoices.size() + ' invoices' );
        selectedInvoices.clear();
        for(InvoiceWrapper invwrapper : currentPageInvoices) 
        {
            if(invwrapper.selected == true)
            {
                selectedInvoices.add(invwrapper.InvoiceObject);
                if(InvoicesSelected)
                {
                    selectedInvoiceIDs += '-';
                }
                selectedInvoiceIDs += invwrapper.InvoiceObject.id;
                InvoicesSelected = true;
            }
        }
        System.debug('selected ' + selectedInvoices.size() + ' invoices' );
        return null;
    }






    public List<InvoiceWrapper> getAllWInvoices() {
        // TODO: cast returned records into list of Accounts
        //(List<USCP_Invoice__c>) getRecords();
        List<InvoiceWrapper> result = new List<InvoiceWrapper>();   
        for(USCP_Invoice__c inv: (List<USCP_Invoice__c>) getAllRecords())
        {
              result.add(new InvoiceWrapper(inv));
        }
        return result;
    }  

    public List<InvoiceWrapper> getWInvoices() {
        // TODO: cast returned records into list of Accounts
        //(List<USCP_Invoice__c>) getRecords();
        List<InvoiceWrapper> result = new List<InvoiceWrapper>();   
        for(USCP_Invoice__c inv: (List<USCP_Invoice__c>) getRecords())
        {
              result.add(new InvoiceWrapper(inv));
        }
        return result;
    }  
    
    //work around collection size limitation
    public List<InvoiceWrapper> getWInvoices(integer pageNumber) {
        List<InvoiceWrapper> result = new List<InvoiceWrapper>();
        if(this.getPageCount()> pageNumber)
        {   
            this.setPageNumber(pageNumber);
            for(USCP_Invoice__c inv: (List<USCP_Invoice__c>) getRecords())
            {
                  result.add(new InvoiceWrapper(inv));
            }
        }
        return result;
    }    

    public List<List<InvoiceWrapper>> getAllWInvoicePages() {
        List<InvoiceWrapper> result = new List<InvoiceWrapper>();
        List<List<InvoiceWrapper>> resultPages =new List<List<InvoiceWrapper>>();

        //store existing page settings
        integer currentPageSize =  this.pageSize;   
        integer currentPage =  this.getPageNumber();
        this.pageSize = 1000; 
        
        this.first();
        do
        {
            if(resultPages.size() <> 0 ) this.next();   
            result = new List<InvoiceWrapper>();
            for(USCP_Invoice__c inv: (List<USCP_Invoice__c>) getRecords())
            {
                  result.add(new InvoiceWrapper(inv));
            } 
            resultPages.add(result);
        }
        while(this.getHasNext());
        
        //restore existing page settings
        this.pageSize = currentPageSize ; 
        this.setPageNumber(currentPage );
        
        System.debug('Returning Pages:' +resultPages.size() );
        
        return resultPages ;
    }     
    
 
    
    private void ResetFilters()
    {
        AccountId      ='';
        Account = '';
        InvoiceNumber  = '';
        Description = '';
        BOLNumber = '';
        Location = '';
        selectedTerminalID='';        
        dateFrom = null; //Date.Today()-1;
        dateTo = null; //Date.Today()-1;
        DateRangeType ='na';  
        
        AllInvoicesSelected  = false;    
        selectedInvoices.clear();
        InvoicesSelected =false;
        selectedInvoices.clear();        
    }
     
       
    public PageReference DownloadSelectedInvoices() {
        if(selectedInvoices.size()>0)
        {
            return null;
        }
        else
        {
            ApexPages.addMessage(new ApexPages.message(ApexPages.severity.ERROR,'Please select at least one Invoice.'));        
            return null;
        }    
    } 

    public PageReference ClearFilter() {

        ResetFilters(); 
        //this.WhereClause  = null;
        this.WhereClause  = ' Name=null '; //always false, no data load
        search();
    
        currentPageInvoices  = getWInvoices();
        return null;
    }    
    public PageReference RefreshData() { 
    
            AllInvoicesSelected  = false;    
            selectedInvoices.clear();
            InvoicesSelected =false;
            selectedInvoices.clear();   
    
     
            String FilteredSOQL = ' Name!=null '; // always true
            Boolean filtersON = false; 
            String dateFieldName = ''; 
            
            if(AccountId!='')
                {
                filteredSOQL  = filteredSOQL + ' and Account__c = \'' +  String.escapeSingleQuotes(AccountId) + '\'';
                filtersON = true;
                }          
                       
            if(InvoiceNumber !='')
                {
                filteredSOQL  = filteredSOQL + ' and Name like \'%' +  String.escapeSingleQuotes(InvoiceNumber ) + '%\'';
                filtersON = true;
                }
            if(BOLNumber !='')
                {
                //filteredSOQL  = filteredSOQL + ' and id in (select Invoice__c from USCP_BOL__c where BOL_Number__c like \'%' +  String.escapeSingleQuotes(BOLNumber ) + '%\')';
                filteredSOQL  = filteredSOQL + ' and id in (select Invoice__c from USCP_BOL_AccTransaction__c where BOL_Number__c like \'%' +  String.escapeSingleQuotes(BOLNumber ) + '%\')';
                filtersON = true;                
                }
                 
            if(selectedTerminalID!='')
                {
                filteredSOQL  = filteredSOQL + ' and id in (select Invoice__c from USCP_BOL_AccTransaction__c where Location__c = \'' +  String.escapeSingleQuotes(selectedTerminalID) + '\')';
                filtersON = true;                
                }              
            else if(Location !='')
                {
                //filteredSOQL  = filteredSOQL + ' and Location__r.name like \'%' +  String.escapeSingleQuotes(Location ) + '%\'';
                filteredSOQL  = filteredSOQL + ' and id in (select Invoice__c from USCP_BOL_AccTransaction__c where Location__r.name  like \'%' +  String.escapeSingleQuotes(Location ) + '%\')';
                
                filtersON = true;                
                }                                

            //ship date - more complicated, we need to search in BOL Movement Date
            /*
            if(DateRangeType =='shipdate')
            {
                if(dateFrom != null)
                {                
                    filteredSOQL  = filteredSOQL + ' and id in (select Invoice__c from USCP_BOL_AccTransaction__c where Movement_Date__c >= ' +  String.valueof(dateFrom) + ')';
                    filtersON = true;                
                }                    
                if(dateTo != null)
                {                
                    filteredSOQL  = filteredSOQL + ' and id in (select Invoice__c from USCP_BOL_AccTransaction__c where Movement_Date__c <= ' +  String.valueof(dateTo) + ')';
                    filtersON = true;                
                }                
            }   
            */
            
            
            //search by date range
            if(DateRangeType !='na')
            {            
            if(DateRangeType =='invoicedate') dateFieldName  = 'Invoice_Date__c';
            if(DateRangeType =='shipdate'  ) dateFieldName  = 'Ship_Date__c';
            if(DateRangeType =='duedate'  ) dateFieldName  = 'Due_Date__c';
                        
            if(dateFrom != null)
                {
                filteredSOQL  = filteredSOQL + ' and ' + dateFieldName + '>= ' +  String.valueof(dateFrom);
                filtersON = true; 
                }
            if(dateTo != null)
                {
                filteredSOQL  = filteredSOQL + ' and ' + dateFieldName + '<= ' +  String.valueof(dateTo);
                filtersON = true; 
                }                            
            }

         if(filtersON )
         {         
             this.WhereClause  = FilteredSOQL;
         }  
         else
         {
            this.WhereClause  = ' Name=null '; //always false, no data load
         }
        query();
        
        currentPageInvoices  = getWInvoices();
                
        return null;
    }  
    @RemoteAction
    public static List<USCP_Terminal__c> searchTerminal(String searchTerminal) {
        System.debug('Terminal Name is: '+searchTerminal);
        List<USCP_Terminal__c> result = Database.query('Select Id, Name from USCP_Terminal__c where id in (select Location__c from USCP_BOL_AccTransaction__c) and name like \'%' + String.escapeSingleQuotes(searchTerminal) + '%\'');
        return result ;
    }   
    @RemoteAction
    public static List<Account > searchAccount(String searchAccount) {
        System.debug('Account Name is: '+searchAccount);
        List<Account> result = Database.query('Select id, Name, SMS_Number__c from Account where id in (select Account__c from USCP_Invoice__c) and (name like \'%' + String.escapeSingleQuotes(searchAccount) + '%\' or Customer_Number__c  like \'%'  + String.escapeSingleQuotes(searchAccount) + '%\' )  order by Name ');
        //RecordType.DeveloperName = \'USCP_Account_Record\' and 
        return result ;
    }   


            
      
      
  public PageReference SaveToExcel() 
   {
       if(this.getResultSize() == 0) return null;
       
       if(this.getResultSize() > 10000)
       {
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, 'More than 10000 invoices selected. Please narrow your filter.'));
            return null;   
       }

       return page.USCP_InvoicesExcel;
   }    
    /*Wrapper class*/   
    public class InvoiceWrapper
    {
        public Boolean selected {get; set;}

        public USCP_Invoice__c InvoiceObject {get;set;}
        public integer BOLCount {get;set;}        
        public integer LocationCount {get;set;}
        public String LocationID {get;set;}        
        public String LocationName {get;set;}   
        public String ShipDate {get;set;}   
                       
        public Set<String> BOLNumbers {get;set;}
        public Set<String> Locations {get;set;}    
        public Set<String> ShipDates {get;set;}            
            
        InvoiceWrapper(USCP_Invoice__c invoice)
        {
            this.InvoiceObject = invoice;
            BOLNumbers = new Set<String>();
            Locations = new Set<String>();  
            ShipDates = new Set<String>();
                      
            for(USCP_BOL_AccTransaction__c acc : invoice.AccTransactions__r)
            {
                BOLNumbers.add(acc.BOL_Number__c);
                Locations.add(acc.Location__r.Name); 
                LocationID =  acc.Location__c; 
                LocationName =  acc.Location__r.Name; 
                
                ShipDates.add(acc.Movement_Date__c.format());              
                ShipDate =  acc.Movement_Date__c.format();
            }
            BOLCount = BOLNumbers.size();
            LocationCount = Locations.size();
               
            if(LocationCount>1) 
            {
                LocationID=null;
                LocationName='Various';                
            }
            
            if(ShipDates.Size()>1)
            {
                ShipDate ='Various';            
            }
                        
                        
        }
    }
    
}