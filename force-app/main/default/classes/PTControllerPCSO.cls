public class PTControllerPCSO {
    private limitWrapper[] pcsoFinalList = new limitWrapper[]{};
        
        private final integer listLimit = 999;
    
    public limitWrapper[] getpcsoFinalList()
    {
        pcsoFinalList = new limitWrapper[]{};
            
        integer counter = 0;
        integer loopCount = 0;
        objectWrapper[] objectWrappedList = new objectWrapper[]{};                    
            
            for(Service__c service : [select id,name,utility_rate_id__r.name,utility_company__r.name,contract_start_date__c,site_id__c,minimum_contract_demand__c,(select id,name,billing_date__c,Average_Billing_Demand_last_12_months__c,	Average_Metered_Demand_last_12_months__c,Average_Total_Volume_last_12_months__c,Average_Wire_Cost_last_12_months__c,Average_KVA_Demand_last_12_months__c from utility_billings__r order by billing_date__c desc limit 1),(SELECT Id, Name,service_id__r.name,pcso_type__c FROM PCSOS__r where pcso_status__c ='Created' and system_review__c='Valid' and original_wire_cost__c = null)  from service__c where service_status__c in ('Active','Idle')])
        {
            List<Utility_Billing__c> utilityBillingList = service.utility_billings__r;
            if(counter < listLimit)
            {
				system.debug(Service);                
                for(PCSO__c pcso : service.pcsos__r){
                    objectWrapper objectWrapped = new objectWrapper(service,pcso,utilityBillingList.get(0));
                    objectWrappedList.add(objectWrapped);
                    counter++;    
                }
            }
            else
            {
                loopCount++;
                for(PCSO__c pcso : service.pcsos__r){
                    pcsoFinalList.add(new limitWrapper(objectWrappedList,loopCount));
                    objectWrappedList = new objectWrapper[]{};
                    objectWrapper objectWrapped = new objectWrapper(service,pcso,utilityBillingList.get(0));
                    objectWrappedList.add(objectWrapped);
                    counter = 0;                    
                }
            }            
        }
        
        if(counter > 0)
        {
            loopCount++;
            pcsoFinalList.add(new limitWrapper(objectWrappedList,loopCount));
        }
        
        return pcsoFinalList;
    }
    
    public class limitWrapper
    {
        public objectWrapper[] objectsWrapper {get;set;}
        public integer blockNumber {get;set;}
        public limitWrapper(objectWrapper[] objectWrapper, integer i)
        {
            objectsWrapper = objectWrapper;
            blockNumber = i;
        }
        
    }
    
    public class objectWrapper
    {
        public service__c service {get;set;}
        public pcso__c pcso {get;set;}
        public utility_billing__c utilityBilling{get;set;}
        
        public objectWrapper(service__c serviceRecord,pcso__c pcsoRecord,utility_billing__c utilityBillingRecord){
            service = serviceRecord;
            pcso = pcsoRecord;
            utilityBilling = utilityBillingRecord;
        }
    }
}