public class USCP_DTN_API 
{   public static String GetSecurityToken(String soldto) 
    {


     USCP_Settings__c  mc = USCP_Settings__c.getOrgDefaults();



     // web service security   
     String WS_UserName = mc.DTN_WS_UserName__c; 
     String WS_Password = mc.DTN_WS_Password__c;  
     String WS_EndPoint = mc.DTN_WS_EndPoint__c; 
     String PortalId = mc.DTN_ALV_PortalID__c; 
     String ComponentID = mc.DTN_ALV_ComponentID__c; 
     String ALV_UserName = mc.DTN_ALV_UserName__c; 
     String ALV_Password = mc.DTN_ALV_Password__c; 
      
     string SoapXMLBody; 
     SoapXMLBody ='<?xml version="1.0" encoding="utf-8"?>';
     SoapXMLBody +='<soapenv:Envelope xmlns:dtn="http://schemas.datacontract.org/2004/07/DTN.RefinedFuel.Security" xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/" xmlns:tem="http://tempuri.org/">';
     SoapXMLBody +='<soapenv:Header><wsse:Security soapenv:mustUnderstand="1" xmlns:wsse="http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-secext-1.0.xsd" '; 
     SoapXMLBody +='xmlns:wsu="http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-utility-1.0.xsd">';
     SoapXMLBody +='<wsse:UsernameToken wsu:Id="UsernameToken-3"><wsse:Username>' + WS_UserName  + '</wsse:Username>';
     SoapXMLBody +='<wsse:Password Type="http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-username-token-profile-1.0#PasswordText">' + WS_Password + '</wsse:Password>';
    // SoapXMLBody +='<wsse:Nonce EncodingType="http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-soap-message-security-1.0#Base64Binary">rjrRUso36cbZedRlrj1qGw==</wsse:Nonce>';
    // SoapXMLBody +='<wsu:Created>2014-01-15T23:32:11.467Z</wsu:Created>';
     SoapXMLBody +='</wsse:UsernameToken></wsse:Security></soapenv:Header>';

     SoapXMLBody +='<soapenv:Body><tem:CreateSecurityToken><tem:PortalId>' + PortalId  +'</tem:PortalId><tem:ComponentID>' + ComponentID  + '</tem:ComponentID>'; 
     SoapXMLBody +='<tem:TokenData>';
     SoapXMLBody +='<dtn:TokenData><dtn:Name>username</dtn:Name><dtn:Value>' + ALV_UserName + '</dtn:Value></dtn:TokenData>';
     SoapXMLBody +='<dtn:TokenData><dtn:Name>password</dtn:Name><dtn:Value>' + ALV_Password + '</dtn:Value></dtn:TokenData>';
     SoapXMLBody +='<dtn:TokenData><dtn:Name>soldto</dtn:Name><dtn:Value>' + soldto+ '</dtn:Value></dtn:TokenData>';
     SoapXMLBody +='</tem:TokenData>';
     SoapXMLBody +='</tem:CreateSecurityToken>';
     SoapXMLBody +='</soapenv:Body>';
     SoapXMLBody +='</soapenv:Envelope>';
                              
     string SoapXML; 
     SoapXML = SoapXMLBody; 
     Integer ContentLength = 0; 
     ContentLength = SoapXML.length(); 
     Http h = new Http(); 
     HttpRequest req = new HttpRequest(); 
     HttpResponse res = new HttpResponse(); 
     req.setMethod('POST'); 
     req.setEndPoint(WS_EndPoint); 
     req.setHeader('Content-type','text/xml'); 
     req.setHeader('Content-Length',ContentLength.format()); 
     req.setHeader('SoapAction','http://tempuri.org/ICreateToken/CreateSecurityToken'); 
     req.setBody(SoapXML); 
     System.Debug(req.getHeader('req.getHeader; '+'Content-Length')); 
     System.Debug('req: '+req); 
     System.Debug('req.getBody'+req.getBody()); 
     
     if (Test.isRunningTest()) { 
        return '12345' ;
     }
  
     req.setTimeout(10000);     
     res = h.send(req); 
    
     if(res.getStatusCode()>299) 
     {
          System.debug('ERROR: '+res.getStatusCode()+': '+res.getStatus());
          System.debug(res.getBody());
          //ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, 'ERROR! ' + res.getStatusCode()+': '+res.getStatus()));  
          USCP_Utils.logError('Web service call to retrieve Allocation Viewer security token failed with status: ' + res.getStatusCode()+' '+res.getStatus() );
          return null;
     } 
     else 
     {         
         System.Debug('res:'+res); 
         String auth = res.getBody(); 
         System.Debug('Debug(auth:'+auth);
          
         String result =  auth.substringBetween('<CreateSecurityTokenResult>', '</CreateSecurityTokenResult>');
         System.Debug(result );   
         return result;    
     }    

  } 
  //this method will make a test call USCP_Settings__c.DTN_ALV_URL__c
  //to check if website is up and running
  /*
  public static boolean Ping()
  {
    try
    {
        USCP_Settings__c  mc = USCP_Settings__c.getOrgDefaults();
        String endpointUrl= mc.DTN_ALV_URL__c; 
    
        Http h = new Http();
        // Instantiate a new HTTP request, specify the method (GET) as well as the endpoint
        HttpRequest req = new HttpRequest();
        System.debug(endpointUrl);
        req.setEndpoint(endpointUrl);
        req.setTimeout(60000);
        System.debug('req.getBody()>>>>>>'+req.getBody());
        req.setMethod('GET');
        // Send the request, and return a response
        HttpResponse res = h.send(req);
        System.debug('res is>>>>>>'+res);
        System.debug('res.getBody()>>>>>>'+res.tostring());
        if(res.getStatusCode()>299) {    
          System.debug('ERROR: '+res.getStatusCode()+': '+res.getStatus());
          System.debug(res.getBody());
           USCP_Utils.logError('Ping call to DTN Allocation viewer site failed with status: ' + res.getStatusCode()+' '+res.getStatus() + '<br/>' + endpointUrl);
           return false;        
        }
        return true;
    }  
    catch (Exception e) 
    {  
        USCP_Utils.logError(e);
    } 
    return false;  
  }
  */
  
}