public with sharing class NCR_CAR_CreatePageControllerX
{

    private final ApexPages.StandardController stdcontroller;
    private final NCR_CAR__c ncr_record;    
    
    private String recordId;
    private ID userID;    
    
    public String businessUnitName {get;set;}
    public String ProjectName {get;set;}
    
    public string currentBU { get; set; }
    public string currentProj { get; set; }    
    
    
    public NCR_CAR_CreatePageControllerX(ApexPages.StandardController stdController) {

        recordId = stdController.getId();
        userID =   UserInfo.getUserid();

        this.stdcontroller = stdController;
        this.ncr_record = (NCR_CAR__c)stdController.getRecord();  
        this.ncr_record.QA_Lead__c = UserInfo.getUserid();
    }
    
    public NCR_CAR__c getNCRCAR(){
        return ncr_record;
    }
    
    public String getRecordId(){
        return recordId;
    }
    
    public PageReference save(){
        
        this.ncr_record.Business_Unit__c = currentBU;
        this.ncr_record.Project__c = currentProj;   
        
        this.stdcontroller.save();
        //upsert this.ncr_record;
        
        Schema.DescribeSObjectResult result = NCR_CAR__c.SObjectType.getDescribe();
        
        PageReference pg = new PageReference('/' + result.getKeyPrefix() + '/o');
        pg.setRedirect(true);        
        
        return pg;           
                    
    }
    
    /*populate business unit picklist*/
    public List<SelectOption> getBusinessUnits() {
        List<SelectOption> options = new List<SelectOption> { new SelectOption('','--None--') };
        
        for(Business_Unit__c b:[SELECT id, name from Business_Unit__c where RecordType.name = 'SAP Profit Center Level 4']){
            options.add(new SelectOption(b.id,b.name));
        }
        
        return options;
    }
    
    /*populate project picklist based on business unit*/
    public List<SelectOption> getProjects() {
        List<SelectOption> options = new List<SelectOption> { new SelectOption('','--None--') };
        
        for(Milestone1_Project__c p:[SELECT id, name from Milestone1_Project__c where RecordType.name = 'SAP Project' and Active__c = true and Business_Unit__c = :currentBU]){
            options.add(new SelectOption(p.id,p.name));
        }
        
        return options;
    }      

}