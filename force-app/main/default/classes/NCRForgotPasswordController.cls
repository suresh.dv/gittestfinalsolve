/**
 * An apex page controller that exposes the site forgot password functionality
 */
public with sharing class NCRForgotPasswordController {
    public String username {get; set;}   
       
    public NCRForgotPasswordController() {}
    
    public PageReference forgotPassword() {
        boolean success = Site.forgotPassword(username);
        PageReference pr = Page.NCRForgotPasswordConfirm;
        pr.setRedirect(true);
        
        if (success) {              
            return pr;
        }
        return null;    
    }
}