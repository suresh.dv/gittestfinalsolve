/*------------------------------------------------------------
Author:        Yen Le
Company:       Thirdwave
Description:   Apex Container of Equipment Transfer Item using in EquipmentTransferToFormCtrl

------------------------------------------------------------*/
public with sharing class EquipmentItemWrapper {
    public Equipment_Transfer_Item__c equipment {get;set;}
    public Attachment photo {get;set;}
    public EquipmentItemWrapper(Equipment_Transfer_Item__c item)
    {
        equipment = item;
        photo = new Attachment();
        if (item.Attachments != null && item.Attachments.size() > 0)
        {
           photo = item.Attachments[0];
        }
        
    } 
}