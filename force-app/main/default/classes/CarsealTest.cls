@isTest 
private class CarsealTest 
{
    // this test class covers everything about carseal
    // CarsealControllerExt class
    // CarsealListController class
    // Carseal_PopulatePlantOnInitImport trigger
    // CarsealStatis_UpdateCurrentStatus trigger
    // CarsealStatus_UpdateDateClosedOnStatusChange trigger
    
    static Carseal__c c;
    static List<Plant__c> pList = new List<Plant__c>();
    static List<Unit__c> uList;
    
    // Map from plant to List of Units
    static Map<Plant__c, List<Unit__c>> puMap = new Map<Plant__c, List<Unit__c>>();
    
    ////////////////////////////////////////////////////////
    // The following methods test CarsealControllerExt class
    // That is it tests the basic New, Edit and Clone function of Carseal objects.
    // TestNewCarseal
    // TestEditExistingCarseal
    // TestCloneExistingCarseal
    ////////////////////////////////////////////////////////
    
    private static void InitializeData(Integer plantNum)
    {
        // plantNum is the number of plants to create
        // the number of units to create for each plant is the plant index times 3.  So plant 0 has 3 units, plant 1 has 
        // 6 units, plant 9 has 30 units.
        // So if there 5 plants, then there are 3 + 6 + 9 + 12 + 15 = 45 units.  If there are 10 plants, there 
        // are 3 * ( 1 + 2 + ... + 10) = 3 * 55 = 165 units.

        // Populate these plants and units into a static map called puMap
        
        for (Integer i = 0 ; i < plantNum ; i++)
        {
            Plant__c p = new Plant__c(Name = 'Plant ' + i);
            pList.add(p);
        }
        insert pList;
        for (Integer i = 0 ; i < plantNum ; i++)
        {
            // Need to refresh the uList variable
            uList = new List<Unit__c>();
            for (Integer j = 0 ; j < (i + 1) * 3 ; j++)
            {
                Unit__c u = new Unit__c(Name = 'Unit ' + j, Plant__c = pList[i].Id);
                uList.add(u);
            }
            insert uList;
            puMap.put(pList[i], uList);
        }
    }
    
    @isTest
    private static void TestNewCarseal()
    {
        c = new Carseal__c();
        ApexPages.standardController std = new ApexPages.standardController(c);
        CarsealControllerExt controllerExt = new CarsealControllerExt(std);
        
        // Because the mode is not Clone, the clone parameter will not be set and so the isClone should be set to false.
        System.assertEquals(false, controllerExt.isClone);

        System.assertEquals(null, controllerExt.plantId.value);
        System.assertEquals(null, controllerExt.unitId.value);
    }
    
    @isTest
    private static void TestEditExistingCarseal()
    {

        // set up 10 plants and units.
        Integer numberOfPlants = 10;
        InitializeData(numberOfPlants);
        
        Integer randomPlantNumber = Integer.valueOf(Math.random() * numberOfPlants);
        Integer randomUnitNumber = Integer.valueOf(Math.random() * randomPlantNumber * 3);

        // create a carseal and simulate opening the carseal page
        c = new Carseal__c(Plant__c = pList[randomPlantNumber].Id, Unit__c = puMap.get(pList[randomPlantNumber])[randomUnitNumber].Id);
        insert c;

        // set the page
        Test.setCurrentPage(Page.Carseal_Edit);
        
        ApexPages.standardController std = new ApexPages.standardController(c);
        CarsealControllerExt controllerExt = new CarsealControllerExt(std);
        
        System.assertEquals(pList[randomPlantNumber].Id, controllerExt.plantId.value);
        System.assertEquals(puMap.get(pList[randomPlantNumber])[randomUnitNumber].Id, controllerExt.unitId.value);
        
        // the Plant picklist and Unit picklist should be populated
        List<SelectOption> plantPickList = controllerExt.getPlants();
        // plus 1 because there is an extra 'Choose a Plant' item.
        System.assertEquals(plantPickList.size(), numberOfPlants + 1);
        
        // User chooses item 4 (or index 3) 
        // item 0 is 'Choose a Plant', so item 4 is actually the 3rd plant, there should be 9 units there, 
        // so the picklist will have 10 items. (again plus 1 for the 'Choose a Unit' option)
        controllerExt.plantId.value = plantPickList[3].getValue();
        System.assertEquals(plantPickList[3].getValue(), controllerExt.plantId.value);
        
        // get the list of units based on the plant selected
        List<SelectOption> unitPickList = controllerExt.getUnits();
        System.assertEquals(unitPickList.size(), 3 * 3 + 1);

        // Now switch to another plant, the size of the units based on the plant should adjust accordingly.
        // Say switch the plant to 7.
        controllerExt.plantId.value = plantPickList[7].getValue();
        System.assertEquals(plantPickList[7].getValue(), controllerExt.plantId.value);
        controllerExt.clearUnit();
        // need to refresh the units because of a change of plant
        unitPickList = controllerExt.getUnits();
        System.assertEquals(unitPickList.size(), 7 * 3 + 1);
                
        // Let's say the user chooses a value for the Unit
        controllerExt.unitId.value = unitPickList[3].getValue();
        System.assertEquals(unitPickList[3].getValue(), controllerExt.unitId.value);
        
        // Now switch back to plant 5. 
        controllerExt.plantId.value = plantPickList[5].getValue();
        controllerExt.clearUnit();
        // need to refresh the units because of a change of plant
        unitPickList = controllerExt.getUnits();
        // the unit should be refreshed via the clearUnit method
        System.assertEquals('*', controllerExt.unitId.value);
        
        // right now the plantId and unitId have no associated errors
        System.assertEquals('', controllerExt.plantId.error);
        System.assertEquals('', controllerExt.unitId.error);
        
        // Now that plant has a value and unit doesn't, if a save is performed, there will be an error populated on the Unit.
        controllerExt.save();
        System.assertEquals('', controllerExt.plantId.error);
        System.assertEquals(false, controllerExt.plantId.getHasError());
        System.assertEquals('You must enter a value', controllerExt.unitId.error);
        System.assertEquals(true, controllerExt.unitId.getHasError());
        
        // If user clears out plant too, then error will be in both places
        controllerExt.plantId.value = plantPickList[0].getValue(); // this is the "Choose a Plant" item
        // need to refresh the units because of a change of plant
        unitPickList = controllerExt.getUnits();
        controllerExt.save();
        System.assertEquals('You must enter a value', controllerExt.plantId.error);
        System.assertEquals(true, controllerExt.plantId.getHasError());
        System.assertEquals('You must enter a value', controllerExt.unitId.error);
        System.assertEquals(true, controllerExt.unitId.getHasError());
        
        // Fill both value in this time and the save should perform successfully.
        controllerExt.plantId.value = plantPickList[5].getValue();
        // need to refresh the units because of a change of plant
        unitPickList = controllerExt.getUnits();
        controllerExt.unitId.value = unitPickList[3].getValue();
        PageReference savePR = controllerExt.save();
        // after saving, the page should be in the view mode of the original carseal standard controller
        System.assertEquals(savePR.getURL(), std.view().getURL());
    }

    @isTest
    private static void TestCloneExistingCarseal()
    {
        // create 2 plants.  This test simply checks that if the Clone parameter is set, the controller is aware.
        // When the mode is Clone, the instance variable isClone should be set to true.
        Integer numberOfPlants = 2;
        InitializeData(numberOfPlants);
        
        Integer randomPlantNumber = 1;
        Integer randomUnitNumber = 1;
        
        c = new Carseal__c(Plant__c = pList[randomPlantNumber].Id, Unit__c = puMap.get(pList[randomPlantNumber])[randomUnitNumber].Id);
        insert c;
        
        // set the page
        Test.setCurrentPage(Page.Carseal_Edit);
        
        ApexPages.standardController std = new ApexPages.standardController(c);
        ApexPages.currentPage().getParameters().put('clone', '1');
        CarsealControllerExt controllerExt = new CarsealControllerExt(std);
        System.assertEquals(true, controllerExt.isClone);
    }
    
    ///////////////////////////////////////////////////////////////////////
    // The following methods test Carseal_PopupatePlantOnInitImport trigger
    // The trigger catches cases when we're importing data, where plant is not available but unit is.
    // TestPopulatePlantOnInitImportTrigger
    ///////////////////////////////////////////////////////////////////////
    
    @isTest
    private static void TestPopulatePlantOnInitImportTrigger()
    {
        // First create plants and units.  Then create 500 carseals providing only units.  After that go back to each carseal and verify Plants are populated properly.
        
        // create 30 plants
        Integer numberOfPlants = 30;
        InitializeData(numberOfPlants);
        
        Integer randomPlantNumber;
        Integer randomUnitNumber;

        List<Plant__c> privatePlantList = new List<Plant__c>();
        List<Unit__c> privateUnitList = new List<Unit__c>();
        
        Integer numberOfCarseals = 2;
        for (Integer i = 0 ; i < numberOfCarseals ; i++)
        {
            randomPlantNumber = Integer.valueOf(Math.random() * numberOfPlants);
            randomUnitNumber = Integer.valueOf(Math.random() * randomPlantNumber * 3);
            privatePlantList.add(pList[randomPlantNumber]);
            privateUnitList.add(puMap.get(pList[randomPlantNumber])[randomUnitNumber]);
        }

        List<Carseal__c> privateCarsealList = new List<Carseal__c>();
        // now create 500 carseals providing only units.  After that, all Plants should be populated and populated with the right Id.
        for (Integer i = 0 ; i < numberOfCarseals ; i++)
        {
            privateCarsealList.add(new Carseal__c(Unit__c = privateUnitList[i].Id));
        }
        insert privateCarsealList;
        
        privateCarsealList = [SELECT Plant__c, Unit__c FROM Carseal__c];
        for (Integer i = 0 ; i < numberOfCarseals ; i++)
        {
            system.assertEquals(privatePlantList[i].Id, privateCarsealList[i].Plant__c);
        }
    }

    ///////////////////////////////////////////////////////////////////////
    // The following methods test CarsealStatus_UpdateCurrentStatus trigger
    // The trigger tests that as statuses are added to carseal, the current status information on the carseal
    // record () is kept up to date.  This applies to all insert, update, delete and undelete
    // TestCarsealInfoOnStatusChangeTrigger
    ///////////////////////////////////////////////////////////////////////
    @isTest
    private static void TestCarsealInfoOnStatusChangeTrigger()
    {
        c = new Carseal__c();
        insert c;
        
        Carseal__c currentCarseal;
        System.assertEquals(null, c.Carseal_Status_Lookup__c);
        Carseal_Status_Code__c code = new Carseal_Status_Code__c(Name ='On');
        insert code;
        // create a new status
        // Stage 1 - 10 days prior to today
        Carseal_Status__c cs1 = new Carseal_Status__c(Carseal__c = c.Id, Date__c = System.today() - 10, Status_Lookup__c = code.Id);
        insert cs1;
        currentCarseal = [SELECT Carseal_Status_Lookup__c FROM Carseal__c WHERE Id = :c.Id];
        System.assertEquals(cs1.Id, currentCarseal.Carseal_Status_Lookup__c);
        
        // create a new status again
        // Stage 2 - 8 days prior to today
        Carseal_Status_Code__c code2 = new Carseal_Status_Code__c(Name ='Off');
        insert code2;
        Carseal_Status__c cs2 = new Carseal_Status__c(Carseal__c = c.Id, Date__c = System.today() - 8, Status_Lookup__c = code2.Id);
        insert cs2;
        currentCarseal = [SELECT Carseal_Status_Lookup__c FROM Carseal__c WHERE Id = :c.Id];
        System.assertEquals(cs2.Id, currentCarseal.Carseal_Status_Lookup__c);
        
        // imagine an admin comes in and delete this most recent status.
        delete cs2;
        // the carseal record should still be updated so that the "current status" reverts to the one earlier, i.e.
        // when it was in stage 1, i.e. 10 days prior to today.  Therefore after deleting cs2, the information on cs1
        // becomes current.
        currentCarseal = [SELECT Carseal_Status_Lookup__c FROM Carseal__c WHERE Id = :c.Id];
        System.assertEquals(cs1.Id, currentCarseal.Carseal_Status_Lookup__c);
        
        // imagine this now gets undeleted.  
        undelete cs2;
        //The info on the carseal record should now go back to Stage 2, i.e. 8 days
        // prior to today.  Therefore un-deleting cs2, the information on cs2 becomes current.
        currentCarseal = [SELECT Carseal_Status_Lookup__c FROM Carseal__c WHERE Id = :c.Id];
        System.assertEquals(cs2.Id, currentCarseal.Carseal_Status_Lookup__c);
        
        // now delete cs1.  This should have no affect on the current carseal status, because the cs2 has a later
        // date than cs1 anyway.
        delete cs1;
        currentCarseal = [SELECT Carseal_Status_Lookup__c FROM Carseal__c WHERE Id = :c.Id];
        System.assertEquals(cs2.Id, currentCarseal.Carseal_Status_Lookup__c);
        
        // now also delete cs2.  That means, all the status for this carseal are deleted.  The current status lookup
        // should become null again.
        delete cs2;
        currentCarseal = [SELECT Carseal_Status_Lookup__c FROM Carseal__c WHERE Id = :c.Id];
        System.assertEquals(null, currentCarseal.Carseal_Status_Lookup__c);
    }

    //////////////////////////////////////////////////////////////////////////////////
    // The following methods test CarsealStatus_UpdateDateClosedOnStatusChange trigger
    // This tests that the status date cannot be a future date, and also you cannot add
    // a status that is prior to your most recent status date.
    // TestCarsealCheckStatusDateTrigger
    //////////////////////////////////////////////////////////////////////////////////
    
    @isTest
    private static void TestCarsealCheckStatusDateTrigger()
    {
        ApexTrigger checkStatusDateTrigger = [Select Id, Status from ApexTrigger where name='CarsealStatus_UpdateDateClosedOnStatusChange'];
        c = new Carseal__c();
        insert c;

        Database.Saveresult insertResult;
        Carseal_Status_Code__c code = new Carseal_Status_Code__c(Name ='On');
        insert code;
        // create a new status.  This is for 10 days ago, so the insert will go fine.
        Carseal_Status__c cs1 = new Carseal_Status__c(Carseal__c = c.Id, Date__c = System.today() - 10, Status_Lookup__c = code.Id);
        insertResult = Database.insert(cs1, false);
        system.assertEquals(insertResult.isSuccess(), true);
        system.assertEquals(insertResult.getErrors().size(), 0);
        
        // create another status, this time for 10 days from now.  There will be an error.
        Carseal_Status_Code__c code2 = new Carseal_Status_Code__c(Name ='Off');
        insert code2;
        Carseal_Status__c cs2 = new Carseal_Status__c(Carseal__c = c.Id, Date__c = System.today() + 10, Status_Lookup__c = code2.Id);
        insertResult = Database.insert(cs2, false);
        if (checkStatusDateTrigger != null && checkStatusDateTrigger.Status == 'Active')
        {
            system.assertEquals(insertResult.isSuccess(), false);
            system.assertEquals(insertResult.getErrors().size(), 1);
            // getErrors is a collection of Database.Error object, and the error message is found in the getMessage() method.
            system.assertEquals(insertResult.getErrors()[0].getMessage(), 'The Date for the Status Update cannot be a future date.');
        }
        // create another status, this time for 20 days ago.  This should result in an error because there is already
        // a status with a later date (cs1).
        Carseal_Status__c cs3 = new Carseal_Status__c(Carseal__c = c.Id, Date__c = System.today() - 20, Status_Lookup__c = code2.Id);
        insertResult = Database.insert(cs3, false);
        if (checkStatusDateTrigger != null && checkStatusDateTrigger.Status == 'Active')
        {
            system.assertEquals(insertResult.isSuccess(), false);
            system.assertEquals(insertResult.getErrors().size(), 1);
            // getErrors is a collection of Database.Error object, and the error message is found in the getMessage() method.
            system.assertEquals(insertResult.getErrors()[0].getMessage(), 
            'An existing Carseal Status update conflicts with this date.  Please confirm the date of your Status change.');
        }
    }
    
    /////////////////////////////////////////////////////////
    // The following methods test CarsealListController class
    // That is it tests the "Carseal Update" function on searching carseals and updating carseal statuses.
    // TestUpdatePageBaseConfiguration
    // TestUpdatePageSearchFunctionality
    // TestUpdatePageMaxRowsSettings
    /////////////////////////////////////////////////////////
    
    static List<Carseal__c> carsealList = new List<Carseal__c>();
    static List<Id> carsealPlantIdList = new List<Id>();
    static List<Id> carsealUnitIdList = new List<Id>();
    static List<String> carsealPIDList = new List<String>();
    static List<String> carsealRefEquipList = new List<String>();
    static Carseal_Settings__c csSettings = new Carseal_Settings__c();
    
    private static void InitializeCarsealSettings(Integer maxRecords)
    {
        csSettings.Carseal_Audit_Report_Id__c = '';
        csSettings.Maximum_Search_Rows__c = maxRecords;
        insert csSettings;
    }
    
    private static void InitializeCarsealData(Integer carsealNum, Integer plantNum)
    {
        // carsealNum is the number of plants to create, plantNum is the number of plants already created.
        // the number of units to create for each plant is the plant index times 3.  So plant 0 has 3 units, plant 1 has 
        // 6 units, plant 9 has 30 units.
        // So if there 5 plants, then there are 3 + 6 + 9 + 12 + 15 = 45 units.  If there are 10 plants, there 
        // are 3 * ( 1 + 2 + ... + 10) = 3 * 55 = 165 units.

        Integer randomPlantIndex;
        Integer randomUnitIndex;
        for (Integer i = 0 ; i < carsealNum; i++)
        {
            randomPlantIndex = Integer.valueOf(Math.random() * plantNum);
            randomUnitIndex = Integer.valueOf(Math.random() * randomPlantIndex * 3);
            carsealPlantIdList.add(pList[randomPlantIndex].Id);
            carsealUnitIdList.add(puMap.get(pList[randomPlantIndex])[randomUnitIndex].Id);
            carsealPIDList.add('PID ' + i);
            carsealRefEquipList.add('Equipment ' + i);
            carsealList.add(new Carseal__c(Plant__c = carsealPlantIdList[i], Unit__c = carsealUnitIdList[i], P_ID__c = carsealPIDList[i],
                Reference_Equipment__c = carsealRefEquipList[i]));
        }
        insert carsealList;
        
    }
    
    @isTest
    private static void TestUpdatePageBaseConfiguration()
    {
        Integer maxRows = 500;
        InitializeCarsealSettings(maxRows);
        CarsealListController carsealController = new CarsealListController();
        system.assertEquals(maxRows, carsealController.maxSearchRows);
        system.assertEquals(carsealController.maxSearchRows, csSettings.Maximum_Search_Rows__c);
        
    }
    
    @isTest
    private static void TestUpdatePageSearchFunctionality()
    {
        // set up 10 plants and units.
        Integer numberOfPlants = 10;
        Integer numberOfCarseals = 3000;
        InitializeData(numberOfPlants);
        InitializeCarsealData(numberOfCarseals, numberOfPlants);
        system.assertEquals(carsealList.size(), numberOfCarseals);
        
        Integer maxRecords = 5000;
        InitializeCarsealSettings(maxRecords);
        
        // set the page
        Test.setCurrentPage(Page.Carseal_Edit);
        
        CarsealListController carsealController = new CarsealListController();

        Test.startTest();

        // when the page is loaded, there are no result sets
        system.assertEquals(carsealController.carsealSearchResults, new List<Carseal__c>());
        
        // assume user chooses plant item 5, unit item 12.
        List<SelectOption> plantPickList = carsealController.getPlantItems();
        carsealController.plantID = pList[5].id;
        carsealController.RefreshUnitList();
        List<SelectOption> unitPickList = carsealController.getUnitItems();
        carsealController.unitID = puMap.get(pList[5])[12].id;
        carsealController.RefreshCarsealList();
        // after this step, the carseal list is stored in carsealSearchResults.  Go through the carseal list and see
        // if we get the same number of records.
        system.assertEquals([SELECT Count() FROM Carseal__c WHERE Plant__c = :carsealController.plantID AND
            Unit__c = :carsealController.unitID], carsealController.carsealSearchResults.size());
        
        // assume user now uses the search box
        // assume user searches by Carseal ID
        carsealController.carSealSearchOption = 'name';
        carsealController.carsealSearchString = '23';
        carsealController.SearchCarseal();
        Integer searchSize = carsealController.carsealSearchResults.size();
        system.assertEquals([SELECT Count() FROM Carseal__c WHERE Name Like '%23%'], searchSize);
        if (searchSize > 1)
        {
            system.assertEquals(carsealController.displayRecordCountInfo, searchSize + ' carseals returned.');
        }
        else
        {
            system.assertEquals(carsealController.displayRecordCountInfo, searchSize + ' carseal returned.');
        }
        
        // assume user now searches by P&ID
        carsealController.carSealSearchOption = 'pid';
        carsealController.carsealSearchString = '400';
        carsealController.SearchCarseal();
        searchSize = carsealController.carsealSearchResults.size();
        system.assertEquals([SELECT Count() FROM Carseal__c WHERE P_ID__c Like '%400%'], searchSize);
        if (searchSize > 1)
        {
            system.assertEquals(carsealController.displayRecordCountInfo, searchSize + ' carseals returned.');
        }
        else
        {
            system.assertEquals(carsealController.displayRecordCountInfo, searchSize + ' carseal returned.');
        }
        
        // assume user now searches by Reference Equipment
        carsealController.carSealSearchOption = 'equip';
        carsealController.carsealSearchString = 'Equipment';
        carsealController.SearchCarseal();
        // all equipments have the word Equipment, so this should be the same as the number of carseals
        system.assertEquals(carsealController.carsealSearchResults.size(), numberOfCarseals);
        system.assertEquals([SELECT Count() FROM Carseal__c WHERE Reference_Equipment__c Like '%Equipment%'], carsealController.carsealSearchResults.size());
        // since numberOfCarseals = 3000, it is plural
        system.assertEquals(carsealController.displayRecordCountInfo, numberOfCarseals + ' carseals returned.');

        // before any carseal is clicked, the current status info is null
        system.assertEquals(null, carsealController.currCarsealStatus);
                
        // imagine clicking the very first one on the list
        ApexPages.currentPage().getParameters().put('currCarseal', carsealController.carsealSearchResults[0].Id);
        carsealController.showSelectedCarseal();
        system.assertEquals(carsealController.CarsealID, carsealController.carsealSearchResults[0].Id);
        
        // before any statuses are added, there will be no list of statuses
        system.assertEquals(0, carsealController.SelectedCarsealStatuses.size());
        
        // if clicking the Update button, there will be an error because status is not provided
        carsealController.updateStatus();
        system.assertEquals(carsealController.StatusIsWrong, true);
      
        system.assertEquals('Error: Please provide a status.', carsealController.errorMsg);
        
        Carseal_Status_Code__c code = new Carseal_Status_Code__c(Name ='On');
        insert code;
        // now put in a status and save, and this time there are no errors.
        carsealController.currCarsealStatus.Status_Lookup__c = code.Id;
        carsealController.updateStatus();
        
        // now save it again, this time an error that there is no change in status
        carsealController.updateStatus();
        system.assertEquals(carsealController.StatusIsWrong, true);
        system.assertEquals('Error: You cannot create a new carseal status without updating the status.', carsealController.errorMsg);
        // So far one status has been added.  Check the count
        system.assertEquals(1, carsealController.SelectedCarsealStatuses.size());
        
        Test.stopTest();
    }   
        
    @isTest
    private static void TestUpdatePageMaxRowsSettings()
    {
        // In the last test we tested general function.  If there are 3000 equipments with the word 'Equipment',
        // and then you set the maximum rows to 5, then the search result will have 6 rows, and the VF page
        // needs to display the error message.
        
        // set up 10 plants and units.
        Integer numberOfPlants = 10;
        Integer numberOfCarseals = 30;
        InitializeData(numberOfPlants);
        InitializeCarsealData(numberOfCarseals, numberOfPlants);
        system.assertEquals(carsealList.size(), numberOfCarseals);
        
        Integer maxRecords = 5;
        InitializeCarsealSettings(maxRecords);
        
        // set the page
        Test.setCurrentPage(Page.Carseal_Edit);
        
        CarsealListController carsealController = new CarsealListController();

        Test.startTest();

        // assume user now searches by Reference Equipment
        carsealController.carSealSearchOption = 'equip';
        carsealController.carsealSearchString = 'Equipment';
        carsealController.SearchCarseal();
        // all equipments have the word Equipment, so this should be the same as the number of carseals
        system.assertEquals(carsealController.carsealSearchResults.size(), maxRecords + 1);
        system.assertEquals(carsealController.displayRecordCountInfo, maxRecords + '+ carseals returned.');
        
        Test.stopTest();
    }   
        
    
}