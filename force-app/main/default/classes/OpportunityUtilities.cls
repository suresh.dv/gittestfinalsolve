public class OpportunityUtilities {
    public static String getUserRoleName(){
        String roleName = '';
        User userDetails =[SELECT UserRole.Name FROM User WHERE Id=:UserInfo.getUserId()];
        if( userDetails != null){
            roleName = userDetails.UserRole.Name;            
        }
        return roleName;
    }
    
    public static Boolean isCardlockFleetRole(String roleName){
    	Set<String> setIds = new Set<String>{
            'CPM Commercial Sales Manager', 
            'Commercial Fuel Account Representative', 
            'CPM Cardlock Account Manger', 
            'Cardlock Sales Representative', 
            'Fleet Sales Representative'
        };    
        
        return setIds.contains(roleName);
    }
    
    public static Boolean isWholesaleRole(String roleName){
    	Set<String> setIds = new Set<String>{
            'National Wholesale Manager Refinery Products'
        };    

        return setIds.contains(roleName);
    }
}