public with sharing class ShiftConfigurationValidation {
	Map<String, List<Shift_Configuration__c>> shiftMap = new Map<String, List<Shift_Configuration__c>>();
    
    /*
     *    @description compares all new and existing shift records to check whether they are operlapping.
     */
    public void validateAll(List<Shift_Configuration__c> newShiftList){
        // Holds all records present in salesforce which are Enabled
        List<Shift_Configuration__c> shiftList = new List<Shift_Configuration__c>();
        
        try{
            shiftList.addAll([SELECT Id, Name, End_Time__c, Friday__c, Monday__c, Tuesday__c,
                                     Saturday__c, Start_Time__c, Sunday__c, Thursday__c,
                                     Wednesday__c, Enabled__c, RecordTypeId
                             FROM Shift_Configuration__c
                             WHERE Enabled__c = true]);
            
            shiftList.addAll(newShiftList);
            
            List<Shift_Configuration__c> dayShiftList;
            
            for(Shift_Configuration__c newShift : shiftList){
                if(newShift.Enabled__c){
					shiftMap = ShiftConfigurationValidationHelper.processShiftConfig(shiftMap, newShift);
                }
            }
          
            for(Shift_Configuration__c newShift : newShiftList)
            {
                if(newShift.Enabled__c)
                {                    
                    for(Shift_Configuration__c storedShift : shiftList)
                    {
                        if (newShift.RecordTypeId == storedShift.RecordTypeId)
                        {
                        	if(storedShift.Enabled__c && isOverlapping(newShift, storedShift) 
	                           && ((storedShift.Id != newShift.Id && !String.isBlank(storedShift.Id))
	                               || (String.isBlank(storedShift.Id) && newShift.Name != storedShift.Name)))
	                        {
	                            newShift.addError('This record has overlapping time with existing record.');
	                        }
                        }
                    }
                }
            }
        } catch(Exception e) {
            System.debug(LoggingLevel.ERROR, e.getMessage());
        }
    }
    
    /*
     *    @description checks whether new shift record has overlapping time with stored record or not
     */
    public boolean isOverlapping(Shift_Configuration__c newShift, Shift_Configuration__c storedShift){
        
        Boolean isOverlappingTime = false;
        Boolean isPrevDayOverlapped = false;
        Boolean isNextDayOverlapped = false;
        Integer newShiftStartTime = Integer.valueOf(newShift.Start_Time__c);
        Integer newShiftEndTime = Integer.valueOf(newShift.End_Time__c);
        Integer storedShiftStartTime = Integer.valueOf(storedShift.Start_Time__c);
        Integer storedShiftEndTime = Integer.valueOf(storedShift.End_Time__c);
        
        if(storedShiftStartTime >= storedShiftEndTime){
        	
            if(newShiftStartTime < newShiftEndTime){
            	
                if(newShiftStartTime >= storedShiftStartTime || newShiftEndTime > storedShiftStartTime){
                    isOverlappingTime = true;
                }
                
                isPrevDayOverlapped = isOverlapWithPrevDay(newShift, storedShift);
                
                if(!isOverlappingTime){
                    isOverlappingTime = isPrevDayOverlapped;
                }
            } else if(newShiftStartTime > newShiftEndTime){
            	 
                if(newShiftStartTime >= storedShiftStartTime){
                    isOverlappingTime = true;
                }
                
                isNextDayOverlapped = isOverlapWithNextDay(newShift, storedShift);
                
                if(!isOverlappingTime){
                    isOverlappingTime = isNextDayOverlapped;
                }
            } else {
            	
                if(newShiftStartTime <= storedShiftStartTime){
                    isOverlappingTime = true;
                }
                
                isPrevDayOverlapped = isOverlapWithPrevDay(newShift, storedShift);
                
                if(!isOverlappingTime){
                    isOverlappingTime = isPrevDayOverlapped;
                }
              
                if(newShiftEndTime > 0){
                    isNextDayOverlapped = isOverlapWithNextDay(newShift, storedShift);
                }
               
                if(!isOverlappingTime){
                    isOverlappingTime = isNextDayOverlapped;
                }
              
            }
        } else {
        	
            if(newShiftStartTime < newShiftEndTime){
            
                if((newShiftStartTime >= storedShiftStartTime && newShiftStartTime < storedShiftEndTime)
                   || (newShiftEndTime > storedShiftStartTime && newShiftEndTime <= storedShiftEndTime)
                   || (newShiftStartTime < storedShiftStartTime && newShiftEndTime > storedShiftEndTime)){
                   
                   isOverlappingTime = true;
                }
                
                isPrevDayOverlapped = isOverlapWithPrevDay(newShift, storedShift);
                
                if(!isOverlappingTime){
                    isOverlappingTime = isPrevDayOverlapped;
                }
            } else if(newShiftStartTime > newShiftEndTime){
            	
               if(newShiftStartTime <= storedShiftStartTime || newShiftStartTime < storedShiftEndTime){
                    isOverlappingTime = true;
                  
                }
                
                isNextDayOverlapped = isOverlapWithNextDay(newShift, storedShift);
                
                if(!isOverlappingTime){
                    isOverlappingTime = isNextDayOverlapped;
                   
                }
            } else {
            	
                if(newShiftStartTime <= storedShiftStartTime || newShiftStartTime < storedShiftEndTime){
                    isOverlappingTime = true;
                }
                
                isPrevDayOverlapped = isOverlapWithPrevDay(newShift, storedShift);
                
                if(!isOverlappingTime){
                    isOverlappingTime = isPrevDayOverlapped;
                }
                
                if(newShiftEndTime > 0){
                    isNextDayOverlapped = isOverlapWithNextDay(newShift, storedShift);
                }
                
                if(!isOverlappingTime){
                    isOverlappingTime = isNextDayOverlapped;
                }
            }
        }        
     
        if(isOverlappingTime){
               if((storedShift.Sunday__c && newShift.Sunday__c)
                  || (storedShift.Monday__c && newShift.Monday__c)
                  || (storedShift.Tuesday__c && newShift.Tuesday__c)
                  || (storedShift.Wednesday__c && newShift.Wednesday__c)
                  || (storedShift.Thursday__c && newShift.Thursday__c)
                  || (storedShift.Friday__c && newShift.Friday__c)
                  || (storedShift.Saturday__c && newShift.Saturday__c)
                  || isNextDayOverlapped || isPrevDayOverlapped)
                   return true;
        }
        return false;
    }
    
    /*
     *    @description checks whether new shift record has overlapping time with next day
     */
    public boolean isOverlapWithNextDay(Shift_Configuration__c newShift, Shift_Configuration__c storedShift){
        List<Shift_Configuration__c> dayShiftList = new List<Shift_Configuration__c>();
        Boolean isPartialOverlapped = false;
        Integer newShiftStartTime = Integer.valueOf(newShift.Start_Time__c);
        Integer newShiftEndTime = Integer.valueOf(newShift.End_Time__c);
        Integer storedShiftStartTime = Integer.valueOf(storedShift.Start_Time__c);
        Integer storedShiftEndTime = Integer.valueOf(storedShift.End_Time__c);
        
        if(newShiftStartTime < storedShiftStartTime){
            isPartialOverlapped = true;
        }
        
        if(newShift.Sunday__c && shiftMap.get('Monday') != Null){
            dayShiftList.addAll(shiftMap.get('Monday'));
        }
        if(newShift.Monday__c && shiftMap.get('Tuesday') != Null){
            dayShiftList.addAll(shiftMap.get('Tuesday'));
        }
        if(newShift.Tuesday__c && shiftMap.get('Wednesday') != Null){
            dayShiftList.addAll(shiftMap.get('Wednesday'));
        }
        if(newShift.Wednesday__c && shiftMap.get('Thursday') != Null){
            dayShiftList.addAll(shiftMap.get('Thursday'));
        }
        if(newShift.Thursday__c && shiftMap.get('Friday') != Null){
            dayShiftList.addAll(shiftMap.get('Friday'));
        }
        if(newShift.Friday__c && shiftMap.get('Saturday') != Null){
            dayShiftList.addAll(shiftMap.get('Saturday'));
        }
        if(newShift.Saturday__c && shiftMap.get('Sunday') != Null){
            dayShiftList.addAll(shiftMap.get('Sunday'));
        }
        
        if(dayShiftList != null){
            for(Shift_Configuration__c currentShift : dayShiftList){
                storedShiftStartTime = Integer.valueOf(currentShift.Start_Time__c);
                storedShiftEndTime = Integer.valueOf(currentShift.End_Time__c);
                
                if(newShiftEndTime > storedShiftStartTime || (isPartialOverlapped && newShiftEndTime > storedShiftEndTime)){
                   
                    return true;
                }
            }
        }
        
        return false;
    }
    
    /*
     *    @description checks whether new shift record has overlapping time with Previous day
     */
    public boolean isOverlapWithPrevDay(Shift_Configuration__c newShift, Shift_Configuration__c storedShift){
        List<Shift_Configuration__c> dayShiftList = new List<Shift_Configuration__c>();
        Boolean isPartialOverlapped = false;
        Integer newShiftStartTime = Integer.valueOf(newShift.Start_Time__c);
        Integer newShiftEndTime = Integer.valueOf(newShift.End_Time__c);
        Integer storedShiftStartTime = Integer.valueOf(storedShift.Start_Time__c);
        Integer storedShiftEndTime = Integer.valueOf(storedShift.End_Time__c);
        
        if(newShift.Sunday__c && shiftMap.get('Saturday') != Null){
            dayShiftList.addAll(shiftMap.get('Saturday'));
        }
        
        if(newShift.Monday__c && shiftMap.get('Sunday') != Null){
            dayShiftList.addAll(shiftMap.get('Sunday'));
        }
        if(newShift.Tuesday__c && shiftMap.get('Monday') != Null){
            dayShiftList.addAll(shiftMap.get('Monday'));
        }
        if(newShift.Wednesday__c && shiftMap.get('Tuesday') != Null){
            dayShiftList.addAll(shiftMap.get('Tuesday'));
        }
        if(newShift.Thursday__c && shiftMap.get('Wednesday') != Null){
            dayShiftList.addAll(shiftMap.get('Wednesday'));
        }
        if(newShift.Friday__c && shiftMap.get('Thursday') != Null){
            dayShiftList.addAll(shiftMap.get('Thursday'));
        }
        if(newShift.Saturday__c && shiftMap.get('Friday') != Null){
            dayShiftList.addAll(shiftMap.get('Friday'));
        }
        
        if(dayShiftList != null){
            for(Shift_Configuration__c currentShift : dayShiftList){
                storedShiftStartTime = Integer.valueOf(currentShift.Start_Time__c);
                storedShiftEndTime = Integer.valueOf(currentShift.End_Time__c);
                if(storedShiftEndTime <= storedShiftStartTime){
                    if(newShiftStartTime < storedShiftEndTime || newShiftEndTime <= storedShiftEndTime){
                        return true;
                    }
                }
            }
        }
        
        return false;
    }
}