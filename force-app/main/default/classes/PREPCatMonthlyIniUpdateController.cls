public class PREPCatMonthlyIniUpdateController 
{

	public String iniID {get;set;}
	public String CatManager {get;set;}

	public Integer iniMonth {get;set;}
	public Integer iniYear {get;set;}

	public PREP_Initiative__c ini {get;set;}
	public PREP_Baseline__c baselineObj {get;set;}
	public PREP_Initiative_Status_Update__c isuObj {get;set;}
	public PREP_Initiative_Status_Update_Comment__c ISUCobj {get;set;}
	public PREP_Initiative_Status_Update__c IniStatUpdate {get;set;}

	public Date ISUDate1 {get;set;}
	public Date ISUDate2 {get;set;}
	public Date ISUDate3 {get;set;}
	public Date ISUDate4 {get;set;}
	public Date ISUDate5 {get;set;}
	public Date ISUDate6 {get;set;}
	public Date ISUDate7 {get;set;}
	public Date ISUDate8 {get;set;}
	public Date ISUDateCAC1 {get;set;}
	public Date ISUDateCAC2 {get;set;}

	public Boolean renderOutput {get;set;}

	public List<statusObject> tableRows {get;set;}
	public List<checkboxObject> checkboxRows {get;set;}
	public List<mainDetailsObject> mainRows {get;set;}
	public List<savingsObject> savRows {get;set;}

	public String techServ {get;set;}
	public String scheComments {get;set;}
	public String globalSrc {get;set;}


	public Integer savCount {get;set;}
	public Integer savSum {get;set;}

	public Decimal testSum {get;set;}

	public Boolean switchVal {get;set;}

	
	public PREPCatMonthlyIniUpdateController() 
	{
		
	}

	public void processRecords()
	{
		if(iniID != null && iniID != '')
		{

			tableRows = new List<statusObject>();
			checkboxRows = new List<checkboxObject>();
			mainRows = new List<mainDetailsObject>();
			savRows = new List<savingsObject>();

			String prepIniId = 'PREP-' + iniId;

			List<PREP_Initiative__c> iniList = [SELECT Id, Status__c, Initiative_Name__c, Global_Sourcing__c, Aboriginal__c, Ariba__c, Workbook__c, 
			Rate_Validation__c, Payment_Terms__c, Rebate__c, Tech_Services__c, PMO__c, OEM__c,
			BU_SME_1__r.Name, BU_SME_2__r.Name, BU_SME_3__r.Name, HSSM_Package__c, Reverse_Auction__c, SRPM_Package__c,
			Category_Manager__r.Name, Category_Specialist__r.Name, Technician__r.Name, Business_Analyst__r.Name, 
			Discipline__r.Name, Risk_Level__c, (SELECT Id FROM Business_Value_Submissions__r) 
			FROM PREP_Initiative__c WHERE Name = :prepIniId];

			if(iniList.size() > 0)
			{
				ini = iniList[0];

				switchVal = true;

				List<PREP_Baseline__c> baseline = 
				[SELECT Id, X01_Segmentation_Team_Selection__c, X02_Business_Requirements__c,
				X03_Supplier_Market_Analysis__c, X04_Sourcing_Options__c,
				X05_Going_To_Market__c, X06_Negotiation__c,
				X07_Contract_Start_Up__c, X08_Supplier_Performance_Management__c,
				XCAC1_CAC_Strategy_Approved__c, XCAC2_CAC_Award_Approval__c,
				Baseline_Spend_Dollar_Target__c, Local_Baseline_Savings_Dollar__c, Local_Baseline_Savings_Percent__c
				FROM PREP_Baseline__c WHERE Initiative_Id__c = :ini.Id];

				baselineObj = baseline[0];

				List<PREP_Initiative_Status_Update__c> ISU = 
				[SELECT Id, RecordTypeId, LastModifiedDate, Savings_Status__c, Schedule_Status_01__c, Schedule_Status_02__c, Schedule_Status_03__c, Schedule_Status_04__c, Schedule_Status_05__c,
				Schedule_Status_06__c, Schedule_Status_07__c, Schedule_Status_08__c, Schedule_Status_CAC1__c, Schedule_Status_CAC2__c,
				Schedule_Comments__c, Local_Forecast_Savings_Dollar__c, Local_Forecast_Savings_Percent__c, Update_Date__c, Forecast_Date_01__c, Forecast_Date_02__c,
				Forecast_Date_03__c, Forecast_Date_04__c, Forecast_Date_05__c, Forecast_Date_CAC1__c,
				Forecast_Date_06__c, Forecast_Date_07__c, Forecast_Date_08__c, Forecast_Date_CAC2__c,
				Actual_Date_01__c, Actual_Date_02__c, Actual_Date_03__c, Actual_Date_04__c, Actual_Date_CAC1__c,
				Actual_Date_05__c, Actual_Date_06__c, Actual_Date_07__c, Actual_Date_08__c, Actual_Date_CAC2__c,
				Aboriginal__c, Ariba__c, Global_Sourcing__c, HSSM_Package__c, OEM__c, Payment_Terms__c, 
				Rate_Validation__c, Reverse_Auction__c, SRPM_Package__c, Workbook__c, Rebate__c
				FROM PREP_Initiative_Status_Update__c WHERE CALENDAR_MONTH(Update_Date__c) = :iniMonth 
				AND CALENDAR_YEAR(Update_Date__c) = :iniYear 
				AND Baseline_id__c = :baselineObj.Id ORDER BY Update_Date__c DESC, LastModifiedDate DESC LIMIT 1];

				if(ISU.size() > 0)
				{

					isuObj = ISU[0];

					switchVal = false;

					Id localRTI = Schema.SObjectType.PREP_Initiative_Status_Update__c.getRecordTypeInfosByName().get('Global Initiative Status Update').getRecordTypeId();
    				Id globalRTI = Schema.SObjectType.PREP_Initiative_Status_Update__c.getRecordTypeInfosByName().get('Local Initiative Status Update').getRecordTypeId();

    				if(isuObj.RecordTypeId == localRTI ) {globalSrc = 'No';} else {globalSrc = 'Yes';}

					if( isuObj.Actual_Date_01__c != null ) { ISUDate1 = isuObj.Actual_Date_01__c; } else { ISUDate1 = isuObj.Forecast_Date_01__c; }
					if( isuObj.Actual_Date_02__c != null ) { ISUDate2 = isuObj.Actual_Date_02__c; } else { ISUDate2 = isuObj.Forecast_Date_02__c; }
					if( isuObj.Actual_Date_03__c != null ) { ISUDate3 = isuObj.Actual_Date_03__c; } else { ISUDate3 = isuObj.Forecast_Date_03__c; }
					if( isuObj.Actual_Date_04__c != null ) { ISUDate4 = isuObj.Actual_Date_04__c; } else { ISUDate4 = isuObj.Forecast_Date_04__c; }
					if( isuObj.Actual_Date_05__c != null ) { ISUDate5 = isuObj.Actual_Date_05__c; } else { ISUDate5 = isuObj.Forecast_Date_05__c; }
					if( isuObj.Actual_Date_06__c != null ) { ISUDate6 = isuObj.Actual_Date_06__c; } else { ISUDate6 = isuObj.Forecast_Date_06__c; }
					if( isuObj.Actual_Date_07__c != null ) { ISUDate7 = isuObj.Actual_Date_07__c; } else { ISUDate7 = isuObj.Forecast_Date_07__c; }
					if( isuObj.Actual_Date_08__c != null ) { ISUDate8 = isuObj.Actual_Date_08__c; } else { ISUDate8 = isuObj.Forecast_Date_08__c; }
					if( isuObj.Actual_Date_CAC1__c != null ) { ISUDateCAC1 = isuObj.Actual_Date_CAC1__c; } else { ISUDateCAC1 = isuObj.Forecast_Date_CAC1__c; }
					if( isuObj.Actual_Date_CAC2__c != null ) { ISUDateCAC2 = isuObj.Actual_Date_CAC2__c; } else { ISUDateCAC2 = isuObj.Forecast_Date_CAC2__c; }

					List<PREP_Initiative_Status_Update_Comment__c> ISUC = 
					[SELECT Id, Main_Stakeholders_Resourcing_Status__c, Market__c,
					Resourcing_Status_Comments__c, Current_Status__c, Issues_Concerns__c
					FROM PREP_Initiative_Status_Update_Comment__c 
					WHERE Initiative_Status_Update_Id__c = :isuObj.Id ORDER BY CreatedDate desc LIMIT 1];

					if(ini.Tech_Services__c == null &&  ini.PMO__c == null){ techServ = 'No';} else{ techServ = 'Yes';}

					tableRows.add(new statusObject('1', 'Segmentation & Team Selection', ISUDate1, baselineObj.X01_Segmentation_Team_Selection__c, isuObj.Schedule_Status_01__c));
					tableRows.add(new statusObject('2', 'Business Requirements', ISUDate2, baselineObj.X02_Business_Requirements__c, isuObj.Schedule_Status_02__c));
					tableRows.add(new statusObject('3', 'Supply Market Analysis', ISUDate3, baselineObj.X03_Supplier_Market_Analysis__c, isuObj.Schedule_Status_03__c));
					tableRows.add(new statusObject('4', 'Sourcing Options', ISUDate4, baselineObj.X04_Sourcing_Options__c, isuObj.Schedule_Status_04__c));
					tableRows.add(new statusObject('', 'Strategy Approved', ISUDateCAC1, baselineObj.XCAC1_CAC_Strategy_Approved__c, isuObj.Schedule_Status_CAC1__c));
					tableRows.add(new statusObject('5', 'Market Test', ISUDate5, baselineObj.X05_Going_To_Market__c, isuObj.Schedule_Status_05__c));
					tableRows.add(new statusObject('', 'Award Approved', ISUDateCAC2, baselineObj.XCAC2_CAC_Award_Approval__c, isuObj.Schedule_Status_CAC2__c));
					tableRows.add(new statusObject('6', 'Contract Award', ISUDate6, baselineObj.X06_Negotiation__c, isuObj.Schedule_Status_06__c));
					tableRows.add(new statusObject('7', 'Contract Start Up', ISUDate7, baselineObj.X07_Contract_Start_Up__c, isuObj.Schedule_Status_07__c));
					tableRows.add(new statusObject('8', 'Suppliers Performance Management', ISUDate8, baselineObj.X08_Supplier_Performance_Management__c, isuObj.Schedule_Status_08__c));

					savRows.add(new savingsObject(baselineObj.Local_Baseline_Savings_Dollar__c, baselineObj.Local_Baseline_Savings_Percent__c, isuObj.Local_Forecast_Savings_Dollar__c, isuObj.Local_Forecast_Savings_Percent__c, isuObj.Savings_Status__c));

					renderOutput = true;

					if(ISUC.size() > 0)
					{
						ISUCobj = ISUC[0];

					}

					else
					{
						ISUCobj = null;
					}
					
				}

				else
				{
					ini = null;
					isuObj = null;
					ISUCobj = null;
					return;
				}	

				if(switchVal == true)
				{
					checkboxRows.add(new checkboxObject('Aboriginal', ini.Aboriginal__c));
					checkboxRows.add(new checkboxObject('Ariba', ini.Ariba__c));
					checkboxRows.add(new checkboxObject('Global Opportunity', ini.Global_Sourcing__c));
					checkboxRows.add(new checkboxObject('HSSM Package', ini.HSSM_Package__c));
					checkboxRows.add(new checkboxObject('OEM', ini.OEM__c));
					checkboxRows.add(new checkboxObject('Payment Terms', ini.Payment_Terms__c));
					checkboxRows.add(new checkboxObject('Rate Validated', ini.Rate_Validation__c));
					checkboxRows.add(new checkboxObject('Rebate Program', ini.Rebate__c));
					checkboxRows.add(new checkboxObject('Reverse Auction', ini.Reverse_Auction__c));
					checkboxRows.add(new checkboxObject('SRPM Package', ini.SRPM_Package__c));
					checkboxRows.add(new checkboxObject('Workbook', ini.Workbook__c));
				}

				else if(switchVal == false)
				{
					checkboxRows.add(new checkboxObject('Aboriginal', isuObj.Aboriginal__c));
					checkboxRows.add(new checkboxObject('Ariba', isuObj.Ariba__c));
					checkboxRows.add(new checkboxObject('Global Opportunity', isuObj.Global_Sourcing__c));
					checkboxRows.add(new checkboxObject('HSSM Package', isuObj.HSSM_Package__c));
					checkboxRows.add(new checkboxObject('OEM', isuObj.OEM__c));
					checkboxRows.add(new checkboxObject('Payment Terms', isuObj.Payment_Terms__c));
					checkboxRows.add(new checkboxObject('Rate Validated', isuObj.Rate_Validation__c));
					checkboxRows.add(new checkboxObject('Rebate Program', isuObj.Rebate__c));
					checkboxRows.add(new checkboxObject('Reverse Auction', isuObj.Reverse_Auction__c));
					checkboxRows.add(new checkboxObject('SRPM Package', isuObj.SRPM_Package__c));
					checkboxRows.add(new checkboxObject('Workbook', isuObj.Workbook__c));
				}

			}

			else
			{
				ini = null;
				ApexPages.addmessage(new ApexPages.message(ApexPages.severity.WARNING,'Please enter value'));
				return;
			}
		}
	}

	public class statusObject
	{	
		public String stepNum {get;set;}
		public String stepName {get;set;}
		public String baseDate {get;set;}
		public String isuDate {get;set;}
		public String statusLight {get;set;}

		public statusObject(String step, String name, Date i, Date b, String light)
		{
			stepNum = step;
			stepName = name;
			baseDate = String.valueOf(b);
			isuDate = String.valueOf(i);
			statusLight = light;
		}
	}

	public class checkboxObject
	{	
		public String checkName {get;set;}
		public String checkStatus {get;set;}

		public checkboxObject(String name, String status)
		{
			checkName = name;
			checkStatus = status;
		}
	}

	public class mainDetailsObject
	{	
		public String catM {get;set;}
		public String catS {get;set;}
		public String tech {get;set;}
		public String busAna {get;set;}
		public String aSpend {get;set;}
		public String disc {get;set;}
		public String hssm {get;set;}
		public String riskLvl {get;set;}

		public mainDetailsObject(String catM, String catS, String tech, String busAna, String aSpend, String disc, String hssm, String riskLvl)
		{
			this.catM = catM;
			this.catS = catS;
			this.tech = tech;
			this.busAna = busAna;
			this.aSpend = aSpend;
			this.disc = disc;
			this.hssm = hssm;
			this.riskLvl = riskLvl;
		}
	}

	public class savingsObject
	{	
		public Decimal bstDA {get;set;}
		public Decimal bstPer {get;set;}
		public Decimal aeDA {get;set;}
		public Decimal aePer {get;set;}
		public String sLight {get;set;}

		public savingsObject(Decimal bstDA2, Decimal bstPer2, Decimal aeDA2, Decimal aePer2, String sLight)
		{
			this.sLight = sLight;
			this.bstDA = bstDA2;
			this.bstPer = bstPer2;
			this.aeDA = aeDA2;
			this.aePer = aePer2;
		}
	}
}