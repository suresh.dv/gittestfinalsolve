/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 *
 * Unit tests are class methods that verify whether a particular piece
 * of code is working properly. Unit test methods take no arguments,
 * commit no data to the database, and are flagged with the testMethod
 * keyword in the method definition.
 *
 * All test methods in an organization are executed whenever Apex code is deployed
 * to a production organization to confirm correctness, ensure code
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployed
 * to a production organization. In addition, all triggers must have some code coverage.
 * 
 * The @isTest class annotation indicates this class only contains test
 * methods. Classes defined with the @isTest annotation do not count against
 * the organization size limit for all Apex scripts.
 *
 * See the Apex Language Reference for more information about Testing and Code Coverage.
 */
@isTest
private class TestATSTriggers {

    static testMethod void testPopulateWonCustomer() {
        User runningUser = UserTestData.createTestsysAdminUser();
        insert runningUser;
        System.AssertNotEquals(runningUser.Id, Null);
        
        System.runAs(runningUser) {
            Account account= AccountTestData.createAccount('Asphalt Acc', Null);
            account.RecordTypeId = ATSTestData.getAsphaltRecordType().Id;
            account.ATS_Customer_Type__c = 'Emulsion';
                
            insert account;
            System.AssertNotEquals(account.Id, Null);
            
            Contact cont = ContactTestData.createContact(account.Id, 'FName', 'LName');
            insert cont; 
                
            User alphaltUser = UserTestData.createAsphalUser();    
            insert alphaltUser;
            
            ATS_Parent_Opportunity__c tender = ATSTestData.createTender('123456', datetime.now(), alphaltUser);
            insert tender; 
            
            ATS_Tender_Customers__c customer = ATSTestData.createTenderCustomer(account.Id, tender.id, cont.id);
            customer.Won__c = true;
            insert customer;  
            
            customer.Won__c = false;
            update customer;
            
            delete customer;
            undelete customer; 
        }
    }
    
    static testMethod void testUpdateATSFreight()
    {
        
        User runningUser = UserTestData.createTestsysAdminUser();
        insert runningUser;
        System.AssertNotEquals(runningUser.Id, Null);
        
        System.runAs(runningUser) {
        
            Account account= AccountTestData.createAccount('Asphalt Acc', Null);
            account.RecordTypeId = ATSTestData.getAsphaltRecordType().Id;
            account.ATS_Customer_Type__c = 'Emulsion';
                
            insert account;
            
            User alphaltUser = UserTestData.createAsphalUser();    
            insert alphaltUser;
            
            ATS_Parent_Opportunity__c tender = ATSTestData.createTender('123456', datetime.now(), alphaltUser);
            insert tender;
             
            Opportunity opp = ATSTestData.CreateOpportunity(account.Id, tender.Id, 'Prospecting');
            insert opp;
            ATS_Freight__c freight = ATSTestData.createATSFreight(opp.Id);
            insert freight;
            delete freight;
            undelete freight;
        }
    }
    static testMethod void testUpdateTenderWorkflow()
    {
        
        User runningUser = UserTestData.createTestUser();
        insert runningUser;
        System.AssertNotEquals(runningUser.Id, Null);
        
        System.runAs(runningUser) {
            
            Account account= AccountTestData.createAccount('Asphalt Acc', Null);
            account.RecordTypeId = ATSTestData.getAsphaltRecordType().Id;
            account.ATS_Customer_Type__c = 'Emulsion';
                
            insert account;
            
            User alphaltUser = UserTestData.createAsphalUser();    
            insert alphaltUser;
            
            ATS_Parent_Opportunity__c tender = ATSTestData.createTender('123456', datetime.now(), alphaltUser);
            insert tender;
            
            Map<String, Schema.Recordtypeinfo> rtByName = Opportunity.SObjectType.getDescribe().getRecordTypeInfosByName();
      
            Opportunity opp1 = ATSTestData.CreateOpportunity(account.Id, tender.Id, 'Opportunity Initiation - Marketer');
            opp1.RecordTypeId = rtByName.get('ATS: Asphalt Product Category').getRecordTypeId();
            insert opp1;
            
            Account account1 = AccountTestData.createAccount('Asphalt Acc test', Null);
            account1.RecordTypeId = ATSTestData.getAsphaltRecordType().Id;
            insert account1;
            
            Opportunity opp2 = ATSTestData.CreateOpportunity(account1.Id, tender.Id, 'Freight Details Analysis - Logistics');
            opp2.RecordTypeId = rtByName.get('ATS: Emulsion Product Category').getRecordTypeId();
            insert opp2;
            
            opp1.StageName = 'Lost';
            update opp1;
            
            
            opp1.StageName = 'Won';
            opp1.Confirmation_Method__c = 'Signed Quote';
            opp1.Carrier_Winner_Multi__c = 'Balaniuk-Yorkton, 9601';
            update opp1;
            
            opp2.StageName = 'Won';
            opp2.Confirmation_Method__c = 'Signed Quote';
            opp2.Carrier_Winner_Multi__c = 'Balaniuk-Yorkton, 9601';
            update opp1;
        
        }
    }

    static testMethod void testPopulateSupplierAndCarrierOpportunity() 
    {
        User runningUser = UserTestData.createTestUser();
        insert runningUser;
        System.AssertNotEquals(runningUser.Id, Null);

        System.runAs(runningUser) {
        
            Account account= AccountTestData.createAccount('Asphalt Acc', Null);
            account.RecordTypeId = ATSTestData.getAsphaltRecordType().Id;
            account.ATS_Customer_Type__c = 'Emulsion';
                
            insert account;
            
            User alphaltUser = UserTestData.createAsphalUser();    
            insert alphaltUser;
            
            ATS_Parent_Opportunity__c tender = ATSTestData.createTender('123456', datetime.now(), alphaltUser);
            insert tender;
             
            Opportunity opp = ATSTestData.CreateOpportunity(account.Id, tender.Id, 'Prospecting');
            insert opp;
            ATS_Freight__c freight = ATSTestData.createATSFreight(opp.Id);
            freight.Husky_Supplier_1__c = 'Burnco--9796--9796';
            freight.Husky_Supplier_2__c = 'CES Edson Terminal (EX: ES03)--9787--9787';
            freight.Emulsions_Carrier_Supplier1__c = 'Balaniuk-Yorkton, 9601';
            freight.Emulsions_Carrier_Supplier2__c = 'Cal Trucking-Calgary AB, 3500';
            insert freight;
            System.assertEquals('Burnco--9796--9796', freight.Husky_Supplier_1__c);
            System.assertEquals('CES Edson Terminal (EX: ES03)--9787--9787', freight.Husky_Supplier_2__c);
            System.assertEquals('Balaniuk-Yorkton, 9601', freight.Emulsions_Carrier_Supplier1__c);
            System.assertEquals('Cal Trucking-Calgary AB, 3500', freight.Emulsions_Carrier_Supplier2__c);


            freight.Husky_Supplier_1_Selected__c = true;
            freight.Husky_Supplier_2_Selected__c = false;
            update freight;
            Opportunity testopp = [Select Id, Supplier_Winner__c, Carrier_Winner_Multi__c
                From Opportunity Where Id =: opp.Id];
            System.assertEquals(testopp.Supplier_Winner__c, 'Burnco--9796--9796');
            System.assertEquals(testopp.Carrier_Winner_Multi__c, 'Balaniuk-Yorkton, 9601');

            freight.Husky_Supplier_1_Selected__c = false;
            freight.Husky_Supplier_2_Selected__c = true;
            update freight;
            testopp = [Select Id, Supplier_Winner__c, Carrier_Winner_Multi__c
                From Opportunity Where Id =: opp.Id];
            System.assertEquals(testopp.Supplier_Winner__c, 'CES Edson Terminal (EX: ES03)--9787--9787');
            System.assertEquals(testopp.Carrier_Winner_Multi__c, 'Cal Trucking-Calgary AB, 3500');

            freight.Husky_Supplier_1_Selected__c = false;
            freight.Husky_Supplier_2_Selected__c = false;
            update freight;
            testopp = [Select Id, Supplier_Winner__c, Carrier_Winner_Multi__c
                From Opportunity Where Id =: opp.Id];
            System.assertEquals(testopp.Supplier_Winner__c, null);
            System.assertEquals(testopp.Carrier_Winner_Multi__c, null);
        }
    } 
    

}