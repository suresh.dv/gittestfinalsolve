global virtual with sharing class DSP_CMS_ReportCard extends DSP_CMS_ReportCardController
{
    global override String getHTML()
    {
        return getReportCardHTML();
    }
}