@IsTest
public class VRMtrgVendorRequestApprover {
    static testmethod void trgVendorRequestApprover(){
        
        jbrvrm__Vendor_Marketplace__c v = new jbrvrm__Vendor_Marketplace__c();
        v.name = 'test';
        v.jbrvrm__type__c = 'Contingent Labor';
        insert v;
        system.assertnotequals(v.id,null);
        
        jbrvrm__Vendor_Marketplace__c v1 = new jbrvrm__Vendor_Marketplace__c();
        v1.name = 'test';
        v1.jbrvrm__type__c = 'Contingent Labor';
        insert v1;
        system.assertnotequals(v1.id,null);
    
       
        account a =new account();
        a.name='test';
        a.type='Vendor';
        insert a;
         
        jbrvrm__Marketplace_Vendor__c mv=new jbrvrm__Marketplace_Vendor__c();
        mv.jbrvrm__Marketplace_Vendor__c=a.id;
        mv.jbrvrm__Vendor_Marketplace__c=v.id;
        insert mv;
        
        jbrvrm__Marketplace_Vendor__c mv1=new jbrvrm__Marketplace_Vendor__c();
        mv1.jbrvrm__Marketplace_Vendor__c=a.id;
        mv1.jbrvrm__Vendor_Marketplace__c=v1.id;
        insert mv1;
        
        
         jbrvrm__Vendor_Requisition__c ven = new jbrvrm__Vendor_Requisition__c();
         ven.jbrvrm__Estimated_Spend__c=4.7;
         ven.name='test1';
         ven.jbrvrm__Vendor_Marketplace__c=v.id;
         ven.jbrvrm__Vendor__c=a.id;
         ven.jbrvrm__Competitive_Process__c='Competitive Process-Proponents Selected';
         ven.jbrvrm__Payment_Terms_or_Timing__c=true;
         ven.jbrvrm__Requisition_Type__c='Contingent Labor';
         insert ven;
         jbrvrm__Vendor_Requisition__c ven1 = new jbrvrm__Vendor_Requisition__c();
         ven1.jbrvrm__Estimated_Spend__c=4.7;
         ven1.name='test1';
         
         ven1.jbrvrm__Vendor__c=a.id;
         insert ven1;
    }
}