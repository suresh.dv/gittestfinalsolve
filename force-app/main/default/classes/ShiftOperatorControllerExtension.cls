/*
This controller is used for New Shift Operator page
a new ShiftOperator can be created from either Team or Team Allocation.
If a ShiftOperator is created from TeamAllocation, the page will display/populate TeamAllocation and Operating District field, and hide Team field
If a ShiftOperator is created from Team, the page will display/populate Team field, and hide TeamAllocation and Operating District field
If Team and TeamAllocation = null, The default page is New ShiftOperator from the active TeamAllocation
*/
public with sharing class ShiftOperatorControllerExtension extends AbstractControllerExtension {
    private static final String ACTIVE_SHIFT_OPERATOR_LIST_PAGE_URI = '/apex/TeamAllocationListView';
    private Boolean isNew; 
    public Shift_Operator__c shiftOp {get;set;}
    public Team_Allocation__c ta {get;set;}
    public Team__c team {get;set;}
    private Id teamAllocationId;
    private Id teamId;
    public boolean newShiftFromTeamAllocation {get;set;}
    public boolean newShiftFromTeam {get;set;}
    public string selectedNone = '';
    public String selectedRole {get;set;}
    
    public ShiftOperatorControllerExtension(ApexPages.StandardController stdController) 
    {
        if (!test.isRunningTest())
        {
            stdController.addFields(new List<String>{'Team_Allocation__c', 'Team__c', 'Role__c', 'Process_Area__c', 'Area__c', 'Description__c', 'Plant__c', 'Operator_Contact__c', 'Operating_District__c'});
        }
        this.shiftOp = (Shift_Operator__c) stdController.getRecord();
        this.isNew = String.isEmpty(this.shiftOp.Id);
        teamAllocationId = ApexPages.currentPage().getParameters().get('taId');
        teamId = ApexPages.currentPage().getParameters().get('teamId');    
        newShiftFromTeamAllocation = (teamAllocationId != null ? true : false);
        newShiftFromTeam = (teamId != null ? true : false);
        
        System.debug('\n*************************************\n'
            + '======= ShiftOperatorControllerExtension ====='
            + '\n'
            + 'Shift Operator Id =' + this.shiftOp.Id
            + '\n'
            + 'Team Allocation Id =' + teamAllocationId                 
            + '\n'
            + 'Role =' + shiftOp.Role__c 
            + '\n'
            + 'Is New = ' + isNew
            + '\n*************************************\n');
            
        lookupData();
        
        if (this.isNew) {
            setDefaults();
        }
        else
        {
            selectedRole = shiftOp.Role__c;
        }
        addErrorIfInactiveEvent();
    }
   
  
  
    //set default value for New ShiftOperator
    private void setDefaults() 
    {
        System.debug('Team Allocation =' + ta);
        
        if (newShiftFromTeam)
        {
            shiftOp.Team__c = team.Id;
            shiftOp.Operating_District__c = team.Operating_District__c;
            
            
        }
        else if (newShiftFromTeamAllocation)
        {
            if (this.ta == null) {
                this.shiftOp.addError('No active Team Allocation for new Shift operator.');
                return;
            }
            //Set time to the current value.
            this.shiftOp.Team_Allocation__c = this.ta.Id;
            this.shiftOp.Operating_District__c = this.ta.Operating_District__c;
        }    
    }
    
    
    private void addErrorIfInactiveEvent() {
        if (this.ta != null) {
            Map<String, Schema.Recordtypeinfo> recTypesByName = Team_Allocation__c.SObjectType.getDescribe().getRecordTypeInfosByName();
            if (this.ta.Status__c == TeamAllocationUtilities.INACTIVE_STATUS_NAME && ta.RecordTypeId == recTypesByName.get('Operations').getRecordTypeId()) {
                this.shiftOp.addError('Read only - This item cannot be modified since its team allocation is not active.');    
            }
        }
    }
        
    private void lookupData() 
    {
        if (isNew) //new mode
        {
            //click New ShiftOperator from tab
            if (!newShiftFromTeam && !newShiftFromTeamAllocation)
               this.ta = TeamAllocationUtilities.getActiveTeamAllocation();
            //new ShiftOperator from Team Allocation
            else if (newShiftFromTeamAllocation)
               this.ta = TeamAllocationUtilities.getTeamAllocation(teamAllocationId);
            //new ShiftOperator from Team   
            else if (newShiftFromTeam)
               team = getTeamById(teamId);
        }
        else //edit mode
        {
            if (shiftOp.Team_Allocation__c != null)
            {
                this.ta = TeamAllocationUtilities.getTeamAllocation(shiftOp.Team_Allocation__c);  
                newShiftFromTeamAllocation = true;
            }
            if (shiftOp.Team__c != null)
            {
               team = getTeamById(shiftOp.Team__c);    
               newShiftFromTeam = true;
            }
        }
        System.debug('newShiftFromTeam =' + newShiftFromTeam );        
    }
    private Team__c getTeamById(Id teamId)
    {
        return [SELECT Id, Operating_District__c, RecordType.Name FROM Team__c where Id =: teamId ];
    }
    public List<selectOption> RoleList 
    {
        get {
            
            List<selectOption> options = new List<selectOption>();
            options.add(new SelectOption(selectedNone,'-- Choose a Role --'));
            //retrieve Role with appropriate Record Type
            String rtName = '%'; 
            if (team != null)
                rtName = team.RecordType.Name;
            else if (ta != null)
                rtName = ta.RecordType.Name;
                
            for (Team_Role__c role : [select Id, Name from Team_Role__c where RecordType.Name like :rtName Order By Leader_Ind__c desc, Name asc limit 999])
                options.add(new SelectOption(role.Id, role.Name));
         
            return options;           
        }
        set;
    }
    public PageReference save() 
    {
         // Save the Shift Operator to the database  
         
         shiftOp.Role__c = selectedRole;      
         return new ApexPages.StandardController(this.shiftOp).save();        
    }
    public PageReference saveAndNew() {
         // Save the Shift Operator to the database  
         System.debug('Save and new newShiftFromTeam =' + newShiftFromTeam);
         shiftOp.Role__c = selectedRole;      
         new ApexPages.StandardController(this.shiftOp).save();        
         
         if (ApexPages.hasMessages())
            return null;
         // Go to the page that adds a new Shift Operator        
         return newShiftOperator();          
     }
     
    private PageReference newShiftOperator() {
        Shift_Operator__c newShiftOperator = new Shift_Operator__c();
        System.debug('newShiftFromTeam =' + newShiftFromTeam );        
        Schema.DescribeSObjectResult describeResult = newShiftOperator.getSObjectType().getDescribe();
        String safeReturnUrl = EncodingUtil.urlEncode(ACTIVE_SHIFT_OPERATOR_LIST_PAGE_URI, 'UTF-8');
        String addNewUrl = '/' 
            + describeResult.getKeyPrefix() + '/e?' ;

        if (newShiftFromTeam)    
      //  if ((newShiftFromTeam) || (shiftOp.Team__c != null))   
        {
            addNewUrl += 'retURL=' + shiftOp.Team__c;
            addNewUrl += '&teamId=' + shiftOp.Team__c;
        }
        else if (newShiftFromTeamAllocation)
//        else if ((newShiftFromTeamAllocation) || (shiftOp.Team_Allocation__c != null))
        {
            addNewUrl += 'retURL=' + safeReturnUrl;
            addNewUrl += '&taId=' + shiftOp.Team_Allocation__c;   
             
        }
        
        
        System.debug(
            '\n**********************************\n'
            + 'METHOD: newShiftOperator()'
            + '\n'
            + 'addNewUrl = ' + addNewUrl
            + '\n**********************************\n');
            
        PageReference pr = new PageReference(addNewUrl);
        pr.setRedirect(true);
        return pr;
    }     
}