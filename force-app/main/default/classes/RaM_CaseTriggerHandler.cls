/**********************************************************************************
  CreatedBy : Subhashini Katta
  Organization:Accenture
  Purpose   :  Handler class For  Trigger RaM_AutoAssignP1Ticket
  Test Class:  RaM_CaseTriggerHandlerTest
  Version:1.0.0.1
**********************************************************************************/
public without Sharing Class RaM_CaseTriggerHandler
{ 
    /**************************************
    CreatedBy:      Accenture
    Method name:    completeCaseMilestone
    Purpose:        Automatically marks milestones Completed on cases when Case status updated to Vendor Complete
    Param:          List<Case> caseList
    Return:         VOid
    *************************************/
    public static void completeCaseMilestone(List<Case> caseList, Map<id,Case> oldMap){
        try
        {
            List<Id> updateCases = new List<Id>(); //list to get all the ids of cases which are updated as "work in progress"
            List<Id> updateCasesList = new List<Id>();//list to get all the ids of cases which are updated as "Vendor Completed"
            //TO get "Vendor Completed" case id 
            for(Case caseRecord : caseList){ 
                 Case caseOldversion = oldMap.get(caseRecord.id); 
               //Check case recordtype is "RaM Child Ticket" or "RaM Ticket" and Type is "P1-Emergency" or "P2-Break/Fix" 
               if((caseRecord.RecordTypeId == RaM_ConstantUtility.CHILD_TICKET_RECORD_TYPE_ID || caseRecord.RecordTypeId == RaM_ConstantUtility.TICKET_RECORD_TYPE_ID) ){
                       
                    if(caseRecord.status==RaM_ConstantUtility.WORKINPROGREE_TEXT && caseOldversion.Status!=RaM_ConstantUtility.WORKINPROGREE_TEXT) {// if status is "work in progress"
                         updateCases.add(caseRecord.Id);
                    }else  if(caseRecord.Status==RaM_ConstantUtility.VENDOR_COMPLETED_TEXT && caseOldversion.Status!=RaM_ConstantUtility.VENDOR_COMPLETED_TEXT) {// if status is "Vendor Completed"
                        updateCasesList.add(caseRecord.Id); 
                    }                       
                }   
            }
            system.debug('---updateCases-----'+updateCases);
            if (updateCases.isEmpty() == false)// check updateCases list
            RaM_CaseTriggerHandler.UpdateCaseMilestone(updateCases);
            
            if (updateCasesList.isEmpty() == false)// check updateCasesList list
            RaM_CaseTriggerHandler.callSAPPIinterface(updateCasesList); //call future method
            
        }catch(DmlException qExcp)
        {
            //Error message getTypeName
            System.debug('An exception occurred: ' + qExcp.getMessage());
            RaM_UtilityHolder.errormethod(RaM_ConstantUtility.CASE_OBJECT,RaM_ConstantUtility.CLASS_NAME_CASE_TRIGGER ,RaM_ConstantUtility.METHOD_NAME_CASE_TRIGGER_MILESTONE , RaM_ConstantUtility.FATAL,qExcp.getMessage());
        }
    }
    
/**************************************
CreatedBy:      Accenture
Method name:    callSAPPIinterface
Param:          List<Id> caseList
Return:         void
Purpose:        call SAPPI to generate SAP record related to case
*************************************/
    @future(callout=true)
    public static void callSAPPIinterface(List<Id> caseList)
    {
        list<case> caseScope=[Select Id, Ticket_Number_R_M__c, Status, LocationNumber__r.SAPFuncLOC__c ,RecordTypeId ,Priority, Subject, EquipmentName__r.SAPEquipmentNumber__c, Case_Completed_Date__c, CreatedDate,SAP_Work_Order__c,SAP_Notification__c   from case where id=:caseList];
        
        map<string,case> caseMap = new map<string,case>();
        for(case caseIns : caseScope)
        {
             caseMap.put(caseIns.Ticket_Number_R_M__c, caseIns);
        }
        system.debug('----caseMap----------'+ caseMap);
        
         RaM_NotificationWOsalesforceComPm.MsgSource_element msgEle=new RaM_NotificationWOsalesforceComPm.MsgSource_element();
         msgEle.InitiatingApplication =RaM_ConstantUtility.DRM_TEXT;
         msgEle.ExternalUserId=RaM_ConstantUtility.RAM_SALESFORCE_TEXT;
 
         RaM_NotificationWOsalesforceComPm.Actions_element actElement=new RaM_NotificationWOsalesforceComPm.Actions_element();
            actElement.CreateNotif=RaM_ConstantUtility.Y_TEXT;
            actElement.ReleaseNotif=RaM_ConstantUtility.N_TEXT;
            actElement.CreateWo=RaM_ConstantUtility.Y_TEXT;
            actElement.ReleaseWo=RaM_ConstantUtility.Y_TEXT;
            actElement.TechCompleteWo=RaM_ConstantUtility.Y_TEXT;
 
        list<RaM_NotificationWOsalesforceComPm.DT_SFDC_Notif_WorkOrder> caseInfoList=new list<RaM_NotificationWOsalesforceComPm.DT_SFDC_Notif_WorkOrder>();
 
        for(case caseIns : caseScope){
        
            RaM_NotificationWOsalesforceComPm.DT_SFDC_Notif_WorkOrder caseInfoIns=new RaM_NotificationWOsalesforceComPm.DT_SFDC_Notif_WorkOrder();
            caseInfoIns.Actions=actElement;
            caseInfoIns.OrderId=caseIns.Ticket_Number_R_M__c;
            caseInfoIns.NotificationType=caseIns.Priority;
           
            caseInfoIns.FunctionalLocation=caseIns.LocationNumber__r.SAPFuncLOC__c;
            caseInfoIns.EquipmentNo=caseIns.EquipmentName__r.SAPEquipmentNumber__c;
            caseInfoIns.RequiredEnd=(caseIns.Case_Completed_Date__c).format('yyyyMMdd');
            caseInfoIns.RequiredStartDate =(caseIns.CreatedDate).format('yyyyMMdd'); 
            caseInfoIns.RequiredStartTime =(caseIns.CreatedDate).format('HHMMDD'); 
            caseInfoIns.ReferenceDate =(caseIns.Case_Completed_Date__c).format('yyyyMMdd');
            caseInfoIns.MaintActivityType=RaM_ConstantUtility.REP_TEXT;
            if(caseIns.Priority==RaM_ConstantUtility.P1_EMEGENCY_TEXT){
                caseInfoIns.OrderType=RaM_ConstantUtility.PONE_TEXT;
                caseInfoIns.Priority=RaM_ConstantUtility.ONE_TEXT  ;
                caseInfoIns.NotificationType=RaM_ConstantUtility.EM_TEXT ;//RaM_ConstantUtility.P1_TEXT;
            } else{
                caseInfoIns.Priority=(caseIns.Priority==RaM_ConstantUtility.P2_BREAK_FIX_TEXT )?RaM_ConstantUtility.TWO_TEXT:RaM_ConstantUtility.THREE_TEXT;
                caseInfoIns.OrderType=(caseIns.Priority==RaM_ConstantUtility.P2_BREAK_FIX_TEXT )?RaM_ConstantUtility.PTWO_TEXT:RaM_ConstantUtility.PTHREE_TEXT ;
                caseInfoIns.NotificationType=RaM_ConstantUtility.EM_TEXT;//(caseIns.Priority==RaM_ConstantUtility.P2_BREAK_FIX_TEXT )?RaM_ConstantUtility.P2_TEXT:RaM_ConstantUtility.P3_TEXT ;
            }
             caseInfoIns.Description=caseIns.Ticket_Number_R_M__c+RaM_ConstantUtility.DASHES+caseInfoIns.Priority+RaM_ConstantUtility.DASHES+caseIns.subject;
            caseInfoList.add(caseInfoIns); 
        }
        system.debug('----caseInfoList-----'+ caseInfoList);
        //string redult=JSON.serialize(caseInfoList);
        //system.debug('----redult-----'+ redult);
        HOG_Settings__c settingsHOG = HOG_Settings__c.getInstance();
        system.debug('----settingsHOG-------'+ settingsHOG);
        RaM_NotificationWOsalesforceComPm.HTTPS_Port  connectionIns=new RaM_NotificationWOsalesforceComPm.HTTPS_Port();
        connectionIns.inputHttpHeaders_x = new Map<String, String>();
        String authString = EncodingUtil.base64Encode(Blob.valueOf(settingsHOG.SAP_DPVR_Username__c + ':' + settingsHOG.SAP_DPVR_Password__c));
        connectionIns.inputHttpHeaders_x.put('Authorization', 'Basic ' + authString);
        connectionIns.clientCertName_x = 'HuskyCertificate';
       
       
        list<RaM_NotificationWOsalesforceComPm.Response_element> response_NotificationWorkOrder=new  list<RaM_NotificationWOsalesforceComPm.Response_element>();
          //  if (!Test.isRunningTest()) 
        response_NotificationWorkOrder=connectionIns.SI_SFDC_Notif_WO_Create_Sync_OB(msgEle , caseInfoList);
        system.debug('-------response_NotificationWorkOrder------------'+response_NotificationWorkOrder );
       list<case> caseUpdateList=new list<case>();
            string notification=RaM_ConstantUtility.BLANK;
            string workOrder=RaM_ConstantUtility.BLANK;
            string caseNumber=RaM_ConstantUtility.BLANK;
        for(RaM_NotificationWOsalesforceComPm.Response_element  resIns:response_NotificationWorkOrder  ){
            if(resIns.RequestId!=RaM_ConstantUtility.BLANK)
             caseNumber=resIns.RequestId;
            
            if(resIns.id==RaM_ConstantUtility.NOTIFICATION_ID && resIns.Number_x==RaM_ConstantUtility.NOTIFICATION_NUMBER){
                notification=(resIns.Message).substringBetween(RaM_ConstantUtility.SPACE_TEXT,RaM_ConstantUtility.SPACE_TEXT);
                system.debug('----notification-----------'+ notification);
            }else if(resIns.id==RaM_ConstantUtility.WORKORDER_ID && resIns.Number_x==RaM_ConstantUtility.WORKORDER_NYMBER){
               workOrder=(resIns.Message).substring(resIns.Message.lastIndexOf(RaM_ConstantUtility.SPACE_TEXT)+1);
                system.debug('----workOrder-----------'+workOrder);
            }
        }
        system.debug('------caseNumber------------'+caseNumber);
        system.debug('------caseMap.get(caseNumber)------------'+caseMap.get(caseNumber));
        case caseIns=new case();
        if(caseNumber!=RaM_ConstantUtility.Blank && caseMap.get(caseNumber)!=null){
              caseIns=caseMap.get(caseNumber);
              caseIns.SAP_Notification__c=notification;
              caseIns.SAP_Work_Order__c=workOrder;
              caseUpdateList.add(caseIns);   
        }
        system.debug('------caseUpdateList----1--------'+caseUpdateList);   
         if (!caseUpdateList.isEmpty())
           update caseUpdateList;
          system.debug('------caseUpdateList----2--------'+caseUpdateList);     
    }
    
/**************************************
CreatedBy:      Accenture
Method name:   UpdateCaseMilestone
Param:          List<Id> caseIds
Return:         VOid
Purpose:        Update Case Milestone Completetion date
*************************************/
  public static void updateCaseMilestone(List<Id> caseIds){
        String milestoneName=System.Label.RaM_Milestone_Name; //Milestone name
        DateTime completionDate = System.now();  //Currrent date and time
        
        //Query to get all the CaseMilestone related to case
        List<CaseMilestone> cmsToUpdate = [select Id, completionDate
            from CaseMilestone 
            where caseId in :caseIds and MilestoneType.Name=:milestoneName 
            and completionDate = null limit:Limits.getLimitQueryRows()];
            system.debug('----cmsToUpdate---'+cmsToUpdate );
            if (cmsToUpdate.isEmpty() == false){
            for (CaseMilestone CaseMile : cmsToUpdate){
                    CaseMile.completionDate = completionDate; //update date
                }
            system.debug('----cmsToUpdate---'+cmsToUpdate );    
            update cmsToUpdate; //update caseMilestone
        }
        
    }
   /**************************************
    Method name:    entitlementMethod
    Purpose:        To return entitlment id
    Return:         Id   
    *************************************/
   public static Id entitlementMethod(){
       Entitlement EntitlementRecord= [select id,name from entitlement where name = :RaM_ConstantUtility.SLA_HUSKY_ENERGY_TEXT limit 1];
       if(EntitlementRecord != NULL)
         return EntitlementRecord.id;
       else
        return null;  
   }
    /**************************************
    Method name:    CaseAssignmentProcess
    Purpose:        To update status,Origin,entitlment,vendor name and Assign fields in Ticket creation or updation process
    Return:         Void  
    *************************************/  
   Public Static Void caseAssignmentProcess(List<Case> caseList,Map<id,Case> oldMap,boolean isInsert){     
    try{
     
     //Declare Variables
     Set<String> locationClasificationList = new Set<String>();
     Set<String> locationServiceCodeList = new Set<String>();
     Set<String> equipmentTypeList= new Set<String>();
     Set<String> equipmentClassList = new Set<String>();
     
     Set<Id> locationIdsSet = new Set<Id>(); 
     Set<Id> equipmentIdsSet = new Set<Id>(); 
      
      
     Set<String> locationIds = new Set<String>();
     Set<string> accountIds = new Set<string>();
     List<RaM_VendorAssignment__c> vaList = new List<RaM_VendorAssignment__c>();
     List<contact> contactRecords = new List<contact>();   
     Map<id,contact> contactMap = new Map<id,contact>();
     List<Case> caseListRaM = new  List<Case>();    
     //get entitlementment id
     id entitlementRecordId=RaM_Entitlement.entitlementMethod();
     
     for(Case caseRecord : caseList){ 
     system.debug('-------Location_Classification__c---------'+caseRecord.LocationNumber__r.Location_Classification__c);
           //Check case recordtype is "RaM Child Ticket" or "RaM Ticket"
           if(caseRecord.RecordTypeId == RaM_ConstantUtility.CHILD_TICKET_RECORD_TYPE_ID || caseRecord.RecordTypeId == RaM_ConstantUtility.TICKET_RECORD_TYPE_ID){
                     //Check RestrictExternalInsert is True(Added on 30 jan 18)
                    Case caseOldversion = oldMap.get(caseRecord.id); 
                   
                   if((isInsert && caseRecord.RestrictExternalInsert__c) ||  !isInsert){
                        locationIdsSet.add(caseRecord.LocationNumber__c);
                        equipmentIdsSet.add(caseRecord.EquipmentName__c);
                        caseListRaM.add(caseRecord);// Add all cases to list
                   } else if(isInsert && !caseRecord.RestrictExternalInsert__c)//show error message if case are not allowed to insert from external system
                     caseRecord.addError(RaM_ConstantUtility.RESTRICT_ERROR_MESSAGE); 
                  
                   //Check case record priority equals to "P1-Emergency" 
                   if((isInsert == true && caseRecord.Priority == RaM_ConstantUtility.P1_EMEGENCY_TEXT && caseRecord.status==RaM_ConstantUtility.NEW_TEXT) || (isInsert == false && caseRecord.Priority == RaM_ConstantUtility.P1_EMEGENCY_TEXT && caseOldversion.Priority != RaM_ConstantUtility.P1_EMEGENCY_TEXT && caseRecord.status == RaM_ConstantUtility.NEW_TEXT))
                    caseRecord.status = RaM_ConstantUtility.ASSIGNED_TEXT ; //  Change Case status to 'Assigned' for P1 type automatically
                 
                   //Logic to populate entitlement name in case record 
                   if((caseRecord.Priority == RaM_ConstantUtility.P1_EMEGENCY_TEXT || caseRecord.Priority == RaM_ConstantUtility.P2_BREAK_FIX_TEXT ) && caseRecord.SLAExempt__c == RaM_ConstantUtility.NO_TEXT){
                      caseRecord.EntitlementId = entitlementRecordId; // Assign Entitlment to Case 
                   } else if(caseRecord.Priority == RaM_ConstantUtility.P3_PROGRAMMED_WORK_TEXT){ // there should be no entitlement applied if case type is P3   
                       caseRecord.EntitlementId = NULL;
                   }
                   system.debug('---Retailer_Status__c---'+caseRecord.Retailer_Status__c);
                   system.debug('---Vendor_Status__c---'+caseRecord.Vendor_Status__c);
                   //uPDATE status field if rtailer and vendor update their status from the community portal
                   if(!isInsert && caseRecord.Retailer_Status__c!=null && caseRecord.Retailer_Status__c!=caseOldversion.Retailer_Status__c){
                        caseRecord.status=caseRecord.Retailer_Status__c;
                   }else if(!isInsert && caseRecord.Vendor_Status__c!=null && caseRecord.Vendor_Status__c!=caseOldversion.Vendor_Status__c){
                        caseRecord.status=caseRecord.Vendor_Status__c;
                   }
                   //To Clear entitlement when SLA Exempt value is "Yes"
                   if(caseRecord.SLAExempt__c != RaM_ConstantUtility.NO_TEXT)
                     caseRecord.EntitlementId = NULL;           

           }
       } //For loop close   
       
        //getting all the location details for prepopulation on case screen
        Map<Id, RaM_Location__c> locationEntries = new Map<Id, RaM_Location__c>(
        [select Id,Name,Location_Classification__c,Service_Area__c,Maintenance_Tech_User__c,Contact__c from RaM_Location__c where id in :LocationIdsSet]
        );
        system.debug('----locationEntries---'+ locationEntries);
        
       //getting all the equipment details for prepopulation on case screen
        Map<Id, RaM_Equipment__c> equipmentEntries = new Map<Id, RaM_Equipment__c>(
        [select Id,Name,Equipment_Class__c,Equipment_Type__c from RaM_Equipment__c where id in :equipmentIdsSet]
        );
       system.debug('----equipmentEntries---'+ equipmentEntries);
      
      
        for(Case caseRecord : caseListRaM){ 
            system.debug('----caseRecord-caseRecord--'+ caseRecord);
            //getting old value incase of update event fire
            Case caseOldversion = oldMap.get(caseRecord.id); 
            // getting value from the Location and Equipment object to prepopulate in case's field before trigger case
            caseRecord.Location_Classification__c=locationEntries.get(caseRecord .LocationNumber__c).Location_Classification__c; 
            caseRecord.Service_Area__c=locationEntries.get(caseRecord .LocationNumber__c).Service_Area__c; 
            caseRecord.Equipment_Type__c=equipmentEntries.get(caseRecord .EquipmentName__c).Equipment_Type__c; 
            caseRecord.Equipment_Class__c=equipmentEntries.get(caseRecord .EquipmentName__c).Equipment_Class__c; 
            //update Location Contact and Maintenance Tech User from location
            caseRecord.Maintenance_Tech_User__c=locationEntries.get(caseRecord .LocationNumber__c).Maintenance_Tech_User__c; 
            caseRecord.ContactLocation__c=locationEntries.get(caseRecord .LocationNumber__c).Contact__c;
            caseRecord.RaM_Search__c=locationEntries.get(caseRecord .LocationNumber__c).name+RaM_ConstantUtility.SPACE_TEXT+ equipmentEntries.get(caseRecord .EquipmentName__c).Equipment_Type__c+RaM_ConstantUtility.SPACE_TEXT+(equipmentEntries.get(caseRecord .EquipmentName__c).Equipment_Class__c != NULL ? equipmentEntries.get(caseRecord .EquipmentName__c).Equipment_Class__c : RaM_ConstantUtility.BLANK); 
                
            // collecting all the location clasification and service area values asigned/update to case    
            if((isInsert == true && caseRecord.LocationNumber__c != NULL) || (isInsert == false && caseRecord.LocationNumber__c != null && caseRecord.LocationNumber__c != caseOldversion.LocationNumber__c)) {
                locationClasificationList.add(caseRecord.Location_Classification__c);
                locationServiceCodeList.add(caseRecord.Service_Area__c);
            }
             system.debug('----caseRecord.Maintenance_Tech_User__c---'+ caseRecord.Maintenance_Tech_User__c);
             // collecting all the Equipment Type and Equipment Class values asigned/update to case 
            if((isInsert == true && caseRecord.EquipmentName__c != NULL) || (isInsert == false && caseRecord.EquipmentName__c != null && caseRecord.EquipmentName__c != caseOldversion.EquipmentName__c)){
                equipmentTypeList.add(caseRecord.Equipment_Type__c);
                equipmentClassList.add(caseRecord.Equipment_Class__c);
            }
            //if user change only vendor assignment
            if( (!isInsert && caseRecord.Vendor_Assignment__c !=caseOldversion.Vendor_Assignment__c) || (!isInsert && caseRecord.status==RaM_ConstantUtility.ASSIGNED_TEXT && caseOldversion.status=='New')){ //if user change status ffrom NEW to Assigned then Vendor Assignment process should execute
                locationClasificationList.add(caseRecord.Location_Classification__c);
                locationServiceCodeList.add(caseRecord.Service_Area__c);
                equipmentTypeList.add(caseRecord.Equipment_Type__c);
                equipmentClassList.add(caseRecord.Equipment_Class__c);
            }
            if(isInsert == false && caseRecord.status == RaM_ConstantUtility.VENDOR_COMPLETED_TEXT && caseRecord.status != caseOldversion.status){
                caseRecord.Case_Completed_Date__c= datetime.now();
            }
        }
        system.debug('----equipmentTypeList---'+ equipmentTypeList);
        system.debug('----equipmentClassList-158 --'+ equipmentClassList);

        system.debug('----locationClasificationList---'+ locationClasificationList);
        system.debug('----locationServiceCodeList---'+ locationServiceCodeList);
        
       
    
       //To Get Vendor assignment records containing previous collected Equipment Type, Equipment Class, location clasification and service area 
       if(!locationClasificationList.isEmpty() || !locationServiceCodeList.isEmpty() || !equipmentTypeList.isEmpty() ||  !equipmentClassList.isEmpty())
       vaList = [select id,Location_Classification_VA__c,ServiceArea__c,Equipment_Class_VA__c,Equipment_Type_VA__c,Account__c,Priority__c,Account__r.email__c from RaM_VendorAssignment__c where 
       Location_Classification_VA__c IN :locationClasificationList 
       and  ServiceArea__c IN :locationServiceCodeList
       and  Equipment_Type_VA__c IN :equipmentTypeList
       and  ( Equipment_Class_VA__c IN :equipmentClassList or Equipment_Class_VA__c=:null)
       ORDER BY Equipment_Class_VA__c NULLS LAST, Priority__c ASC /*DESC*/];
       
       //converting list to map
       Map<Id,RaM_VendorAssignment__c> mapFromListVendorAssign = new Map<Id,RaM_VendorAssignment__c>(vaList);
       
       system.debug('******mapFromListVendorAssign********'+mapFromListVendorAssign);
       system.debug('******vaList********'+vaList);
       system.debug('******caseListRaM********'+caseListRaM);
       //start for loop
       for(Case caseRecord : caseListRaM){
            Case caseOldversion = oldMap.get(caseRecord.id); 
            system.debug('******caseRecord********'+caseRecord);
            //( caseRecord.Priority == RaM_ConstantUtility.P1_EMEGENCY_TEXT || caseRecord.Priority == RaM_ConstantUtility.P2_BREAK_FIX_TEXT || caseRecord.Priority == RaM_ConstantUtility.P3_PROGRAMMED_WORK_TEXT) &&
           if((isInsert && caseRecord.Priority == RaM_ConstantUtility.P1_EMEGENCY_TEXT && caseRecord.status==RaM_ConstantUtility.ASSIGNED_TEXT) || ( !isInsert && caseRecord.status==RaM_ConstantUtility.ASSIGNED_TEXT && caseOldversion.status==RaM_ConstantUtility.NEW_TEXT) ){
             for(RaM_VendorAssignment__c vaRecord  : vaLIst){
                 if(null != caseRecord.LocationNumber__c  && null != caseRecord.EquipmentName__c  && caseRecord.Location_Classification__c == vaRecord.Location_Classification_VA__c  && caseRecord.Service_Area__c == vaRecord.ServiceArea__c && caseRecord.Service_Area__c == vaRecord.ServiceArea__c && caseRecord.Equipment_Type__c == vaRecord.Equipment_Type_VA__c && (caseRecord.Equipment_Class__c == vaRecord.Equipment_Class_VA__c || null == vaRecord.Equipment_Class_VA__c )) 
                 { 
                    caseRecord.Vendor_Assignment__c=vaRecord.id;
                    caseRecord.Vendor_Name__c = vaRecord.Account__c;
                    caseRecord.VendorEmail__c=vaRecord.Account__r.Email__c;
                    break;
                 }          
              }
              system.debug('******caseRecord***updated VA and VN*****'+caseRecord);
              if(null==caseRecord.Vendor_Assignment__c) // show error message if
               caseRecord.addError(RaM_ConstantUtility.VENDOR_ERROR_MESSAGE);
                
          }else if(!isInsert && caseRecord.Vendor_Assignment__c!=oldMap.get(caseRecord.id).Vendor_Assignment__c)//execute this code if case type is p2/p3 and vendor is already is assigned to it
          {
                    RaM_VendorAssignment__c vaRecord=new RaM_VendorAssignment__c();
                    if(caseRecord.Vendor_Assignment__c==null){
                        caseRecord.Vendor_Assignment__c=null;
                        caseRecord.Vendor_Name__c = null;
                        caseRecord.VendorEmail__c=null;
                    }else{
                        vaRecord=mapFromListVendorAssign.get(caseRecord.Vendor_Assignment__c);
                        system.debug('---old value--vaRecord---'+ vaRecord);
                        if(vaRecord!=null){
                            caseRecord.Vendor_Assignment__c=vaRecord.id;
                            caseRecord.Vendor_Name__c = vaRecord.Account__c;
                            caseRecord.VendorEmail__c=vaRecord.Account__r.Email__c;
                        }
                    }
                    if(null==vaRecord) 
                    caseRecord.addError(RaM_ConstantUtility.VENDOR_ERROR_MESSAGE);
          }
       } //For loop close    
       
       if(isInsert && !caseListRaM.isEmpty()){
           // Mark 5/8/2018 - Added logic to create Ticket Number (R&M), and also set the TicketExternalId field.
            RaM_Setting__c cSettings = RaM_Setting__c.getInstance(RaM_ConstantUtility.NEXT_CASE_COUNTER);
            system.debug('--cSettings---'+cSettings);
            Integer LatestNumber = Integer.valueOf(cSettings.R_M_Ticket_Number_Next_Sequence__c) ;
            for(Case caseRecord : caseListRaM){
                system.debug('---- caseRecord.Ticket_Number_R_M__c-----' +  caseRecord.Ticket_Number_R_M__c);
                   // caseRecord.Ticket_Number_R_M__c=null; // to maintain uniqueness of the case field Ticket_Number_R_M__c
                 if(caseRecord.Ticket_Number_R_M__c == null){
                        String strTicketNumber = 'R' + String.valueOf(LatestNumber).leftPad(7, '0');
                        caseRecord.Ticket_Number_R_M__c = strTicketNumber;
                        LatestNumber = LatestNumber+1;
                 }
                 caseRecord.TicketExternalId__c = caseRecord.Ticket_Number_R_M__c;
            }
            cSettings.R_M_Ticket_Number_Next_Sequence__c = Decimal.valueOf(LatestNumber);
            update cSettings;
       }
       
       }Catch(DmlException qExcp){
        //Error message getTypeName
            System.debug('An exception occurred: ' + qExcp.getMessage());
            RaM_UtilityHolder.errormethod(RaM_ConstantUtility.CASE_OBJECT,RaM_ConstantUtility.CLASS_NAME_CASE_TRIGGER ,RaM_ConstantUtility.METHOD_NAME_CASE_TRIGGER_UPDATE_ASSIGN_PROCESS , RaM_ConstantUtility.FATAL,qExcp.getMessage());
       } 
    }  
    /******************************************************************************
    Method name:    ChangeCaseOrgin
    Purpose:        To Update TicketCreator(Origin) based on logged in user role.
    Return:         Void  
    *******************************************************************************/ 
    public Static Void changeCaseOrgin(List<Case> caseList){
      try{
          Set<id> caseOwnerIds = new Set<id>();
          Map<Id,User> userRoleMap;
          List<case> caseRecords = new List<Case>();
          for(Case caseRecord : caseList){
               if(caseRecord.RestrictExternalInsert__c == true && caseRecord.RecordTypeId == RaM_ConstantUtility.CHILD_TICKET_RECORD_TYPE_ID || caseRecord.RecordTypeId == RaM_ConstantUtility.TICKET_RECORD_TYPE_ID){
                  caseOwnerIds.add(caseRecord.OwnerId); // Add OwnerIds to Set
                  caseRecords.add(caseRecord); // Add Case records to list
              } 
              else
              {
                if(caseRecord.RestrictExternalInsert__c == false && caseRecord.RecordTypeId == RaM_ConstantUtility.CHILD_TICKET_RECORD_TYPE_ID || caseRecord.RecordTypeId == RaM_ConstantUtility.TICKET_RECORD_TYPE_ID)
                 caseRecord.addError(RaM_ConstantUtility.RESTRICT_ERROR_MESSAGE); 
              }   
          } 
          //To retrieve loggedIn user's role information.
          userRoleMap = new Map<Id,User>([select id,Name,UserRole.Id,UserRole.Name,profile.name from User where id IN :caseOwnerIds]);
          
          system.debug('************userRoleMap******userRoleMap*******'+userRoleMap);
          
            for(Case caseRec : caseRecords){
                system.debug('********userRoleMap*********'+userRoleMap);
                if(caseRec.Priority == RaM_ConstantUtility.P1_EMEGENCY_TEXT && userRoleMap.containsKey(caseRec.OwnerId) &&  userRoleMap.get(caseRec.OwnerId).profile.Name==RaM_ConstantUtility.RETAILER_PROFILE_NAME  ){
                    caseRec.addError(Label.Ram_Ticket_P1_Emergency_case_error_message);
                    system.debug('---- caseRec.Origin--2--'+ caseRec.Origin);
                }else if(userRoleMap.containsKey(caseRec.OwnerId) && userRoleMap.get(caseRec.OwnerId).profile.Name==RaM_ConstantUtility.RETAILER_PROFILE_NAME ){
                    caseRec.Origin = RaM_ConstantUtility.RETAILER_TEXT; 
                    system.debug('---- caseRec.Origin--1--'+ caseRec.Origin);
                    
                }else{
                    caseRec.Origin = RaM_ConstantUtility.MAIN_TENANCETECH_TEXT;
                }
                system.debug('---- caseRec.Origin--3--'+ caseRec.Origin);
            }
        }catch(DmlException qExcp){
         //Error message getTypeName
            System.debug('An exception occurred: ' + qExcp.getMessage());
            RaM_UtilityHolder.errormethod(RaM_ConstantUtility.CASE_OBJECT,RaM_ConstantUtility.CLASS_NAME_CASE_TRIGGER ,RaM_ConstantUtility.METHOD_NAME_CASE_TRIGGER_ORIGIN , RaM_ConstantUtility.FATAL,qExcp.getMessage());
        }
    }
}