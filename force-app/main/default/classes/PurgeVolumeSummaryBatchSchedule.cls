global class PurgeVolumeSummaryBatchSchedule implements Database.Batchable<SObject>, Schedulable, Database.Stateful
{
	
/*******************************************************************************************************
*******************************************************************************************************
*******************************************************************************************************
NOTE: 
For now the result emails are sent to Chris Sembaluk
SELECT Email FROM User WHERE Profile.Name = 'System Administrator' And IsActive = true AND FirstName = 'Chris' AND Lastname = 'Sembaluk'
Need to make sure that clause is not there when rolled into production.
*******************************************************************************************************
*******************************************************************************************************
*******************************************************************************************************/
	
	global Date mostRecentDate;
	global Date lastWantedDate;
	global Date specificSundayDate;  // this is for manual run
	global Integer recordCount = 0;
	global Boolean isManualRun = false;
	
	// The purpose of this batch is to maintain 2 years of data in the Volume Transaction 
	// Summary object.
	// The algorithm will look for the latest week that exists in the Summary object.  Then simply subtract
	// 2 from the year.  Any data that are for weeks prior to that day will then be purged.
	// For example, if the latest transaction end date is for 2013-08-18, then subtract two from the year
	// will give you 2011.  So purge anything that's before 2011-08-18.
	
	global PurgeVolumeSummaryBatchSchedule()
	{
		this(null);
	}

	global PurgeVolumeSummaryBatchSchedule(Date pdWeeklyEndDate)
	{
		if (pdWeeklyEndDate == null)
		{
	    	this.mostRecentDate = (Date)[SELECT Max(Transaction_Week_Ending_Date__c) BatchDate FROM 
	    		Volume_Transaction_Summary__c][0].get('BatchDate');
	    	this.lastWantedDate = mostRecentDate.addYears(-2);
		}
		else
		{
			this.isManualRun = true;
			this.specificSundayDate = pdWeeklyEndDate;
		}		
	}
	
    global Database.QueryLocator start(Database.BatchableContext BC)
    {
    	if (this.specificSundayDate == null) 
    	{
	        return Database.getQueryLocator([SELECT Id FROM Volume_Transaction_Summary__c WHERE 
	        	Transaction_Week_Ending_Date__c < : this.lastWantedDate]);
    	}
    	else   // i.e. if a specific date is provided
    	{
	        return Database.getQueryLocator([SELECT Id FROM Volume_Transaction_Summary__c WHERE 
	        	Transaction_Week_Ending_Date__c = : this.specificSundayDate]);
    	}
    }
    
    global void execute(Database.BatchableContext BC, List<SObject> scope)
    {
    	this.recordCount = scope.size();
    	List<Volume_Transaction_Summary__c> vtsList = new List<Volume_Transaction_Summary__c>();
    	for (SObject s : scope)
    	{
    		Volume_Transaction_Summary__c vts = (Volume_Transaction_Summary__c)s;
    		vtsList.add(vts);
    	}
    	delete vtsList;
    }
    
    global void finish(Database.BatchableContext BC)
    {
    	String sysMessage = '';
    	
    	if (this.isManualRun)
    	{
	    	String specificSundayDateString = Datetime.newInstance(this.specificSundayDate, time.newinstance(0,0,0,0)).format('yyyy-MM-dd');
	    	
	    	sysMessage += 'Dear Sir/Madam\n\r';
	    	sysMessage += 'The following is the statistics from batch job PurgeVolumeSummaryBatchSchedule:\n\r';
	    	sysMessage += 'You have requested to purge data from Transaction Volume Summary file for date ' + specificSundayDateString + '\n\r';
	    	sysMessage += 'The number of records purged is ' + this.recordCount + '\n\r';
	        sysMessage += 'Thank you very much.\n\r';
	        sysMessage += 'Husky Retail Team';
    	} 
    	else
    	{
	    	String mostRecentDateString = Datetime.newInstance(this.mostRecentDate, time.newinstance(0,0,0,0)).format('yyyy-MM-dd');
	    	String lastWantedDateString = Datetime.newInstance(this.lastWantedDate, time.newinstance(0,0,0,0)).format('yyyy-MM-dd');
	    	
	    	sysMessage += 'Dear Sir/Madam\n\r';
	    	sysMessage += 'The following is the statistics from batch job PurgeVolumeSummaryBatchSchedule:\n\r';
	    	sysMessage += 'Most recent date in Transaction Volume Summary file is ' + mostRecentDateString + '\n\r';
	    	sysMessage += 'Last wanted date in the file is ' + lastWantedDateString + '\n\r';
	    	sysMessage += 'The number of records purged is ' + this.recordCount + '\n\r';
	        sysMessage += 'Thank you very much.\n\r';
	        sysMessage += 'Husky Retail Team';
    	}  
    	      
        List<String> systemAdmins = new List<String>();
        for (List<User> users : [ SELECT Email FROM User WHERE Profile.Name = 'System Administrator' And IsActive = true AND FirstName = 'Chris' AND Lastname = 'Sembaluk'])
        {
            for (User user: users)
            {
                systemAdmins.add(user.Email);
            }
        }
        if (systemAdmins.size() > 0)
        {
	        Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();  
	        mail.setToAddresses(systemAdmins);
	        mail.setSenderDisplayName('Husky Retail Team');
	        mail.setSubject('Purge Volume Summary Batch Schedule results');
	        mail.setPlainTextBody(sysMessage);
	        mail.setBccSender(false);
	        mail.setSaveAsActivity(false);
	        mail.setUseSignature(false);
	        Messaging.sendEmail(new Messaging.SingleEmailMessage[] { mail });
        }
    }

	// This is for the scheduling
    global void execute(SchedulableContext SC)
    {
        PurgeVolumeSummaryBatchSchedule b = new PurgeVolumeSummaryBatchSchedule();
        Database.executeBatch(b);
    }
    
}