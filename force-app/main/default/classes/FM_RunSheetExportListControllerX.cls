public class FM_RunSheetExportListControllerX{

    ApexPages.StandardSetController setCon;
    
    public Boolean EditMode {get; private set;}
    public List<FM_Run_Sheet__c> ExportList {get; private set;}
	private List<FM_Run_Sheet__c> lStatusChange;

    public static final String STATUS_CANCELLED = 'Cancelled';
    public static final String STATUS_EXPORTED = 'Exported';

    public FM_RunSheetExportListControllerX(ApexPages.StandardSetController controller) {
		setCon = controller;
		this.setCon.setPageSize(500);

		ExportList = [select id, Name, Date__c, Route__c,  Well_Name_Tank__c,Tank_Size__c,Tank_Low_Level__c, Act_Tank_Level__c, 
                  Act_Flow_Rate__c,Tonight_Load__c, Tomorrow_Load__c, Outstanding_Load__c, Status__c, Sour__c, Unit_Configuration__c,
                  Standing_Comments__c, Well__r.Functional_Location_Description_SAP__c, Surface_Location__c
                  from FM_Run_Sheet__c
                  WHERE id in :setCon.getRecords() order by Route__c, Name];
	}

    public PageReference EditList()
    {
        EditMode  = true;
        return null;
    }
    public PageReference SaveList()
    {
        setCon.save();
        EditMode  = false;
        return null;
    }
   
    public PageReference CancelEditList()
    {
        setCon.cancel();
        EditMode  = false;
        return null;
    }    
    
    public PageReference ExportList()
    {
		List<FM_Run_Sheet__c> selectedlist;
		lStatusChange = new List<FM_Run_Sheet__c>();

		List<PermissionSetAssignment> lPermissions = [SELECT Id FROM PermissionSetAssignment WHERE AssigneeId = :UserInfo.getUserId() AND PermissionSet.Name = 'HOG_FM_Dispatcher_Vendor'];

		if(lPermissions.size() > 0)
		{
			selectedlist = [select id, Name, Date__c, Route__c,  Well_Name_Tank__c,Tank_Size__c,Tank_Low_Level__c, Act_Tank_Level__c, 
							Act_Flow_Rate__c,Tonight_Load__c, Tomorrow_Load__c, Outstanding_Load__c, Status__c, Sour__c, Axle__c,
							Standing_Comments__c, Well__r.Functional_Location_Description_SAP__c, Surface_Location__c, Unit_Configuration__c 
							from FM_Run_Sheet__c
							WHERE Status__c <> :STATUS_CANCELLED 
							AND id in :this.setCon.getRecords() order by Route__c, Name];

			this.ExportList = selectedlist;

			for(FM_Run_Sheet__c runsheet: selectedlist)
			{   
				if(runsheet.Date__c == Date.Today() && runsheet.Status__c == 'New') //only export today's run sheets and if it was not exported yet
				{
					if(runsheet.Status__c <> STATUS_CANCELLED)
           			{
						runsheet.Status__c = STATUS_EXPORTED;
						this.lStatusChange.add(runsheet);
					}
				}
			}
		}

		try
		{
			if(this.lStatusChange.size() > 0)
			{
				update(this.lStatusChange);
			}

			PageReference redirectto = Page.FM_FluidExportExcel;
			return redirectto;           
		}
		catch(Exception ex)    
		{
			FM_Utilities.logError(ex);
		}

		return null;
	}

    public integer getMySelectedSize() {
        return setCon.getSelected().size();
    }
    public integer getMyRecordsSize() {
        return setCon.getRecords().size();
    }

    public Boolean getIsVendorDispatcher() {
    	return FM_Utilities.IsVendorDispatcher();
    }
}