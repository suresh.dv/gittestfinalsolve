@isTest
private class PREPTaxonomyControllerTest {
    static testmethod void callingDefaultMethods() {
        test.startTest();
            PREPTaxonomyController controller = new PREPTaxonomyController();
            controller.prepWrapper = new PREPTaxonomyController.PREPWrapper();
            //controller.getInitiatives();
            controller.inoselectItem();
            controller.ctrprselectItem();
            controller.ctrcrselectItem();
            controller.ctrcmselectItem();
            controller.ctrcpselectItem();
            controller.ctrbaselectItem();
            controller.ctrselectItem();
            controller.ctrplselectItem();
            controller.ctrctselectItem();
            controller.ctrgsselectItem();
            controller.taxcmSelectItem();
            controller.selectItem();
            controller.cpselectItem();
            controller.cmselectItem();
            controller.baselectItem();
            controller.prjbaselectItem();
            controller.gsselectItem();
            controller.prselectItem();
            controller.crselectItem();
            controller.ctselectItem();
            controller.plselectItem();
            controller.oselectItem();
            controller.piOwner();
            controller.ciOwner();
            controller.prjselectItem();
            controller.taxcsSelectItem();
            controller.taxowner();
            controller.getUserTypes();
            controller.getPrepWrapperList();
            controller.getPrepInitiativeList();
            controller.getPrepprjInitiativeList();
            controller.getPrepTaxonomyList();
            controller.getSavedRequestHeaderList();
            controller.getCompletedRequestHeaderList();
            controller.getInprogressRequestHeaderList();
            controller.getActionableRequestHeaderList();
        test.stopTest();
    }
    
    static testmethod void displayRecordsTest()
    {
        Profile p = [ SELECT Id FROM Profile WHERE Name='Standard SEP User (Platform)' ]; 
        
        User u = new User( Alias = 'standt', Email='PREP@testorg.com', EmailEncodingKey='UTF-8', LastName='Testing',
                    LanguageLocaleKey='en_US', LocaleSidKey='en_US', ProfileId = p.Id, TimeZoneSidKey='America/Los_Angeles',
                    UserName='PREP123@testorg.com' ); 
        insert u;
        
        PREP_Discipline__c disp = new PREP_Discipline__c( Name = 'Engineering & Construction', Active__c = true, SCM_Manager__c = u.Id );
        insert disp;
        
        PREP_Category__c cat = new PREP_Category__c( Name = 'Category', Active__c = true, Discipline__c = disp.Id );
        insert cat;
        
        PREP_Sub_Category__c sub = new PREP_Sub_Category__c( Name = 'SubCat', Category__c = cat.Id, Category_Manager__c = u.Id,
                                                                Category_Specialist__c = u.Id, Active__c = true );
        insert sub;
        
        PREP_Initiative__c ini = new PREP_Initiative__c( Initiative_Name__c = 'Test ini', Discipline__c = disp.Id, Category__c = cat.Id,
                                Sub_Category__c = sub.Id, Status__c = 'Active', Sourcing_Program__c='delivary Plan', Type__c = 'Category',
                                Risk_Level__c = 'High', HSSM_Package__c = 'Full', Workbook__c = 'Yes', SRPM_Package__c = 'As Needed',
                                Aboriginal__c = 'No', Global_Sourcing__c = 'Yes', OEM__c = 'Yes', Ariba__c='Yes', Ariba_Exception_Reason__c='Singlr Source',
                                Rate_Validation__c = 'Yes', Estimated_Number_of_Master_Data_Required__c=100, Reverse_Auction__c = 'No',
                                Rebate__c = 'Multiple Area Award', Rebate_Frequency__c = 'Annually',
                                Volume_Discount_Frequency__c='Annually', Payment_Terms__c='Net 30', SCM_Manager__c = u.Id, SCM_Team__c='E&C - Equipment',
                                Category_Manager__c = u.Id, Category_Specialist__c = u.Id, RecordTypeId ='01216000001QPTu' );
        insert ini;
        
        test.startTest();
            PREPTaxonomyController controller = new PREPTaxonomyController();
            controller.ownerFlag = false;
            controller.ownerFlag1 = false;
            controller.prjOwner = false;
            controller.inOwner = false;
            controller.cinOwner = false;
            controller.SCMManager = false;
            controller.scmManager1 = false;
            controller.ctrscmManager = false;
            controller.categorySpecialist = false;
            controller.ctrcategorySpecialist = false;
            controller.CategoryTechnician = false;
            controller.ctrCategoryTechnician = false;
            controller.BusinessAnalyst = false;
            controller.prjBusinessAnalyst = false;
            controller.ctrBusinessAnalyst = false;
            controller.GSO = false;
            controller.ctrGSO = false;
            controller.ProcurementRepresentative = false;
            controller.ctrProcurementRepresentative = false;
            controller.CommercialRepresentative = false;
            controller.ctrCommercialRepresentative = false;
            controller.ProjectLead = false;
            controller.ctrProjectLead = false;
            controller.t_Owner = false;
            controller.t_taxOwner = false;
            controller.t_categoryManager = false;
            controller.t_taxcategoryManager = false;
            controller.t_categorySpecialist = false;
            controller.t_taxcategorySpecialist = false;
            controller.ctrcategoryManager = false;
            controller.CategoryManager = false;
            
            controller.user.category_Manager__c = u.Id;
            controller.displayTaxonomyAndInitiatives();
            
            system.assertEquals( controller.prepWrapper.initWrapperList.size(), 1 );
            
            controller.prepWrapper.initWrapperList[0].selected = true;
            controller.user.Category_Specialist__c = UserInfo.getUserId();
            controller.saveAndSubmit();
            controller.saveTaxonomyAndInitiatives();
        test.stopTest();
    }
    
    static testmethod void displayRecordsForAllTest()
    {
        Profile p = [ SELECT Id FROM Profile WHERE Name='Standard SEP User (Platform)' ]; 
        
        User u = new User( Alias = 'standt', Email='PREP@testorg.com', EmailEncodingKey='UTF-8', LastName='Testing',
                    LanguageLocaleKey='en_US', LocaleSidKey='en_US', ProfileId = p.Id, TimeZoneSidKey='America/Los_Angeles',
                    UserName='PREP123@testorg.com' ); 
        insert u;
        
        User currentUser = [ Select Id, Name, Email from User where Id = :UserInfo.getUserId() ];
            
        Account acc = new Account( Name = 'Test' );
        insert acc;
        
        Contact con = new Contact( LastName = 'Test', AccountId = acc.Id, Email = 'PREP@testorg.com' );
        Contact con1 = new Contact( LastName = 'Test', AccountId = acc.Id, Email = currentUser.Email );
        
        List<Contact> contacts = new List<Contact>{ con, con1 };
        insert contacts;
        
        PREP_Discipline__c disp = new PREP_Discipline__c( Name = 'Engineering & Construction', Active__c = true, SCM_Manager__c = u.Id );
        insert disp;
        
        PREP_Category__c cat = new PREP_Category__c( Name = 'Category', Active__c = true, Discipline__c = disp.Id );
        insert cat;
        
        PREP_Sub_Category__c sub = new PREP_Sub_Category__c( Name = 'SubCat', Category__c = cat.Id, Category_Manager__c = u.Id,
                                                                Category_Specialist__c = u.Id, Active__c = true );
        insert sub;
        
        PREP_Initiative__c ini = new PREP_Initiative__c( Initiative_Name__c = 'Test ini', Discipline__c = disp.Id, Category__c = cat.Id,
                                Sub_Category__c = sub.Id, Status__c = 'Active', Sourcing_Program__c='delivary Plan', Type__c = 'Category',
                                Risk_Level__c = 'High', HSSM_Package__c = 'Full', Workbook__c = 'Yes', SRPM_Package__c = 'As Needed',
                                Aboriginal__c = 'No', Global_Sourcing__c = 'Yes', OEM__c = 'Yes', Ariba__c='Yes', Ariba_Exception_Reason__c='Singlr Source',
                                Rate_Validation__c = 'Yes', Estimated_Number_of_Master_Data_Required__c=100, Reverse_Auction__c = 'No',
                                Rebate__c = 'Multiple Area Award', Rebate_Frequency__c = 'Annually',
                                Volume_Discount_Frequency__c='Annually', Payment_Terms__c='Net 30', SCM_Manager__c = u.Id, SCM_Team__c='E&C - Services',
                                Category_Manager__c = u.Id, Category_Specialist__c = u.Id, RecordTypeId ='01216000001QPTu',
                                Technician__c = u.Id, Business_Analyst__c = u.Id, GSO__c = u.Id );
        
        PREP_Initiative__c ini1 = new PREP_Initiative__c( Initiative_Name__c = 'Test ini', Discipline__c = disp.Id, Category__c = cat.Id,
                                Sub_Category__c = sub.Id, Status__c = 'Active', Sourcing_Program__c='delivary Plan', Type__c = 'Category',
                                Risk_Level__c = 'High', HSSM_Package__c = 'Full', Workbook__c = 'Yes', SRPM_Package__c = 'As Needed',
                                Aboriginal__c = 'No', Global_Sourcing__c = 'Yes', OEM__c = 'Yes', Ariba__c='Yes', Ariba_Exception_Reason__c='Singlr Source',
                                Rate_Validation__c = 'Yes', Estimated_Number_of_Master_Data_Required__c=100, Reverse_Auction__c = 'No',
                                Rebate__c = 'Multiple Area Award', Rebate_Frequency__c = 'Annually',
                                Volume_Discount_Frequency__c='Annually', Payment_Terms__c='Net 30', SCM_Manager__c = u.Id, SCM_Team__c='E&C - Equipment',
                                Commercial_Representative__c = u.Id, Procurement_Representative__c = u.Id, RecordTypeId = '01216000001QPTv',
                                Project_Lead__c = con.Id, Business_Analyst__c = u.Id, GSO__c = u.Id );
        
        List<PREP_Initiative__c> initList = new List<PREP_Initiative__c>{ ini, ini1 };
        insert initList;
        
        PREP_Material_Service_Group__c materialGroup = new PREP_Material_Service_Group__c( Name = 'MS Name', Material_Service_Group_Name__c = 'MS',
                                                        Sub_Category__c = sub.Id, Category_Manager__c = u.Id, SAP_Short_Text_Name__c = 'MS', 
                                                        Active__c = true, Type__c = 'Material', Category_Specialist__c = u.Id, Includes__c = 'MS',
                                                        Does_Not_Include__c = 'MS', Category_Link__c = cat.Id, Discipline_Link__c = disp.Id );
        insert materialGroup;
                
        test.startTest();
            PREPTaxonomyController controller = new PREPTaxonomyController();
            controller.ownerFlag = true;
            controller.ownerFlag1 = true;
            controller.prjOwner = true;
            controller.inOwner = false;
            controller.cinOwner = true;
            controller.SCMManager = true;
            controller.scmManager1 = true;
            controller.ctrscmManager = true;
            controller.categorySpecialist = true;
            controller.ctrcategorySpecialist = true;
            controller.CategoryTechnician = true;
            controller.ctrCategoryTechnician = true;
            controller.BusinessAnalyst = true;
            controller.prjBusinessAnalyst = true;
            controller.ctrBusinessAnalyst = true;
            controller.GSO = true;
            controller.ctrGSO = true;
            controller.ProcurementRepresentative = true;
            controller.ctrProcurementRepresentative = true;
            controller.CommercialRepresentative = true;
            controller.ctrCommercialRepresentative = true;
            controller.ProjectLead = true;
            controller.ctrProjectLead = true;
            controller.t_Owner = true;
            controller.t_taxOwner = true;
            controller.t_categoryManager = true;
            controller.t_taxcategoryManager = true;
            controller.t_categorySpecialist = true;
            controller.t_taxcategorySpecialist = true;
            controller.ctrcategoryManager = true;
            controller.CategoryManager = true;
            
            controller.user.category_Manager__c = u.Id;
            controller.displayTaxonomyAndInitiatives();
            
            system.assertEquals( controller.prepWrapper.initWrapperList.size(), 1 );
            system.assertEquals( controller.prepWrapper.prjinitWrapperList.size(), 1 );
            system.assertEquals( controller.prepWrapper.taxonomyWrapperList.size(), 1 );
            
            controller.prepWrapper.initWrapperList[0].selected = true;
            controller.prepWrapper.prjinitWrapperList[0].selected = true;
            controller.prepWrapper.taxonomyWrapperList[0].selected = true;
            controller.user.Category_Specialist__c = UserInfo.getUserId();
            controller.saveAndSubmit();
            controller.saveTaxonomyAndInitiatives();
        test.stopTest();
    }
}