public class FeedbackClass {

    public Pagereference submit() 
    {                               
        if(comments !=null && caseRecord!=null)
        {        
            caseRecord.Feedback_Comments__c = comments;
        }
        if(selectedFeedback!=null)
        {
            caseRecord.Feedback__c=selectedFeedback;
        }
        
        if(rating!=null)
        {
            caseRecord.Feedback_Rating__c=rating;
        }
        //caseRecord.Status='New';
        Database.SaveResult sr;
        if(caseRecord!=null)
         sr = Database.update(caseRecord);
        if(sr!=null && sr.isSuccess())
        {
        } 
        else
        {
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.Info,'An expected error occurred, please submit after some time.'));
        }
        //PageReference p = new PageReference('http://dev31-huskyenergy.cs19.force.com/apex/Thanks');
        PageReference p = new PageReference('http://huskyenergy.force.com/apex/Thanks');
          
        p.setRedirect(false);
        return p;
    }


    public String MYVAR { get; set; }
    public String selectedFeedback {get;set;}
    public String comments {get;set;}
    public String caseId {get;set;}
    public String rating {get;set;}
    public String caseNumber{get;set;}
    public Case caseRecord{get;set;}
        
      
    public FeedbackClass()
    {
        caseId= ApexPages.currentPage().getParameters().get('id');
        rating = ApexPages.currentPage().getParameters().get('rating');
        if(rating!=null)
        {
            calculate(rating);        
        }
        if(caseId!=null)
        {
            List<Case> cases = [SELECT Feedback__c, CaseNumber, Feedback_Rating__c, Feedback_Comments__c FROM Case WHERE Id=:caseId];
            caseRecord = new Case();
            if(cases!=null && cases.size()>0)
            {
                caseRecord = cases[0];
                caseNumber = cases[0].CaseNumber; 
                //caseNumber = cases[0].Id;               
            }
        }
    }  
    public void submitSatisfactionComment()
    {
        System.debug('Selected :::::::::::::::::::::::'+MYVAR);
        rating = MYVAR;
        if(rating!=null)
        {
            calculate(MYVAR);
        }
    }
    public void calculate(String var)
    {
        if(var.equals('1') )
        {
            selectedFeedback = 'Very Unsatisfied';            
        }
        else if(var.equals('2') )
        {
            selectedFeedback = 'Unsatisfied';            
        }
        else if(var.equals('3') )
        {
            selectedFeedback = 'Satisfied';            
        }
        else if(var.equals('4') )
        {
            selectedFeedback = 'Very Satisfied';           
        }
    }
}