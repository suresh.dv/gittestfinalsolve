@isTest
global class RaM_NotificationWorkOrderMockTest implements WebServiceMock {
    global void doInvoke(
    Object stub, Object request, Map < String, Object > response, String endpoint, String soapAction, String requestName, String responseNS, String responseName, String responseType) {
        RaM_NotificationWOsalesforceComPm.DT_SFDC_Notif_WorkOrder_Resp response_x=new RaM_NotificationWOsalesforceComPm.DT_SFDC_Notif_WorkOrder_Resp();
        // start - specify the response you want to send
        RaM_NotificationWOsalesforceComPm.Response_element response_x1 = new RaM_NotificationWOsalesforceComPm.Response_element();
        response_x1.RequestId = '12345';
        response_x1.Type_x = 'test';
        response_x1.Id = 'ZIPM';
        response_x1.Number_x = '100';
        response_x1.System_x = 'test';
        response_x1.Message = 'test';
        
         RaM_NotificationWOsalesforceComPm.Response_element response_x11 = new RaM_NotificationWOsalesforceComPm.Response_element();
        response_x11.RequestId = '12345';
        response_x11.Type_x = 'test';
        response_x11.Id = 'IW';
        response_x11.Number_x = '080';
        response_x11.System_x = 'test';
        response_x11.Message = 'test';
        
        List<RaM_NotificationWOsalesforceComPm.Response_element> response_x1Lst= new list<RaM_NotificationWOsalesforceComPm.Response_element>();
        
        // end
        response_x1Lst.add(response_x1);
        
        response_x.Response=response_x1Lst;
        response.put('response_x', response_x);
    }
}