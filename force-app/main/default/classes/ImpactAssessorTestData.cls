public class ImpactAssessorTestData {
	
	public static Impact_Assessor__c createImpactAssessor(Id ChangeRequestId, Id impactedFunctionId) {
		Impact_Assessor__c assessor = new Impact_Assessor__c();
		assessor.Change_Request__c = ChangeRequestId;
		assessor.Assessor__c = UserTestData.createSunriseTestUser(1)[0].Id;
		assessor.Name = '<<Do Not Remove';
		assessor.Impacted_Function__c = impactedFunctionId;
		
		return assessor;
	}
}