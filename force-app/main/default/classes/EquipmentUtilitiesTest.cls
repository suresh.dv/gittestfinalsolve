@isTest
private class EquipmentUtilitiesTest {

	@isTest 
	private static void testUserSecuritySystemAdmin() {
	Test.startTest();
		//System Admin
		System.runAs(EquipmentTestData.createUser('Maintenance Administrator','System Administrator','SysAdmin3232195765@test.com')) {
			EquipmentUtilities.UserPermissions userpermissions = new EquipmentUtilities.UserPermissions();
			System.assertEquals(userpermissions.isSystemAdmin, true);
			System.assertEquals(userpermissions.isSapSupportLead, false);
			System.assertEquals(userpermissions.isHogAdmin, false);
			}
		Test.stopTest();
	}
	
	@isTest 
	private static void testUserSecuritySapSupportLead() {
	Test.startTest();
		//Sap Support Lead
		System.runAs(EquipmentTestData.createUser('SAP Support - Lead','Standard HOG - General User','SapSupportLead@test.com')) {
			EquipmentUtilities.UserPermissions userpermissions = new EquipmentUtilities.UserPermissions();
			System.assertEquals(userpermissions.isSystemAdmin, false);
			System.assertEquals(userpermissions.isSapSupportLead, true);
			System.assertEquals(userpermissions.isHogAdmin, false);
			}
		Test.stopTest();
	}
	
	@isTest 
	private static void testUserSecurityHogAdmin() {
	User hogAdminUser = EquipmentTestData.createUser('Maintenance Administrator','Standard HOG - Administrator','HogAdmin@test.com');
	EquipmentTestData.assignPermissionSet(hogAdminUser,'HOG_Administrator');
	Test.startTest();
		//HOG Admin
		System.runAs(hogAdminUser) {
			EquipmentUtilities.UserPermissions userpermissions = new EquipmentUtilities.UserPermissions();
			System.assertEquals(userpermissions.isSystemAdmin, false);
			System.assertEquals(userpermissions.isSapSupportLead, false);
			System.assertEquals(userpermissions.isHogAdmin, true);
			}
		Test.stopTest();
	}

}