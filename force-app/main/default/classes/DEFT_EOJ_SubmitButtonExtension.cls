public class DEFT_EOJ_SubmitButtonExtension {
    
    private List<Attachment> 	attList;
    public HOG_EOJ__c			eojToValidate {get; private set;}
    
    public Boolean				renderButtons {get; private set;}
    
    public DEFT_EOJ_SubmitButtonExtension(ApexPages.StandardController stdController) {
        String objId = stdController.getId();
        
        attList = [select Name from Attachment where parentId = :objId];
        
        eojToValidate = [select Bail_Well__c, Budget_SRP__c, Coil_Kit2__c ,Coil_Kit1__c, Drain_Type2__c, 
                        		Comments2__c, Comments1__c, Drain_Placement2__c, Drain_Placement1__c,
                         		Drain_Type1__c, EOJ_Details__c, Final_Cost__c, Final_Tag_Depth__c, 
                         		Fluid_Inj__c, Fluid_Recovered__c, Initial_Tag_Depth__c, Intake_Depth2__c, 
                         		Intake_Depth1__c, Job_Cost_Comments__c, Length_Off_Tag2__c, Length_Off_Tag1__c,
                         		Next_Location__c, NTT_Type2__c, NTT_Type1__c, PBTD__c, Perf_Interval__c, Pin_Size2__c,
                         		Pin_Size1__c, Polished_Rod_Length2__c, Polished_Rod_Length1__c, Previous_Failure_Detail__c, 
                         		Previous_Service_Date__c, Producing_Zone__c, Pull_Out_of_Sand__c, 
                         		Pump_Displacement_Size2__c, Pump_Displacement_Size1__c,
                         		Pump_Elastomer2__c, Pump_Elastomer1__c, Pump_Lift2__c, Pump_Lift1__c,
                         		Pump_Type2__c, Pump_Type1__c, Pump_Vendor2__c, Pump_Vendor1__c,
                         		Rig_Hours__c, Rig__c, Rig_Company__c, Rod_Comments2__c, Rod_Comments1__c,
                         		Rod_Condition2__c, Rod_Condition1__c, Rod_Failure__c, Rod_Grade2__c, Rod_Grade1__c,
                         		Rod_Size2__c, Rod_Size1__c, Rod_Type2__c, Rod_Type1__c, Rotor_Condition2__c,
                         		Rotor_Condition1__c, Rotor_Length2__c, Rotor_Length1__c, Rotor_Tag_Type2__c,
                         		Rotor_Tag_Type1__c, Scope_Joint2__c, Scope_Joint1__c, Service_Completed__c,
                         		Service_Detail__c, Service_General__c, Service_Rig_Program__c, Service_Started__c,
                         		Shear_Placement2__c, Shear_Placement1__c, Shear_Type2__c,
                         		Shear_Type1__c, Stator_Condition2__c, Stator_Condition1__c, Status__c, 
                         		Test_Efficiency2__c, Test_Efficiency1__c, Tubing_Failure__c, Tubing_JIH2__c,
                         		Tubing_JIH1__c, Tubing_Size2__c, Tubing_Size1__c
                        		//Service_Type__c,	Submitted__c,, Well_Servicing_Manager__c
                         from HOG_EOJ__c where Id = :objId];
        
        renderButtons = true;
    }
    
    public PageReference submit(){
        
        Boolean isError = false;
        renderButtons = false;
        
        if(attList.isEmpty()){
            ApexPages.addmessage(new ApexPages.message(ApexPages.severity.FATAL,'You can\'t submit EOJ without attachment.'));
        	isError = true;    
        }
        if(String.isBlank(eojToValidate.Bail_Well__c)){
            ApexPages.addmessage(new ApexPages.message(ApexPages.severity.FATAL,'Bail Well is empty.'));
        	isError = true;  
        }
        if(eojToValidate.Budget_SRP__c == null){
            ApexPages.addmessage(new ApexPages.message(ApexPages.severity.FATAL,'Budget is empty.'));
        	isError = true;  
        }
        if(String.isBlank(eojToValidate.Coil_Kit2__c)){
            ApexPages.addmessage(new ApexPages.message(ApexPages.severity.FATAL,'Coil Kit [I] is empty.'));
        	isError = true;  
        }
        if(String.isBlank(eojToValidate.Coil_Kit1__c)){
            ApexPages.addmessage(new ApexPages.message(ApexPages.severity.FATAL,'Coil Kit [P] is empty.'));
        	isError = true;  
        }
        if(String.isBlank(eojToValidate.Drain_Type2__c)){
            ApexPages.addmessage(new ApexPages.message(ApexPages.severity.FATAL,'Drain Type [I] is empty.'));
        	isError = true;  
        }
        if(String.isBlank(eojToValidate.Drain_Type1__c)){
            ApexPages.addmessage(new ApexPages.message(ApexPages.severity.FATAL,'Drain Type [P] is empty.'));
        	isError = true;  
        }
        if(String.isBlank(eojToValidate.Drain_Placement2__c)){
            ApexPages.addmessage(new ApexPages.message(ApexPages.severity.FATAL,'Drain Placement [I] is empty.'));
        	isError = true;  
        }
        if(String.isBlank(eojToValidate.Drain_Placement1__c)){
            ApexPages.addmessage(new ApexPages.message(ApexPages.severity.FATAL,'Drain Placement [P] is empty.'));
        	isError = true;  
        }
        if(String.isBlank(eojToValidate.Comments2__c)){
            ApexPages.addmessage(new ApexPages.message(ApexPages.severity.FATAL,'Comments [I] is empty.'));
        	isError = true;  
        }
        if(String.isBlank(eojToValidate.Comments1__c)){
            ApexPages.addmessage(new ApexPages.message(ApexPages.severity.FATAL,'Comments [P] is empty.'));
        	isError = true;  
        }
        if(String.isBlank(eojToValidate.EOJ_Details__c)){
            ApexPages.addmessage(new ApexPages.message(ApexPages.severity.FATAL,'EOJ Details is empty.'));
        	isError = true;  
        }
        if(String.isBlank(eojToValidate.Final_Tag_Depth__c)){
            ApexPages.addmessage(new ApexPages.message(ApexPages.severity.FATAL,'Final Tag Depth is empty.'));
        	isError = true;  
        }
        if(String.isBlank(eojToValidate.Fluid_Inj__c)){
            ApexPages.addmessage(new ApexPages.message(ApexPages.severity.FATAL,'Fluid Inj (m3) is empty.'));
        	isError = true;  
        }
        if(String.isBlank(eojToValidate.Fluid_Recovered__c)){
            ApexPages.addmessage(new ApexPages.message(ApexPages.severity.FATAL,'Fluid Recovered (m3) is empty.'));
        	isError = true;  
        }
        if(String.isBlank(eojToValidate.Initial_Tag_Depth__c)){
            ApexPages.addmessage(new ApexPages.message(ApexPages.severity.FATAL,'Initial Tag Depth is empty.'));
        	isError = true;  
        }
        if(String.isBlank(eojToValidate.Intake_Depth2__c)){
            ApexPages.addmessage(new ApexPages.message(ApexPages.severity.FATAL,'Intake Depth [I] is empty.'));
        	isError = true;  
        }
        if(String.isBlank(eojToValidate.Intake_Depth1__c)){
            ApexPages.addmessage(new ApexPages.message(ApexPages.severity.FATAL,'Intake Depth [P] is empty.'));
        	isError = true;  
        }
        if(String.isBlank(eojToValidate.Job_Cost_Comments__c)){
            ApexPages.addmessage(new ApexPages.message(ApexPages.severity.FATAL,'Job Cost Comments is empty.'));
        	isError = true;  
        }
        if(String.isBlank(eojToValidate.Length_Off_Tag2__c)){
            ApexPages.addmessage(new ApexPages.message(ApexPages.severity.FATAL,'Length Off Tag [I] is empty.'));
        	isError = true;  
        }
        if(String.isBlank(eojToValidate.Length_Off_Tag1__c)){
            ApexPages.addmessage(new ApexPages.message(ApexPages.severity.FATAL,'Length Off Tag [P] is empty.'));
        	isError = true;  
        }
        if(String.isBlank(eojToValidate.Next_Location__c)){
            ApexPages.addmessage(new ApexPages.message(ApexPages.severity.FATAL,'Next Location is empty.'));
        	isError = true;  
        }
        if(String.isBlank(eojToValidate.NTT_Type2__c)){
            ApexPages.addmessage(new ApexPages.message(ApexPages.severity.FATAL,'NTT Type [I] is empty.'));
        	isError = true;  
        }
        if(String.isBlank(eojToValidate.NTT_Type1__c)){
            ApexPages.addmessage(new ApexPages.message(ApexPages.severity.FATAL,'NTT Type [P] is empty.'));
        	isError = true;  
        }
        if(String.isBlank(eojToValidate.PBTD__c)){
            ApexPages.addmessage(new ApexPages.message(ApexPages.severity.FATAL,'PBTD is empty.'));
        	isError = true;  
        }
        if(String.isBlank(eojToValidate.Perf_Interval__c)){
            ApexPages.addmessage(new ApexPages.message(ApexPages.severity.FATAL,'Perf Interval is empty.'));
        	isError = true;  
        }
        if(String.isBlank(eojToValidate.Pin_Size2__c)){
            ApexPages.addmessage(new ApexPages.message(ApexPages.severity.FATAL,'Pin Size [I] is empty.'));
        	isError = true;  
        }
        if(String.isBlank(eojToValidate.Pin_Size1__c)){
            ApexPages.addmessage(new ApexPages.message(ApexPages.severity.FATAL,'Pin Size [P] is empty.'));
        	isError = true;  
        }
        if(String.isBlank(eojToValidate.Polished_Rod_Length2__c)){
            ApexPages.addmessage(new ApexPages.message(ApexPages.severity.FATAL,'Polished Rod Length [I] is empty.'));
        	isError = true;  
        }
        if(String.isBlank(eojToValidate.Polished_Rod_Length1__c)){
            ApexPages.addmessage(new ApexPages.message(ApexPages.severity.FATAL,'Polished Rod Length [P] is empty.'));
        	isError = true;  
        }
        if(String.isBlank(eojToValidate.Previous_Failure_Detail__c)){
            ApexPages.addmessage(new ApexPages.message(ApexPages.severity.FATAL,'Previous Failure - Detail is empty.'));
        	isError = true;  
        }
        if(eojToValidate.Previous_Service_Date__c == null){
            ApexPages.addmessage(new ApexPages.message(ApexPages.severity.FATAL,'Previous Service Date is empty.'));
        	isError = true;  
        }
        if(String.isBlank(eojToValidate.Producing_Zone__c)){
            ApexPages.addmessage(new ApexPages.message(ApexPages.severity.FATAL,'Producing Zone is empty.'));
        	isError = true;  
        }
        if(String.isBlank(eojToValidate.Pull_Out_of_Sand__c)){
            ApexPages.addmessage(new ApexPages.message(ApexPages.severity.FATAL,'Pull Out of Sand is empty.'));
        	isError = true;  
        }
        if(String.isBlank(eojToValidate.Pump_Elastomer2__c)){
            ApexPages.addmessage(new ApexPages.message(ApexPages.severity.FATAL,'Pump Elastomer [I] is empty.'));
        	isError = true;  
        }
        if(String.isBlank(eojToValidate.Pump_Elastomer1__c)){
            ApexPages.addmessage(new ApexPages.message(ApexPages.severity.FATAL,'Pump Elastomer [P] is empty.'));
        	isError = true;  
        }
        if(String.isBlank(eojToValidate.Pump_Lift2__c)){
            ApexPages.addmessage(new ApexPages.message(ApexPages.severity.FATAL,'Pump Lift [I] is empty.'));
        	isError = true;  
        }
        if(String.isBlank(eojToValidate.Pump_Lift1__c)){
            ApexPages.addmessage(new ApexPages.message(ApexPages.severity.FATAL,'Pump Lift [P] is empty.'));
        	isError = true;  
        }
        if(String.isBlank(eojToValidate.Pump_Type2__c)){
            ApexPages.addmessage(new ApexPages.message(ApexPages.severity.FATAL,'Pump Type (Installed) is empty.'));
        	isError = true;  
        }
        if(String.isBlank(eojToValidate.Pump_Type1__c)){
            ApexPages.addmessage(new ApexPages.message(ApexPages.severity.FATAL,'Pump Type (Pulled) is empty.'));
        	isError = true;  
        }
        
        if(String.isBlank(eojToValidate.Pump_Vendor2__c)){
            ApexPages.addmessage(new ApexPages.message(ApexPages.severity.FATAL,'Pump Vendor [I] is empty.'));
        	isError = true;  
        } else if( eojToValidate.Pump_Vendor2__c != 'Other' && String.isBlank(eojToValidate.Pump_Displacement_Size2__c)){
            ApexPages.addmessage(new ApexPages.message(ApexPages.severity.FATAL,'Pump Displacement/Size [I] is empty.'));
        	isError = true;  
        }
        if(String.isBlank(eojToValidate.Pump_Vendor1__c)){
            ApexPages.addmessage(new ApexPages.message(ApexPages.severity.FATAL,'Pump Vendor [P] is empty.'));
        	isError = true;  
        } else if( eojToValidate.Pump_Vendor1__c != 'Other' && String.isBlank(eojToValidate.Pump_Displacement_Size1__c)){
            ApexPages.addmessage(new ApexPages.message(ApexPages.severity.FATAL,'Pump Displacement/Size [P] is empty.'));
        	isError = true;  
        }        
        
        if(eojToValidate.Rig_Hours__c == null){
            ApexPages.addmessage(new ApexPages.message(ApexPages.severity.FATAL,'Rig Hours is empty.'));
        	isError = true;  
        }
        if(String.isBlank(eojToValidate.Rig__c)){
            ApexPages.addmessage(new ApexPages.message(ApexPages.severity.FATAL,'Rig Name is empty.'));
        	isError = true;  
        }
        if(String.isBlank(eojToValidate.Rig_Company__c)){
            ApexPages.addmessage(new ApexPages.message(ApexPages.severity.FATAL,'Rig Name Company is empty.'));
        	isError = true;  
        }
        if(String.isBlank(eojToValidate.Rod_Comments2__c)){
            ApexPages.addmessage(new ApexPages.message(ApexPages.severity.FATAL,'Rod Comments [I] is empty.'));
        	isError = true;  
        }
        if(String.isBlank(eojToValidate.Rod_Comments1__c)){
            ApexPages.addmessage(new ApexPages.message(ApexPages.severity.FATAL,'Rod Comments [P] is empty.'));
        	isError = true;  
        }
        if(String.isBlank(eojToValidate.Rod_Condition2__c)){
            ApexPages.addmessage(new ApexPages.message(ApexPages.severity.FATAL,'Rod Condition [I] is empty.'));
        	isError = true;  
        }
        if(String.isBlank(eojToValidate.Rod_Condition1__c)){
            ApexPages.addmessage(new ApexPages.message(ApexPages.severity.FATAL,'Rod Condition [P] is empty.'));
        	isError = true;  
        }
        if(String.isBlank(eojToValidate.Rod_Failure__c)){
            ApexPages.addmessage(new ApexPages.message(ApexPages.severity.FATAL,'Rod Failure is empty.'));
        	isError = true;  
        }
        if(String.isBlank(eojToValidate.Rod_Grade2__c)){
            ApexPages.addmessage(new ApexPages.message(ApexPages.severity.FATAL,'Rod Grade [I] is empty.'));
        	isError = true;  
        }
        if(String.isBlank(eojToValidate.Rod_Grade1__c)){
            ApexPages.addmessage(new ApexPages.message(ApexPages.severity.FATAL,'Rod Grade [P] is empty.'));
        	isError = true;  
        }
        if(String.isBlank(eojToValidate.Rod_Size2__c)){
            ApexPages.addmessage(new ApexPages.message(ApexPages.severity.FATAL,'Rod Size [I] is empty.'));
        	isError = true;  
        }
        if(String.isBlank(eojToValidate.Rod_Size1__c)){
            ApexPages.addmessage(new ApexPages.message(ApexPages.severity.FATAL,'Rod Size [P] is empty.'));
        	isError = true;  
        }
        if(String.isBlank(eojToValidate.Rod_Type2__c)){
            ApexPages.addmessage(new ApexPages.message(ApexPages.severity.FATAL,'Rod Type [I] is empty.'));
        	isError = true;  
        }
        if(String.isBlank(eojToValidate.Rod_Type1__c)){
            ApexPages.addmessage(new ApexPages.message(ApexPages.severity.FATAL,'Rod Type [P] is empty.'));
        	isError = true;  
        }
        if(String.isBlank(eojToValidate.Rotor_Condition2__c)){
            ApexPages.addmessage(new ApexPages.message(ApexPages.severity.FATAL,'Rotor Condition [I] is empty.'));
        	isError = true;  
        }
        if(String.isBlank(eojToValidate.Rotor_Condition1__c)){
            ApexPages.addmessage(new ApexPages.message(ApexPages.severity.FATAL,'Rotor Condition [P] is empty.'));
        	isError = true;  
        }
        if(String.isBlank(eojToValidate.Rotor_Length2__c)){
            ApexPages.addmessage(new ApexPages.message(ApexPages.severity.FATAL,'Rotor Length [I] is empty.'));
        	isError = true;  
        }
        if(String.isBlank(eojToValidate.Rotor_Length1__c)){
            ApexPages.addmessage(new ApexPages.message(ApexPages.severity.FATAL,'Rotor Length [P] is empty.'));
        	isError = true;  
        }
        if(String.isBlank(eojToValidate.Rotor_Tag_Type2__c)){
            ApexPages.addmessage(new ApexPages.message(ApexPages.severity.FATAL,'Rotor Tag Type [I] is empty.'));
        	isError = true;  
        }
        if(String.isBlank(eojToValidate.Rotor_Tag_Type1__c)){
            ApexPages.addmessage(new ApexPages.message(ApexPages.severity.FATAL,'Rotor Tag Type [P] is empty.'));
        	isError = true;  
        }
        if(String.isBlank(eojToValidate.Scope_Joint2__c)){
            ApexPages.addmessage(new ApexPages.message(ApexPages.severity.FATAL,'Scope Joint [I] is empty.'));
        	isError = true;  
        }
        if(String.isBlank(eojToValidate.Scope_Joint1__c)){
            ApexPages.addmessage(new ApexPages.message(ApexPages.severity.FATAL,'Scope Joint [P] is empty.'));
        	isError = true;  
        }
        if(String.isBlank(eojToValidate.Shear_Placement2__c)){
            ApexPages.addmessage(new ApexPages.message(ApexPages.severity.FATAL,'Shear Placement [I] is empty.'));
        	isError = true;  
        }
        if(String.isBlank(eojToValidate.Shear_Placement1__c)){
            ApexPages.addmessage(new ApexPages.message(ApexPages.severity.FATAL,'Shear Placement [P] is empty.'));
        	isError = true;  
        }
        if(String.isBlank(eojToValidate.Shear_Type2__c)){
            ApexPages.addmessage(new ApexPages.message(ApexPages.severity.FATAL,'Shear Type [I] is empty.'));
        	isError = true;  
        }
        if(String.isBlank(eojToValidate.Shear_Type1__c)){
            ApexPages.addmessage(new ApexPages.message(ApexPages.severity.FATAL,'Shear Type [P] is empty.'));
        	isError = true;  
        }
        if(String.isBlank(eojToValidate.Stator_Condition2__c)){
            ApexPages.addmessage(new ApexPages.message(ApexPages.severity.FATAL,'Stator Condition [I] is empty.'));
        	isError = true;  
        }
        if(String.isBlank(eojToValidate.Stator_Condition1__c)){
            ApexPages.addmessage(new ApexPages.message(ApexPages.severity.FATAL,'Stator Condition [P] is empty.'));
        	isError = true;  
        }
        if(String.isBlank(eojToValidate.Test_Efficiency2__c)){
            ApexPages.addmessage(new ApexPages.message(ApexPages.severity.FATAL,'Test Efficiency [I] is empty.'));
        	isError = true;  
        }
        if(String.isBlank(eojToValidate.Test_Efficiency1__c)){
            ApexPages.addmessage(new ApexPages.message(ApexPages.severity.FATAL,'Test Efficiency [P] is empty.'));
        	isError = true;  
        }      
        if(String.isBlank(eojToValidate.Tubing_Failure__c)){
            ApexPages.addmessage(new ApexPages.message(ApexPages.severity.FATAL,'Tubing Failure is empty.'));
        	isError = true;  
        }
        if(String.isBlank(eojToValidate.Tubing_JIH2__c)){
            ApexPages.addmessage(new ApexPages.message(ApexPages.severity.FATAL,'Tubing JIH [I] is empty.'));
        	isError = true;  
        }
        if(String.isBlank(eojToValidate.Tubing_JIH1__c)){
            ApexPages.addmessage(new ApexPages.message(ApexPages.severity.FATAL,'Tubing JIH [P] is empty.'));
        	isError = true;  
        }
        if(String.isBlank(eojToValidate.Tubing_Size2__c)){
            ApexPages.addmessage(new ApexPages.message(ApexPages.severity.FATAL,'Tubing Size [I] is empty.'));
        	isError = true;  
        }
        if(String.isBlank(eojToValidate.Tubing_Size1__c)){
            ApexPages.addmessage(new ApexPages.message(ApexPages.severity.FATAL,'Tubing Size [P] is empty.'));
        	isError = true;  
        }          
        if(!isError){
            eojToValidate.Status__c = 'Submitted'; 
            
            try{
                update eojToValidate;
                ApexPages.addmessage(new ApexPages.message(ApexPages.severity.CONFIRM,'EOJ successfully submitted.'));
            } catch (Exception ex) {
                ApexPages.addmessage(new ApexPages.message(ApexPages.severity.ERROR,'An unexpected error has occurred: ' + ex.getMessage()));
            }

        }
        
        
        return null;

    }

}