global class PTSchedulableInactivateMeter implements Schedulable {
    global void execute(SchedulableContext ctx){
        set<Service__c> serviceUtilityMeterList = new set<Service__c>([select id,name,(select id,name,createddate,meter_status__c from Utility_Meters__r where meter_status__c != 'Inactive' order by createddate) from service__c]);
        set<Utility_Meter__c> utilityMeterList = new set <Utility_Meter__c>();
        for (Service__c ser : serviceUtilityMeterList){
			if(ser.utility_meters__r.size()>1)
            {
                system.debug(ser.name);
                integer count = 0;
                do{
                    Utility_Meter__c utilityMeter = ser.utility_meters__r[count];
                    system.debug(utilityMeter);
                    utilityMeterList.add(utilityMeter);
                    count++;
                }while(ser.utility_Meters__r[count].id != ser.utility_Meters__r[ser.utility_meters__r.size()-1].id); 				
            }
        }
        
        for (Utility_Meter__c utilityMeter : utilityMeterList)
        {
        	utilityMeter.Meter_Status__c = 'Inactive';
            update utilityMeter;
        }
    }    
}