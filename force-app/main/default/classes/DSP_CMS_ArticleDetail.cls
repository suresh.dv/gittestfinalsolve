global virtual with sharing class DSP_CMS_ArticleDetail extends DSP_CMS_ArticleController
{
    global override String getHTML()
    {
        return articleDetailHTML();
    }
}