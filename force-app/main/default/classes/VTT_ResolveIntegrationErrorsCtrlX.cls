//Test Class: VTT_ResolveIntegrationErrorsCtrlXTest
public with sharing class VTT_ResolveIntegrationErrorsCtrlX {
	private ApexPages.StandardSetController stdController;
    private List<VTT_Integration_Error__c> filteredErrorList;
    private Set<Id> activityIdSet;
    private List<Work_Order_Activity__c> activitiesToUpdate;

    public List<VTT_Integration_Error__c> selectedErrorList {get; private set;}

    // The extension constructor initializes the private member
    // variable mysObject by using the getRecord method from the standard
    // controller.
    public VTT_ResolveIntegrationErrorsCtrlX(ApexPages.StandardSetController stdController) {
        this.stdController = stdController;
        refreshResults();
    }

    public void UpdateSAP() {
    	this.activitiesToUpdate = [Select Id, Name, Operation_Number__c, Sub_Operation_Number__c,
                                          Maintenance_Work_Order__r.Work_Order_Number__c, Description__c, 
                                          Work_Details__c, Status__c, User_Status__c,
                                          (Select Id, Name, Status__c, Status_Icon__c, Error_Log__c
                                    	   From VTT_Integration_Error__r
                                           Where RecordType.DeveloperName =: VTT_IntegrationUtilities.RECORDTYPE_UPDATE_ACTIVITY_INTEGRATION_ERROR
                                           Order By SystemModStamp Desc)
                                   From Work_Order_Activity__c where Id in :activityIdSet];

    	//Callout (Synchronous to update page)
    	VTT_IntegrationUtilities.handleActivityUpdateResponse(VTT_IntegrationUtilities.SAP_UpdateWorkOrderActivityStatuses(
            this.activitiesToUpdate), this.activitiesToUpdate);
        
    	//Refresh Results on page
    	this.refreshResults();
    }

    private void getFilteredConfirmationLists() {
        this.filteredErrorList = new List<VTT_Integration_Error__c>();
        this.activityIdSet = new Set<Id>();
        for(VTT_Integration_Error__c error : this.selectedErrorList) {
            if(error.Status__c == VTT_IntegrationUtilities.STATUS_ERROR) {
            	this.filteredErrorList.add(error);
            	this.activityIdSet.add(error.Work_Order_Activity__c);
            }
        }
    }

    private void refreshResults() {
        this.selectedErrorList = [Select Id, Name, Status__c, Status_Icon__c, Display_Name__c,
        								 Error_Log__c, Work_Order_Activity__c
                                  From VTT_Integration_Error__c
                                  Where Id In :this.stdController.getSelected()];
        getFilteredConfirmationLists();
    }
}