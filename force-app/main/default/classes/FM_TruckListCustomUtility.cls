/*************************************************************************************************\
Author:         ??? 
Company:        Husky Energy
Description:    Utility class for Truck List.
Test Class:     FM_TruckListCustomCtrlTest
                FM_TruckListCustomUtilityTest
History:        
**************************************************************************************************/
public class FM_TruckListCustomUtility {

    public static Boolean isFirst;

    /* *** *** *** *** *** *** *** *** *** *** *** *** *** *** *** *** *** *** *** *** *** *** *** *** ***
     * truck trips query helper
     * *** *** *** *** *** *** *** *** *** *** *** *** *** *** *** *** *** *** *** *** *** *** *** *** *** */

    public static String getQueryString(){
        return 'Select Id, Name, Run_Sheet_Date__c, Shift__c, Unit__c, Truck_Trip_Status__c, Unit__r.Carrier__r.Carrier_Name__c, Carrier__r.Carrier_Name_For_Fluid__c, CreatedBy.Name, '+
            'TL_Phone_Lookup__r.Name, TL_Phone_Lookup__r.Phone__c, TL_Phone_Lookup__r.Grouping__c, TL_Dispatched_By__c,TL_First_Booked_By__c,TL_First_Booked_By__r.Name, TL_Dispatched_On__c, Unit__r.Unit_Email__c, TL_Truck_List_Comments__c, '+
            'Carrier__r.Name, TL_Phone_Display__c, Route__c, Route_Id__c, Load_Request__r.Act_Flow_Rate__c, TL_Dispatched_By__r.Name, Load_Type__c, '+
            'Load_Request__r.Equipment_Tank__r.SCADA_Tank_Level__c, Load_Request__r.Equipment_Tank__r.Latest_Tank_Reading_Date__c, Carrier__c, Facility__c, Well__c, '+
            'Load_Request__r.Name, Load_Request__r.Tank_Low_Level__c, Run_Sheet_Lookup__c, Order__c, Product__c, Load_Type_Formula__c, Standing_Comments__c, Sour__c, '+
            'Run_Sheet_Lookup__r.RM_County__c, Axle__c, Unit_Configuration__c, Unit__r.Name, Facility_Type__c, Facility_Lookup__c, Facility_Lookup__r.Name, '+
            'Facility_Lookup__r.Facility_Name_For_Fluid__c, Location_Lookup__c, Location_Lookup__r.Name, Location_Lookup__r.Location_Name_For_Fluid__c,' +
            'Facility_String_Hidden__c, Rebook_Reason__c, '+
            'Load_Request__r.Source_Facility__c, Load_Request__r.Source_Facility__r.Name, Load_Request__r.Source_Location__c, Load_Request__r.Source_Location__r.Name,Load_Request__c, Load_Request_Cancel_Reason__c,Planned_Dispatch_Date__c, '+
            'Stuck__c, Shift_Day__c, Load_Weight__c, LastModifiedDate, ' +
            'Facility_Lookup_Original__c, Facility_Lookup_Original__r.Name, Facility_Lookup_Original__r.Facility_Name_for_Fluid__c, ' + 
            'Well_Lookup_Original__c, Well_Lookup_Original__r.Name, Well_Lookup_Original__r.Location_Name_for_Fluid__c, Is_Reroute__c, ' + 
            'Load_Request__r.Source_Location__r.PVR_AVGVOL_30D_OIL__c, Load_Request__r.Source_Location__r.PVR_AVGVOL_30D_WATER__c, Load_Request__r.Source_Location__r.PVR_AVGVOL_30D_SAND__c, ' +
            'Product_Change_Reason__c, Product_Change_Comments__c ';
    }
    

    public static String whereOrAnd(){
        if(isFirst){
            isFirst = false;
            return ' where';
        }
        else{
            return ' and';
        }
    }
    public static String whereOrOr(){
        if(isFirst){
            isFirst = false;
            return ' where';
        }
        else{
            return ' or';
        }
    }

    public static List<FM_Truck_Trip__c> getTruckTripList(Date dispatchDate, String shift, String status, String unit, List<String> desks) {
        return getTruckTripMap(dispatchDate, shift, status, unit, desks).values();
    }

    public static Map<Id, FM_Truck_Trip__c> getTruckTripMap(Date dispatchDate, String shift, String status, String unit, List<String> desks){

        system.debug('FM_TruckListCustomUtility.getTruckTripMap');
        system.debug('dispatchDate:'+dispatchDate);
        system.debug('shift:'+shift);
        system.debug('status:'+status);
        system.debug('unit:'+unit);
        system.debug('desks:'+desks);

        String queryString = FM_TruckListCustomUtility.getQueryString() + 'From FM_Truck_Trip__c';

        isFirst = true;
        if(dispatchDate != null){
            queryString += whereOrAnd();
            queryString += ' Planned_Dispatch_Date__c =: dispatchDate';
        }
        if(!String.isBlank(shift)){
            queryString += whereOrAnd();
            queryString += ' Shift__c = \''+shift+'\'';
        }
        if(!String.isBlank(status)){
            queryString += whereOrAnd();
            //queryString += ' Truck_Trip_Status__c = \''+status+'\'';
            queryString += ' Truck_Trip_Status__c In ' + FM_Utilities.getStatusQueryString(status);
        } else {
            queryString += whereOrAnd();
            queryString += ' Truck_Trip_Status__c != \'' + FM_TruckTrip_Utilities.STATUS_UNCANCELLED + '\'';
        }
        if(!String.isBlank(unit)){
            queryString += whereOrAnd();
            queryString += ' Unit__c = \''+unit+'\'';
        }
        if(desks != null && !desks.isEmpty()) {
            queryString += whereOrAnd();
            queryString += ' (TL_Phone_Lookup__c In :desks Or';
            queryString += ' Load_Request__r.Source_Facility__r.Plant_Section__r.Dispatch_Desk__c In :desks Or';
            queryString += ' Load_Request__r.Source_Location__r.Route__r.Dispatch_Desk__c In :desks)';
        }


        queryString += ' ORDER BY Unit__r.Name ASC NULLS LAST, Run_Sheet_Date__c DESC, Shift__c, Truck_Trip_Status__c, LastModifiedDate LIMIT 1000';

        system.debug('queryString: '+queryString);

        return new Map<Id, FM_Truck_Trip__c>((List<FM_Truck_Trip__c>) database.query(queryString));
    }

    public static Map<Id, FM_Truck_Trip__c> getTruckTripMap(Set<Id> truckTripIdSet) {
        return new Map<Id, FM_Truck_Trip__c>((List<FM_Truck_Trip__c>) database.query(getQueryString() + 'From FM_Truck_Trip__c where Id IN :truckTripIdSet'));
    }
}