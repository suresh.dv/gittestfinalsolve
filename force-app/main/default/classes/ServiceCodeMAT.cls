/*----------------------------------------------------------------------------------------------------------
Author     : Rey Ocampo
Company    : Husky Energy
Description: A controller class for creating a MAT Code
Inputs     : ApexPages.StandardController
Test Class : TestServiceCodeMAT
History    :
			01.08.16	rbo Modifications due to Refactoring
							replaced all HOG_Rigless_Servicing_Form__c with HOG_Maintenance_Servicing_Form__c				
-------------------------------------------------------------------------------------------------------------*/
public with sharing class ServiceCodeMAT
{                
    @TestVisible private ApexPages.StandardController std;

    @TestVisible private Boolean isNew {get; set;}

    public String recordTypeID {get; set;}

    public HOG_Service_Code_MAT__c serviceCodeMAT;

    Map<String, String> mapSelectListValues = new Map<String, String>();
    
    private static final String SERVICE_CODE_NEW_PAGE_URI = '/apex/ServiceCodeMAT';
            
    public ServiceCodeMAT (ApexPages.StandardController controller)
    {
        std = controller;

        serviceCodeMAT = (HOG_Service_Code_MAT__c)controller.getRecord();

        //isNew = String.isEmpty(serviceCodeMAT.id);
 
        System.debug('\n***************************************\n'
            + 'serviceCodeMAT'
            + '\nserviceCodeMAT: ' + serviceCodeMAT
            + '\n***************************************\n');  
                               
        getRecordTypes();
    }

    List<SelectOption> recordTypeOptions = new List<SelectOption>();

    public void getRecordTypes()
    {
        Schema.DescribeSObjectResult objectResult = HOG_Maintenance_Servicing_Form__c.SObjectType.getDescribe();
        List<Schema.RecordTypeInfo> recordTypeInfo = objectResult.getRecordTypeInfos();

        System.debug('\n***************************************\n'
            + 'METHOD: ServiceCodeMATController.getRecordTypes()'
            + '\nrecordTypeInfo.size(): ' + recordTypeInfo.size()
            + '\nCurrent record: ' + serviceCodeMAT
            + '\n***************************************\n');        
                     
        for (Schema.RecordTypeInfo rt : recordTypeInfo)  
        {                        
            //recordTypeOptions.add(new SelectOption(rt.getRecordTypeId(), rt.getName(), rt.isAvailable()));
            recordTypeOptions.add(new SelectOption(rt.getRecordTypeId(), rt.getName()));

            // prepare map of select options
            mapSelectListValues.put(rt.getRecordTypeId(), rt.getName());

            System.debug('\n***************************************\n'
                + 'Schema.RecordTypeInfo'
                + '\nrecordTypeId: ' + rt.getRecordTypeId()
                + '\nrecordTypeName: ' + rt.getName()
                + '\nisAvailable: ' + rt.isAvailable()
                + '\n***************************************\n');                        
        }        
    }

    public List<SelectOption> getRecordTypeOptions()
    {
        return recordTypeOptions;
    }

    public PageReference newServiceCodeMAT() 
    {
        HOG_Service_Code_MAT__c newServiceCode = new HOG_Service_Code_MAT__c();
                
        Schema.DescribeSObjectResult describeResult = newServiceCode.getSObjectType().getDescribe();
        String safeReturnUrl = EncodingUtil.urlEncode(SERVICE_CODE_NEW_PAGE_URI, 'UTF-8');
        String addNewUrl = '/' 
            + describeResult.getKeyPrefix() + '/e?' 
            + 'retURL=' + safeReturnUrl;
                    
        PageReference pr = new PageReference(addNewUrl);
        pr.setRedirect(true);
        return pr;
    }
    
    public pageReference cancel() 
    {
       return std.cancel();
    }

    public pageReference save() 
    {
        for (SelectOption item : recordTypeOptions)  
        {                        
            System.debug('\n***************************************\n'
                + 'Schema.RecordTypeInfo'
                + '\nrecordTypeId: ' + item
                + '\nrecordTypeName: ' 
                + '\nisAvailable: ' + 
                + '\n***************************************\n');                        
        }   
        
        System.debug('\n***************************************\n'
            + 'METHOD: save()'
            + '\nRecord Type ID: ' + serviceCodeMAT.Record_Type_ID__c
            + '\nRecord Type Name: ' 
            + '\nCurrent record: ' + mapSelectListValues.get(serviceCodeMAT.Record_Type_ID__c)
            + '\n***************************************\n');        
        
        //upsert serviceCodeMAT;
                        
        //PageReference pr = new PageReference('/' + serviceCodeMAT.id);
        //pr.setRedirect(true);
        //return pr;

        //return null;
                
        serviceCodeMAT.Record_Type_Name__c = mapSelectListValues.get(serviceCodeMAT.Record_Type_ID__c);
                
        return std.save();
    }

    public PageReference saveAndNew() 
    {     
        // Save the Service Code MAT to the database
        new ApexPages.StandardController(serviceCodeMAT).save();

        // Go to the page that adds a new Service Code MAT
        return newServiceCodeMAT();      
    }        
}