@isTest
private class FM_CustomizeFacilityFavoritesCtrlTest {
    
    @isTest static void ConstructorTest_SourceFacility() {
        
        CreateTestData();
        
        Test.setCurrentPageReference(Page.FM_DispatchViewNew);
        String facId = [select Id from Facility__c where Facility_Name_For_Fluid__c = 'Test Facility For Fluid'].Id;
        System.currentPageReference().getParameters().put('funclocId',facId);
        FM_CustomizeFacilityFavoritesController controller = new FM_CustomizeFacilityFavoritesController();
        
        System.assertEquals(controller.Save().getURL(),Page.FM_DispatchViewNew.getURL());
        
    }
    
    @isTest static void ConstructorTest_SourceWellLocation() {
        
        CreateTestData();
        
        Test.setCurrentPageReference(Page.FM_DispatchViewNew);
        String locId = [select Id from Location__c where Name = 'Test Location'].Id;
        System.currentPageReference().getParameters().put('funclocId',locId);
        FM_CustomizeFacilityFavoritesController controller = new FM_CustomizeFacilityFavoritesController();
        
        System.assertEquals(controller.Save().getURL(),Page.FM_DispatchViewNew.getURL());
        
    }
    
    private static void CreateTestData()
    {
        // Get HOG Profile
        List<Profile> lProfiles = [SELECT Id FROM Profile WHERE Name = 'Standard HOG - General User' LIMIT 1];

        // Test USER
        User oUser = new User();
        oUser.FirstName = 'Guy';
        oUser.LastName = 'Incognito';
        oUser.CommunityNickname = 'Homer';
        oUser.Email = oUser.FirstName.toLowerCase() + '.' + oUser.LastName.toLowerCase() + '@testinghuskyfluidmanagement.com';
        oUser.UserName = oUser.Email;
        oUser.Alias = 'guyincog';
        oUser.TimeZoneSidKey = 'America/New_York';
        oUser.LocaleSidKey = 'en_US';
        oUser.EmailEncodingKey = 'ISO-8859-1'; 
        oUser.LanguageLocaleKey='en_US';
        oUser.ProfileId = lProfiles[0].Id;
        oUser.IsActive = true;
        insert oUser;

        PermissionSet oPermission;
        PermissionSetAssignment oPermissionAssign;

        System.runAs(oUser)
        {
            // Permission Set Assignment - Field Operator
            oPermission = [SELECT Id, Name FROM PermissionSet WHERE Name = :FM_Utilities.FLUID_PERMISSION_SET_OPERATOR LIMIT 1];
            oPermissionAssign = new PermissionSetAssignment(AssigneeId = oUser.Id, PermissionSetId = oPermission.id);
            insert oPermissionAssign;

            // Permission Set Assignment - Internal Dispatcher
            oPermission = [SELECT Id, Name FROM PermissionSet WHERE Name = :FM_Utilities.FLUID_PERMISSION_SET_DISPATCH_STANDARD LIMIT 1];
            oPermissionAssign = new PermissionSetAssignment(AssigneeId = oUser.Id, PermissionSetId = oPermission.id);
            insert oPermissionAssign;
        }

        // FLOC Business Department
        Business_Department__c oBusiness = new Business_Department__c();
        oBusiness.Name = 'Test Business';
        insert oBusiness;

        // FLOC Operating District
        Operating_District__c oDistrict = new Operating_District__c();
        oDistrict.Name = 'Test District';
        oDistrict.Business_Department__c = oBusiness.Id;
        insert oDistrict;

        // FLOC Field
        Field__c oField = new Field__c();
        oField.Name = 'Test AMU';
        oField.Operating_District__c = oDistrict.Id;
        insert oField;

        // Well Location
        Location__c oLocation = new Location__c();
        oLocation.Name = 'Test Location';
        oLocation.Fluid_Location_Ind__c = true;
        oLocation.Operating_Field_AMU__c = oField.Id;
        insert oLocation;

        // Facility
        Facility__c oFacility = new Facility__c();
        oFacility.Fluid_Facility_Ind__c = true;
        oFacility.Name = 'Test Facility';
        oFacility.Facility_Name_For_Fluid__c = 'Test Facility For Fluid';
        insert oFacility;

        // Secondary Facility
        Facility__c oSecondaryFacility = new Facility__c();
        oSecondaryFacility.Fluid_Facility_Ind__c = true;
        oSecondaryFacility.Name = 'Test Facility Capacity';
        oSecondaryFacility.Facility_Name_For_Fluid__c = 'Test Facility For Fluid Capacity';
        insert oSecondaryFacility;

        // Facility Capacity
        Facility_Capacity__c oFacilityCapacity = new Facility_Capacity__c();
        oFacilityCapacity.Facility__c = oFacility.Id;
        oFacilityCapacity.Total_Capacity_Oil__c = 10;
        oFacilityCapacity.Total_Capacity_Water__c = 10;
        oFacilityCapacity.Unique_Facility__c = oFacility.Id;
        insert oFacilityCapacity;

        // Test Account
        Account oAccount = new Account();
        oAccount.Name = 'Carrier Account';
        insert oAccount;

        // Test Carrier
        Carrier__c oCarrier = new Carrier__c();
        oCarrier.Carrier__c = oAccount.Id;
        oCarrier.Carrier_Name_For_Fluid__c = 'Test Carrier';
        insert oCarrier;

        // Test Carrier Unit
        RecordType oCarrierUnitRecordType = [SELECT Id FROM RecordType WHERE SobjectType = 'Carrier_Unit__c' AND DeveloperName = 'Fluid_Carrier'];
        Carrier_Unit__c oCarrierUnit = new Carrier_Unit__c();
        oCarrierUnit.Carrier__c = oCarrier.Id;
        oCarrierUnit.RecordTypeId = oCarrierUnitRecordType.Id;
        oCarrierUnit.Unit_Email__c = 'testing@testing.com';
        oCarrierUnit.Name = 'Unit1';
        insert oCarrierUnit;

        // Test Carrier Desk
        RecordType oDispatchDeskRecordType = [SELECT Id FROM RecordType WHERE SobjectType = 'Dispatch_Desk__c' AND DeveloperName = 'Fluid_Dispatch'];
        Dispatch_Desk__c oDispatchDesk = new Dispatch_Desk__c();
        oDispatchDesk.Name = 'Desk 1';
        oDispatchDesk.Grouping__c = 'Alberta';
        oDispatchDesk.Phone__c = '403-555-1234';
        oDispatchDesk.RecordTypeId = oDispatchDeskRecordType.Id;
        insert oDispatchDesk;

        // Runsheet
        FM_Run_Sheet__c oOneSheet = new FM_Run_Sheet__c();
        oOneSheet.Date__c = Date.today();
        oOneSheet.Well__c = oLocation.Id;
        oOneSheet.Act_Tank_Level__c = 40;
        oOneSheet.Tomorrow_Oil__c = 1;
        oOneSheet.Tomorrow_Water__c = 3;
        oOneSheet.Tonight_Oil__c = 5;
        oOneSheet.Tonight_Water__c = 2;
        insert oOneSheet;

        // Get Truck Trips
        List<FM_Truck_Trip__c> lTripData = [SELECT Id, Name, Axle__c, Carrier__c, Unit__c, Facility__c, Priority__c, Shift__c FROM FM_Truck_Trip__c WHERE Run_Sheet_Lookup__c = :oOneSHeet.Id LIMIT 5000];

        for(Integer iCount = 0; iCount < lTripData.size(); iCount++)
        {
            lTripData[iCount].Facility_Type__c = 'Facility';
            lTripData[iCount].Facility_Lookup__c = oFacility.Id;
            lTripData[iCount].Carrier__c = oCarrier.Id;
            lTripData[iCount].Unit__c = oCarrierUnit.Id;
            lTripData[iCount].Priority__c = '1 = Haul first';
            lTripData[iCount].Truck_Trip_Status__c = 'Booked';
        }

        // Update Truck Trips --> Booked Status
        update lTripData;
    }
    
}