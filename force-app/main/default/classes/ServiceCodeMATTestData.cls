/*-----------------------------------------------------------------------------------------------
Author     : Rey Ocampo
Company    : Husky Energy
Description: A utility class creating test data for the HOG_Service_Code_MAT__c
-------------------------------------------------------------------------------------------------*/
@isTest                
public class ServiceCodeMATTestData
{
    public static HOG_Service_Code_MAT__c createServiceCodeMAT
    (
    	String name,
    	String matCode, 
    	String recordTypeName
   	)
    {                
        HOG_Service_Code_MAT__c results = new HOG_Service_Code_MAT__c
            (
                Name = name,
                MAT_Code__c = matCode,
                Record_Type_Name__c = recordTypeName
            );

        return results;
    }
}