public class USCP_PricingListController {
    public USCP_PricingListController()
    {
        PriceDate = Date.Today();
    }

    public PageReference search() {
        return null;
    }

    public Date PriceDate {get;set;}

    public String PriceDateStr 
    {
        get
        {
            Date d = priceDate ;
            if(d==null)
            {
                return null;
            }
            return d.format() ;
        }
        set
        {
            priceDate = USCP_Utils.convertDate(value); 
            
            system.debug(priceDate.daysBetween(priceDate.toStartOfWeek()));
            if(priceDate.daysBetween(priceDate.toStartOfWeek())==-6)
                priceDate = priceDate.addDays(-1); // this is Sut, move to Fri
            else if(priceDate.daysBetween(priceDate.toStartOfWeek())==0)
                priceDate = priceDate.addDays(-2); // this is Sanday, move to Friday
                
            //PriceDateStr  = priceDate.format();               
            
        }
    }  
    
    public List<USCP_Terminal__C> getTerminals()
    {
        if(PriceDate!=null)
        {
            return [SELECT id, Name, 
                    ( select id,Product__r.Name, Price__c, Change__c 
                        from Rack_Price__r where Date__c = :PriceDate
                        order by Product__r.Name                         
                        ) 
                    FROM USCP_Terminal__c where id in (select Terminal__c from USCP_Rack_Price__c where Date__c = :PriceDate ) ORDER BY Name];
        }
        return new List<USCP_Terminal__C>(); 
    }
    
    public List<USCP_Rack_Price__c> getRackPrices()
    {
        if(PriceDate!=null)
        {
            return [select Date__c, Terminal__r.Name, Product__r.Name, Price__c, Change__c, Price_cent_per_gallon__c, Change_cent_per_gallon__c  
                    from USCP_Rack_Price__c 
                    where Date__c = :PriceDate  
                    ORDER BY Terminal__r.Name, Product__r.Name];
        }
        return new List<USCP_Rack_Price__c>(); 

    }
        


  public PageReference SaveToExcel() 
   {
        return page.USCP_PricingExcel;
   }      
    
      
}