/*************************************************************************************************\
Author:         Jakub Schon
Company:        Husky Energy
Related Class:  HOG_SObjectFactory
				HOG_GeneralUtilities
				HOG_VentGas_Utilities
Description:    Vent Gas SObjects field defaults for Testing purposes. 
History:        jschn 05.22.2018 - Created.
**************************************************************************************************/
@isTest
public class HOG_VentGas_FieldDefaults {

	public static final String CLASS_NAME = 'HOG_VentGas_FieldDefaults';

	public class HOG_Vent_Gas_AlertDefaults implements HOG_SObjectFactory.SObjectFieldDefaults {

		String defaultPriority = HOG_VentGas_Utilities.ALERT_PRIORITY_MEDIUM;
		String defaultStatus = 	HOG_GeneralUtilities.getDefaultOrFirstForPicklist(HOG_Vent_Gas_Alert__c.Status__c.getDescribe().getSObjectField());
		String defaultType = 	HOG_VentGas_Utilities.ALERT_TYPE_EXPIRED_TEST;
		Datetime now = Datetime.now();
		String name = defaultStatus + ' - ' + String.valueOf(now);

		public Map<Schema.SObjectField, Object> getFieldDefaults() {
			return new Map<Schema.SObjectField, Object> {
				HOG_Vent_Gas_Alert__c.Description__c => 'Default test description',
				HOG_Vent_Gas_Alert__c.Start_Date__c => now,
				HOG_Vent_Gas_Alert__c.Priority__c => defaultPriority,
				HOG_Vent_Gas_Alert__c.Status__c => defaultStatus,
				HOG_Vent_Gas_Alert__c.Name => name,
				HOG_Vent_Gas_Alert__c.Type__c => defaultType
			};
		}
	}

	public class HOG_Vent_Gas_Alert_TaskDefaults implements HOG_SObjectFactory.SObjectFieldDefaults {

		String defaultAssigneeType = HOG_VentGas_Utilities.ASSIGNEE_TYPE_ROUTE_OPERATOR;
		String defaultPriority = HOG_VentGas_Utilities.ALERT_PRIORITY_MEDIUM;
		String defaultStatus = HOG_GeneralUtilities.getDefaultOrFirstForPicklist(HOG_Vent_Gas_Alert_Task__c.Status__c.getDescribe().getSObjectField());
		Datetime now = Datetime.now();
		String name = defaultStatus + ' - ' + String.valueOf(now);

		public Map<Schema.SObjectField, Object> getFieldDefaults() {
			return new Map<Schema.SObjectField, Object> {
				HOG_Vent_Gas_Alert_Task__c.Assignee_Type__c => defaultAssigneeType,
				HOG_Vent_Gas_Alert_Task__c.Priority__c => defaultPriority,
				HOG_Vent_Gas_Alert_Task__c.Status__c => defaultStatus,
				HOG_Vent_Gas_Alert_Task__c.Comments__c => 'Default test comments',
				HOG_Vent_Gas_Alert_Task__c.Name => name,
				HOG_Vent_Gas_Alert_Task__c.Remind__c => true
			};
		}
	}

}