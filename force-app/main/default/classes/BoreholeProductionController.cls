public class BoreholeProductionController
{   
    public static Decimal calcBOE(Decimal Oil, Decimal Gas, Decimal NGL)
    {
        if(Oil == null && Gas == null && NGL == null) return null;
        
        if(Oil == null) Oil = 0;
        if(Gas == null) Gas = 0;
        if(NGL == null) NGL = 0;
        
        return (Oil + NGL + (Gas/6.0)).setScale(2, RoundingMode.HALF_UP);
    }

    // The Id of the borehole we`re showing
    public Id boreholeId {get;set;}
    
    public GRD_Borehole__c borehole {get;set;}
    
    public List<Well_Production__c> well_production_list {get;set;}
    
    public List<Type_Curve__c> type_curve_list {get;set;}
    
    // A list that contains production data for the well for every month
    public List<ProductionWrapper> productionByMonth {get;set;}
    
    public class ProductionWrapper
    {
        public Decimal month {get;set;}
        public Date calMonth {get;set;}
        public Decimal typeCurveOil {get;set;}
        public Decimal typeCurveNGL {get;set;}
        public Decimal typeCurveGas {get;set;}
        public Decimal typeCurveBOE {get;set;}
        public Decimal actualOil {get;set;}
        public Decimal actualNGL {get;set;}
        public Decimal actualGas {get;set;}
        public Decimal actualBOE {get;set;}
        public Decimal proposedProfile {get;set;}
        public Decimal manualOil {get;set;}
        public Decimal manualNGL {get;set;}
        public Decimal manualGas {get;set;}
        public Decimal manualBOE {get;set;}
        public Decimal usedProfile {get;set;}
        public String sfid {get;set;}
    }
    
    // Constructor
    public BoreholeProductionController()
    {
        type_curve_list = new List<Type_Curve__c>();
        
        // Try to get the borehole Id from the URL
        if(ApexPages.currentPage().getParameters().containsKey('id'))
            boreholeId = ApexPages.currentPage().getParameters().get('id');
        else
            return;
        
        // Fetch the borehole information from the database.
        borehole = [SELECT Name,
                           Target_Formation__c,
                           Well_Orientation__c,
                           Pool__c,
                           Risked__c,
                           Strategy__c,
                           CreatedBy.Name,
                           Production_Start__c,
                           GRD_GUID__c,
                           RecordType.DeveloperName
                    FROM GRD_Borehole__c
                    WHERE Id =: boreholeId];
        
        system.debug('mikep: borehole: '+borehole);
        system.debug('mikep: borehole.RecordType.DeveloperName: '+borehole.RecordType.DeveloperName);
        
        
        List<RecordType> recordType = [SELECT Id
                                       FROM RecordType
                                       WHERE DeveloperName =: borehole.RecordType.DeveloperName AND
                                             SobjectType =: 'Type_Curve__c'];
        
        if(recordType.size() == 0)
        {
            return;
        }
        
        system.debug('mikep: recordType: '+recordType);
        
        // Get the latest type curve id
        List<Type_Curve__c> curvesById = [SELECT Id,
                                                 Curve_Id__c
                                          FROM Type_Curve__c
                                          WHERE Target_Formation__c =: borehole.Target_Formation__c AND
                                                Pool__c =: borehole.Pool__c AND
                                                Well_Type__c =: borehole.Well_Orientation__c AND
                                                RecordTypeId =: recordType[0].Id
                                          ORDER BY Curve_Id__c
                                          LIMIT 1];
        
        if(curvesById.size() == 0)
        {
            return;
        }
        
        system.debug('mikep: Found curve matching target formation: '+curvesById[0]);
        system.debug('mikep: curve id: '+curvesById[0].Curve_Id__c);
        
        // TODO: Grab the correct typecurve list based on the GRD_Borehole_c object         
        type_curve_list = [SELECT Month__c,
                                  Oil_bbl__c,
                                  NGL_bbl__c,
                                  Gas_mcf__c,
                                  Curve_Date__c
                           FROM Type_Curve__c
                           WHERE Curve_Id__c =: curvesById[0].Curve_Id__c AND
                                 Target_Formation__c =: borehole.Target_Formation__c AND
                                 Pool__c =: borehole.Pool__c AND
                                 Well_Type__c =: borehole.Well_Orientation__c AND
                                 RecordTypeId =: recordType[0].Id
                           ORDER BY month__c];
        
        system.debug('mikep: type curves found: '+type_curve_list.size());
        
        system.debug('mikep: type curves: '+type_curve_list);
        
        well_production_list = [SELECT Id,
                                       YYYYMM__c,
                                       Oil_Override_bbl__c,
                                       Oil_Actual_bbl__c,
                                       NGL_Override_bbl__c,
                                       NGL_Actual_bbl__c,
                                       Gas_Override_mcf__c,
                                       Gas_Actual_mcf__c
                           FROM Well_Production__c
                           WHERE Borehole__c =: boreholeId];
        
        system.debug('mikep: well_production_list: '+well_production_list);
        
        // This maps the YYYYMM (as a Date) to the well production object
        Map<Date, Well_Production__c> productionByYYYYMM = new Map<Date, Well_Production__c>();
        
        Date minimumYYYYMM = null;
        
        for(Well_Production__c well_production : well_production_list)
        {
            try
            {
                system.debug('mikep: well_production: '+well_production);
                
                Integer YYYY = Integer.valueOf(well_production.YYYYMM__c.substring(0,4));
                Integer MM = Integer.valueOf(well_production.YYYYMM__c.substring(4));
                Date YYYYMM = Date.newInstance(YYYY,MM,1);
    system.debug('mikep2 YYYYMM '+YYYYMM);
                productionByYYYYMM.put(YYYYMM,well_production);
                if(minimumYYYYMM == null || minimumYYYYMM > YYYYMM)
                    minimumYYYYMM = YYYYMM;
            }
            catch (Exception ex)
            {
                continue;
            }
        }
        
        // Here we start to build the rows for the well production table       
        // Build object row by row to include all the needed data for the excel form
        productionByMonth = new List<ProductionWrapper>();
        
        // Set the initial calender start date and increment it each loop

        if(borehole.Production_Start__c == null)
            return;






// add check where if borehole.Production_Start__c is null we show nothing... because if it`s null the below code will crash







        Date onProd = borehole.Production_Start__c;
        
        Date oneYearAgo = Date.newInstance(System.today().year()-1,System.today().month(),1);
        
        Integer month = 1;
        
        Date start;
        
        if(oneYearAgo > onProd)
        {
            // we`ll be starting on the date ``oneYearAgo``
            start = oneYearAgo;
            // starting on this month #
            month = onProd.monthsBetween(oneYearAgo) * -1;
system.debug('mikep start = oneYearAgo = '+oneYearAgo);
system.debug('mikep month = onProd.monthsBetween(oneYearAgo) * -1 = '+(onProd.monthsBetween(oneYearAgo) * -1));
        }
        else if(minimumYYYYMM != null && oneYearAgo > minimumYYYYMM)
        {
            // we`ll be starting on the date ``oneYearAgo``
            start = oneYearAgo;
            month = oneYearAgo.monthsBetween(onProd);
system.debug('mikep start = oneYearAgo = '+oneYearAgo);
system.debug('mikep month = oneYearAgo.monthsBetween(onProd) = '+oneYearAgo.monthsBetween(onProd));
        }
        else if(minimumYYYYMM != null && onProd > minimumYYYYMM)
        {
            // we`ll be starting on minimumYYYYMM
            start = minimumYYYYMM;
            month = onProd.monthsBetween(minimumYYYYMM) * -1;
system.debug('mikep start = minimumYYYYMM = '+minimumYYYYMM);
system.debug('mikep month = onProd.monthsBetween(minimumYYYYMM) * -1 = '+(onProd.monthsBetween(minimumYYYYMM) * -1));
        }
        else
        {
            start = onProd;
            month = 1;
system.debug('mikep start = onProd = '+onProd);
system.debug('mikep month = 1');
        }
        
        // Fill an empty list
        
        Date emptyList = start;
        
        for(Integer i = 0; i < 36; i++)
        {
            ProductionWrapper prodWrapper = new ProductionWrapper();
            prodWrapper.calMonth = emptyList;
            
            // Add the month
            prodWrapper.month = borehole.Production_Start__c.monthsBetween(emptyList);
            
            productionByMonth.add(prodWrapper);
            emptyList = emptyList.addMonths(1);
        }
        
        Date typeCurveDate = borehole.Production_Start__c;
        
        for(Type_Curve__c typeCurve : type_curve_list)
        {
system.debug('mikep type curve = '+typeCurve);
            if(typeCurveDate.monthsBetween(start) > 0)
            {
                // If we haven`t reached the date we`re starting at in the user interface, just continue.
                typeCurveDate = typeCurveDate.addMonths(1);
                continue;
            }
            
            Integer index = start.monthsBetween(typeCurveDate);
            
            // Get the type curves for each type
            productionByMonth[index].typeCurveOil = typeCurve.oil_bbl__c;
            productionByMonth[index].typeCurveNGL = typeCurve.NGL_bbl__c;
            productionByMonth[index].typeCurveGas = typeCurve.Gas_mcf__c;
            
            productionByMonth[index].typeCurveBOE = calcBOE(typeCurve.oil_bbl__c, typeCurve.Gas_mcf__c, typeCurve.NGL_bbl__c);
            
            // Increment the calaneder start date for this month
            typeCurveDate = typeCurveDate.addMonths(1);
////////            month++;
            
            // Stop once we've seen 3 years of data.
            if(start.monthsBetween(typeCurveDate) >= 36)
                break;
        }
        
        Date productionDate = Date.newInstance(start.year(),start.month(),1);
        
        for(Date YYYYMM : productionByYYYYMM.keySet())
        {
            Integer monthIndex = start.monthsBetween(YYYYMM);
            
system.debug('mikep monthIndex '+monthIndex);

            if(monthIndex >= 0 && monthIndex < 36)
            {
                Well_Production__c well_production = productionByYYYYMM.get(YYYYMM);
system.debug('mikep wp '+well_production);
                
                // Actuals
                productionByMonth[monthIndex].actualOil = well_production.Oil_Actual_bbl__c;
                productionByMonth[monthIndex].actualNGL = well_production.NGL_Actual_bbl__c;
                productionByMonth[monthIndex].actualGas = well_production.Gas_Actual_mcf__c;
                
                productionByMonth[monthIndex].actualBOE = calcBOE(well_production.Oil_Actual_bbl__c,
                                                                  well_production.Gas_Actual_mcf__c,
                                                                  well_production.NGL_Actual_bbl__c);
                
                // Overrides
                productionByMonth[monthIndex].manualOil = well_production.Oil_Override_bbl__c;
                productionByMonth[monthIndex].manualNGL = well_production.NGL_Override_bbl__c;
                productionByMonth[monthIndex].manualGas = well_production.Gas_Override_mcf__c;
                
                productionByMonth[monthIndex].manualBOE = calcBOE(well_production.Oil_Override_bbl__c,
                                                                  well_production.Gas_Override_mcf__c,
                                                                  well_production.NGL_Override_bbl__c);
                
                // Add in the SFID
                productionByMonth[monthIndex].sfid = well_production.id; 
            }
        }
        
        system.debug('mikep productionByMonth '+productionByMonth);
    }
}