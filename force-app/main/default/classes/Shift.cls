public with sharing class Shift {
	public Shift_Configuration__c shiftConfig{get; set;}
	public Simple_DTO__c teamAllocation{get; set;}
	
	public List<Shift_Operator__c> shiftOperators{get; set;}
	
	public Boolean hasShiftOperators{
		get{
			return (this.shiftOperators != null) && (this.shiftOperators.size() > 0);
		}
	}
	
	public List<Simple_DTO__c> operationalEvents{get; set;}
	
	public Boolean hasOperationalEvents{
		get{
			return (this.operationalEvents != null) && (this.operationalEvents.size() > 0);
		}
	}
	
	public Shift(Shift_Configuration__c shiftConfig, Simple_DTO__c teamAllocation) {
		this.shiftConfig = shiftConfig;
		this.teamAllocation = teamAllocation;
	}
}