public with sharing class OpportunityProduct {
    private ApexPages.StandardController std;
    public OpportunityLineItem oli {get;set;}
  
    public String viewMode{get; set;}
    public ATS_Freight__c freight {get;set;}
    public Product2 prod {get;set;}
    
    public String refineryNetbackErrorMessage {get;set;}

    public OpportunityProduct(ApexPages.StandardController controller) {
        std = controller;
        if (!Test.isRunningTest()) {
            std.addFields(new List<String> {'Opportunity.ATS_Freight__c', 'PricebookEntry.Product2.Density__c', 'Opportunity.RecordTypeId'});
        }
        
        refineryNetbackErrorMessage = '';
        
        oli = (OpportunityLineItem) std.getRecord();
        
        viewMode = 'edit';
        
        if (oli.Opportunity.ATS_Freight__c != null)
            freight = [SELECT Id,
                              Prices_F_O_B__c,
                              Emulsion_Rate5_Supplier1__c,
                              Emulsion_Rate5_Supplier2__c, 
                              Emulsion_Rate6_Supplier2__c,
                              Emulsion_Rate6_Supplier_1__c, 
                              Emulsion_Rate7_Supplier1__c,
                              Emulsion_Rate7_Supplier2__c,
                              Emulsion_Rate8_Supplier1__c,
                              Emulsion_Rate8_Supplier2__c,
                              Husky_Supplier_1_Selected__c,
                              Husky_Supplier_2_Selected__c,
                              Supplier_1_Unit__c,
                              Supplier_2_Unit__c
                       FROM ATS_Freight__c
                       WHERE Id =: oli.Opportunity.ATS_Freight__c];
        
        if(oli.Product2Id != null)
            prod = [SELECT Id, Density__c FROM Product2 WHERE Id =: oli.Product2Id];
        
        // Fetch more needed oli fields
        
        OpportunityLineItem tempOli = [SELECT Id,
                                              Axle_5_Price_Set_Manually__c,
                                              Axle_6_Price_Set_Manually__c,
                                              Axle_7_Price_Set_Manually__c,
                                              Axle_8_Price_Set_Manually__c
                                       FROM OpportunityLineItem
                                       WHERE Id =: oli.Id];
        
        oli.Axle_5_Price_Set_Manually__c = tempOli.Axle_5_Price_Set_Manually__c;
        oli.Axle_6_Price_Set_Manually__c = tempOli.Axle_6_Price_Set_Manually__c;
        oli.Axle_7_Price_Set_Manually__c = tempOli.Axle_7_Price_Set_Manually__c;
        oli.Axle_8_Price_Set_Manually__c = tempOli.Axle_8_Price_Set_Manually__c;
    }
    public List<SelectOption> getSupplierWinnerList()
    {
        List<SelectOption> options = new List<SelectOption>();
        options.add(new SelectOption('','--None--'));
        options.add(new SelectOption('Husky', 'Husky'));
        //use Set to avoid duplicate item 
        Set<SelectOption> optionSet = new Set<SelectOption>();
        if (oli.Competitor_1__c != null)
           optionSet.add(new SelectOption(oli.Competitor_1__c, oli.Competitor_1__c));
        if (oli.Competitor_2__c != null)
           optionSet.add(new SelectOption(oli.Competitor_2__c, oli.Competitor_2__c));
        if (oli.Competitor_3__c != null)
           optionSet.add(new SelectOption(oli.Competitor_3__c, oli.Competitor_3__c));
        if (oli.Competitor_4__c != null)
           optionSet.add(new SelectOption(oli.Competitor_4__c, oli.Competitor_4__c));
        if (oli.Competitor_5__c != null)
           optionSet.add(new SelectOption(oli.Competitor_5__c, oli.Competitor_5__c));
        options.addAll(optionSet);   
        return options;               
    }
    
    public PageReference save() {
        try
        {
            Map<String, Schema.Recordtypeinfo> recTypesByName = Opportunity.SObjectType.getDescribe().getRecordTypeInfosByName();
            if (recTypesByName.get('ATS: Asphalt Product Category').getRecordTypeId() == oli.Opportunity.RecordTypeId)
            {
                System.debug(oli.Currency__c + '  ' + oli.Exchange_Rate_to_CAD__c);
                if (oli.Currency__c == 'US' && oli.Exchange_Rate_to_CAD__c == 0 )
                {
                    ApexPages.addMessage(new ApexPages.Message(ApexPages.severity.ERROR, 'Exchange Rate is required when Currency is US.'));
                    return null;
                }
            }
            return std.save();
        }
        catch(Exception e)
        {
           ApexPages.addMessages(e);
            return null;
        }
    }
    public PageReference redirect()
    {
        Map<String, Schema.Recordtypeinfo> recTypesByName = Opportunity.SObjectType.getDescribe().getRecordTypeInfosByName();
        String retURL = ApexPages.currentPage().getParameters().get('retURL');
        PageReference editPage = null;
        System.debug('oli.Opportunity.RecordType: ' + oli.Opportunity.RecordTypeId);
        if (recTypesByName.get('ATS: Asphalt Product Category').getRecordTypeId() == oli.Opportunity.RecordTypeId)
            editPage = new PageReference('/apex/OpportunityProductEdit_Asphalt?id=' + oli.Id + '&retURL=' + retURL);
        else if (recTypesByName.get('ATS: Emulsion Product Category').getRecordTypeId() == oli.Opportunity.RecordTypeId)
            editPage = new PageReference('/apex/OpportunityProductEdit_Emulsion?id=' + oli.Id + '&retURL=' + retURL);
        else if(recTypesByName.get('ATS: Residual Product Category').getRecordTypeId() == oli.Opportunity.RecordTypeId)
            return null;
        editPage.setRedirect(true);
        return editPage;
    }
    
    public PageReference recalculateNetback()
    {
        // Re-fetch freight and prod
        
        freight = null;
        if(oli.Opportunity.ATS_Freight__c != null)
            freight = [select Id, Prices_F_O_B__c, Emulsion_Rate5_Supplier1__c, Emulsion_Rate5_Supplier2__c, 
                    Emulsion_Rate6_Supplier2__c, Emulsion_Rate6_Supplier_1__c, 
                    Emulsion_Rate7_Supplier1__c, Emulsion_Rate7_Supplier2__c,
                    Emulsion_Rate8_Supplier1__c, Emulsion_Rate8_Supplier2__c,
                    Husky_Supplier_1_Selected__c,Husky_Supplier_2_Selected__c,
                    Supplier_1_Unit__c, Supplier_2_Unit__c
                    FROM ATS_Freight__c WHERE Id =: oli.Opportunity.ATS_Freight__c];
        
        prod = null;
        if(oli.Product2Id != null)
            prod = [SELECT Id, Density__c FROM Product2 WHERE Id =: oli.Product2Id];
        
        if(oli == null || freight == null || prod == null)
            return null;

        Decimal refineryNetback = calculateNetback.calculateAsphaltRefineryNetback(oli, freight, prod);
        Decimal terminalNetback = calculateNetback.calculateAsphaltTerminalNetback(oli, freight, prod);
        
        if(refineryNetback != null)
            refineryNetback = refineryNetback.setScale(4, RoundingMode.HALF_UP);
        if(terminalNetback != null)
            terminalNetback = terminalNetback.setScale(4, RoundingMode.HALF_UP);
        
        oli.Refinery_Netback_CAD__c = refineryNetback;
        if(oli.Refinery_Netback_CAD__c != null)
            oli.Refinery_Netback_CAD__c.setScale(4, RoundingMode.HALF_UP);
        
        oli.Terminal_Netback_CAD__c = terminalNetback;
        if(oli.Terminal_Netback_CAD__c != null)
            oli.Terminal_Netback_CAD__c.setScale(4, RoundingMode.HALF_UP);

        return null;
    }
    public PageReference recalculateRefineryNetback()
    {
        // Re-fetch freight and prod
        
        freight = null;
        if(oli.Opportunity.ATS_Freight__c != null)
            freight = [select Id, Prices_F_O_B__c, Emulsion_Rate5_Supplier1__c, Emulsion_Rate5_Supplier2__c, 
                    Emulsion_Rate6_Supplier2__c, Emulsion_Rate6_Supplier_1__c, 
                    Emulsion_Rate7_Supplier1__c, Emulsion_Rate7_Supplier2__c,
                    Emulsion_Rate8_Supplier1__c, Emulsion_Rate8_Supplier2__c,
                    Husky_Supplier_1_Selected__c,Husky_Supplier_2_Selected__c,
                    Supplier_1_Unit__c, Supplier_2_Unit__c
                    FROM ATS_Freight__c WHERE Id =: oli.Opportunity.ATS_Freight__c];
        
        prod = null;
        if(oli.Product2Id != null)
            prod = [SELECT Id, Density__c FROM Product2 WHERE Id =: oli.Product2Id];
        
        if(oli == null || prod == null)
            return null;

        Decimal refineryNetback = calculateNetback.calculateAsphaltRefineryNetback(oli, freight, prod);
        
        if(refineryNetback != null)
            refineryNetback = refineryNetback.setScale(4, RoundingMode.HALF_UP);
        
        oli.Refinery_Netback_CAD__c = refineryNetback;
        if(oli.Refinery_Netback_CAD__c != null)
            oli.Refinery_Netback_CAD__c.setScale(4, RoundingMode.HALF_UP);
        
        return null;
    }
    
    public PageReference axle5PriceManuallySet()
    {
        oli.Axle_5_Price_Set_Manually__c = true;
        return null;
    }
    
    //
    // Called whenever the Axle 6 Price is manually updated, mark it as so on the record.
    //
    public PageReference axle6PriceManuallySet()
    {
        oli.Axle_6_Price_Set_Manually__c = true;
        return null;
    }
    
    //
    // Called whenever the Axle 7 Price is manually updated, mark it as so on the record.
    //
    public PageReference axle7PriceManuallySet()
    {
        oli.Axle_7_Price_Set_Manually__c = true;
        return null;
    }
    
    //
    // Called whenever the Axle 8 Price is manually updated, mark it as so on the record.
    //
    public PageReference axle8PriceManuallySet()
    {
        oli.Axle_8_Price_Set_Manually__c = true;
        return null;
    }
    
    public PageReference recalculateAxlePrices()
    {
        // Since this function is only called when an Emulsion value is changed, we know the axle prices are calculated.
        // So if they were set before as manual, change that flag.
        
        oli.Axle_5_Price_Set_Manually__c = false;
        oli.Axle_6_Price_Set_Manually__c = false;
        oli.Axle_7_Price_Set_Manually__c = false;
        oli.Axle_8_Price_Set_Manually__c = false;
        
        // Re-fetch freight
        
        freight = null;
        if(oli.Opportunity.ATS_Freight__c != null)
            freight = [SELECT Id,
                              Prices_F_O_B__c,
                              Emulsion_Rate5_Supplier1__c,
                              Emulsion_Rate5_Supplier2__c, 
                              Emulsion_Rate6_Supplier2__c,
                              Emulsion_Rate6_Supplier_1__c, 
                              Emulsion_Rate7_Supplier1__c,
                              Emulsion_Rate7_Supplier2__c,
                              Emulsion_Rate8_Supplier1__c,
                              Emulsion_Rate8_Supplier2__c,
                              Husky_Supplier_1_Selected__c,
                              Husky_Supplier_2_Selected__c,
                              Supplier_1_Unit__c,
                              Supplier_2_Unit__c
                       FROM ATS_Freight__c
                       WHERE Id =: oli.Opportunity.ATS_Freight__c];
        
        prod = null;
        if(oli.Product2Id != null)
            prod = [SELECT Id, Density__c FROM Product2 WHERE Id =: oli.Product2Id];
        
        if(prod == null)
            return null;
        
        Map<Integer,Decimal> axlePrices = calculateNetback.calculateEmulsionAxlePrices(oli, freight, prod);
        
        if(axlePrices.containsKey(5))
        {
            oli.axle_5_Price__c = axlePrices.get(5);
            if(oli.axle_5_Price__c != null)
                oli.axle_5_Price__c.setScale(4, RoundingMode.HALF_UP);
        }
        if(axlePrices.containsKey(6))
        {
            oli.axle_6_Price__c = axlePrices.get(6);
            if(oli.axle_6_Price__c != null)
                oli.axle_6_Price__c.setScale(4, RoundingMode.HALF_UP);
        }
        if(axlePrices.containsKey(7))
        {
            oli.axle_7_Price__c = axlePrices.get(7);
            if(oli.axle_7_Price__c != null)
                oli.axle_7_Price__c.setScale(4, RoundingMode.HALF_UP);
        }
        if(axlePrices.containsKey(8))
        {
            oli.axle_8_Price__c = axlePrices.get(8);
            if(oli.axle_8_Price__c != null)
                oli.axle_8_Price__c.setScale(4, RoundingMode.HALF_UP);
        }
        
        return null;
    }
    
    public PageReference recalculateCalculatedAxlePrices()
    {
        // Re-fetch freight
        
        freight = null;
        if(oli.Opportunity.ATS_Freight__c != null)
            freight = [SELECT Id,
                              Prices_F_O_B__c,
                              Emulsion_Rate5_Supplier1__c,
                              Emulsion_Rate5_Supplier2__c, 
                              Emulsion_Rate6_Supplier2__c,
                              Emulsion_Rate6_Supplier_1__c, 
                              Emulsion_Rate7_Supplier1__c,
                              Emulsion_Rate7_Supplier2__c,
                              Emulsion_Rate8_Supplier1__c,
                              Emulsion_Rate8_Supplier2__c,
                              Husky_Supplier_1_Selected__c,
                              Husky_Supplier_2_Selected__c,
                              Supplier_1_Unit__c,
                              Supplier_2_Unit__c
                       FROM ATS_Freight__c
                       WHERE Id =: oli.Opportunity.ATS_Freight__c];
        
        prod = null;
        if(oli.Product2Id != null)
            prod = [SELECT Id, Density__c FROM Product2 WHERE Id =: oli.Product2Id];
        
        if(prod == null)
            return null;
        
        Map<Integer,Decimal> axlePrices = calculateNetback.calculateEmulsionAxlePrices(oli, freight, prod);
        
        if(axlePrices.containsKey(5))// && oli.Axle_5_Price_Set_Manually__c == false)
        {
            oli.axle_5_Price__c = axlePrices.get(5);
            if(oli.axle_5_Price__c != null)
                oli.axle_5_Price__c.setScale(4, RoundingMode.HALF_UP);
        }
        if(axlePrices.containsKey(6))// && oli.Axle_6_Price_Set_Manually__c == false)
        {
            oli.axle_6_Price__c = axlePrices.get(6);
            if(oli.axle_6_Price__c != null)
                oli.axle_6_Price__c.setScale(4, RoundingMode.HALF_UP);
        }
        if(axlePrices.containsKey(7))// && oli.Axle_7_Price_Set_Manually__c == false)
        {
            oli.axle_7_Price__c = axlePrices.get(7);
            if(oli.axle_7_Price__c != null)
                oli.axle_7_Price__c.setScale(4, RoundingMode.HALF_UP);
        }
        if(axlePrices.containsKey(8))// && oli.Axle_8_Price_Set_Manually__c == false)
        {
            oli.axle_8_Price__c = axlePrices.get(8);
            if(oli.axle_8_Price__c != null)
                oli.axle_8_Price__c.setScale(4, RoundingMode.HALF_UP);
        }
        
        return null;
    }
    
    public String requestedCompetitor {get;set;}
    
    public PageReference recalculateCompetitor1Netback()
    {
        // Re-fetch freight
        
        freight = null;
        if(oli.Opportunity.ATS_Freight__c != null)
            freight = [SELECT Id,
                              Prices_F_O_B__c,
                              Emulsion_Rate5_Supplier1__c,
                              Emulsion_Rate5_Supplier2__c, 
                              Emulsion_Rate6_Supplier2__c,
                              Emulsion_Rate6_Supplier_1__c, 
                              Emulsion_Rate7_Supplier1__c,
                              Emulsion_Rate7_Supplier2__c,
                              Emulsion_Rate8_Supplier1__c,
                              Emulsion_Rate8_Supplier2__c,
                              Husky_Supplier_1_Selected__c,
                              Husky_Supplier_2_Selected__c,
                              Emulsion_Rate5_Competitor1__c,
                              Emulsion_Rate5_Competitor2__c,
                              Emulsion_Axle5_Rate_Competitor3__c,
                              Emulsion_Axle5_Rate_Competitor4__c,
                              Emulsion_Rate6_Competitor1__c,
                              Emulsion_Rate6_Competitor2__c,
                              Emulsion_Axle6_Rate_Competitor3__c,
                              Emulsion_Axle6_Rate_Competitor4__c,
                              Emulsion_Rate7_Competitor1__c,
                              Emulsion_Rate7_Competitor2__c,
                              Emulsion_Rate7_Competitor3__c,
                              Emulsion_Rate7_Competitor4__c,
                              Emulsion_Rate8_Competitor1__c,
                              Emulsion_Rate8_Competitor2__c,
                              Emulsion_Axle8_Rate_Competitor3__c,
                              Emulsion_Axle8_Rate_Competitor4__c,
                              Supplier_1_Unit__c,
                              Supplier_2_Unit__c
                       FROM ATS_Freight__c
                       WHERE Id =: oli.Opportunity.ATS_Freight__c];
        
        Decimal refineryNetback = calculateNetback.calculateCompetitorRefineryNetback(1, oli, freight);
        Decimal terminalNetback = calculateNetback.calculateCompetitorTerminalNetback(1, oli, freight);
        
        oli.Competitor_1_Refinery_Netback_CAD__c = refineryNetback;
        if(oli.Competitor_1_Refinery_Netback_CAD__c != null)
            oli.Competitor_1_Refinery_Netback_CAD__c.setScale(4, RoundingMode.HALF_UP);
        
        oli.Competitor_1_Terminal_Netback_CAD__c = terminalNetback;
        if(oli.Competitor_1_Terminal_Netback_CAD__c != null)
            oli.Competitor_1_Terminal_Netback_CAD__c.setScale(4, RoundingMode.HALF_UP);
        
        return null;
    }
    
    public PageReference recalculateCompetitor2Netback()
    {
        // Re-fetch freight
        
        freight = null;
        if(oli.Opportunity.ATS_Freight__c != null)
            freight = [SELECT Id,
                              Prices_F_O_B__c,
                              Emulsion_Rate5_Supplier1__c,
                              Emulsion_Rate5_Supplier2__c, 
                              Emulsion_Rate6_Supplier2__c,
                              Emulsion_Rate6_Supplier_1__c, 
                              Emulsion_Rate7_Supplier1__c,
                              Emulsion_Rate7_Supplier2__c,
                              Emulsion_Rate8_Supplier1__c,
                              Emulsion_Rate8_Supplier2__c,
                              Husky_Supplier_1_Selected__c,
                              Husky_Supplier_2_Selected__c,
                              Emulsion_Rate5_Competitor1__c,
                              Emulsion_Rate5_Competitor2__c,
                              Emulsion_Axle5_Rate_Competitor3__c,
                              Emulsion_Axle5_Rate_Competitor4__c,
                              Emulsion_Rate6_Competitor1__c,
                              Emulsion_Rate6_Competitor2__c,
                              Emulsion_Axle6_Rate_Competitor3__c,
                              Emulsion_Axle6_Rate_Competitor4__c,
                              Emulsion_Rate7_Competitor1__c,
                              Emulsion_Rate7_Competitor2__c,
                              Emulsion_Rate7_Competitor3__c,
                              Emulsion_Rate7_Competitor4__c,
                              Emulsion_Rate8_Competitor1__c,
                              Emulsion_Rate8_Competitor2__c,
                              Emulsion_Axle8_Rate_Competitor3__c,
                              Emulsion_Axle8_Rate_Competitor4__c,
                              Supplier_1_Unit__c,
                              Supplier_2_Unit__c
                       FROM ATS_Freight__c
                       WHERE Id =: oli.Opportunity.ATS_Freight__c];

        Decimal refineryNetback = calculateNetback.calculateCompetitorRefineryNetback(2, oli, freight);
        Decimal terminalNetback = calculateNetback.calculateCompetitorTerminalNetback(2, oli, freight);
        
        oli.Competitor_2_Refinery_Netback_CAD__c = refineryNetback;
        if(oli.Competitor_2_Refinery_Netback_CAD__c != null)
            oli.Competitor_2_Refinery_Netback_CAD__c.setScale(4, RoundingMode.HALF_UP);
        
        oli.Competitor_2_Terminal_Netback_CAD__c = terminalNetback;
        if(oli.Competitor_2_Terminal_Netback_CAD__c != null)
            oli.Competitor_2_Terminal_Netback_CAD__c.setScale(4, RoundingMode.HALF_UP);
        
        return null;
    }
    
    public PageReference recalculateCompetitor3Netback()
    {
        // Re-fetch freight
        
        freight = null;
        if(oli.Opportunity.ATS_Freight__c != null)
            freight = [SELECT Id,
                              Prices_F_O_B__c,
                              Emulsion_Rate5_Supplier1__c,
                              Emulsion_Rate5_Supplier2__c, 
                              Emulsion_Rate6_Supplier2__c,
                              Emulsion_Rate6_Supplier_1__c, 
                              Emulsion_Rate7_Supplier1__c,
                              Emulsion_Rate7_Supplier2__c,
                              Emulsion_Rate8_Supplier1__c,
                              Emulsion_Rate8_Supplier2__c,
                              Husky_Supplier_1_Selected__c,
                              Husky_Supplier_2_Selected__c,
                              Emulsion_Rate5_Competitor1__c,
                              Emulsion_Rate5_Competitor2__c,
                              Emulsion_Axle5_Rate_Competitor3__c,
                              Emulsion_Axle5_Rate_Competitor4__c,
                              Emulsion_Rate6_Competitor1__c,
                              Emulsion_Rate6_Competitor2__c,
                              Emulsion_Axle6_Rate_Competitor3__c,
                              Emulsion_Axle6_Rate_Competitor4__c,
                              Emulsion_Rate7_Competitor1__c,
                              Emulsion_Rate7_Competitor2__c,
                              Emulsion_Rate7_Competitor3__c,
                              Emulsion_Rate7_Competitor4__c,
                              Emulsion_Rate8_Competitor1__c,
                              Emulsion_Rate8_Competitor2__c,
                              Emulsion_Axle8_Rate_Competitor3__c,
                              Emulsion_Axle8_Rate_Competitor4__c,
                              Supplier_1_Unit__c,
                              Supplier_2_Unit__c
                       FROM ATS_Freight__c
                       WHERE Id =: oli.Opportunity.ATS_Freight__c];

        Decimal refineryNetback = calculateNetback.calculateCompetitorRefineryNetback(3, oli, freight);
        Decimal terminalNetback = calculateNetback.calculateCompetitorTerminalNetback(3, oli, freight);
        
        oli.Competitor_3_Refinery_Netback_CAD__c = refineryNetback;
        if(oli.Competitor_3_Refinery_Netback_CAD__c != null)
            oli.Competitor_3_Refinery_Netback_CAD__c.setScale(4, RoundingMode.HALF_UP);
        
        oli.Competitor_3_Terminal_Netback_CAD__c = terminalNetback;
        if(oli.Competitor_3_Terminal_Netback_CAD__c != null)
            oli.Competitor_3_Terminal_Netback_CAD__c.setScale(4, RoundingMode.HALF_UP);
        
        return null;
    }
    
    public PageReference recalculateCompetitor4Netback()
    {
        // Re-fetch freight
        
        freight = null;
        if(oli.Opportunity.ATS_Freight__c != null)
            freight = [SELECT Id,
                              Prices_F_O_B__c,
                              Emulsion_Rate5_Supplier1__c,
                              Emulsion_Rate5_Supplier2__c, 
                              Emulsion_Rate6_Supplier2__c,
                              Emulsion_Rate6_Supplier_1__c, 
                              Emulsion_Rate7_Supplier1__c,
                              Emulsion_Rate7_Supplier2__c,
                              Emulsion_Rate8_Supplier1__c,
                              Emulsion_Rate8_Supplier2__c,
                              Husky_Supplier_1_Selected__c,
                              Husky_Supplier_2_Selected__c,
                              Emulsion_Rate5_Competitor1__c,
                              Emulsion_Rate5_Competitor2__c,
                              Emulsion_Axle5_Rate_Competitor3__c,
                              Emulsion_Axle5_Rate_Competitor4__c,
                              Emulsion_Rate6_Competitor1__c,
                              Emulsion_Rate6_Competitor2__c,
                              Emulsion_Axle6_Rate_Competitor3__c,
                              Emulsion_Axle6_Rate_Competitor4__c,
                              Emulsion_Rate7_Competitor1__c,
                              Emulsion_Rate7_Competitor2__c,
                              Emulsion_Rate7_Competitor3__c,
                              Emulsion_Rate7_Competitor4__c,
                              Emulsion_Rate8_Competitor1__c,
                              Emulsion_Rate8_Competitor2__c,
                              Emulsion_Axle8_Rate_Competitor3__c,
                              Emulsion_Axle8_Rate_Competitor4__c,
                              Supplier_1_Unit__c,
                              Supplier_2_Unit__c
                       FROM ATS_Freight__c
                       WHERE Id =: oli.Opportunity.ATS_Freight__c];
        
        Decimal refineryNetback = calculateNetback.calculateCompetitorRefineryNetback(4, oli, freight);
        Decimal terminalNetback = calculateNetback.calculateCompetitorTerminalNetback(4, oli, freight);
        
        oli.Competitor_4_Refinery_Netback_CAD__c = refineryNetback;
        if(oli.Competitor_4_Refinery_Netback_CAD__c != null)
            oli.Competitor_4_Refinery_Netback_CAD__c.setScale(4, RoundingMode.HALF_UP);
        
        oli.Competitor_4_Terminal_Netback_CAD__c = terminalNetback;
        if(oli.Competitor_4_Terminal_Netback_CAD__c != null)
            oli.Competitor_4_Terminal_Netback_CAD__c.setScale(4, RoundingMode.HALF_UP);
        
        
        return null;
    }
    
    public PageReference recalculateCompetitor1Margin()
    {
        // Re-fetch freight
        
        freight = null;
        if(oli.Opportunity.ATS_Freight__c != null)
            freight = [SELECT Id,
                              Prices_F_O_B__c,
                              Emulsion_Rate5_Supplier1__c,
                              Emulsion_Rate5_Supplier2__c, 
                              Emulsion_Rate6_Supplier2__c,
                              Emulsion_Rate6_Supplier_1__c, 
                              Emulsion_Rate7_Supplier1__c,
                              Emulsion_Rate7_Supplier2__c,
                              Emulsion_Rate8_Supplier1__c,
                              Emulsion_Rate8_Supplier2__c,
                              Husky_Supplier_1_Selected__c,
                              Husky_Supplier_2_Selected__c,
                              Emulsion_Rate5_Competitor1__c,
                              Emulsion_Rate5_Competitor2__c,
                              Emulsion_Axle5_Rate_Competitor3__c,
                              Emulsion_Axle5_Rate_Competitor4__c,
                              Emulsion_Rate6_Competitor1__c,
                              Emulsion_Rate6_Competitor2__c,
                              Emulsion_Axle6_Rate_Competitor3__c,
                              Emulsion_Axle6_Rate_Competitor4__c,
                              Emulsion_Rate7_Competitor1__c,
                              Emulsion_Rate7_Competitor2__c,
                              Emulsion_Rate7_Competitor3__c,
                              Emulsion_Rate7_Competitor4__c,
                              Emulsion_Rate8_Competitor1__c,
                              Emulsion_Rate8_Competitor2__c,
                              Emulsion_Axle8_Rate_Competitor3__c,
                              Emulsion_Axle8_Rate_Competitor4__c,
                              Supplier_1_Unit__c,
                              Supplier_2_Unit__c
                       FROM ATS_Freight__c
                       WHERE Id =: oli.Opportunity.ATS_Freight__c];
        
        Decimal margin = calculateNetback.calculateCompetitorMargin(1, oli, freight);
        
        oli.Competitor_1_Emulsion_Margin_CAD__c = margin;
        if(oli.Competitor_1_Emulsion_Margin_CAD__c != null)
            oli.Competitor_1_Emulsion_Margin_CAD__c.setScale(4, RoundingMode.HALF_UP);
        
        return null;
    }
    
    public PageReference recalculateCompetitor2Margin()
    {
        // Re-fetch freight
        
        freight = null;
        if(oli.Opportunity.ATS_Freight__c != null)
            freight = [SELECT Id,
                              Prices_F_O_B__c,
                              Emulsion_Rate5_Supplier1__c,
                              Emulsion_Rate5_Supplier2__c, 
                              Emulsion_Rate6_Supplier2__c,
                              Emulsion_Rate6_Supplier_1__c, 
                              Emulsion_Rate7_Supplier1__c,
                              Emulsion_Rate7_Supplier2__c,
                              Emulsion_Rate8_Supplier1__c,
                              Emulsion_Rate8_Supplier2__c,
                              Husky_Supplier_1_Selected__c,
                              Husky_Supplier_2_Selected__c,
                              Emulsion_Rate5_Competitor1__c,
                              Emulsion_Rate5_Competitor2__c,
                              Emulsion_Axle5_Rate_Competitor3__c,
                              Emulsion_Axle5_Rate_Competitor4__c,
                              Emulsion_Rate6_Competitor1__c,
                              Emulsion_Rate6_Competitor2__c,
                              Emulsion_Axle6_Rate_Competitor3__c,
                              Emulsion_Axle6_Rate_Competitor4__c,
                              Emulsion_Rate7_Competitor1__c,
                              Emulsion_Rate7_Competitor2__c,
                              Emulsion_Rate7_Competitor3__c,
                              Emulsion_Rate7_Competitor4__c,
                              Emulsion_Rate8_Competitor1__c,
                              Emulsion_Rate8_Competitor2__c,
                              Emulsion_Axle8_Rate_Competitor3__c,
                              Emulsion_Axle8_Rate_Competitor4__c,
                              Supplier_1_Unit__c,
                              Supplier_2_Unit__c
                       FROM ATS_Freight__c
                       WHERE Id =: oli.Opportunity.ATS_Freight__c];
        
        Decimal margin = calculateNetback.calculateCompetitorMargin(2, oli, freight);
        
        oli.Competitor_2_Emulsion_Margin_CAD__c = margin;
        if(oli.Competitor_2_Emulsion_Margin_CAD__c != null)
            oli.Competitor_2_Emulsion_Margin_CAD__c.setScale(4, RoundingMode.HALF_UP);
        
        return null;
    }
    
    public PageReference recalculateCompetitor3Margin()
    {
        // Re-fetch freight
        
        freight = null;
        if(oli.Opportunity.ATS_Freight__c != null)
            freight = [SELECT Id,
                              Prices_F_O_B__c,
                              Emulsion_Rate5_Supplier1__c,
                              Emulsion_Rate5_Supplier2__c, 
                              Emulsion_Rate6_Supplier2__c,
                              Emulsion_Rate6_Supplier_1__c, 
                              Emulsion_Rate7_Supplier1__c,
                              Emulsion_Rate7_Supplier2__c,
                              Emulsion_Rate8_Supplier1__c,
                              Emulsion_Rate8_Supplier2__c,
                              Husky_Supplier_1_Selected__c,
                              Husky_Supplier_2_Selected__c,
                              Emulsion_Rate5_Competitor1__c,
                              Emulsion_Rate5_Competitor2__c,
                              Emulsion_Axle5_Rate_Competitor3__c,
                              Emulsion_Axle5_Rate_Competitor4__c,
                              Emulsion_Rate6_Competitor1__c,
                              Emulsion_Rate6_Competitor2__c,
                              Emulsion_Axle6_Rate_Competitor3__c,
                              Emulsion_Axle6_Rate_Competitor4__c,
                              Emulsion_Rate7_Competitor1__c,
                              Emulsion_Rate7_Competitor2__c,
                              Emulsion_Rate7_Competitor3__c,
                              Emulsion_Rate7_Competitor4__c,
                              Emulsion_Rate8_Competitor1__c,
                              Emulsion_Rate8_Competitor2__c,
                              Emulsion_Axle8_Rate_Competitor3__c,
                              Emulsion_Axle8_Rate_Competitor4__c,
                              Supplier_1_Unit__c,
                              Supplier_2_Unit__c
                       FROM ATS_Freight__c
                       WHERE Id =: oli.Opportunity.ATS_Freight__c];
        
        Decimal margin = calculateNetback.calculateCompetitorMargin(3, oli, freight);
        
        oli.Competitor_3_Emulsion_Margin_CAD__c = margin;
        if(oli.Competitor_3_Emulsion_Margin_CAD__c != null)
            oli.Competitor_3_Emulsion_Margin_CAD__c.setScale(4, RoundingMode.HALF_UP);
        
        return null;
    }
    
    public PageReference recalculateCompetitor4Margin()
    {
        // Re-fetch freight
        
        freight = null;
        if(oli.Opportunity.ATS_Freight__c != null)
            freight = [SELECT Id,
                              Prices_F_O_B__c,
                              Emulsion_Rate5_Supplier1__c,
                              Emulsion_Rate5_Supplier2__c, 
                              Emulsion_Rate6_Supplier2__c,
                              Emulsion_Rate6_Supplier_1__c, 
                              Emulsion_Rate7_Supplier1__c,
                              Emulsion_Rate7_Supplier2__c,
                              Emulsion_Rate8_Supplier1__c,
                              Emulsion_Rate8_Supplier2__c,
                              Husky_Supplier_1_Selected__c,
                              Husky_Supplier_2_Selected__c,
                              Emulsion_Rate5_Competitor1__c,
                              Emulsion_Rate5_Competitor2__c,
                              Emulsion_Axle5_Rate_Competitor3__c,
                              Emulsion_Axle5_Rate_Competitor4__c,
                              Emulsion_Rate6_Competitor1__c,
                              Emulsion_Rate6_Competitor2__c,
                              Emulsion_Axle6_Rate_Competitor3__c,
                              Emulsion_Axle6_Rate_Competitor4__c,
                              Emulsion_Rate7_Competitor1__c,
                              Emulsion_Rate7_Competitor2__c,
                              Emulsion_Rate7_Competitor3__c,
                              Emulsion_Rate7_Competitor4__c,
                              Emulsion_Rate8_Competitor1__c,
                              Emulsion_Rate8_Competitor2__c,
                              Emulsion_Axle8_Rate_Competitor3__c,
                              Emulsion_Axle8_Rate_Competitor4__c,
                              Supplier_1_Unit__c,
                              Supplier_2_Unit__c
                       FROM ATS_Freight__c
                       WHERE Id =: oli.Opportunity.ATS_Freight__c];
        
        Decimal margin = calculateNetback.calculateCompetitorMargin(4, oli, freight);
        
        oli.Competitor_4_Emulsion_Margin_CAD__c = margin;
        if(oli.Competitor_4_Emulsion_Margin_CAD__c != null)
            oli.Competitor_4_Emulsion_Margin_CAD__c.setScale(4, RoundingMode.HALF_UP);
        
        return null;
    }
    
    public PageReference recalculateAllNetbacks()
    {
        recalculateNetback();
        recalculateCompetitor1Netback();
        recalculateCompetitor2Netback();
        recalculateCompetitor3Netback();
        recalculateCompetitor4Netback();
        
        return null;
    }
    
    public PageReference recalculateAllEmulsion()
    {
        recalculateAxlePrices();
        recalculateCompetitor1Margin();
        recalculateCompetitor2Margin();
        recalculateCompetitor3Margin();
        recalculateCompetitor4Margin();
        
        return null;
    }
}