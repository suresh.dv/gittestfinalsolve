/**
 * An apex page controller that supports self registration of users in communities that allow self registration
 */
public class WeldingTrackerSelfRegistration {

    public String firstName {get; set;}
    public String lastName {get; set;}
    public String email {get; set;}
    public String password {get; set {password = value == null ? value : value.trim(); } }
    public String confirmPassword {get; set { confirmPassword = value == null ? value : value.trim(); } }
    public String communityNickname {get; set { communityNickname = value == null ? value : value.trim(); } }
    public String registrationCode {get; set{registrationCode = value == null ? value : value.trim(); } }
    public String userName {get; set;}
    public String publicKey { get; set; }
    
    private static String baseUrl = 'https://www.google.com/recaptcha/api/siteverify';
    private String privateKey = '';
    
    // Whether the submission has passed reCAPTCHA validation or not
    public Boolean verified { get; public set; }
    
    public WeldingTrackerSelfRegistration() {
        SelfRegistrationCodes__c sc = SelfRegistrationCodes__c.getInstance('Welding Tracker Community User');
            
        this.verified = false;
        this.privateKey = sc.CAPTCHA_Private_Key__c;
        publicKey = sc.CAPTCHA_Public_Key__c;       
    }
    
    private boolean isValidPassword() {
        return password == confirmPassword;
    }

    private void addError(String message){
        ApexPages.Message msg = new ApexPages.Message(ApexPages.Severity.ERROR, message);
        ApexPages.addMessage(msg);        
    }
    
    public static Boolean isValidEmail(String email) {
        String emailRegex = '^[a-zA-Z0-9._|\\\\%#~`=?&/$^*!}{+-]+@[a-zA-Z0-9.-]+\\.[a-zA-Z]{2,4}$';
        Pattern MyPattern = Pattern.compile(emailRegex);
        Matcher MyMatcher = MyPattern.matcher(email);
        return MyMatcher.matches();
    }
    
    public PageReference registerUser(){
        System.debug('registerUser method is entered'); 
        
        Profile profile;
        
        if( !isValidEmail( userName )){
            addError('Your username must be in the format of an email address.');
            return null;
        }
        
        // it's okay if password is null - we'll send the user a random password in that case
        if (!isValidPassword()) {
            addError(Label.site.passwords_dont_match);
            return null;
        }
        
        List<SelfRegistrationCodes__c> selfRegistrationCodes = SelfRegistrationCodes__c.getall().values();
        String profileName = '';
        for(SelfRegistrationCodes__c selfRegistrationCode : selfRegistrationCodes) {
            if(selfRegistrationCode.Registration_Code__c == registrationCode) {
                profileName = selfRegistrationCode.Name;
                break;
            }
        }
        
        /*Check number of active licenses*/
        SelfRegistrationCodes__c sc = SelfRegistrationCodes__c.getInstance(profileName);
        
        List<User> actUsr = [SELECT Id, Name FROM User where profile.Name =: profileName and IsActive = true];
        if(actUsr.size() >= sc.Max_Licenses_Count__c){
            addError('The system is currently undergoing maintenance and will be available shortly. Please try again later.');
            sendAlertEmail(sc, 'Welding Tracker maximum license count exceeded', 
                    'Welding Tracker maximum license count ' + Integer.valueof(sc.Max_Licenses_Count__c) +' has been exceeded. Please contact the Husky IS Support team.');
            return null;
        }
        
        if( profileName == '' ) {
            addError('The Registration Code is invalid.');
            return null;
        }
        else {
            profile = [SELECT Id, Name FROM Profile WHERE Name =: profileName];
        }
        
        List<User> usrList = [SELECT Id, Username, Email, IsActive, UserType, IsPortalEnabled, ContactId 
                    FROM User 
                    WHERE Email =: email AND Username =: userName AND UserType =: 'PowerPartner' AND IsPortalEnabled = True];
        
        User usr;
        
        if(usrList != Null && usrList.size() > 0)
            usr = usrList[0];
        else
            usr = Null;
        
        if(usr != Null && usr.IsActive == True) {
            addError('You are already registered in the system and active. Please login using your credentials.');
            return null;
        }
        else if(usr != Null && usr.IsActive == False) {
            usr.IsActive = True;
            update usr;
            System.resetPassword(usr.Id, True);
            
            addError('You already have a user record which was inactive. We have Activated the user record and reset ' +
                                                          'your password. Please check your email.');
            
            return null;
        }
        
        if( !Test.isRunningTest() ){
            system.debug('Private Key==='+privateKey);
            system.debug('base URL==='+baseUrl);
            system.debug('Response==='+response);
            system.debug('Remote Host==='+remoteHost);
            // call goes to to API endpoint
            HttpResponse r = makeRequest(baseUrl,
                'secret=' + privateKey +
                '&response='  + response +
                '&remoteip='  + remoteHost //+
            // '&challenge=' + challenge +
            );
 
            if ( r!= null )
            {
                 
                JSONParser parser = JSON.createParser( r.getBody() );
                //system.assert(false, parser);
                //system.assert( false, r.getBody()+'::::::::'+parser+':::::::::::'+JSONToken.FIELD_NAME );
                while( parser.nextToken() != null ){
                    system.debug('====current Token===='+parser.getCurrentToken());
                    system.debug('====JSONToken.FIELD_NAME===='+JSONToken.FIELD_NAME);
                    system.debug('====parser.getText()===='+parser.getText());
                    
                    if(( parser.getCurrentToken() == JSONToken.FIELD_NAME ) && ( parser.getText() == 'success' ))
                    {
                        parser.nextToken();
                        system.debug('====parser.getBooleanValue()===='+parser.getBooleanValue());
                        if( parser.getBooleanValue() == true ){
                            this.verified = true;
                        } else {
                            this.verified = false;
                        }
                    }
                }
            }
            
            if( !this.verified ){
                addError( 'Invalid reCAPTCHA verification attempt' );
                return null;
            }
            
            /*Alert if # of licenses used exceed 75% */
            if( actUsr.size() > ( sc.Max_Licenses_Count__c * .75 )){
                sendAlertEmail( sc, 'Welding Tracker license count 75% threshold exceeded', 'Please contact the Husky IS Support team.' );            
            }
        }
        
        /*if(!Test.isRunningTest()){
            HttpResponse r = makeRequest( baseUrl,
                'secret=' + privateKey +
                '&remoteip='  + remoteHost +
                '&response='  + response
            );
    
            if ( r != null ) {
            
                JSONParser parser = JSON.createParser(r.getBody());
                //system.assert( false, r.getBody()+'::::::::'+parser+':::::::::::'+JSONToken.FIELD_NAME );
         while (parser.nextToken() != null)
         {
          if ((parser.getCurrentToken() == JSONToken.FIELD_NAME) && (parser.getText() == 'success'))
          {
           parser.nextToken();
           if (parser.getBooleanValue()==true)
           {this.verified =true;}
           else{this.verified = false;}
          }
         }}
         
    
            if(!this.verified) {
                addError('Invalid reCAPTCHA verification attempt');
                return null;
            }
            
            /*Alert if # of licenses used exceed 75% 
            if(actUsr.size() > (sc.Max_Licenses_Count__c * .75)){
                sendAlertEmail(sc, 'Welding Tracker license count 75% threshold exceeded', 'Please contact the Husky IS Support team.');            
            }
        }*/

        String roleEnum = null; // To be filled in by customer.
        String accountId = ''; // To be filled in by customer.
        
        Account acct = [SELECT Id FROM Account WHERE Name=: 'Master Unassigned Welding Tracker Users'];
        accountId = acct.Id;        
        
        User u = new User();
        u.Username = userName;
        u.Email = email;
        u.FirstName = firstName;
        u.LastName = lastName;
        u.CommunityNickname = communityNickname;
        u.ProfileId = profile.Id;
        
        String userId;
        
        System.debug('User is about to be created: ' + Site.getAdminEmail());
        try {
            userId = Site.createPortalUser(u, accountId, password);             
            System.debug('User creation succeeded: ' + userId);
        }
        catch(Exception e) {
            System.debug('User creation failed: ' + e.getMessage());
            addError('There was an error creating the user:' + e.getMessage());
            return null;
        }
        
        if (userId != null) { 
            if (password != null && password.length() > 1) {
                return Site.login(userName, password, null);
            }
            else {
                PageReference page = System.Page.CommunitiesSelfRegConfirm;
                page.setRedirect(true);
                return page;
            }
        }
        return null;
    }

    public String response  {
        get {
            return ApexPages.currentPage().getParameters().get('g-recaptcha-response');
        }
    }
    
    @TestVisible
    private String remoteHost {
        get {
            String ret = '127.0.0.1';
            // also could use x-original-remote-host
            Map<String, String> hdrs = ApexPages.currentPage().getHeaders();
            if (hdrs.get('x-original-remote-addr')!= null)
                ret =  hdrs.get('x-original-remote-addr');
            else if (hdrs.get('X-Salesforce-SIP')!= null)
                ret =  hdrs.get('X-Salesforce-SIP');
            return ret;
        }
    }
    
    @TestVisible
    private static HttpResponse makeRequest(string url, string body)  {
        HttpResponse response = null;
        HttpRequest req = new HttpRequest();  
        req.setEndpoint(url);
        req.setMethod('POST');
        req.setBody (body);
        try {
            Http http = new Http();
            response = http.send(req);
            System.debug('reCAPTCHA response: ' + response);
            System.debug('reCAPTCHA body: ' + response.getBody());
        } catch(System.Exception e) {
            System.debug('ERROR: ' + e);
        }
        return response;
    }    
    
    @TestVisible
    private static void sendAlertEmail(SelfRegistrationCodes__c sc, String subject, String body){
        Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
        
        String[] toAddresses = new String[] {sc.Alert_Email__c}; 
        mail.setToAddresses(toAddresses);
        mail.setSubject(subject);
        mail.setBccSender(false);
        mail.emailPriority = 'Highest';
        mail.setPlainTextBody(body);
        
        Messaging.sendEmail(new Messaging.SingleEmailMessage[] { mail });
    }   
}