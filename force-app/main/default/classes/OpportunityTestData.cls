public class OpportunityTestData {

    public static Opportunity createOpportunity(Id recordTypeId) {
        Account acct = AccountTestData.createAccount('Test Account', null);
        insert acct;
        
        Opportunity opp = new Opportunity();
        opp.Name = 'Test Opportunity';
        opp.AccountId = acct.Id;
        opp.CloseDate = Date.today();
        opp.StageName = 'Opportunity Initiation - Marketer';
        opp.RecordTypeId = recordTypeId;
        
        return opp;
    }
}