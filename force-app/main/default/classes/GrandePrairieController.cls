public class GrandePrairieController
{
    public Boolean hasChatterAccess {get;set;}
    
    public String spotfireURL {get;set;}
    
    public String GrandePrairieTeamChatterGroup {get;set;}
    
    public List<AssetWrapper> assets {get;set;}
    
   public class AssetWrapper
    {
        public String URL  {get;set;}
        public String name {get;set;}
        public String icon {get;set;}
        public Boolean isActive {get;set;}
        
        public AssetWrapper(String URL, String name, String icon, Boolean active)
        {
            this.name = name;
            this.URL = URL;
            this.icon = icon;
            isActive = active;
        }
    }
    
    public List<TeamWrapper> teams {get;set;}
    
    public class TeamWrapper
    {
        public String URL  {get;set;}
        public String name {get;set;}
        public String pic  {get;set;}
        
        public TeamWrapper(String URL, String name, String pic)
        {
            this.name = name;
            this.URL = URL;
            this.pic = pic;
        }
    }
    
    public GrandePrairieController()
    {
        AURASpotfireURLs__c spotfireURLs = AURASpotfireURLs__c.getOrgDefaults();
        
        spotfireURL = spotfireURLs.GrandePrairieSpotfireSession__c;
        
        List<String> chatterNames = new List<String>{'Grande Prairie Team','Grande Prairie Reservoir','Grande Prairie Development','Grande Prairie D&C','Grande Prairie Production Ops','Grande Prairie Finance','Grande Prairie G&G','Grande Prairie BPI'};
        
        Map<String,Id> chatterName2Id = new Map<String,Id>();
        
        for(CollaborationGroup cg : [SELECT Id,Name FROM CollaborationGroup WHERE Name IN: chatterNames])
        {
            chatterName2Id.put(cg.Name,cg.Id);
        }
        
        GrandePrairieTeamChatterGroup = '';
        if(chatterName2Id.containsKey('Grande Prairie Team'))
            GrandePrairieTeamChatterGroup = chatterName2Id.get('Grande Prairie Team');
        
        // Does the current user have access to the Grande Prairie Chatter Group?
        hasChatterAccess = false;
        List<CollaborationGroupMember> GrandePrairieGroupMembers = [SELECT Id FROM CollaborationGroupMember WHERE CollaborationGroupId =: GrandePrairieTeamChatterGroup AND MemberId =: UserInfo.getUserId()];
        if(GrandePrairieGroupMembers.size() > 0)
            hasChatterAccess = true;
        
        assets = new List<AssetWrapper>();
        assets.add(new AssetWrapper('/apex/WCPWells?show=Western Canada Production', 'Wells',  'wells.png', true));
        assets.add(new AssetWrapper('#','Routes / Pipelines', 'pipelines.png', false));
        assets.add(new AssetWrapper('#','Facilities', 'facilities.png', false));
        
        teams = new List<TeamWrapper>();
        
        if(chatterName2Id.containsKey('Grande Prairie Reservoir'))
            teams.add(new TeamWrapper('../_ui/core/chatter/groups/GroupProfilePage?g='+chatterName2Id.get('Grande Prairie Reservoir'),'Reservoir','Reservoir.png'));
        else
            teams.add(new TeamWrapper('#','Reservoir','Reservoir.png'));
        
        if(chatterName2Id.containsKey('Grande Prairie G&G'))
            teams.add(new TeamWrapper('../_ui/core/chatter/groups/GroupProfilePage?g='+chatterName2Id.get('Grande Prairie G&G'),'G & G','GandG.jpg'));
        else
            teams.add(new TeamWrapper('#','G & G','GandG.jpg'));
        
        if(chatterName2Id.containsKey('Grande Prairie Development'))
            teams.add(new TeamWrapper('../_ui/core/chatter/groups/GroupProfilePage?g='+chatterName2Id.get('Grande Prairie Development'),'Development','Development.jpg'));
        else
            teams.add(new TeamWrapper('#','Development','Development.jpg'));
        
        if(chatterName2Id.containsKey('Grande Prairie Production Ops'))
            teams.add(new TeamWrapper('../_ui/core/chatter/groups/GroupProfilePage?g='+chatterName2Id.get('Grande Prairie Production Ops'),'Production Ops','Production.jpg'));
        else
            teams.add(new TeamWrapper('#','Production Ops','Production.jpg'));
        
        if(chatterName2Id.containsKey('Grande Prairie BPI'))
            teams.add(new TeamWrapper('../_ui/core/chatter/groups/GroupProfilePage?g='+chatterName2Id.get('Grande Prairie BPI'),'BPI','BPI.jpg'));
        else
            teams.add(new TeamWrapper('#','BPI','BPI.jpg'));
        
        if(chatterName2Id.containsKey('Grande Prairie Finance'))
            teams.add(new TeamWrapper('../_ui/core/chatter/groups/GroupProfilePage?g='+chatterName2Id.get('Grande Prairie Finance'),'Finance','Finance.jpg'));
        else
            teams.add(new TeamWrapper('#','Finance','Finance.jpg'));
        
        if(chatterName2Id.containsKey('Grande Prairie D&C'))
            teams.add(new TeamWrapper('../_ui/core/chatter/groups/GroupProfilePage?g='+chatterName2Id.get('Grande Prairie D&C'),'D & C','DandC.png'));
        else
            teams.add(new TeamWrapper('#','D & C','DandC.png'));
    }
}