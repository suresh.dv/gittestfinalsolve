/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 *
 * Unit tests are class methods that verify whether a particular piece
 * of code is working properly. Unit test methods take no arguments,
 * commit no data to the database, and are flagged with the testMethod
 * keyword in the method definition.
 *
 * All test methods in an organization are executed whenever Apex code is deployed
 * to a production organization to confirm correctness, ensure code
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployed
 * to a production organization. In addition, all triggers must have some code coverage.
 * 
 * The @isTest class annotation indicates this class only contains test
 * methods. Classes defined with the @isTest annotation do not count against
 * the organization size limit for all Apex scripts.
 *
 * See the Apex Language Reference for more information about Testing and Code Coverage.
 */
@isTest (SeeAllData = false)
private class USCP_HomePage_ControllerTest{

    static testMethod void myUnitTest() {
        
        User runningUser = UserTestData.createTestsysAdminUser();
        insert runningUser;
        System.AssertNotEquals(runningUser.Id, Null);
        
        System.runAs(runningUser) {
        
            // Create a Parent Account with the name 100
            Account account = AccountTestData.createAccount('100', Null);
            insert account;
            System.AssertNotEquals(account.Id, Null);

            // Create a Contact for Account 100
            Contact andreAgassi = ContactTestData.createContact(account.Id, 'Andre', 'Agassi');
            andreAgassi.Title = 'portal user';
            insert andreAgassi;
            System.AssertNotEquals(andreAgassi.Id, Null);
            
            Test.startTest();
                // Create User for andreAgassi
                User andreAgassiUser = USCP_TestData.createTestCustomerPortalUser(andreAgassi.Id);
                System.AssertNotEquals(andreAgassiUser.Id, Null);

                System.runAs(andreAgassiUser ) {
                    ApexPages.StandardController sc = new ApexPages.standardController(account);
                    USCP_HomePage_Controller controller = new  USCP_HomePage_Controller(sc);

                    System.AssertNotEquals(null,controller.redirect()); 
                    System.AssertEquals('/apex/uscp_customer?id='+account.id, controller.redirect().getUrl());                    
                }
                
            Test.stopTest(); 

        }
        
    }
}