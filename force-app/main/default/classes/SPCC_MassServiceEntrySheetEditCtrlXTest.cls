@isTest
private class SPCC_MassServiceEntrySheetEditCtrlXTest {

	@testSetup static void setup() {

		//User epcUser = SPCC_TestData.createEPCUser();

		SPCC_Engineering_Work_Request__c ewr = SPCC_TestData.createEWR('TestProject', '12345', '','Other');
		insert ewr;

		SPCC_Authorization_for_Expenditure__c afe = SPCC_TestData.createAFE('A1-F7GH56-4D-J7-K2', 'A1-F7GH56-4D-J7-K2', 'Test AFE', ewr.Id);
		insert afe;

		SPCC_Cost_Element__c costElement = SPCC_TestData.createCostElement(afe.Id, 'Test Category', 
													 'Test Description', 
													 '6000000',
													 'Test Cose Element',
													 5000,
													 0);
		insert costElement;

		List<Account> vendorAccounts = new List<Account>();
		for(Integer i=0; i<10; i++) {
			vendorAccounts.add(SPCC_TestData.createSPCCVendor('Test Account' + i));
		}
		insert vendorAccounts;

		List<SPCC_Vendor_Account_Assignment__c> vaaAssignments = new List<SPCC_Vendor_Account_Assignment__c>();
		for(Account vendorAccount : vendorAccounts) {
			vaaAssignments.add(SPCC_TestData.createVAA(ewr.Id, vendorAccount.Id));
		}
		insert vaaAssignments;

		List<SPCC_Purchase_Order__c> purchaseOrders = new List<SPCC_Purchase_Order__c>();
		for(Integer i=0; i<10; i++) {
			purchaseOrders.add(SPCC_TestData.createPO('012345678' + i,
												  	ewr.Id,
												  	afe.Id,
												  	vendorAccounts[0].Id,
												  	'',
												  	2000,
												  	'Test Purchase Order',
												  	null));
		}
		purchaseOrders[0].Estimate_to_Complete__c = 500;
		insert purchaseOrders;

		List<SPCC_Line_Item__c> lineItems = new List<SPCC_Line_Item__c>();
		for(SPCC_Purchase_Order__c po : purchaseOrders) {
			SPCC_Line_Item__c lineItem = SPCC_TestData.createLineItem(po.Id, costElement.Id, 100, '0010', 'Test Line Item ' + po.Purchase_Order_Number__c);
			lineItems.add(lineItem);
		}
		insert lineItems;

		List<SPCC_Service_Entry_Sheet__c> serviceEntrySheets = new List<SPCC_Service_Entry_Sheet__c>();
		for(Integer i=0; i<10; i++) {
			serviceEntrySheets.add(SPCC_TestData.createSES(purchaseOrders[i].Id, lineItems[i].Id, 100+i, Date.today().addDays(-i),'12340' + i));
		}
		insert serviceEntrySheets;

		//Create Users in future call
		createUsers();
	}

	@isTest static void testRedirectToDetail() {
		SPCC_Engineering_Work_Request__c ewr = [Select Id, Name, EWR_Number__c, Project_Name__c,
													   Start_Date__c, Status__c, Project_Type__c
												From SPCC_Engineering_Work_Request__c];

		Test.startTest();
			System.runAs(SPCC_TestData.createPLUser()) {
				//Set Page
				PageReference pRef = Page.SPCC_MassServiceEntrySheetEdit;
				Test.setCurrentPage(pRef);

				//Create Controller
				ApexPages.StandardController stdController = new ApexPages.StandardController(ewr);
				SPCC_MassServiceEntrySheetEditCtrlX extCtrl = new SPCC_MassServiceEntrySheetEditCtrlX(stdController);

				//Check of user allowed to view detail
				System.assert(extCtrl.isUserAllowedToViewDetail);

				PageReference standardDetailPage = stdController.view();
            	standardDetailPage.getParameters().put('nooverride', '1');
				System.assertEquals(standardDetailPage.getUrl(), extCtrl.redirectToDetail().getUrl());
			}
		Test.stopTest();
	}
	
	
	@isTest static void testRedirectToReport() {
		SPCC_Engineering_Work_Request__c ewr = [Select Id, Name, EWR_Number__c, Project_Name__c,
													   Start_Date__c, Status__c, Project_Type__c
												From SPCC_Engineering_Work_Request__c];

		//Assign CSR to Project
		User csrUser = [Select Id, Name
						From User
						Where LastName Like '%_CSR'];
		SPCC_CSR_Assignment__c csrAssignment = SPCC_TestData.createCSRAssignment(ewr.Id, csrUser.Id);
		
		
		Test.startTest();
			System.runAs(csrUser) {
				//Set Page
				PageReference pRef = Page.SPCC_MassServiceEntrySheetEdit;
				Test.setCurrentPage(pRef);

				//Create Controller
				ApexPages.StandardController stdController = new ApexPages.StandardController(ewr);
				SPCC_MassServiceEntrySheetEditCtrlX extCtrl = new SPCC_MassServiceEntrySheetEditCtrlX(stdController);

				//Check of user allowed to view detail
				System.assert(extCtrl.isUserAllowedToViewReport);
				//Check for redirect URL
				System.assertEquals(URL.getSalesforceBaseUrl().toExternalForm() + '/' + extCtrl.reportId + '?pv0=' + ewr.Name, extCtrl.redirectToReport().getUrl());
			    //No notification on success disaplyed 
			    System.assertEquals(false, extCtrl.pageNotification.show);   
			    
			}
		Test.stopTest();
	}
	
	@isTest static void testControllerCRUDActions() {
		//Query Setup Data
		SPCC_Engineering_Work_Request__c ewr = [Select Id, Name, EWR_Number__c, Project_Name__c,
													   Start_Date__c, Status__c
												From SPCC_Engineering_Work_Request__c];
		SPCC_Authorization_for_Expenditure__c afe = [Select Id, Name, AFE_Number__c, Description__c
													 From SPCC_Authorization_for_Expenditure__c];
		List<SPCC_Purchase_Order__c> purchaseOrders = [Select Id, Name, Vendor_Account__r.Name
													   From SPCC_Purchase_Order__c];
		List<SPCC_Line_Item__c> lineItems = [Select Id, Name, Purchase_Order__c, Cost_Element__c,
													Committed_Amount__c, Short_Text__c, Item__c
											 From SPCC_Line_Item__c];
		List<SPCC_Service_Entry_Sheet__c> serviceEntrySheets = [Select Id, Name, Purchase_Order__c, Amount__c, Date_Entered__c, Ticket_Number__c 
															    From SPCC_Service_Entry_Sheet__c
															    Order By Date_Entered__c DESC NULLS LAST];

		//Assign CSR to Project
		User csrUser = [Select Id, Name
						From User
						Where LastName Like '%_CSR'];
		SPCC_CSR_Assignment__c csrAssignment = SPCC_TestData.createCSRAssignment(ewr.Id, csrUser.Id);
		insert csrAssignment;

		Test.startTest();
			System.runAs(csrUser) {
				//Set Page
				PageReference pRef = Page.SPCC_MassServiceEntrySheetEdit;
				Test.setCurrentPage(pRef);

				//Create Controller
				ApexPages.StandardController stdController = new ApexPages.StandardController(ewr);
				SPCC_MassServiceEntrySheetEditCtrlX extCtrl = new SPCC_MassServiceEntrySheetEditCtrlX(stdController);

				//Check existing records
				System.assertEquals(purchaseOrders.size(), extCtrl.purchaseOrderMap.size());
				for(SPCC_Purchase_Order__c po : purchaseOrders) {
					System.assertEquals(po.Vendor_Account__r.Name , extCtrl.purchaseOrderMap.get(po.Id).Vendor_Account__r.Name);
				}
				System.assertEquals(serviceEntrySheets.size(), extCtrl.serviceEntrySheets.size());
				for(Integer i=0; i < serviceEntrySheets.size(); i++) {
					System.assertEquals(serviceEntrySheets[i].Purchase_Order__c, extCtrl.serviceEntrySheets[i].serviceEntrySheet.Purchase_Order__c);
					System.assertEquals(serviceEntrySheets[i].Amount__c, extCtrl.serviceEntrySheets[i].serviceEntrySheet.Amount__c);
					System.assertEquals(serviceEntrySheets[i].Date_Entered__c, extCtrl.serviceEntrySheets[i].serviceEntrySheet.Date_Entered__c);
					System.assertEquals(serviceEntrySheets[i].Ticket_Number__c, extCtrl.serviceEntrySheets[i].serviceEntrySheet.Ticket_Number__c);
					System.assertEquals('VIEW', extCtrl.serviceEntrySheets[i].getMode());
					System.assertEquals('EXISTING', extCtrl.serviceEntrySheets[i].getState());
					System.assertEquals(false, extCtrl.serviceEntrySheets[i].getIsChanged());
				}

				//Check Screen Options
				//System.assertEquals(purchaseOrders.size() + lineItems.size() +1, extCtrl.poAndLineItemComboOptions.size());
				System.assertEquals(purchaseOrders.size(), extCtrl.poAccountOptionsList.size());

				//Check Page Actions -- Add New Service Entry Sheet
				System.assertEquals(extCtrl.addNewServiceEntrySheet(), null);
				System.assertEquals(extCtrl.serviceEntrySheets.size(), serviceEntrySheets.size()+1);
				Integer newIndex = 0;
				System.assertEquals('EDIT', extCtrl.serviceEntrySheets[newIndex].getMode());
				System.assertEquals('CREATED', extCtrl.serviceEntrySheets[newIndex].getState());
				System.assertEquals(true, extCtrl.serviceEntrySheets[newIndex].getIsChanged());
				System.assertEquals(true, extCtrl.areChangedRecords);

				//Check Page Actions -- Remove Service Entry Sheet
				ApexPages.currentPage().getParameters().put('sesIndex', String.valueOf(newIndex));
				System.assertEquals(null, extCtrl.removeServiceEntrySheet());
				System.assertEquals(extCtrl.serviceEntrySheets.size(), serviceEntrySheets.size());
				System.assertEquals(false, extCtrl.areChangedRecords);

				//Check Page Actions -- Edit Service Entry Sheet
				Integer editIndex = 3;
				ApexPages.currentPage().getParameters().put('sesIndex', String.valueOf(editIndex));
				System.assertEquals(null, extCtrl.editServiceEntrySheet());
				System.assertEquals('EDIT', extCtrl.serviceEntrySheets[editIndex].getMode());
				System.assertEquals('EXISTING', extCtrl.serviceEntrySheets[editIndex].getState());
				System.assertEquals(false, extCtrl.serviceEntrySheets[editIndex].getIsChanged());
				extCtrl.serviceEntrySheets[editIndex].serviceEntrySheet.Amount__c = 1000.00;
				System.assertEquals(null, extCtrl.saveServiceEntrySheet()); //Commit change to page
				System.assertEquals(true, extCtrl.serviceEntrySheets[editIndex].getIsChanged());
				System.assertEquals('VIEW', extCtrl.serviceEntrySheets[editIndex].getMode());

				//Check Page Actions -- Add Description
				editIndex = 3;
				extCtrl.serviceEntrySheets.get(editIndex).serviceEntrySheet.Description__c = 'Test Description';
				ApexPages.currentPage().getParameters().put('sesIndex', String.valueOf(editIndex));
				System.assertEquals(null, extCtrl.showDescriptionEditPopup());
				System.assertEquals(extCtrl.serviceEntrySheets.get(editIndex).serviceEntrySheet.Id, extCtrl.descriptionPopup.ses.serviceEntrySheet.Id);
				System.assert(extCtrl.descriptionPopup.show);
				System.assertEquals('Test Description', extCtrl.descriptionPopup.oldDescription);
				System.assertEquals(null, extCtrl.saveDescriptionEditPopup());
				System.assert(!extCtrl.descriptionPopup.show);
				System.assertEquals(null, extCtrl.descriptionPopup.ses);
				System.assertEquals(null, extCtrl.showDescriptionEditPopup());
				System.assertEquals(null, extCtrl.cancelDescriptionEditPopup());

				//Check Page Actions -- Delete Service Entry Sheet
				Integer deleteIndex = 2;
				SPCC_Service_Entry_Sheet__c sesToDelete = extCtrl.serviceEntrySheets[deleteIndex].serviceEntrySheet;
				ApexPages.currentPage().getParameters().put('sesIndex', String.valueOf(deleteIndex));
				System.assertEquals(null, extCtrl.deleteServiceEntrySheet());
				System.assertEquals(1, extCtrl.serviceEntrySheetsToDelete.size());
				System.assertEquals(sesToDelete.Purchase_Order__c, sesToDelete.Purchase_Order__c);

				//Check Page Actions -- Submit Changes
				System.assertEquals(null, extCtrl.addNewServiceEntrySheet());//Add Blank Service Entry Sheet to check error case
				System.assertEquals(null, extCtrl.submit());
				System.assertEquals(true, extCtrl.pageNotification.show);
				System.assertEquals('ERROR', extCtrl.pageNotification.getMode());
				System.assertEquals(null, extCtrl.closeNotification());
				System.assertEquals(false, extCtrl.pageNotification.show);
				newIndex = 0;
				extCtrl.serviceEntrySheets[newIndex].poId = purchaseOrders[0].Name + '-' + purchaseOrders[0].Vendor_Account__r.Name + ' --- ' + lineItems[0].Item__c + '-' + lineItems[0].Short_Text__c;
				extCtrl.serviceEntrySheets[newIndex].setEstimateToFinish();
				System.debug('1 : ' + extCtrl.serviceEntrySheets[newIndex].poOverUnder);
				System.debug('2 : ' + extCtrl.serviceEntrySheets[newIndex].liCostRate);
				System.debug('3 : ' + extCtrl.serviceEntrySheets[newIndex].poId);
				//original value of estimate to finish
				System.assertEquals(500, [SELECT Estimate_to_Complete__c FROM SPCC_Purchase_Order__c WHERE Id =: purchaseOrders[0].Id].Estimate_to_Complete__c);
				extCtrl.serviceEntrySheets[newIndex].serviceEntrySheet.Ticket_Number__c = 'as34a3zss';
				extCtrl.serviceEntrySheets[newIndex].serviceEntrySheet.Amount__c = 1000;
				extCtrl.serviceEntrySheets[newIndex].serviceEntrySheet.Date_Entered__c = Date.today();
				extCtrl.serviceEntrySheets[newIndex].estimateToFinish = '600';
				extCtrl.serviceEntrySheets[newIndex].setNewEstimateToFinish();
				System.assertEquals(null, extCtrl.submit());
				System.assertEquals(true, extCtrl.pageNotification.show);
				System.assertEquals('SUCCESS', extCtrl.pageNotification.getMode());
				//new value of estimate to finish - after ses ticket submit
				System.assertEquals(600, [SELECT Estimate_to_Complete__c FROM SPCC_Purchase_Order__c WHERE Id =: purchaseOrders[0].Id].Estimate_to_Complete__c);
			}
		Test.stopTest();
	}

	@isTest static void testControllerSortActions() {
		//Query Setup Data
		SPCC_Engineering_Work_Request__c ewr = [Select Id, Name, EWR_Number__c, Project_Name__c,
													   Start_Date__c, Status__c
												From SPCC_Engineering_Work_Request__c];
		SPCC_Authorization_for_Expenditure__c afe = [Select Id, Name, AFE_Number__c, Description__c
													 From SPCC_Authorization_for_Expenditure__c];
		List<SPCC_Purchase_Order__c> purchaseOrders = [Select Id, Name, Vendor_Account__r.Name
													   From SPCC_Purchase_Order__c];

		//Assign CSR to Project
		User csrUser = [Select Id, Name
						From User
						Where LastName Like '%_CSR'];
		SPCC_CSR_Assignment__c csrAssignment = SPCC_TestData.createCSRAssignment(ewr.Id, csrUser.Id);
		insert csrAssignment;

		Test.startTest();
			System.runAs(csrUser) {
				//Set Page
				PageReference pRef = Page.SPCC_MassServiceEntrySheetEdit;
				Test.setCurrentPage(pRef);

				//Create Controller
				ApexPages.StandardController stdController = new ApexPages.StandardController(ewr);
				SPCC_MassServiceEntrySheetEditCtrlX extCtrl = new SPCC_MassServiceEntrySheetEditCtrlX(stdController);

				//Check existing records - Sorted By Date in Descending Order
				List<SPCC_Service_Entry_Sheet__c> serviceEntrySheets = [Select Id, Name, Purchase_Order__c, Amount__c, Date_Entered__c, Ticket_Number__c 
																	    From SPCC_Service_Entry_Sheet__c
																	    Order By Date_Entered__c DESC NULLS LAST];
				System.assertEquals(serviceEntrySheets.size(), extCtrl.serviceEntrySheets.size());
				System.assertEquals(SPCC_MassServiceEntrySheetEditCtrlX.SORT_BY_DATE, extCtrl.sortColumn);
				System.assertEquals(SPCC_MassServiceEntrySheetEditCtrlX.SORT_DESCENDING, extCtrl.sortOrder);
				for(Integer i=0; i < serviceEntrySheets.size(); i++) {
					System.assertEquals(serviceEntrySheets[i].Id, extCtrl.serviceEntrySheets[i].serviceEntrySheet.Id);
				}

				//Check Sorted By Date in Ascending Order
				serviceEntrySheets = [Select Id, Name, Purchase_Order__c, Amount__c, Date_Entered__c, Ticket_Number__c 
								      From SPCC_Service_Entry_Sheet__c
								      Order By Date_Entered__c Asc NULLS LAST];
				ApexPages.currentPage().getParameters().put('sortOptions', 'SORT_BY_DATE;SORT_ASCENDING');
				System.assertEquals(null, extCtrl.sortServiceEntrySheets());
				for(Integer i=0; i < serviceEntrySheets.size(); i++) {
					System.assertEquals(serviceEntrySheets[i].Id, extCtrl.serviceEntrySheets[i].serviceEntrySheet.Id);
				}

				//Check Sorted By Purchase Order in Ascending Order
				serviceEntrySheets = [Select Id, Name, Purchase_Order__c, Amount__c, Date_Entered__c, Ticket_Number__c 
								      From SPCC_Service_Entry_Sheet__c
								      Order By Purchase_Order__r.Name Asc NULLS LAST];
				ApexPages.currentPage().getParameters().put('sortOptions', 'SORT_BY_PURCHASEORDER_NUMBER;SORT_ASCENDING');
				System.assertEquals(null, extCtrl.sortServiceEntrySheets());
				for(Integer i=0; i < serviceEntrySheets.size(); i++) {
					System.assertEquals(serviceEntrySheets[i].Id, extCtrl.serviceEntrySheets[i].serviceEntrySheet.Id);
				}

				//Check Sorted By Purchase Order in Descending Order
				serviceEntrySheets = [Select Id, Name, Purchase_Order__c, Amount__c, Date_Entered__c, Ticket_Number__c 
								      From SPCC_Service_Entry_Sheet__c
								      Order By Purchase_Order__r.Name DESC NULLS LAST];
				ApexPages.currentPage().getParameters().put('sortOptions', 'SORT_BY_PURCHASEORDER_NUMBER;SORT_DESCENDING');
				System.assertEquals(null, extCtrl.sortServiceEntrySheets());
				for(Integer i=0; i < serviceEntrySheets.size(); i++) {
					System.assertEquals(serviceEntrySheets[i].Id, extCtrl.serviceEntrySheets[i].serviceEntrySheet.Id);
				}

				//Check Sorted By Ticket Number in Ascending Order
				serviceEntrySheets = [Select Id, Name, Purchase_Order__c, Amount__c, Date_Entered__c, Ticket_Number__c 
								      From SPCC_Service_Entry_Sheet__c
								      Order By Ticket_Number__c Asc NULLS LAST];
				ApexPages.currentPage().getParameters().put('sortOptions', 'SORT_BY_TICKET_NUMBER;SORT_ASCENDING');
				System.assertEquals(null, extCtrl.sortServiceEntrySheets());
				for(Integer i=0; i < serviceEntrySheets.size(); i++) {
					System.assertEquals(serviceEntrySheets[i].Id, extCtrl.serviceEntrySheets[i].serviceEntrySheet.Id);
				}

				//Check Sorted By Ticket Number in Descending Order
				serviceEntrySheets = [Select Id, Name, Purchase_Order__c, Amount__c, Date_Entered__c, Ticket_Number__c 
								      From SPCC_Service_Entry_Sheet__c
								      Order By Ticket_Number__c DESC NULLS LAST];
				ApexPages.currentPage().getParameters().put('sortOptions', 'SORT_BY_TICKET_NUMBER;SORT_DESCENDING');
				System.assertEquals(null, extCtrl.sortServiceEntrySheets());
				for(Integer i=0; i < serviceEntrySheets.size(); i++) {
					System.assertEquals(serviceEntrySheets[i].Id, extCtrl.serviceEntrySheets[i].serviceEntrySheet.Id);
				}

				//Check Sorted By Amount in Ascending Order
				serviceEntrySheets = [Select Id, Name, Purchase_Order__c, Amount__c, Date_Entered__c, Ticket_Number__c 
								      From SPCC_Service_Entry_Sheet__c
								      Order By Amount__c Asc NULLS LAST];
				ApexPages.currentPage().getParameters().put('sortOptions', 'SORT_BY_AMOUNT;SORT_ASCENDING');
				System.assertEquals(null, extCtrl.sortServiceEntrySheets());
				for(Integer i=0; i < serviceEntrySheets.size(); i++) {
					System.assertEquals(serviceEntrySheets[i].Id, extCtrl.serviceEntrySheets[i].serviceEntrySheet.Id);
				}

				//Check Sorted By Amount in Descending Order
				serviceEntrySheets = [Select Id, Name, Purchase_Order__c, Amount__c, Date_Entered__c, Ticket_Number__c 
								      From SPCC_Service_Entry_Sheet__c
								      Order By Amount__c DESC NULLS LAST];
				ApexPages.currentPage().getParameters().put('sortOptions', 'SORT_BY_AMOUNT;SORT_DESCENDING');
				System.assertEquals(null, extCtrl.sortServiceEntrySheets());
				for(Integer i=0; i < serviceEntrySheets.size(); i++) {
					System.assertEquals(serviceEntrySheets[i].Id, extCtrl.serviceEntrySheets[i].serviceEntrySheet.Id);
				}

				//Check Sorted By Vendor in Ascending Order
				serviceEntrySheets = [Select Id, Name, Purchase_Order__c, Amount__c, Date_Entered__c, Ticket_Number__c,
											 Purchase_Order__r.Vendor_Account__r.Name
								      From SPCC_Service_Entry_Sheet__c
								      Order By Purchase_Order__r.Vendor_Account__r.Name Asc NULLS LAST];
				ApexPages.currentPage().getParameters().put('sortOptions', 'SORT_BY_VENDOR;SORT_ASCENDING');
				System.assertEquals(null, extCtrl.sortServiceEntrySheets());
				for(Integer i=0; i < serviceEntrySheets.size(); i++) {
					System.assertEquals(serviceEntrySheets[i].Purchase_Order__r.Vendor_Account__r.Name, 
						extCtrl.serviceEntrySheets[i].serviceEntrySheet.Purchase_Order__r.Vendor_Account__r.Name);
				}

				//Check Sorted By Vendor in Descending Order
				serviceEntrySheets = [Select Id, Name, Purchase_Order__c, Amount__c, Date_Entered__c, Ticket_Number__c,
											 Purchase_Order__r.Vendor_Account__r.Name 
								      From SPCC_Service_Entry_Sheet__c
								      Order By Purchase_Order__r.Vendor_Account__r.Name DESC NULLS LAST];
				ApexPages.currentPage().getParameters().put('sortOptions', 'SORT_BY_VENDOR;SORT_DESCENDING');
				System.assertEquals(null, extCtrl.sortServiceEntrySheets());
				for(Integer i=0; i < serviceEntrySheets.size(); i++) {
					System.assertEquals(serviceEntrySheets[i].Purchase_Order__r.Vendor_Account__r.Name, 
						extCtrl.serviceEntrySheets[i].serviceEntrySheet.Purchase_Order__r.Vendor_Account__r.Name);
				}

				//Check Sorted By PO Over Under in Ascending Order
				serviceEntrySheets = [Select Id, Name, Purchase_Order__c, Amount__c, Date_Entered__c, Ticket_Number__c 
								      From SPCC_Service_Entry_Sheet__c
								      Order By Purchase_Order__r.PO_Over_Under__c Asc NULLS LAST];
				ApexPages.currentPage().getParameters().put('sortOptions', 'SORT_BY_PO_OVERUNDER;SORT_ASCENDING');
				System.assertEquals(null, extCtrl.sortServiceEntrySheets());
				for(Integer i=0; i < serviceEntrySheets.size(); i++) {
					System.assertEquals(serviceEntrySheets[i].Id, extCtrl.serviceEntrySheets[i].serviceEntrySheet.Id);
				}

				//Check Sorted By PO Over Under in Descending Order
				serviceEntrySheets = [Select Id, Name, Purchase_Order__c, Amount__c, Date_Entered__c, Ticket_Number__c 
								      From SPCC_Service_Entry_Sheet__c
								      Order By Purchase_Order__r.PO_Over_Under__c DESC NULLS LAST];
				ApexPages.currentPage().getParameters().put('sortOptions', 'SORT_BY_PO_OVERUNDER;SORT_DESCENDING');
				System.assertEquals(null, extCtrl.sortServiceEntrySheets());
				for(Integer i=0; i < serviceEntrySheets.size(); i++) {
					System.assertEquals(serviceEntrySheets[i].Id, extCtrl.serviceEntrySheets[i].serviceEntrySheet.Id);
				}

				//Check Sorted By Incurred Cost Rate in Ascending Order
				serviceEntrySheets = [Select Id, Name, Purchase_Order__c, Amount__c, Date_Entered__c, Ticket_Number__c 
								      From SPCC_Service_Entry_Sheet__c
								      Order By Line_Item__r.Incurred_Cost_Rate__c Asc NULLS LAST];
				ApexPages.currentPage().getParameters().put('sortOptions', 'SORT_BY_INCURRED_COST_RATE;SORT_ASCENDING');
				System.assertEquals(null, extCtrl.sortServiceEntrySheets());
				for(Integer i=0; i < serviceEntrySheets.size(); i++) {
					System.assertEquals(serviceEntrySheets[i].Id, extCtrl.serviceEntrySheets[i].serviceEntrySheet.Id);
				}

				//Check Sorted By Incurred Cost Rate in Descending Order
				serviceEntrySheets = [Select Id, Name, Purchase_Order__c, Amount__c, Date_Entered__c, Ticket_Number__c 
								      From SPCC_Service_Entry_Sheet__c
								      Order By Line_Item__r.Incurred_Cost_Rate__c DESC NULLS LAST];
				ApexPages.currentPage().getParameters().put('sortOptions', 'SORT_BY_INCURRED_COST_RATE;SORT_DESCENDING');
				System.assertEquals(null, extCtrl.sortServiceEntrySheets());
				for(Integer i=0; i < serviceEntrySheets.size(); i++) {
					System.assertEquals(serviceEntrySheets[i].Id, extCtrl.serviceEntrySheets[i].serviceEntrySheet.Id);
				}

				//Check Sorted By Line Item Number in Ascending Order
				serviceEntrySheets = [Select Id, Name, Purchase_Order__c, Amount__c, Date_Entered__c, Ticket_Number__c,
											 Line_Item__r.Item__c 
								      From SPCC_Service_Entry_Sheet__c
								      Order By Line_Item__r.Item__c Asc NULLS LAST];
				ApexPages.currentPage().getParameters().put('sortOptions', 'SORT_BY_LINE_ITEM;SORT_ASCENDING');
				System.assertEquals(null, extCtrl.sortServiceEntrySheets());
				for(Integer i=0; i < serviceEntrySheets.size(); i++) {
					System.assertEquals(serviceEntrySheets[i].Line_Item__r.Item__c, extCtrl.serviceEntrySheets[i].serviceEntrySheet.Line_Item__r.Item__c);
				}

				//Check Sorted By Line Item Number in Descending Order
				serviceEntrySheets = [Select Id, Name, Purchase_Order__c, Amount__c, Date_Entered__c, Ticket_Number__c,
											 Line_Item__r.Item__c 
								      From SPCC_Service_Entry_Sheet__c
								      Order By Line_Item__r.Item__c DESC NULLS LAST];
				ApexPages.currentPage().getParameters().put('sortOptions', 'SORT_BY_LINE_ITEM;SORT_DESCENDING');
				System.assertEquals(null, extCtrl.sortServiceEntrySheets());
				for(Integer i=0; i < serviceEntrySheets.size(); i++) {
					System.assertEquals(serviceEntrySheets[i].Line_Item__r.Item__c, extCtrl.serviceEntrySheets[i].serviceEntrySheet.Line_Item__r.Item__c);
				}
				
				//Check Sorted By Estimate To Finish in Ascending Order
				serviceEntrySheets = [Select Id, Name, Purchase_Order__c, Amount__c, Date_Entered__c, Ticket_Number__c,
											 Purchase_Order__r.Estimate_to_Complete__c 
								      From SPCC_Service_Entry_Sheet__c
								      Order By Purchase_Order__r.Estimate_to_Complete__c ASC];
				ApexPages.currentPage().getParameters().put('sortOptions', 'SORT_BY_ESTIMATE_TO_FINISH;SORT_ASCENDING');
				System.assertEquals(null, extCtrl.sortServiceEntrySheets());
				for(Integer i=0; i < serviceEntrySheets.size(); i++) {
					System.assertEquals(serviceEntrySheets[i].Purchase_Order__r.Estimate_to_Complete__c, extCtrl.serviceEntrySheets[i].serviceEntrySheet.Purchase_Order__r.Estimate_to_Complete__c);
				}

				//Check Sorted By Estimate To Finish in Descending Order
				serviceEntrySheets = [Select Id, Name, Purchase_Order__c, Amount__c, Date_Entered__c, Ticket_Number__c,
											 Purchase_Order__r.Estimate_to_Complete__c 
								      From SPCC_Service_Entry_Sheet__c
								      Order By Purchase_Order__r.Estimate_to_Complete__c DESC NULLS LAST];
				ApexPages.currentPage().getParameters().put('sortOptions', 'SORT_BY_ESTIMATE_TO_FINISH;SORT_DESCENDING');
				System.assertEquals(null, extCtrl.sortServiceEntrySheets());
				for(Integer i=0; i < serviceEntrySheets.size(); i++) {
					System.assertEquals(serviceEntrySheets[i].Purchase_Order__r.Estimate_to_Complete__c, extCtrl.serviceEntrySheets[i].serviceEntrySheet.Purchase_Order__r.Estimate_to_Complete__c);
				}
				
				//Check Sorted By AFE Number in Ascending Order
				serviceEntrySheets = [Select Id, Name, Purchase_Order__c, Amount__c, Date_Entered__c, Ticket_Number__c,
											 Purchase_Order__r.Authorization_for_Expenditure__r.AFE_Number__c 
								      From SPCC_Service_Entry_Sheet__c
								      Order By Purchase_Order__r.Authorization_for_Expenditure__r.AFE_Number__c Asc NULLS LAST];
				ApexPages.currentPage().getParameters().put('sortOptions', 'SORT_BY_AFE_NUMBER;SORT_ASCENDING');
				System.assertEquals(null, extCtrl.sortServiceEntrySheets());
				for(Integer i=0; i < serviceEntrySheets.size(); i++) {
					System.assertEquals(serviceEntrySheets[i].Purchase_Order__r.Authorization_for_Expenditure__r.AFE_Number__c, extCtrl.serviceEntrySheets[i].serviceEntrySheet.Purchase_Order__r.Authorization_for_Expenditure__r.AFE_Number__c);
				}

				//Check Sorted By AFE Numberh in Descending Order
				serviceEntrySheets = [Select Id, Name, Purchase_Order__c, Amount__c, Date_Entered__c, Ticket_Number__c,
											 Purchase_Order__r.Authorization_for_Expenditure__r.AFE_Number__c 
								      From SPCC_Service_Entry_Sheet__c
								      Order By Purchase_Order__r.Authorization_for_Expenditure__r.AFE_Number__c DESC NULLS LAST];
				ApexPages.currentPage().getParameters().put('sortOptions', 'SORT_BY_AFE_NUMBER;SORT_DESCENDING');
				System.assertEquals(null, extCtrl.sortServiceEntrySheets());
				for(Integer i=0; i < serviceEntrySheets.size(); i++) {
					System.assertEquals(serviceEntrySheets[i].Purchase_Order__r.Authorization_for_Expenditure__r.AFE_Number__c, extCtrl.serviceEntrySheets[i].serviceEntrySheet.Purchase_Order__r.Authorization_for_Expenditure__r.AFE_Number__c);
				}	
					
				//Check Sorted By Gl Code in Ascending Order
				serviceEntrySheets = [Select Id, Name, Purchase_Order__c, Amount__c, Date_Entered__c, Ticket_Number__c,
											 Line_Item__r.Cost_Element__r.GL_Number__c 
								      From SPCC_Service_Entry_Sheet__c
								      Order By Line_Item__r.Cost_Element__r.GL_Number__c Asc NULLS LAST];
				ApexPages.currentPage().getParameters().put('sortOptions', 'SORT_BY_GL_NUMBER;SORT_ASCENDING');
				System.assertEquals(null, extCtrl.sortServiceEntrySheets());
				for(Integer i=0; i < serviceEntrySheets.size(); i++) {
					System.assertEquals(serviceEntrySheets[i].Line_Item__r.Cost_Element__r.GL_Number__c, extCtrl.serviceEntrySheets[i].serviceEntrySheet.Line_Item__r.Cost_Element__r.GL_Number__c);
				}

				//Check Sorted By Gl Code in Descending Order
				serviceEntrySheets = [Select Id, Name, Purchase_Order__c, Amount__c, Date_Entered__c, Ticket_Number__c,
											 Line_Item__r.Cost_Element__r.GL_Number__c 
								      From SPCC_Service_Entry_Sheet__c
								      Order By Line_Item__r.Cost_Element__r.GL_Number__c DESC NULLS LAST];
				ApexPages.currentPage().getParameters().put('sortOptions', 'SORT_BY_GL_NUMBER;SORT_DESCENDING');
				System.assertEquals(null, extCtrl.sortServiceEntrySheets());
				for(Integer i=0; i < serviceEntrySheets.size(); i++) {
					System.assertEquals(serviceEntrySheets[i].Line_Item__r.Cost_Element__r.GL_Number__c, extCtrl.serviceEntrySheets[i].serviceEntrySheet.Line_Item__r.Cost_Element__r.GL_Number__c);	
				}	
			}
		Test.stopTest();
	}

	@future
	private static void createUsers() {
		SPCC_TestData.createCSRUser();
	}
}