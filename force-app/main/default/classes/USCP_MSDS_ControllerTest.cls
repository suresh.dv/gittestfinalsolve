/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 *
 * Unit tests are class methods that verify whether a particular piece
 * of code is working properly. Unit test methods take no arguments,
 * commit no data to the database, and are flagged with the testMethod
 * keyword in the method definition.
 *
 * All test methods in an organization are executed whenever Apex code is deployed
 * to a production organization to confirm correctness, ensure code
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployed
 * to a production organization. In addition, all triggers must have some code coverage.
 * 
 * The @isTest class annotation indicates this class only contains test
 * methods. Classes defined with the @isTest annotation do not count against
 * the organization size limit for all Apex scripts.
 *
 * See the Apex Language Reference for more information about Testing and Code Coverage.
 */
@isTest (SeeAllData = false)
private class USCP_MSDS_ControllerTest{
     static testMethod void myUnitTest() {
        
        User runningUser = UserTestData.createTestsysAdminUser();
        insert runningUser;
        System.AssertNotEquals(runningUser.Id, Null);
        
        System.runAs(runningUser) {
        
            PageReference pageRef = Page.USCP_MSDS;
            Test.setCurrentPageReference(pageRef);
            
            /* folders can not be added by apex
            Folder newFolder = new Folder();
            newFolder.Name = 'USCP MSDS' ;
            insert newFolder;            
            */

            USCP_MSDS_Controller controller = new USCP_MSDS_Controller();
            
            Document dummyDocument = new Document();
            dummyDocument.name = 'Test Doc';
            dummyDocument.body = Blob.valueOf('Unit Test Document Body');
            dummyDocument.folderid = UserInfo.getUserId(); //newFolder.id;     
            insert dummyDocument;

            controller.FolderID =  UserInfo.getUserId(); 

            //check if we now have this document available in the controller 
            System.AssertEquals(1,controller.DocumentList.size());

        }
    }        
}